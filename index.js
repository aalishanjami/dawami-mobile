/**
 * @format
 */
import React from 'react';
import {AppRegistry, Platform} from 'react-native';
import {App} from './App';
import {name as appName} from './app.json';
import {Text} from 'components';
import messaging from '@react-native-firebase/messaging';

// AppRegistry.registerComponent(appName, () => App);

function HeadlessCheck({isHeadless}) {
  if (isHeadless) {
    // App has been launched in the background by iOS, ignore
    return null;
  }
  return <App />;
}
messaging().setBackgroundMessageHandler(async remoteMessage => {
//   console.log('Message handled in the background!', remoteMessage);
});
AppRegistry.registerComponent(appName, () => HeadlessCheck);
