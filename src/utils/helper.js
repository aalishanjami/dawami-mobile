import {
  AccessToken,
  LoginManager,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
import {
  GoogleSign,
  statusCodes,
  GoogleSignin,
} from '@react-native-community/google-signin';
import {Platform} from 'react-native';

const CheckFbAppOnMobile = () => {
  if (LoginManager.setLoginBehavior('native_only')) {
    console.log('here');
  } else if (LoginManager.setLoginBehavior('web_only')) {
    console.log('there');
  } else {
    console.log('no where');
  }
};

export const fbHandler = async () => {
  const token = await AccessToken.getCurrentAccessToken();
  // LoginManager.setLoginBehavior(Platform.OS === 'ios' ? 'native' : 'web_only');
  // LoginManager.setLoginBehavior(Platform.OS === 'ios' ? 'web' : 'web_only')
  // LoginManager.setLoginBehavior(
  //   Platform.OS === 'ios' ? 'browser' : 'native_only',
  // );

  // console.log('==============');
  // console.log(token);

  if (token && token.accessToken) {
    const userInfo = await getFacebookData(token.accessToken);
    console.log('====== FACEBOOK HELPER CLASS ==========');
    console.log('userInfo');
    console.log(userInfo);
    return userInfo;
  } else {
    console.log("I DIDN'T GET TOKEN SO... ");
    // LoginManager.setLoginBehavior('NATIVE_ONLY');
    const res = await LoginManager.logInWithPermissions([
      'public_profile',
      'email',
    ]);
    console.log('LOGIN MANAGER LOGIN WITH PERMISSION');
    console.log(res);
    if (res) {
      // debugger;
      const token = await AccessToken.getCurrentAccessToken();
      console.log('======FACRBOOK TOKEN============');
      console.log(token);

      let userInfo = await getFacebookData(token.accessToken);
      console.log('======ELSE USER INFO BASED ON TOKEN============');
      console.log(userInfo);
      return userInfo;
    }
  }
};
const getFacebookData = accessToken =>
  new Promise(resolve => {
    const infoRequest = new GraphRequest(
      '/me?fields=email,name,first_name,last_name,picture',
      {accessToken: accessToken},
      (error, result) => {
        if (error) {
          console.log('Error fetching data: ' + error.toString());
          resolve(null);
          console.log(error);
          return;
        }
        resolve(result);
      },
    );
    new GraphRequestManager().addRequest(infoRequest).start();
  });
// ---------------------------GOOGLE-------------------------------
export const googleSigning = async () => {
  try {
    await GoogleSignin.hasPlayServices();
    const userInfo = await GoogleSignin.signIn();
    console.log('data.user========');
    console.log(userInfo.user);
    return userInfo.user;
  } catch (error) {
    console.log(' i am in catching state right now ');
    if (error.code === statusCodes.SIGN_IN_CANCELLED) {
      console.log('cancelled');
      // return
    } else if (error.code === statusCodes.IN_PROGRESS) {
      console.log('in progress');
      // return
    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      console.log('play error');
      // return
    } else {
      console.log(error);
      // return
    }
  }
};

export const sortByProperty = property => {
  return (a, b) => {
    if (a[property] > b[property]) return 1;
    else if (a[property] < b[property]) return -1;

    return 0;
  };
};
// ====================== ================================================
const CIVIL = 'Civil Engineer.';
const CIVIL_AR = 'مهندس مدني';
const OPERATION = 'Operations';
const OPERATION_AR = 'عمليات';
const OIL = 'Oil & Gas';
const OIL_AR = 'النفط والغاز';
const MEDIA = 'Media';
const MEDIA_AR = 'وسائل الإعلام';
const BANKING = 'Banking';
const BANKING_AR = 'الخدمات المصرفية';
const MEDICAL = 'Medical';
const MEDICAL_AR = 'طبي';
const IT = 'Information Technology';
const IT_AR = 'تكنولوجيا المعلومات';
const EE = 'Electrical Engineer';
const EE_AR = 'مهندس كهرباء';
const SAFETY = 'Safety.';
const SAFETY_AR = 'سلامة';
const CONSTRUCTION = 'Construction';
const CONSTRUCTION_AR = 'اعمال بناء';
// Values for job position
const CVL = {name: CIVIL, name_ar: CIVIL_AR, value: '1'};
const OPR = {name: OPERATION, name_ar: OPERATION_AR, value: '2'};
const OILG = {name: OIL, name_ar: OIL_AR, value: '3'};
const MDA = {name: MEDIA, name_ar: MEDIA_AR, value: '4'};
const BNKING = {name: BANKING, name_ar: BANKING_AR, value: '5'};
const MEDI = {name: MEDICAL, name_ar: MEDICAL_AR, value: '6'};
const INFOTECH = {name: IT, name_ar: IT_AR, value: '9'};
const ELE = {name: EE, name_ar: EE_AR, value: '10'};
const SFTY = {name: SAFETY, name_ar: SAFETY_AR, value: '11'};
const CONS = {name: CONSTRUCTION, name_ar: CONSTRUCTION_AR, value: '12'};
// Function for get data against cate id
export const getJobPosition = id => {
  let result;
  switch (id) {
    case CVL.value:
      return (result = {name: CVL.name, name_ar: CIVIL_AR});
    case OPR.value:
      return (result = {name: CVL.name, name_ar: OPR.name_ar});
    case OILG.value:
      return (result = {name: CVL.name, name_ar: OIL_AR});
    case MDA.value:
      return (result = {name: CVL.name, name_ar: MDA.name_ar});
    case BNKING.value:
      return (result = {name: CVL.name, name_ar: BNKING.name_ar});
    case MEDI.value:
      return (result = {name: CVL.name, name_ar: MEDI.name_ar});
    case INFOTECH.value:
      return (result = {name: CVL.name, name_ar: INFOTECH.name_ar});
    case ELE.value:
      return (result = {name: CVL.name, name_ar: ELE.name_ar});
    case SFTY.value:
      return (result = {name: CVL.name, name_ar: SFTY.name_ar});
    case CONS.value:
      return (result = {name: CVL.name, name_ar: CONS.name_ar});
    default:
      return (result = 'Default');
  }
};

// -----job type const
const FREELANCER = 'Freelancer';
const FREELANCER_AR = 'مستقل';
const PARTTIME = 'Part Time';
const PARTTIME_AR = 'دوام جزئى';
const FULLTIME = 'Full Time';
const FULLTIME_AR = 'وقت كامل';
const TEMPORARY = 'Temporary';
const TEMPORARY_AR = 'مؤقت';
// values for
const freelancer = {name: FREELANCER, name_ar: FREELANCER_AR, value: '0'};
const parttime = {name: PARTTIME, name_ar: PARTTIME_AR, value: '1'};
const fulltime = {name: FULLTIME, name_ar: FREELANCER_AR, value: '2'};
const temporary = {name: TEMPORARY, name_ar: TEMPORARY_AR, value: '3'};

export const getJobType = id => {
  let result;
  switch (id) {
    case freelancer.value:
      return (result = {name: freelancer.name, name_ar: freelancer.name_ar});
    case parttime.value:
      return (result = {
        name: parttime.name,
        name_ar: parttime.name_ar,
      });
    case fulltime.value:
      return (result = {
        name: fulltime.name,
        name_ar: fulltime.name_ar,
      });

    case temporary.value:
      return (result = {
        name: temporary.name,
        name_ar: temporary.name_ar,
      });

    default:
      return (result = ' Default ');
  }
};



const NETWORK_ERROR = 'Network Error';

export const errorHandler = error => {
  if (error.message === NETWORK_ERROR) {
    // return {type: NETWORK};
    return error.message
  } else {
    return error;
  }
};
