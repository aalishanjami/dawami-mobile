export const Welcome = 'Welcome';
export const WelcomAgain = 'WelcomeAgain';
export const LOGIN = 'Login';
export const SIGNUP = 'Sign Up';
export const RESET = 'Reset Password';

export const HOME = 'Home';
export const COMPANY = 'Company';
export const COMPANY_DETAILS = 'Company PortFolio';
export const JOB = 'Jobs';
export const JOB_DETAILS = 'Job Details';
export const CV = 'CV BUILDER';
export const TRAININGS = 'TRAINGING_CATELOG';
export const MESSAGES = 'Messages';
export const CHAT = 'Chats';
export const PROFILE = 'Profile';
export const EDIT_PRO = 'Edit Profile';
export const FAVORITES = 'Favorites';
export const ABOUT = 'About Us';
export const CONTACT = 'Contact Us';
export const APP_JOB = 'Applied Jobs';
export const FAV_COMP = 'Favorite Companies';
export const LOGOUT = 'LOGOUT';
export const CATELOG_DETAILS = 'Catelog Details';

export const IMAGE_VIEWER = 'ImageViwer';
