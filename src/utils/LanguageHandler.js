import { requestResume } from "redux/actions/getRequests";
import { store } from "redux/storeConfig";
import { PROFILE, RESET } from "./RouteConstants";

export const COMPANY = 'Companies';
export const COMPANY_AR = 'الشركات';
export const JOB = 'Jobs';
export const JOB_AR = 'مهنة';
export const CV_BUILDER = 'Cv Builder';
export const CV_BUILDER_AR = 'منشئ السيرة الذاتية';
export const TRAINING_CATELOG = 'Training Catalogue';
export const TRAINING_CATE_AR = 'كتالوج التدريب';
export const HOME_TITLE = 'Your Future Job Gateway';
export const HOME_TITLE_AR = 'بوابة وظيفتك المستقبلية';
export const Search_placeholder = 'Job Title or Keyword';
export const Search_placeholder_ar = 'المسمى الوظيفي أو الكلمة الرئيسية';
export const Area_placeholder = 'Area';
export const Area_placeholder_ar = 'منطقة';
export const SEARCH = 'Search';
export const SEARCH_AR = 'بحث';
export const NO_RESULT_FOUND = 'No Result Found';
export const NO_RESULT_FOUND_AR = 'لم يتم العثور على نتائج';
export const MEDIA = 'Media';
export const MEDIA_AR = 'وسائل الإعلام';

export const MESSAGE = 'Messages';
export const MESSAGES_AR = 'رسالة';
export const FAVORITE = 'Favorites';
export const FAVORITE_AR = 'المفضلة';
export const PROFIE = 'My Profile';
export const PROFILE_AR = 'الملف الشخصي';

export const FAV_COMP = 'Favorite Companies';
export const FAV_COMP_AR = 'الشركات المفضلة';
export const APP_JOB = 'Applied Jobs';
export const APP_JOB_AR = 'وظائف تطبيقية';
export const CONTACT = 'Contact Us';
export const CONTACT_AR = 'اتصل بنا';
export const ABOUT = 'About Us';
export const ABOUT_AR = 'معلومات عنا';
export const LOGOUT = 'LOGOUT';
export const LOGOUT_AR = 'تسجيل خروج';
export const NO_JOB = 'Currently No Job Available.';
export const NO_JOB_AR = 'حاليا لا توجد وظيفة متاحة';
export const FOLLOW_AR = 'إتبع';
export const FOLLOW = 'Follow';
export const UNFOLLOW = 'Un-follow';
export const UNFOLLOW_AR = 'الغاء المتابعة';
export const VACANCIES = 'Vacancies';
export const VACANCIES_AR = 'الشواغر';
export const REMEMBER = 'Remember Me';
export const REMEMBER_AR = 'تذكرنى';
export const FORGET_PASSWORD = 'Forget Password ?';
export const FORGET_PASSWORD_AR = 'نسيت كلمة المرور';
export const EMAIL = 'EMAIL';
export const EMAIL_AR = 'البريد الإلكتروني';
export const PASSWORD = 'PASSWORD';
export const PASSWORD_AR = 'كلمه السر';
export const OR_CONNECT = 'Or Connect With';
export const OR_CONNECT_AR = 'أو تواصل مع';
export const DONT_HAVE_AC = "Don't Have an account ? Sign Up";
export const DONT_HAVE_AC_AR = 'ليس لديك حساب؟ سجل';
export const LOGIN = 'Login';
export const LOGIN_AR = 'تسجيل الدخول';
export const GOOGLE = 'Google';
export const GOOGLE_AR = 'جوجل';
export const FB = 'Facebook';
export const FB_AR = ' الفيسبوك';
export const internetError = 'Please Check Your Internet Connectivity';
export const internetError_ar = 'يرجى التحقق من اتصال الإنترنت';
export const SHOW_ALL = 'Show All';
export const SHOW_ALL_AR = 'عرض الكل';
export const EDU = 'Education & Experience';
export const EDU_AR = 'التعليم و الخبرة';
export const RES = 'Responsbilites';
export const RES_AR = 'المسؤوليات';
export const BEN = 'Benifits';
export const BEN_AR = 'فوائد أخرى';
export const AboutCompany = 'About';
export const AboutCompany_AR = 'حول';
export const searchPlaceholderInComp_ar = 'ابحث عن الشركات هنا';
export const searchPlaceholderInComp = 'Find For  here';
export const INDUSTRY_AR = 'صناعة';
export const INDUSTRY = 'Industry';
export const CATE = 'Category';
export const CATE_AR = 'الفئة';
export const OpenJobs = 'Open Jobs';
export const OpenJobs_ar = 'افتح الوظائف';
export const newReq = 'Submitted Resume Request Status';
export const newReq_ar = 'حالة طلب السيرة الذاتية المقدمة';
export const viewProg = 'View Progress';
export const viewProg_ar = 'عرض التقدم';
export const reqNewResume = 'Request New Resume';
export const reqNewResume_ar = 'طلب استئناف جديد';
export const findJobHere = 'Find Your Job Here';
export const findJobHere_ar = 'ابحث عن وظائف هنا';
export const PleaseSelect = 'Please Select';
export const PleaseSelect_AR = 'يرجى التحديد';
export const Civil = 'Civil';
export const Civil_AR = 'مدني';
export const Operation = 'Operation';
export const Operation_AR = 'عملية';
export const Oil = 'Oil & Gas';
export const Oil_AR = 'النفط والغاز';
export const Media = 'Media';
export const Media_AR = 'وسائل الإعلام';
export const Banking = 'Banking';
export const Banking_AR = 'الخدمات المصرفية';
export const Medical = 'Medical';
export const Medical_AR = 'طبي';
export const InfomationTec = 'Infomation Technology';
export const InfomationTec_AR = 'معلومات التكنولوجيا';
export const ElecEng = 'Electrical Eng';
export const ElecEng_AR = 'مهندس كهرباء';
export const Safety = 'Safety';
export const Safety_AR = 'سلامة';
export const construct = 'Contruction';
export const construct_AR = 'اعمال بناء';
export const defaultJob = 'Default';
export const defaultJob_AR = 'إفتراضي';
export const Govt = 'Government';
export const Govt_AR = 'حكومة';
export const SemiGovt = 'Semi Govt';
export const SemiGovt_AR = 'حكومة شبه';
export const PrivateJob = 'Private';
export const PrivateJob_AR = 'نشر';
export const OpenJobsOpt = 'Company With Open Jobs';
export const OpenJobsOpt_AR = 'شركة بوظائف مفتوحة';
export const Reset = 'Reset';
export const Reset_AR = 'إعادة تعيين';
export const Finance = 'Finance';
export const Finana_AR = 'المالية';
export const Design = 'Design';
export const Design_AR = 'التصميم';
export const HR = 'Human Resources';
export const HR_AR = 'الموارد البشرية';
export const ClassRoom = 'Class Room';
export const ClassRoom_AR = 'قاعة الدراسة';
export const Online = 'Online';
export const Online_AR = 'عبر الانترنت';
export const Arabic = 'Arabic';
export const Arabic_AR = 'عربى';
export const English = 'English';
export const Engish_AR = 'الإنجليزية';
export const Beginner = 'Beginner';
export const Beginner_AR = 'مبتدئ';
export const Intermediate = 'Intermediate';
export const Intermediate_AR = 'متوسط';
export const Expert = 'Expert';
export const Expert_AR = 'خبير';
export const Type = 'Type';
export const Type_AR = 'اكتب';
export const LANG = 'Language ';
// export const LANG_AR = 'لغة';
export const LANG_AR = 'EN';
export const LEVEL = 'Level';
export const LEVEL_AR = 'مستوى';
export const Male = 'Male';
export const Male_AR = 'الذكر';
export const Female = 'Female';
export const Female_AR = 'أنثى';
export const Freelancer = 'Freelancer';
export const Freelancer_AR = 'مستقل';
export const FullTime = 'Full Time';
export const FullTime_AR = 'وقت كامل';
export const PartTime = 'Part Time';
export const PartTime_AR = 'دوام جزئى';
export const MoreThanTen = 'More Than 10 Years';
export const MoreThanTen_AR = 'أكثر من عشر سنوات';
export const TenYears = '10 Years';
export const TenYears_AR = 'عشر سنوات';
export const NineYears = '9 Years';
export const NineYears_AR = 'تسع سنوات';
export const EightYears = '8 Years';
export const EightYears_AR = 'ثماني سنوات';
export const SevenYears = '7 Years';
export const SevenYear_AR = 'سبع سنوات';
export const SixYears = '6 Years';
export const SixYears_AR = 'ست سنوات';
export const FiveYears = '5 Years';
export const FiveYears_AR = 'خمس سنوات';
export const FourYears = '4 Years';
export const FourYears_AR = 'أربع سنوات';
export const ThreeYears = '3 Years';
export const ThreeYears_AR = 'ثلاث سنوات';
export const TwoYears = '2 Years';
export const TwoYears_AR = 'سنتان';
export const OneYear = '1 Years';
export const OneYear_AR = 'سنة واحدة';
export const LessThanOne = 'Less Than One Year';
export const LessThanOne_AR = 'اقل من عام واحد';
export const Qarat = 'Qatar';
export const Qatar_AR = 'دولة قطر';
export const Pakistan = 'Pakistan';
export const Pakistan_AR = 'باكستان';
export const Experience = 'Experience';
export const Experience_AR = 'تجربة';
export const Gender = 'Gender';
export const Gender_AR = 'جنس';
export const jobType = 'Job Type';
export const jobType_AR = 'نوع الوظيفة';
export const Country = 'Country';
export const Country_AR = 'بلد';
export const NoDataFound = 'No Data Found';
export const NoDataFound_AR = 'لاتوجد بيانات';
export const Application = 'Applications';
export const Application_ar = 'التطبيقات';
export const MessageSent = 'Message Sent Successfully';
export const MessageSent_AR = 'تم إرسال الرسالة بنجاح';
export const JORDAN_AR = 'الأردن';
export const JORDAN = "JORDAN"
export const CITIZEN_AR = 'مواطن';
export const CITIZEN = 'CITIZEN';
export const RES_VISA_AR = 'تأشيرة الإقامة (قابلة للتحويل)';
export const RES_VISA = 'Residence Visa (Transferable)';
export const NON_RES_VISA__AR = 'تأشيرة الإقامة (غير قابلة للتحويل)';
export const NON_RES_VISA  = 'Residence Visa (Non-Transferable)';

export const STDUENT = "STUDENT"
export const STUDENT_AR = 'طالب علم';
export const Transit_Visa = "Transit Visa"
export const Transit_Visa_AR = "تأشيرة العبور"
export const Visit_VISA  = "VISIT VISA"
export const  Visit_VISA_AR = "تأشيرة زيارة"
export const No_VISA = "No Visa"
export const No_VISA_AR = "لا تأشيرة"


export const MANAGEMENT = "MANAGEMENT"
export const MANAGEMENT_AR = "إدارة"

export const BANK = "BANK"
export const BANK_AR = "مصرف"

export const Security = "Security"
export const Security_AR = "الأمان"

export const Volunteer = "Volunteer"
export const Volunteer_AR ="تطوع"

export const  internship = "Internship"
export const internship_ar = "فترة تدريب"

export const baking = "baking"
export const baking_ar = "الخبز"

export const TYRES = 'TYRES';
export const TYRES_AR = 'الإطارات';


export const BAKERS = 'Bakers';
export const BAKERS_AR = 'الخبازين';

export const IMMEDIATELY = "IMMEDIATELY"
export const IMMEDIATELY_AR = "فورا"

export const MORE_THAN_ONE_MONTH = "More Than One Month"
export const MORE_THAN_ONE_MONTH_AR = "أكثر من شهر"

export const MORE_THAN_THREE_MONTHS = "MORE THAN Three Months"
export const MORE_THAN_THREE_MONTHS_AR = "أكثر من ثلاثة أشهر"

export const MORE_THAN_SIX_MONTHS = "More Than Six Months"
export const MORE_THAN_SIX_MONTHS_AR = "أكثر من ستة أشهر"

export const MORE_THAN_ONE_YEAR = "MORE THAN ONE YEAR"
export const MORE_THAN_ONE_YEAR_AR = "أكثر من سنة"

export const addJobTitle = "Add Job Title"
export const addJobTitle_ar = 'أضف المسمى الوظيفي';

// export const  = ""
// export const  = ""




export const languages = [
         {
           label: {
             EN: addJobTitle,
             AR: addJobTitle_ar,
           },
         },
         {
           label: {
             EN: MORE_THAN_ONE_MONTH,
             AR: MORE_THAN_ONE_MONTH_AR,
           },
         },
         {
           label: {
             EN: MORE_THAN_THREE_MONTHS,
             AR: MORE_THAN_THREE_MONTHS_AR,
           },
         },
         {
           label: {
             EN: MORE_THAN_SIX_MONTHS,
             AR: MORE_THAN_SIX_MONTHS_AR,
           },
         },
         {
           label: {
             EN: MORE_THAN_ONE_YEAR,
             AR: MORE_THAN_ONE_YEAR_AR,
           },
         },
         //
         {
           label: {
             EN: STDUENT,
             AR: STUDENT_AR,
           },
         },
         {
           label: {
             EN: internship,
             AR: internship_ar,
           },
         },
         {
           label: {
             EN: Volunteer,
             AR: Volunteer_AR,
           },
         },
         {
           label: {
             EN: TYRES,
             AR: TYRES_AR,
           },
         },
         {
           label: {
             EN: BAKERS,
             AR: BAKERS_AR,
           },
         },
         {
           label: {
             EN: Transit_Visa,
             AR: Transit_Visa_AR,
           },
         },
         {
           label: {
             EN: IMMEDIATELY,
             AR: IMMEDIATELY_AR,
           },
         },
         {
           label: {
             EN: Visit_VISA,
             AR: Visit_VISA_AR,
           },
         },
         {
           label: {
             EN: No_VISA,
             AR: No_VISA_AR,
           },
         },
         {
           label: {
             EN: NON_RES_VISA,
             AR: NON_RES_VISA__AR,
           },
         },
         {
           label: {
             EN: MANAGEMENT,
             AR: MANAGEMENT_AR,
           },
         },
         {
           label: {
             EN: Security,
             AR: Security_AR,
           },
         },
         //
         {
           label: {
             EN: CITIZEN,
             AR: CITIZEN_AR,
           },
         },
         {
           label: {
             EN: RES_VISA,
             AR: RES_VISA_AR,
           },
         },
         {
           label: {
             EN: JORDAN,
             AR: JORDAN_AR,
           },
         },
         {
           label: {
             EN: MessageSent,
             AR: MessageSent_AR,
           },
         },
         {
           label: {
             EN: Application,
             AR: Application_ar,
           },
         },
         {
           label: {
             EN: NoDataFound,
             AR: NoDataFound_AR,
           },
         },
         {
           label: {
             EN: Country,
             AR: Country_AR,
           },
         },
         {
           label: {
             EN: jobType,
             AR: jobType_AR,
           },
         },
         {
           label: {
             EN: Gender,
             AR: Gender_AR,
           },
         },
         {
           label: {
             EN: Experience,
             AR: Experience_AR,
           },
         },
         {
           label: {
             EN: Pakistan,
             AR: Pakistan_AR,
           },
         },
         {
           label: {
             EN: Qarat,
             AR: Qatar_AR,
           },
         },
         {
           label: {
             EN: LessThanOne,
             AR: LessThanOne_AR,
           },
         },
         {
           label: {
             EN: OneYear,
             AR: OneYear_AR,
           },
         },
         {
           label: {
             EN: TwoYears,
             AR: TwoYears_AR,
           },
         },
         {
           label: {
             EN: ThreeYears,
             AR: ThreeYears_AR,
           },
         },
         {
           label: {
             EN: FourYears,
             AR: FourYears_AR,
           },
         },
         {
           label: {
             EN: FiveYears,
             AR: FiveYears_AR,
           },
         },
         {
           label: {
             EN: SixYears,
             AR: SixYears_AR,
           },
         },
         {
           label: {
             EN: SevenYears,
             AR: SevenYear_AR,
           },
         },
         {
           label: {
             EN: EightYears,
             AR: EightYears_AR,
           },
         },
         {
           label: {
             EN: NineYears,
             AR: NineYears_AR,
           },
         },

         {
           label: {
             EN: TenYears,
             AR: TenYears_AR,
           },
         },
         {
           label: {
             EN: MoreThanTen,
             AR: MoreThanTen_AR,
           },
         },
         {
           label: {
             EN: PartTime,
             AR: PartTime_AR,
           },
         },
         {
           label: {
             EN: FullTime,
             AR: FullTime_AR,
           },
         },
         {
           label: {
             EN: Freelancer,
             AR: Freelancer_AR,
           },
         },
         {
           label: {
             EN: Female,
             AR: Female_AR,
           },
         },
         {
           label: {
             EN: Male,
             AR: Male_AR,
           },
         },
         {
           label: {
             EN: LEVEL,
             AR: LEVEL_AR,
           },
         },
         {
           label: {
             EN: Type,
             AR: Type_AR,
           },
         },
         {
           label: {
             EN: Expert,
             AR: Expert_AR,
           },
         },
         {
           label: {
             EN: Intermediate,
             AR: Intermediate_AR,
           },
         },
         {
           label: {
             EN: Beginner,
             AR: Beginner_AR,
           },
         },
         {
           label: {
             EN: English,
             AR: Engish_AR,
           },
         },
         {
           label: {
             EN: Arabic,
             AR: Arabic_AR,
           },
         },
         {
           label: {
             EN: Online,
             AR: Online_AR,
           },
         },
         {
           label: {
             EN: ClassRoom,
             AR: ClassRoom_AR,
           },
         },
         {
           label: {
             EN: HR,
             AR: HR_AR,
           },
         },
         {
           label: {
             EN: Design,
             AR: Design_AR,
           },
         },
         {
           label: {
             EN: Finance,
             AR: Finana_AR,
           },
         },
         {
           label: {
             EN: RESET,
             AR: Reset_AR,
           },
         },
         {
           label: {
             EN: OpenJobsOpt,
             AR: OpenJobsOpt_AR,
           },
         },
         {
           label: {
             EN: PrivateJob,
             AR: PrivateJob_AR,
           },
         },
         {
           label: {
             EN: SemiGovt,
             AR: SemiGovt_AR,
           },
         },
         {
           label: {
             EN: Govt,
             AR: Govt_AR,
           },
         },
         {
           label: {
             EN: defaultJob,
             AR: defaultJob_AR,
           },
         },
         {
           label: {
             EN: construct,
             AR: construct_AR,
           },
         },
         {
           label: {
             EN: Safety,
             AR: Safety_AR,
           },
         },
         {
           label: {
             EN: ElecEng,
             AR: ElecEng_AR,
           },
         },
         {
           label: {
             EN: InfomationTec,
             AR: InfomationTec_AR,
           },
         },
         {
           label: {
             EN: Medical,
             AR: Medical_AR,
           },
         },
         {
           label: {
             EN: Banking,
             AR: Banking_AR,
           },
         },
         {
           label: {
             EN: BANK,
             AR: BANK_AR,
           },
         },
         {
           label: {
             EN: MEDIA,
             AR: MEDIA_AR,
           },
         },
         {
           label: {
             EN: Oil,
             AR: Oil_AR,
           },
         },
         {
           label: {
             EN: Operation,
             AR: Operation_AR,
           },
         },
         {
           label: {
             EN: Civil,
             AR: Civil_AR,
           },
         },
         {
           label: {
             EN: PleaseSelect,
             AR: PleaseSelect_AR,
           },
         },
         {
           label: {
             EN: findJobHere,
             AR: findJobHere_ar,
           },
         },
         {
           label: {
             EN: reqNewResume,
             AR: reqNewResume_ar,
           },
         },
         {
           label: {
             EN: viewProg,
             AR: viewProg_ar,
           },
         },
         {
           label: {
             EN: newReq,
             AR: newReq_ar,
           },
         },
         {
           label: {
             EN: OpenJobs,
             AR: OpenJobs_ar,
           },
         },
         {
           label: {
             EN: CATE,
             AR: CATE_AR,
           },
         },
         {
           label: {
             EN: INDUSTRY,
             AR: INDUSTRY_AR,
           },
         },
         {
           label: {
             EN: searchPlaceholderInComp,
             AR: searchPlaceholderInComp_ar,
           },
         },
         {
           label: {
             EN: ABOUT,
             AR: ABOUT_AR,
           },
         },
         {
           label: {
             EN: BEN,
             AR: BEN_AR,
           },
         },
         {
           label: {
             EN: RES,
             AR: RES_AR,
           },
         },
         {
           label: {
             EN: EDU,
             AR: EDU_AR,
           },
         },
         {
           label: {
             EN: SHOW_ALL,
             AR: SHOW_ALL_AR,
           },
         },
         {
           label: {
             EN: internetError,
             AR: internetError_ar,
           },
         },
         {
           label: {
             EN: FB,
             AR: FB_AR,
           },
         },
         {
           label: {
             EN: GOOGLE,
             AR: GOOGLE_AR,
           },
         },
         {
           label: {
             EN: LOGIN,
             AR: LOGIN_AR,
           },
         },
         {
           label: {
             EN: DONT_HAVE_AC,
             AR: DONT_HAVE_AC_AR,
           },
         },
         {
           label: {
             EN: OR_CONNECT,
             AR: OR_CONNECT_AR,
           },
         },
         {
           label: {
             EN: PASSWORD,
             AR: PASSWORD_AR,
           },
         },
         {
           label: {
             EN: EMAIL,
             AR: EMAIL_AR,
           },
         },
         {
           label: {
             EN: FORGET_PASSWORD,
             AR: FORGET_PASSWORD_AR,
           },
         },
         {
           label: {
             EN: REMEMBER,
             AR: REMEMBER_AR,
           },
         },
         {
           label: {
             EN: VACANCIES,
             AR: VACANCIES_AR,
           },
         },
         {
           label: {
             EN: UNFOLLOW,
             AR: UNFOLLOW_AR,
           },
         },
         {
           label: {
             EN: FOLLOW,
             AR: FOLLOW_AR,
           },
         },
         {
           label: {
             EN: NO_JOB,
             AR: NO_JOB_AR,
           },
         },
         {
           label: {
             EN: LOGOUT,
             AR: LOGOUT_AR,
           },
         },
         {
           label: {
             EN: ABOUT,
             AR: ABOUT_AR,
           },
         },
         {
           label: {
             EN: CONTACT,
             AR: CONTACT_AR,
           },
         },
         {
           label: {
             EN: APP_JOB,
             AR: APP_JOB_AR,
           },
         },
         {
           label: {
             EN: FAV_COMP,
             AR: FAV_COMP_AR,
           },
         },
         {
           label: {
             EN: PROFILE,
             AR: PROFILE_AR,
           },
         },
         {
           label: {
             EN: FAVORITE,
             AR: FAVORITE_AR,
           },
         },
         {
           label: {
             EN: MESSAGE,
             AR: MESSAGES_AR,
           },
         },
         {
           label: {
             EN: MEDIA,
             AR: MEDIA_AR,
           },
         },
         {
           label: {
             EN: NO_RESULT_FOUND,
             AR: NO_RESULT_FOUND_AR,
           },
         },
         {
           label: {
             EN: SEARCH,
             AR: SEARCH_AR,
           },
         },

         {
           label: {
             EN: Area_placeholder,
             AR: Area_placeholder_ar,
           },
         },
         {
           label: {
             EN: Search_placeholder,
             AR: Search_placeholder_ar,
           },
         },
         {
           label: {
             EN: HOME_TITLE,
             AR: HOME_TITLE_AR,
           },
         },
         {
           label: {
             EN: CV_BUILDER,
             AR: CV_BUILDER_AR,
           },
         },
         {
           label: {
             EN: JOB,
             AR: JOB_AR,
           },
         },
         {
           label: {
             EN: COMPANY,
             AR: COMPANY_AR,
           },
         },
         {
           label: {
             EN: '',
             AR: '',
           },
         },
         {
           label: {
             EN: '',
             AR: '',
           },
         },

         {
           label: {
             EN: '',
             AR: '',
           },
         },
         {
           label: {
             EN: '',
             AR: '',
           },
         },
         {
           label: {
             EN: '',
             AR: '',
           },
         },
         {
           label: {
             EN: '',
             AR: '',
           },
         },
       ];

export const __ = value => {
    
  const languageSelected = store.getState().userInfo.currentLN;
  let data = languages.find(temp => {
    return value == temp.label.EN;
  });
  return data
    ? data.label[languageSelected]
      ? data.label[languageSelected]
      : data.label.EN
    : value;
}


