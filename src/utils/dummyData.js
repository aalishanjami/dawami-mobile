import * as images from 'assets/images';

export const catalogueData = [
  {
    key: '1',
    name: 'Quad xHTML Templete NewsBlog and Product Catelogue',
    price: '500',
    image: images.picTwo,
    rating: '4',
    isLiked: true,
  },
  {
    key: '2',
    name: 'Quad xHTML Templete NewsBlog and Product Catelogue',
    image: images.picOne,
    price: '250',
    rating: '4',
    isLiked: true,
  },
  {
    key: '3',
    name: 'Quad xHTML Templete NewsBlog and Product Catelogue',
    image: images.picTwo,
    price: '1000',
    rating: '4',
    isLiked: true,
  },
  {
    key: '4',
    name: 'Quad xHTML Templete NewsBlog and Product Catelogue',
    image: images.picOne,
    price: '300',
    rating: '4',
    isLiked: true,
  },
  {
    key: '5',
    name: 'Quad xHTML Templete NewsBlog and Product Catelogue',
    price: '500',
    image: images.picTwo,
    rating: '4',
    isLiked: true,
  },
  {
    key: '6',
    name: 'Quad xHTML Templete NewsBlog and Product Catelogue',
    image: images.picOne,
    price: '250',
    rating: '4',
    isLiked: true,
  },
  {
    key: '7',
    name: 'Quad xHTML Templete NewsBlog and Product Catelogue',
    image: images.picOne,
    price: '1000',
    rating: '4',
    isLiked: true,
  },
];

// ========================= compaines List=============

export const compainesList = [
  {key: '1', name: 'Qatar Gas', jobPosted: '3'},
  {key: '2', name: 'Qatar AirWays', jobPosted: '2'},
  {key: '3', name: 'Jaddah Group', jobPosted: '110'},
  {key: '4', name: 'Qatar Nation Bank', jobPosted: '81'},
  {key: '5', name: 'Qatar Gas', jobPosted: '1'},
  {key: '6', name: 'Qatar AirWays', jobPosted: '23'},
  {key: '7', name: 'Jaddah Group', jobPosted: '44'},
  {key: '8', name: 'Qatar Nation Bank', jobPosted: '11'},
];

// VACANCIES LIST=========================================

export const vacancies = [
  {
    key: '1',
    title: 'BankRupcy / Foreclousre Coordinator',
    subTitle: 'Part Time',
    data: '06 March 2020',
  },
  {
    key: '2',
    title: 'Consumer Credit Analyst',
    subTitle: 'Full Time',
    data: '08 March 2020',
  },
  {
    key: '3',
    title: 'Default Specialist',
    subTitle: 'Part Time',
    data: '8 MArch 2020',
  },
  {
    key: '4',
    title: 'Consumer Credit Analyst',
    subTitle: 'Full Time',
    data: '08 March 2020',
  },
  {
    key: '5',
    title: 'Default Specialist',
    subTitle: 'Part Time',
    data: '8 MArch 2020',
  },
  {
    key: '6',
    title: 'Consumer Credit Analyst',
    subTitle: 'Full Time',
    data: '08 March 2020',
  },
  {
    key: '7',
    title: 'Consumer Credit Analyst',
    subTitle: 'Full Time',
    data: '08 March 2020',
  },
  {
    key: '8',
    title: 'Consumer Credit Analyst',
    subTitle: 'Full Time',
    data: '08 March 2020',
  },
];


// Media =============
export const media = [
  {key:'1', image:images.qnbOne},
  {key:'2', image:images.qnbTwo},
  {key:'3', image:images.qnbOne},
  {key:'4', image:images.qnbTwo},
  {key:'5', image:images.qnbOne},
  {key:'6', image:images.map},
]


// conversations=================
export const inbox = [
  {key:'1', name:'Jaidah',fullName:'Jaidah Constructor Company' , unreadMessages:null, time:'2'},
  {key:'2', name:'Al Fardan',fullName:'Qatar Nation Bank' , unreadMessages:'3', time:null},
  {key:'3', name:'Hadayu',fullName:'Hukailyu Master Band' , unreadMessages:null, time:'42'},
  {key:'4', name:'Khalif',fullName:'Khalif Software Development' , unreadMessages:'110', time:null},
  {key:'5', name:'Kaeirn',fullName:'Korien Manufacturing' , unreadMessages:null, time:'54'},
  {key:'6', name:'Mekhaz', unreadMessages:'4', time:null},
  {key:'7', name:'Mekhaz', unreadMessages:'4', time:null},
  {key:'8', name:'Kaeirn', unreadMessages:null, time:'54'},
  {key:'9', name:'Mekhaz', unreadMessages:'4', time:null},
  {key:'10', name:'Kaeirn', unreadMessages:null, time:'54'},
  {key:'11', name:'Mekhaz', unreadMessages:'4', time:null},
  {key:'12', name:'Kaeirn', unreadMessages:null, time:'54'},
]



// cv builder =========================

export const cvData = [
  {key:'1', sr:'1', status:"Pending", created:'25-2-2020', closed:'29-2-2020',staffName:'Urdan', draft:'0' },
  {key:'2', sr:'2', status:"Pending",created:'25-2-2020', closed:'29-2-2020',staffName:'', draft:'0' },
  {key:'3', sr:'3', status:"Completed",created:'25-2-2020', closed:'29-2-2020',staffName:'', draft:'0' },
  {key:'4', sr:'4', status:"Pending",created:'25-2-2020', closed:'29-2-2020',staffName:'Mohmmad', draft:'0' },
  {key:'5', sr:'5', status:"Pending",created:'25-2-2020', closed:'29-2-2020',staffName:'', draft:'0' },
  {key:'6', sr:'6', status:"Cancelled",created:'25-2-2020', closed:'29-2-2020',staffName:'', draft:'0' },
  {key:'7', sr:'7', status:"Pending",created:'25-2-2020', closed:'29-2-2020',staffName:'', draft:'0' },
  {key:'8', sr:'8', status:"Completed",created:'25-2-2020', closed:'29-2-2020',staffName:'', draft:'0' },
  {key:'9', sr:'9', status:"Pending",created:'25-2-2020', closed:'29-2-2020',staffName:'', draft:'3' },
  {key:'10', sr:'10',status:"Cancelled", created:'25-2-2020', closed:'29-2-2020',staffName:'', draft:'0' },
  {key:'11', sr:'11',status:"Pending", created:'25-2-2020', closed:'29-2-2020',staffName:'Kareem', draft:'0' },
]