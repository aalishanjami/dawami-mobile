import {
  catalogueData,
  compainesList,
  vacancies,
  media,
  inbox,
} from './dummyData';

import {validateField, validateFields} from './Validations';
import {
  LoginFormState,
  ForgetPasswordFormState,
  SignupFormState,
  PasswordFormState,
} from './formInitialState';

import {
  fbHandler,
  googleSigning,
  sortByProperty,
  getJobPosition,
} from './helper';

import * as WORDS from './LanguageHandler';
import * as NAV from './RouteConstants';

export {
  LoginFormState,
  ForgetPasswordFormState,
  PasswordFormState,
  SignupFormState,
  validateFields,
  validateField,
  catalogueData,
  compainesList,
  vacancies,
  media,
  inbox,
  WORDS,
  NAV,
};

export {fbHandler, googleSigning, sortByProperty, getJobPosition};
