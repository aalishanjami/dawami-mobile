import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {PixelRatio, Platform, Dimensions} from 'react-native';
import {theme} from 'styles';

const scaleFont = size => size * PixelRatio.getFontScale();

export const ios =  Platform.OS === "ios"
export const iphone6s = Dimensions.get('screen').width <= 375
export const newDevices = Dimensions.get('screen').width >= 1124;

const colors = {
  spaceGreen: '#3D5952',
  newGray: '#C1BEB4',
  red: '#98003A',
  customRed: '#3D5952',
  fadeGreen: '#899C98',
  lightPink: '#DCB3B6',
  brown: '#cf8e5b',
  darkBrown: '#9B5242',
  primary: '#FFFFFF',
  secondary: '#F5138E',
  black: '#000000',
  gray: '#A7A7A7',
  gray: '#979797',
  gray2: '#707070',
  gray5: '#505050',
  gray3: '#2C262B',
  lightmaroon: '#B22222',
  // gray4: '#1A1A1A',
  // black2: '#120810',
};
const screenSize = wp('100%') + hp('100%');
const getDimensions = {
  width: wp('100%'),
  height: hp('100%'),
};


const sizes = {
  // global sizes
  base: Math.floor(screenSize * 0.0125), //16
  border: Math.floor(screenSize * 0.008), //10
  padding: Math.floor(screenSize * 0.011), //14
  borderRaduis: screenSize * 0.015,

  getWidth: width => wp(width),
  getHeight: height => hp(height),

  withWidth: size => getDimensions.width * size,
  withHeight: size => getDimensions.height * size,
  withScreen: size => screenSize * size,
  getDimensions: {
    width: wp('100%'),
    height: hp('100%'),
  },
  getDimensions,
  screenSize,
  // font sizes
  font: scaleFont(!ios ? 16: iphone6s?13:newDevices?22:18 ), //18
  h1: scaleFont(newDevices?23:20), //20
  h2: scaleFont(iphone6s ? 17 : 16), //16
  h3: scaleFont(!ios ? 14 : iphone6s?14:20), //14
  h4: scaleFont(!ios? 13 : iphone6s ? 11:15), //12
  title: scaleFont(!ios ? 24 : 27), //24
  title2: scaleFont(22), //22
  header: scaleFont(26), //26
  customFont: size => scaleFont(size),
};

const fonts = {
  default: {
    //fontFamily: 'Rubik-Light',
    // fontFamily: 'CormorantGaramond-Regular',
    fontFamily: 'EBGaramond-Regular',
    fontSize: sizes.font,
  },
  h1: {
    //fontFamily: 'Rubik-Light',
    fontSize: sizes.h1,
  },
  h2: {
    //fontFamily: 'Rubik-Medium',
    fontSize: sizes.h2,
  },
  h3: {
    //fontFamily: 'Rubik-Regular',
    fontSize: sizes.h3,
  },
  h4: {
    //fontFamily: 'Rubik-Light',
    fontSize: sizes.h4,
  },
  header: {
    //fontFamily: 'Rubik-Bold',
    fontSize: sizes.header,
  },
  title: {
    //fontFamily: 'Rubik-Regular',
    fontSize: sizes.title,
    // fontFamily: 'CormorantGaramond-Regular',
    fontFamily: 'CormorantGaramond-Regular',
  },
  title2: {
    //fontFamily: 'Rubik-Regular',
    fontSize: sizes.title2,
  },
  header: {
    //fontFamily: 'Rubik-Regular',
    fontSize: sizes.header,
  },
  GMR: {
    // fontFamily: 'CormorantGaramond-Regular',
    fontFamily: 'EBGaramond-Regular',
  },
  GMM: {
    // fontFamily: 'CormorantGaramond-Medium',
    fontFamily: 'EBGaramond-Medium',
  },
  GMSB: {
    // fontFamily: 'CormorantGaramond-SemiBold',
    fontFamily: 'EBGaramond-SemiBold',
  },
  GMB: {
    // fontFamily: 'CormorantGaramond-Bold',
    fontFamily: 'EBGaramond-Bold',
  },
};
function handleMargins(margin) {
  if (typeof margin === 'number') {
    return {
      marginTop: margin,
      marginRight: margin,
      marginBottom: margin,
      marginLeft: margin,
    };
  }

  if (typeof margin === 'object') {
    const marginSize = Object.keys(margin).length;
    switch (marginSize) {
      case 1:
        return {
          marginTop: margin[0],
          marginRight: margin[0],
          marginBottom: margin[0],
          marginLeft: margin[0],
        };
      case 2:
        return {
          marginTop: margin[0],
          marginRight: margin[1],
          marginBottom: margin[0],
          marginLeft: margin[1],
        };
      case 3:
        return {
          marginTop: margin[0],
          marginRight: margin[1],
          marginBottom: margin[2],
          marginLeft: margin[1],
        };
      default:
        return {
          marginTop: margin[0],
          marginRight: margin[1],
          marginBottom: margin[2],
          marginLeft: margin[3],
        };
    }
  }
}

function handlePaddings(padding) {
  if (typeof padding === 'number') {
    return {
      paddingTop: padding,
      paddingRight: padding,
      paddingBottom: padding,
      paddingLeft: padding,
    };
  }

  if (typeof padding === 'object') {
    const paddingSize = Object.keys(padding).length;
    switch (paddingSize) {
      case 1:
        return {
          paddingTop: padding[0],
          paddingRight: padding[0],
          paddingBottom: padding[0],
          paddingLeft: padding[0],
        };
      case 2:
        return {
          paddingTop: padding[0],
          paddingRight: padding[1],
          paddingBottom: padding[0],
          paddingLeft: padding[1],
        };
      case 3:
        return {
          paddingTop: padding[0],
          paddingRight: padding[1],
          paddingBottom: padding[2],
          paddingLeft: padding[1],
        };
      default:
        return {
          paddingTop: padding[0],
          paddingRight: padding[1],
          paddingBottom: padding[2],
          paddingLeft: padding[3],
        };
    }
  }
}
export {colors, sizes, fonts, handleMargins, handlePaddings};
