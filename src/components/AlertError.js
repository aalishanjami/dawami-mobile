import React from 'react';
import {Text, Block} from 'components';
import {colors} from 'styles/theme';

const CustomAlert = ({text, textColor, textAlign}) => {
  return (
    <Block center>
      <Text
        h4
        style={{textAlign: textAlign}}
        color={textColor || colors.spaceGreen}>
        {text}
      </Text>
    </Block>
  );
};

// const Alert = props => {
//   const {messageRef,color,textColor} = props;
//   return messageRef.current?.show(
//     <Block center style={{backgroundColor: color || colors.spaceGreen,}}>
//       <Text
//         h4
//         style={{textAlign: textAlign}}
//         color={textColor || colors.primary}>
//         {text}
//       </Text>
//     </Block>,
//     AlertTiming,
//   );
// };

export {CustomAlert};
