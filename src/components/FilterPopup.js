import React, {useEffect, useRef, Children} from 'react';
import {Block, Text} from 'components';
import {TextInput, Image, StyleSheet, Easing, Animated} from 'react-native';
import {sizes, colors} from 'styles/theme';

const FilterPopup = ({children}) => {
  const animatedValue = new Animated.Value(0);

  useEffect(() => {
    Animated.timing(animatedValue, {
      useNativeDriver: true,
      toValue: 1,
      duration: 300,
    }).start();
  }, ['']);

  return (
    <Block middle center style={styles.popupCon}>
      {/* <Block flex={false} style={{borderWidth: 1, width: '100%'}}>
        <Text>Heading</Text>
      </Block> */}
      <Animated.View
        style={{
          ...styles.popUp,
          transform: [
            {
              scaleX: animatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [0, 1],
              }),
            },
            {
              scaleY: animatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [0, 1],
              }),
            },
          ],
        }}>
        {children}
      </Animated.View>
    </Block>
  );
};

export {FilterPopup};

const styles = StyleSheet.create({
  popupCon: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: '#fefefe88',
    zIndex: 10,
  },
  popUp: {
    // borderWidth: 1,
    width: sizes.getWidth(80),
    // height: sizes.getHeight(80),
    borderRadius: sizes.getWidth(2),
    backgroundColor: colors.primary,
    elevation: 5,
    padding: sizes.getWidth(2),
  },
});
