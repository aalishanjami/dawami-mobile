import React, {useEffect, useRef, Children} from 'react';
import {Block, Text} from 'components';
import {
  TextInput,
  Image,
  StyleSheet,
  Easing,
  Animated,
  Platform,
} from 'react-native';
import {sizes, colors} from 'styles/theme';
import {Picker} from '@react-native-community/picker';
import { useSelector } from 'react-redux';
import { LANG_AR } from 'utils/LanguageHandler';
import { LANG_EN } from 'redux/constants';
// import {Dropdown} from 'react-native-material-dropdown';

const FilterOptions = props => {
  const {title, selectedValue, options, values, changedItem} = props;
  //   console.log('======FILTER OPTIONS=======');
  //   console.log(selectedValue);
  const {currentLN} = useSelector(state=>state.userInfo)
  const AR = currentLN === LANG_AR
  return (
    <Block middle style={styles.textField}>
      {/* =========================================================== */}
      <Block flex={false} style={{marginVertical: sizes.getHeight(1)}}>
        <Text h4 style={{
          textAlign: AR ?"right":"left"
        }} color={colors.customRed}>
          {title}
        </Text>
      </Block>
      {/* =========================================================== */}
      <Block style={styles.optionCon}>
        <Picker
          style={{
            justifyContent: 'flex-start',
            // alignItems: 'flex-start',
            alignContent: 'flex-start',
            width: '100%',
            // backgroundColor: 'red',
          }}
          selectedValue={selectedValue}
          onValueChange={item => {
            changedItem(item);
          }}>
          {values.length ? (
            values.map(({name, value}) => {
              return <Picker.Item key={name} value={value} label={name} />;
            })
          ) : (
            <Picker.Item label="No Options" value="0" />
          )}
        </Picker>
      </Block>
    </Block>
  );
};

export {FilterOptions};

const styles = StyleSheet.create({
  optionCon: {
    elevation: 0.5,
    borderWidth: Platform.OS === 'ios' ? 0 : 0.3,
    overflow: 'hidden',
    borderColor: colors.gray,
    borderRadius: sizes.getWidth(0.7),
    // borderWidth: 1,
    justifyContent: 'center',
    height: sizes.getHeight(8),
    flex: 0,
  },
  textField: {
    flex: 0,
    height: sizes.getHeight(12),
    // borderWidth: 1,w
    justifyContent: 'center',
    marginVertical: sizes.getWidth(1.6),
  },
});
