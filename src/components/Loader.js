import React from 'react';
import {View, ActivityIndicator, StyleSheet} from 'react-native';
import Modal from './ModalLumper';
import {colors, sizes} from 'styles/theme';
import { Block } from 'components';

const Lumper = ({visible}) => (
  <Modal visible={visible} style={styles.container}>
    {/* <View style={styles.subContainer}>
      <ActivityIndicator color={colors.primary} animating={true} size="large" />
    </View> */}
    <Block center middle style={styles.holder}>
      <Block
        flex={false}
        center
        middle
        style={{
          ...styles.loader,
          backgroundColor: colors.primary,
        }}>
        <ActivityIndicator size={`large`} color={colors.customRed} />
      </Block>
    </Block>
  </Modal>
);
export default Lumper;
const styles = StyleSheet.create({
  container: {flex: 1, justifyContent: 'center', alignItems: 'center'},
  subContainer: {
    backgroundColor: colors.neavyBlue,
    width: sizes.screenSize * 0.067,
    height: sizes.screenSize * 0.067,
    elevation: 3,
    borderRadius: 5,
    justifyContent: 'center',
    shadowColor: colors.gray,
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 1,
    shadowRadius: sizes.screenSize * 0.003,
  },
  loader: {
    height: sizes.screenSize * 0.06,
    width: sizes.screenSize * 0.06,
    // width: sizes.getDimensions.width * 0.3,
    borderRadius: sizes.withScreen(0.003),
    elevation: 10,
    shadowColor: colors.black,
    shadowOpacity: 0.8,
    shadowRadius: sizes.getWidth(0.4),
    shadowOffset: {width: 0, height: 0},
  },
  holder: {
    position: 'absolute',
    bottom: 0,
    // top:0,
    // height: sizes.getDimensions.height,
    height: '100%',
    width: sizes.getDimensions.width,
    zIndex: 10,
    // backgroundColor: 'red',
  },
});
// backgroundColor: 'rgba(0,0,0,0.0)' color with opacity
