import React, {useEffect} from 'react';
import {Block, Text} from 'components';
import {ActivityIndicator, Modal, StyleSheet} from 'react-native';
import {colors, sizes} from 'styles/theme';
import {LANG_AR} from 'utils/LanguageHandler';
import {useSelector} from 'react-redux';
import { prototype } from 'react-native/Libraries/Image/ImageBackground';



export const ActivitySign= props => {
  const {bc} = props;
  return (
    <Block center middle style={styles.holder}>
      <Block
        flex={false}
        center
        middle
        style={{
          ...styles.loader,
          backgroundColor: bc || colors.primary,
        }}>
        <ActivityIndicator
          size={props.size || `large`}
          color={bc ? colors.primary : colors.customRed}
        />
      </Block>
    </Block>
  );
};

const styles = StyleSheet.create({
  loader: {
    height: sizes.screenSize * 0.06,
    width: sizes.screenSize * 0.06,
    // width: sizes.getDimensions.width * 0.3,
    borderRadius: sizes.withScreen(0.003),
    elevation: 10,
    shadowColor: colors.black,
    shadowOpacity: 0.8,
    shadowRadius: sizes.getWidth(0.4),
    shadowOffset: {width: 0, height: 0},
  },
  holder: {
    position: 'absolute',
    bottom: 0,
    // top:0,
    // height: sizes.getDimensions.height,
    height: '100%',
    width: sizes.getDimensions.width,
    zIndex: 10,
    // backgroundColor: 'red',
  },
});

export default ActivitySign;
