import React from 'react';
import {Block, Text, Button} from 'components';
import {sizes, colors} from 'styles/theme';
import {Image, TextInput, Dimensions, Platform} from 'react-native';
import * as icons from 'assets/icons';

const Searchbar = props => {
  const ios = Platform.OS === 'ios';
  const iphone6s = ios && Dimensions.get('screen').width === 375;
  const {title, placeholder, bg, onChangeText, onIconPress} = props;
  return (
    <Block
      padding={[0, sizes.padding * 1.5]}
      flex={false}
      height={sizes.getHeight(iphone6s ? 18 : 15)}
      style={{
        // borderWidth: 1,
        backgroundColor: bg ? bg : colors.spaceGreen,
      }}>
      <Block flex={false} height={sizes.getHeight(iphone6s ? 8 : 6)} center>
        <Text style={{fontSize:sizes.customFont(iphone6s?12:18)}} color={colors.primary}>
          {title}
        </Text>
      </Block>
      <Block
        row
        center
        flex={false}
        style={{
          backgroundColor: colors.primary,
          borderRadius: sizes.withScreen(0.004),
          height: sizes.getHeight(6),
        }}>
        <Image
          source={icons.search}
          style={{
            tintColor: colors.gray,
            resizeMode: 'contain',
            width: sizes.getWidth(8),
            //  backgroundColor: 'red',
            height: '100%',
          }}
        />
        <TextInput
          onChangeText={e => onChangeText(e)}
          placeholder={placeholder || 'Please Enter Place holder'}
          style={{
            textAlign: 'left',
            width: '78%',
            fontSize:sizes.customFont(iphone6s?10:14)
            // borderWidth: 1
          }}
        />

        <Button
          onPress={onIconPress}
          center
          middle
          style={{
            // borderWidth: 1,
            width: '13%',
          }}>
          <Image
            source={icons.filter}
            style={{
              resizeMode: 'contain',
              width: sizes.getWidth(6),
            }}
          />
        </Button>
      </Block>
    </Block>
  );
};

export default Searchbar;
