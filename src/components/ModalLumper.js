import React from 'react';
import {Modal, View, StyleSheet, ViewPropTypes} from 'react-native';
import PropTypes from 'prop-types';

const CustomModal = props => {
  const {visible, children, style} = props;
  return (
    <Modal animationType={'slide'} transparent={true} visible={visible}>
      <View style={[styles.container, style]}>{children}</View>
    </Modal>
  );
};

CustomModal.propTypes = {
  visible: PropTypes.bool,
  children: PropTypes.node,
  style: ViewPropTypes.style,
};
export default CustomModal;

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
  },
});
