import React from 'react';
import {Block, Text} from 'components';
import {TextInput, Image, Dimensions, Platform} from 'react-native';
import {sizes, colors} from 'styles/theme';
import {theme} from 'styles';

const ios = Platform.OS === 'ios';
const iphone6s = Dimensions.get('screen').width <= 375;
export const CustomInput = React.forwardRef((props,ref) => {

  const {
    source,
    placeholder,
    placeholderColor,
    width,
    numeric,
    noRounded,
    noIcon,
    bb,
    bc,
    bw,
    textarea,
    height,
    numberOfLine,
    pv,
    ph,
    imgStyling,
    name,
    onChangeText,
    forwardRef,
    blockStyle,
  } = props;
  return (
    <Block
      center
      middle
      flex={false}
      padding={[pv ? pv : 0, ph ? ph : 0]}
      row
      style={{
        marginVertical: sizes.getHeight(0.3),
        borderWidth: bw && 1,
        borderBottomWidth: bb && 0.7,
        borderColor: bc ? bc : colors.customRed,
        width: width || '100%',
        height: height || sizes.getHeight(7),
        // borderWidth: 4,
        // borderWidth: 1,
        borderRadius: !noRounded ? sizes.withScreen(0.1) : 0,
        ...blockStyle,
      }}>
      {!noIcon && (
        <Image
          source={source}
          style={{
            resizeMode: 'contain',
            height: sizes.getHeight(2.5),
            width: sizes.getWidth(6),
            // borderWidth: 1,
            ...imgStyling,
          }}
        />
      )}
      <TextInput
        // ref={forwardRef}
        ref={ref}
        numberOfLines={numberOfLine}
        multiline={textarea && true}
        keyboardType={numeric && 'number-pad'}
        placeholderTextColor={placeholderColor}
        placeholder={placeholder}
        // name={name}
        style={{
          // textAlignVertical: 'top',
          // paddingBottom:20,
          // paddingTop: 10,
          // alignContent: 'flex-start',
          // alignSelf: 'flex-start',
          // borderWidth: 1,
          fontSize: sizes.customFont(iphone6s ? 10 : 15),
          textAlign: 'left',
          justifyContent: 'flex-start',
          textAlignVertical: 'top',
          borderBottomColor: 'red',
          width: '90%',
          color: colors.spaceGreen,
          // height: sizes.getHeight(7),
          paddingHorizontal: sizes.getWidth(2),
          ...props.inputyStyle,
          ...theme.fonts.GMR,
        }}
        {...props}
        onChangeText={text => onChangeText({name, text})}
      />
    </Block>
  );
});

export const CustomTextArea = React.forwardRef((props,ref) => {
  const {placeholder, forwardRef, name, numberOfLines, onChangeText} = props;
  return (
    <TextInput
      ref={ref}
      placeholder={placeholder}
      multiline={true}
      onChangeText={text => onChangeText({name, text})}
      numberOfLines={numberOfLines || 8}
      style={{
        fontSize: sizes.customFont(iphone6s ? 10 : 15),
        textAlignVertical: 'top',
        paddingTop: 0,
        paddingBottom: 0,
        width: '100%',
        color: colors.customRed,
        paddingHorizontal: sizes.getWidth(2),
        paddingTop: sizes.getHeight(1),
        ...theme.fonts.GMR,
      }}
    />
  );
},['']);
