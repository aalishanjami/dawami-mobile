import React from 'react';
import {StyleSheet, Image, ActivityIndicator, Dimensions, Platform} from 'react-native';
import {sizes, colors} from 'styles/theme';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {DrawerItem} from '@react-navigation/drawer';
import {Block, Text} from 'components';
import {useSelector} from 'react-redux';
import { LANG_AR } from 'utils/LanguageHandler';

const DrawerItems = ({
  label,
  image,
  route,
  to,
  // labelStyle,
  imgStyle,
  isWaiting,
  // runFunction,
  onPress,
}) => {
    const ios = Platform.OS === 'ios';
    const iphone6s = Dimensions.get('screen').width <= 375;
  const currentLN = useSelector(state => state.userInfo.currentLN);
  const AR  = currentLN === LANG_AR
  return (
    // <DrawerItem
    //   labelStyle={styles.labelStyle}
    //   style={styles.listStyle}
    //   icon={({color, size}) => (
    //     <Image source={image} style={{...styles.iconStyle, ...imgStyle}} />
    //   )}
    //   label={() => (
    //       <Text
    //         style={{
    //           borderWidth: 1,
    //           // height: sizes.getHeight(10),
    //           fontSize: sizes.customFont(11),
    //         }}>
    //         {label}
    //       </Text>
    //   )}
    //   onPress={onPress}
    // />
    <DrawerItem
      style={styles.itemStyle}
      onPress={onPress}
      labelStyle={styles.labelStyle}
      icon={() => (
        <Block  flex={0}>
          <Image source={image} style={{...styles.iconStyle, ...imgStyle}} />
        </Block>
      )}
      label={e => (
        <Block
          middle
          style={{
            // borderWidth: 1,
            width: sizes.getWidth(40),
            minHeight: sizes.getHeight(iphone6s?7:4),
            // backgroundColor: 'red',
            flex: 0,
          }}>
          <Text
            style={{
              // borderWidth: 1,
              fontSize: sizes.customFont(AR?iphone6s?7:14:iphone6s?9:14),
              // height:sizes.getHeight(7),
              // width: '100%',
              textAlign:'left',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            {label}
          </Text>
        </Block>
      )}
    />
  );
};

const styles = StyleSheet.create({
  itemStyle: {
    alignItems: 'flex-start',
    // borderWidth: 1,
    height: sizes.getHeight(4),
    justifyContent: 'center',
    // paddingHorizontal:sizes.getWidth(4)
  },
  labelStyle: {
    // borderWidth:2
  },
  iconStyle: {
    tintColor: colors.gray,
    resizeMode: 'contain',
    // flex:1,
    width: sizes.getWidth(5),
    height:sizes.getHeight(3)
    // borderWidth:1
  },
  // labelStyle: {
  //   borderWidth: 1,
  //   // backgroundColor:'red',
  //   // width: sizes.getWidth(50),
  //   paddingLeft: sizes.getWidth(1),
  //   marginHorizontal: -sizes.getHeight(4),
  //   fontSize: sizes.h2,
  // },
  // listStyle: {
  //   // alignItems: 'center',
  //   borderWidth: 1,
  //   height: sizes.getHeight(5),
  //   justifyContent: 'center',
  // },
  // iconStyle: {
  //   tintColor: colors.gray3,
  //   width: sizes.getWidth(4),
  //   resizeMode: 'contain',
  //   // borderWidth: 1,
  // },
});

export default DrawerItems;
