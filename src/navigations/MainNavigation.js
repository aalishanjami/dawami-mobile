import React, { useEffect } from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import DrawerContent from './DrawerContent';
import {AppStack, AuthStack, UserStack} from 'navigations';
import {useSelector} from 'react-redux';
import {NAV} from 'utils';
import {WelcomeScreen, WelcomingScreen} from 'screens';
import {AppState} from 'react-native';


export const Navigations = () => {
  const {status} = useSelector(state => state.auth);
  //if auth success then navigate to Home

  const Drawer = createDrawerNavigator();
  const Stack = createStackNavigator();

  // const Splash = props => {
  //   return (
  //     <Stack.Navigator headerMode="none">
  //       <Stack.Screen
  //         name={NAV.Welcome}
  //         component={WelcomeScreen}
  //         options={{headerShown: false}}
  //       />
  //       {/* <Stack.Screen
  //         name={NAV.WelcomAgain}
  //         component={WelcomingScreen}
  //         options={{headerShown: false}}
  //       /> */}
  //     </Stack.Navigator>
  //   );
  // };

  const AppStackWithDrawer = props => {
    return (
      <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
        <Drawer.Screen name="app" component={AppStack} {...props} />
      </Drawer.Navigator>
    );
  };
  // const ScreenStack = () => {
  //   const SStack = createStackNavigator();
  //   return (
  //     <SStack.Navigator headerMode="none">
  //       {status ? (
  //         <Stack.Screen name="app" component={AppStackWithDrawer} />
  //       ) : (
  //         <Stack.Screen name="auth" component={AppStack} />
  //       )}
  //     </SStack.Navigator>
  //   );
  // };

  return (
    <NavigationContainer>
      <Stack.Navigator headerMode="none">
        {status ? (
          <Stack.Screen name="app" component={AppStackWithDrawer} />
        ) : (
          <Stack.Screen name="auth" component={AppStack} />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};




