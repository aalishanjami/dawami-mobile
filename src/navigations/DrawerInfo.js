import React from 'react';
import {Block, Button, Text} from 'components';
import {sizes, colors} from 'styles/theme';
import {Image, View, Dimensions} from 'react-native';
import * as images from 'assets/images';
import * as icons from 'assets/icons';
import {useSelector, useDispatch} from 'react-redux';
import {SET_LANGUAGE, CURRENT_COMP, LANG_AR, LANG_EN} from 'redux/constants';

const DrawerInfo = props => {
  const userInfo = useSelector(state => state.auth?.userBasicProfile);
  const {currentLN} = useSelector(state => state.userInfo);
  const dispatch = useDispatch();
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width === 375;


  const changeLN = () => {
    let selected = currentLN;

    dispatch({
      type: SET_LANGUAGE,
      payload: currentLN === LANG_AR ? LANG_EN : LANG_AR,
    });
  };
  const usrImgPath = 'https://dawami.wedigits.dev/images/user_avatars/';

  return (
    <Block
      margin={[sizes.getHeight(iphone6s?4:0), 0, sizes.getHeight(0), 0]}
      flex={false}
      height={sizes.getHeight(20)}
      style={{borderWidth: 0}}>
      <Block top crossRight padding={[sizes.getHeight(0), 0]}>
        <Button onPress={changeLN}>
          <Text 
          style={{fontSize:iphone6s?15:18}}
          color={'green'}>{currentLN}</Text>
        </Button>
      </Block>
      <Block
        center
        flex={2}
        padding={[0, sizes.getWidth(1)]}
        row
        style={{backgroundColor: 'white', elevation: 1}}>
        {/* <View
          style={{
            // borderWidth: 1,
            
          }}>
          <Text>123</Text>
        </View> */}
        <Block
          flex={false}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: 0.2,
            elevation: 2,
            backgroundColor: colors.primary,
            borderColor: colors.spaceGreen,
            marginRight: sizes.getWidth(3),
            width: sizes.screenSize * 0.06,
            height: sizes.screenSize * 0.06,
            borderRadius: sizes.screenSize * 2,
            // overflow: 'hidden',
            shadowColor: colors.black,
            shadowOpacity: 0.1,
            shadowOffset: {width: 0, height: 1},
            shadowRadius: 1,
          }}>
          <Block
            style={{
              // borderWidth: 1,
              flex: 0,
              width: sizes.screenSize * 0.06,
              height: sizes.screenSize * 0.06,
              borderRadius: sizes.screenSize * 0.1,
              overflow: 'hidden',
              justifyContent: 'center',
              alignItems: 'center',
              padding: sizes.getWidth(1),
            }}>
            <Button
              center
              middle
              style={{
                width: '100%',
                height: '100%',
                // borderWidth: 1,
              }}>
              {userInfo?.avatar ? (
                <Image
                  source={{uri: usrImgPath + userInfo?.avatar}}
                  style={{
                    resizeMode: 'contain',
                    flex: 1,
                    width: '100%',
                    borderColor: colors.black,
                    // tintColor: colors.spaceGreen,
                  }}
                />
              ) : (
                <Image
                  source={icons.avatarDummy}
                  style={{
                    resizeMode: 'contain',
                    borderColor: colors.black,
                    flex: 1,
                    width: '100%',
                    // tintColor: colors.spaceGreen,
                  }}
                />
              )}
            </Button>
          </Block>
        </Block>
        <Block flex={2}>
          <Text h3 bold style={{
            fontSize:iphone6s?18:20
            ,textTransform: 'capitalize'}}>
            {userInfo?.name || 'Not Proivder'}
          </Text>
          {/* <Text h3>Web Developer</Text> */}
          <Text style={{fontSize: userInfo?.expertise ? sizes.h3 : sizes.h4}}>
            {userInfo?.expertise || ' -- '}
          </Text>
        </Block>
      </Block>
    </Block>
  );
};

export default DrawerInfo;
