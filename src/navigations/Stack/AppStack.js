import React from 'react';
import {
  CompanyPorfolio,
  HomePage,
  Companies,
  Contact,
  About,
  JobList,
  JobDescription,
  TrainingCatalogue,
  CatalogueDetails,
  WelcomeScreen,
  WelcomingScreen,
  Profile,
  EditProfile,
  Messages,
  Chat,
  Favorites,
  ForgetPassword,
  Login,
  Signup,
  CvBuilder,
} from 'screens';
import {createStackNavigator} from '@react-navigation/stack';
import {CustomHeader, Block} from 'components';
import {colors} from 'styles/theme';
import {FavComp} from 'screens/Favorites/Section/FavComp';
import {AppliedJob} from 'screens/Favorites/Section/AppliedJob';
import {NAV} from 'utils';
import {useSelector} from 'react-redux';
import {LANG_EN} from 'redux/constants';
import {ImageViewer} from 'screens/CompanyProfile/Section/ImageViewer';

const AppStack = ({route, navigation}) => {
  const {currentLN} = useSelector(state => state.userInfo);
  const AppNavigation = createStackNavigator();

  const currentRoute = currentLN ? NAV.HOME : NAV.Welcome;

  return (
    <AppNavigation.Navigator
      initialRouteName={currentRoute}
      // initialRouteName={NAV.PROFILE}
      // initialRouteName={NAV.EDIT_PRO}
      mode="modal"
      // screenOptions={(route,navigation) => ({
      //   cardOverlayEnabled: true,
      // })}
      headerMode="none">
      {/* --------------------- */}
      <AppNavigation.Screen
        name={NAV.Welcome}
        component={WelcomeScreen}
        options={{headerShown: false}}
      />
      <AppNavigation.Screen
        name={NAV.HOME}
        component={HomePage}
        options={{headerShown: false}}
      />

      <AppNavigation.Screen name={NAV.COMPANY} component={Companies} />
      <AppNavigation.Screen
        name={NAV.COMPANY_DETAILS}
        component={CompanyPorfolio}
      />
      <AppNavigation.Screen
        name={NAV.CV}
        component={CvBuilder}
        options={{headerShown: false}}
      />
      <AppNavigation.Screen name={NAV.JOB} component={JobList} />
      <AppNavigation.Screen
        name={NAV.JOB_DETAILS}
        component={JobDescription}
      />
      <AppNavigation.Screen
        name={NAV.TRAININGS}
        component={TrainingCatalogue}
      />
      {/* <AppNavigation.Screen name={NAV.FAV_COMP} component={FavComp} /> */}
      {/* <AppNavigation.Screen name={NAV.APP_JOB} component={AppliedJob} /> */}
      <AppNavigation.Screen
        name={NAV.CATELOG_DETAILS}
        component={CatalogueDetails}
        options={{headerShown: false}}
      />
      <AppNavigation.Screen
        name={NAV.CONTACT}
        component={Contact}
        // options={{
        //   header: props => <CustomHeader bg={colors.brown} {...props} />,
        // }}
      />
      <AppNavigation.Screen name={NAV.ABOUT} component={About} />

      {/* LOGIN */}

      <AppNavigation.Screen
        name={NAV.RESET}
        component={ForgetPassword}
        options={{headerShown: false}}
      />
      <AppNavigation.Screen
        name={NAV.LOGIN}
        component={Login}
        options={{headerShown: false}}
      />
      <AppNavigation.Screen
        name={NAV.SIGNUP}
        component={Signup}
        options={{headerShown: false}}
      />
      {/* LOGIN END */}
      {/* USER ROUTES */}
      <AppNavigation.Screen
        name={NAV.PROFILE}
        component={Profile}
        options={{
          header: props => (
            <CustomHeader backBtn bg={colors.customRed} {...props} />
          ),
        }}
      />
      <AppNavigation.Screen
        name={NAV.EDIT_PRO}
        component={EditProfile}
        options={{
          header: props => (
            <CustomHeader backBtn bg={colors.customRed} {...props} />
          ),
        }}
      />
      <AppNavigation.Screen
        name={NAV.MESSAGES}
        component={Messages}
        options={{
          headerShown: false,
          // header: props => <CustomHeader bg={colors.darkBrown} {...props} />,
        }}
      />
      <AppNavigation.Screen
        name={NAV.CHAT}
        component={Chat}
        options={{
          headerShown: false,

          // options={{
          //   header: props => <CustomHeader bg={colors.darkBrown} {...props} />,
        }}
      />
      <AppNavigation.Screen
        name={NAV.FAVORITES}
        component={Favorites}
        options={{
          header: props => <CustomHeader backBtn {...props} />,
        }}
      />

      <AppNavigation.Screen
        name={NAV.IMAGE_VIEWER}
        component={ImageViewer}
        options={{headerShown: false}}
      />
    </AppNavigation.Navigator>
  );
};

export {AppStack};
