import React, {useState, useRef, useEffect} from 'react';
import {Block, Text, Button, CustomAlert, ActivitySign} from 'components';
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';
import * as icons from 'assets/icons';
import {sizes, colors} from 'styles/theme';
import {Image, StyleSheet, ActivityIndicator} from 'react-native';
import * as images from 'assets/images';
import DrawerItems from './drawerItem';
import DrawerInfo from './DrawerInfo';
import {useSelector, useDispatch} from 'react-redux';
import {
  logout,
  loggedout,
  GetAllUserChat,
  AboutUs,
  getAllTrainings,
  GetJobsList,
  GetCompaniesList,
  FavoriteComp,
  AppliedJobs,
  FavoriteJob,
  fetchMessage,
  getCompanies,
  getJobs,
  getTrainings,
} from 'redux/actions';
import {
  TRAININGS,
  APPLIED_JOB,
  ADD_FAV_COMP,
  LANG_AR,
  COMPAINES_LIST_STATE,
  JOB_LIST_STATE,
} from 'redux/constants';
import {WORDS, NAV} from 'utils';
import {SafeAreaView} from 'react-native-safe-area-context';
import Toast from 'react-native-easy-toast';
import {JobList} from 'screens';

const DrawerContent = props => {
  const messageRef = useRef();
  // console.log(props);
  const {navigation, route} = props;
  const {navigate} = navigation;
  const authSection = useSelector(state => state.auth);
  const {status} = authSection;
  const id = authSection?.userBasicProfile?.id;
  const {currentLN} = useSelector(state => state.userInfo);
  const dispatch = useDispatch();
  const currentJob = useSelector(state => state.getLists?.currentJobStatus);
  const chats = useSelector(state => state.getLists.all_chats);
  const compaines = useSelector(state => state.getLists.listOfCompanies);
  const jobslist = useSelector(state => state.getLists.listOfJobs);
  const trainingslist = useSelector(state => state.getLists.Trainings);
   const action = {
     dispatch,
     messageRef,
     navigate,
     compaines,
     jobslist,
     trainingslist,
     chats,
     id,
   };

  // ----------------------------------------------------------------
  const AR = currentLN === LANG_AR;
  const profile = AR ? WORDS.PROFILE_AR : WORDS.PROFIE;
  const message = AR ? WORDS.MESSAGES_AR : WORDS.MESSAGE;
  const favorites = AR ? WORDS.FAVORITE_AR : WORDS.FAVORITE;
  const company = AR ? WORDS.COMPANY_AR : WORDS.COMPANY;
  const jobee = AR ? WORDS.JOB_AR : WORDS.JOB;
  const trainings = AR ? WORDS.TRAINING_CATE_AR : WORDS.TRAINING_CATELOG;
  const cv = AR ? WORDS.CV_BUILDER_AR : WORDS.CV_BUILDER;
  const favComp = AR ? WORDS.FAV_COMP_AR : WORDS.FAV_COMP;
  const appJobs = AR ? WORDS.APP_JOB_AR : WORDS.APP_JOB;
  const contact = AR ? WORDS.CONTACT_AR : WORDS.CONTACT;
  const about = AR ? WORDS.ABOUT_AR : WORDS.ABOUT;
  const signout = AR ? WORDS.LOGOUT_AR : WORDS.LOGOUT;

  const FAV_JOB_TITLE = 'Favorite Jobs';
  const FAV_JOB_TITLE_AR = 'الوظائف المفضلة';

  const favJobs = AR ? FAV_JOB_TITLE_AR : FAV_JOB_TITLE;
  // const appJobs = AR ? WORDS.APP_JOB_AR : WORDS.APP_JOB;
  // ----------------------------------------------------------------

  const loggingout = async () => {
    const result = await loggedout(id, err => {
      console.log('=======ERROR IN LOG OUT=========');
      console.log(err);
    });

    result && dispatch(result);
  };

  const fetchMessages = async () => {
    if(!chats?.length){
      fetchMessage(action,setIsLoading)
    }else{
      navigate(NAV.MESSAGES)
    }
  };

  const getTrainings = async () => {
    if (!trainingslist?.length) {
      getTrainings(action, setIsLoading);
    } else {
      navigate(NAV.TRAININGS);
    }
  };


  const fetchCompanies = async () => {
      getCompanies(action,setIsLoading)
  };

  const [isLoading, setIsLoading] = useState(false);
  // =--=====================>>>>
  const getJob = async () => {
      getJobs(action,setIsLoading)
  };
  // =--=====================>>>>
  const goToAboutUs = async () => {
    navigation.navigate(NAV.ABOUT);
  };
  
  const followdCompany = async () => {
    setIsLoading(true);
    const result_favComp = await FavoriteComp(id, err => {
      console.log(err);
      messageRef.current?.show(err?.message || err, 2000);
      setIsLoading(false);
    });
    console.log(result_favComp);
    result_favComp &&
      dispatch({
        type: COMPAINES_LIST_STATE,
        payload: {listOfCompanies: result_favComp},
      });
    // dispatch({
    //   type: ADD_FAV_COMP,
    //   payload: result_favComp,
    // });
    setIsLoading(false);
    navigation.navigate(NAV.COMPANY, {favCompList: true, title: favComp});
    // navigation.navigate(NAV.FAV_COMP);
  };

  // ===============>
  const appliedJobs = async () => {
    setIsLoading(true);
    const result_appliedJobs = await AppliedJobs(id, err => {
      console.log(err);
      messageRef.current?.show(err?.message || err, 2000);
      setIsLoading(false);
    });

    // dispatch({
    //   type: APPLIED_JOB,
    //   payload: result_appliedJobs,
    // });
    if (result_appliedJobs) {
      dispatch({
        type: JOB_LIST_STATE,
        payload: {listOfJobs: result_appliedJobs},
      });
      setIsLoading(false);
      navigation.navigate(NAV.JOB, {appJobList: true, title: appJobs});
      // navigation.navigate(NAV.APP_JOB);
    }
  };
  // =================>
  const getFav = async () => {
    setIsLoading(true);
    const result_favJobs = await FavoriteJob(id, err => {
      console.log(err);
      setIsLoading(false);
    });
    dispatch({type: JOB_LIST_STATE, payload: {listOfJobs: result_favJobs}});
    setIsLoading(false);
    navigation.navigate(NAV.JOB, {favJobList: true, title: favJobs});
  };

  // useEffect(() => {
  //   return () => null;
  // }, ['']);

  return (
    <>
      <SafeAreaView style={{flex: 1}}>
        <Block padding={[0, sizes.getWidth(4)]} style={{borderWidth: 0}}>
          <DrawerInfo />
          <DrawerContentScrollView {...props}>
            <DrawerItems
              label={profile}
              onPress={() => navigation.navigate(NAV.PROFILE)}
              image={icons.white_user}
            />
            <DrawerItems
              onPress={fetchMessages}
              label={message}
              image={icons.message}
            />

            <DrawerItems
              label={favorites}
              onPress={getFav}
              image={icons.empty_heart}
            />
            <DrawerItems
              onPress={fetchCompanies}
              label={company}
              image={icons.company}
            />
            <DrawerItems
              label={jobee}
              isWaiting={isLoading}
              onPress={getJob}
              image={icons.job}
            />
            <DrawerItems
              label={trainings}
              onPress={getTrainings}
              image={icons.training_catalog}
            />
            <DrawerItems
              onPress={() => props.navigation.navigate(NAV.CV)}
              label={cv}
              image={icons.cv}
            />
            <DrawerItems
              label={favComp}
              onPress={followdCompany}
              image={icons.heartPlus}
            />
            <DrawerItems
              label={appJobs}
              onPress={appliedJobs}
              image={icons.tic}
            />
            <DrawerItems
              label={about}
              onPress={goToAboutUs}
              image={icons.company}
            />
            <DrawerItems
              label={contact}
              onPress={() => navigation.navigate(NAV.CONTACT)}
              image={icons.phone1}
            />
          </DrawerContentScrollView>
          {isLoading && (
            <Block style={styles.loader}>
              <Block bottom width="100%">
                <ActivityIndicator size="large" color={colors.customRed} />
              </Block>
            </Block>
          )}

          {/* <DrawerItem
        icon={({color, size}) => (
          <Image source={icons.logout} style={styles.iconStyle} />
        )}
        style={styles.listStyle}
        label={signout}
        onPress={loggingout}
        labelStyle={styles.labelStyle}
      /> */}
          <Block flex={false} margin={[0, 0, sizes.getHeight(2), 0]}>
            <DrawerItems
              label={signout}
              onPress={loggingout}
              image={icons.logout}
            />
          </Block>
        </Block>
      </SafeAreaView>
      <Toast
        ref={messageRef}
        style={{
          backgroundColor: colors.customRed,
          // width: sizes.getWidth(100),
          width: '100%',
          borderRadius: 0,
        }}
        positionValue={sizes.getDimensions.height}
        fadeInDuration={200}
        fadeOutDuration={100}
        opacity={1}
      />
    </>
  );
};

const styles = StyleSheet.create({
  // labelStyle: {
  //   // borderWidth: 1,
  //   marginHorizontal: -sizes.getHeight(3),
  //   fontSize: sizes.h2,
  // },
  listStyle: {
    // borderWidth: 1,
    height: sizes.getHeight(7),
    justifyContent: 'center',
  },
  iconStyle: {
    tintColor: colors.gray,
    width: sizes.getWidth(3.5),
    resizeMode: 'contain',
  },
  loader: {
    position: 'absolute',
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    paddingBottom: sizes.getHeight(10),
    zIndex: 1000,
  },
});

export default DrawerContent;
