import AsyncStorage from '@react-native-community/async-storage';
import {combineReducers, applyMiddleware, createStore} from 'redux';
import {authReducer, userInfoReducer, getRequestReducer} from 'redux/reducer';
import {persistReducer, persistStore} from 'redux-persist';
import logger, {createLogger} from 'redux-logger';
import autoMergeLevel2 from 'redux-persist/es/stateReconciler/autoMergeLevel2';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  stateReconciler: autoMergeLevel2,
  timeout: 0,
};
const rootReducer = combineReducers({
  auth: authReducer,
  userInfo: userInfoReducer,
  getLists: getRequestReducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer, applyMiddleware(createLogger()));

let persistedStore = persistStore(store);

export {store, persistedStore};
