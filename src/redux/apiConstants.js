export const baseUrl = 'https://dawami.wedigits.dev/api/';
export const ResumeDownload = 'https://dawami.wedigits.dev/resumes';
export const FileDownload = 'https://dawami.wedigits.dev/messages/files';

const ImageBasePath = 'https://dawami.wedigits.dev/images/';
export const companyLogo = `${ImageBasePath}company_logos/`;

export const TrainingImageBasePath = `https://dawami.wedigits.dev/training/images/`;

export const Registration = `${baseUrl}register`;
export const LoginUser = `${baseUrl}login`;
export const Logout = `${baseUrl}logout/`;
export const ForgetPasswordSendEmail = `${baseUrl}reset/password/sendEmail`;
export const ForgetPasswordVerifyCode = `${baseUrl}reset/password/verify`;
export const ResetPassword = `${baseUrl}reset/password`;
export const GetCompanies = `${baseUrl}companies`;
export const GetJobs = `${baseUrl}jobs`;

export const HomePageSearch = `${baseUrl}filter`;

export const Google = `${baseUrl}google-login`; // params email + name
export const Facebook = `${baseUrl}facebook-login`; // params email + name

export const CheckUserName = `${baseUrl}check/username/`;
export const CheckEmail = `${baseUrl}check/email/`;
export const RemoveEmail = `${baseUrl}remove/user/email/`;
export const UserVerfication = `${baseUrl}verify/token`;

// ==========
export const resumeRequest = `${baseUrl}request/resume/`;
export const seeAllResumeRequest = `${baseUrl}requests/all/`;
export const viewProgress = `${baseUrl}request/resume/progress/`;
export const draftLike = `${baseUrl}request/resume/liked/`;
export const draftDislike = `${baseUrl}request/resume/disliked/draft`;
export const checkResponse = `${baseUrl}request/resume/draft/response/`;

// User Profile
export const ProfileInfo = `${baseUrl}user/`;
export const BasicInfoUpdate = `${baseUrl}profile/`;

// Messages
export const GetAllChat = `${baseUrl}user/contacts/`;
export const GetOneChat = `${baseUrl}chat/`;
export const ChatWith = `${baseUrl}chat/store`;
export const StoreFile = `${ChatWith}/file`;

// trainings catelogue
// https://dawami.wedigits.dev/api/training/{id}/view
export const GetTrainingList = `${baseUrl}trainings`;
export const GetTrainingDetails = `${baseUrl}training`;

//apply for job
// https://dawami.wedigits.dev/api/apply-for-job/{job_id}/user/{user_id}
export const ApplyJob = `${baseUrl}apply-for-job/`;

// Save Job
// https://dawami.wedigits.dev/api/save-job/{job_id/user/{user_id}
export const SaveJob = `${baseUrl}save-job/`;

//view specifice job if user logged
export const CheckJobStatus = `${baseUrl}job/`;

export const TrainingFilter = `${baseUrl}trainings/filter`;
export const JobFilter = `${baseUrl}jobs/filter`;
export const CompanyFilter = `${baseUrl}companies/filter`;

export const FollowCompany = `${baseUrl}follow-company/`;

export const ViewCompany = `${baseUrl}company/`;

export const FavComps = `${baseUrl}favorite-companies/`;
export const FavJobs = `${baseUrl}favorite-jobs/`;
export const AppJobs = `${baseUrl}applied-jobs/`;

export const Contact = `${baseUrl}contact/message`;
export const CompamyContact = `${baseUrl}contact-information`;
export const About = `${baseUrl}about-us-information`;
