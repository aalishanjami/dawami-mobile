import Axios from 'axios';
import {
  GetCompanies,
  GetJobs,
  resumeRequest,
  seeAllResumeRequest,
  viewProgress,
  checkResponse as callResponseApi,
  draftDislike,
  draftLike,
  GetAllChat,
  GetOneChat,
  ChatWith,
  StoreFile,
  GetTrainingList,
  GetTrainingDetails,
  ApplyJob,
  CheckJobStatus,
  SaveJob,
  HomePageSearch,
  TrainingFilter,
  JobFilter,
  FollowCompany,
  CompanyFilter,
  ViewCompany,
  FavJobs,
  FavComps,
  AppJobs,
  Contact,
  About,
  CompamyContact,
} from 'redux/apiConstants';
import {
  COMPAINES_LIST_STATE,
  JOB_LIST_STATE,
  ALL_CHATS,
  SAVE_JOB_STATUS,
  IS_FAV,
  IS_APPLIED,
  TRAININGS
} from 'redux/constants';

const companyList = data => {
  // console.log('DAA_--------------')
  return {
    type: COMPAINES_LIST_STATE,
    payload: {listOfCompanies: data.companies},
  };
};

const jobList = data => {
  // console.log('DAA_--------------')
  return {type: JOB_LIST_STATE, payload: {listOfJobs: data.jobs}};
};
const chatList = allChats => {
  return {type: ALL_CHATS, payload: allChats};
};
const SaveIsFav = status => {
  return {type: IS_FAV, payload: status};
};

const jobApplied = status => {
  return {type: IS_APPLIED, payload: status};
};

// ------------------ASYNC FUNCTION ==============================================================
export const GetCompaniesList = async onError => {
  try {
    const response = await Axios.get(GetCompanies);
    console.log('------API RESPONSE GET COMPANIES LIST');
    console.log(response);
    return response.data.success
      ? companyList(response.data)
      : (console.log('I am in first error'), onError(response.data.success));
    return result;
  } catch (e) {
    // console.log('CATCHING STATE');
    // console.log(e);
    onError(e);
  }
};

export const GetJobsList = async onError => {
  try {
    const response = await Axios.get(GetJobs);
    console.log('===========JOB RESULT===============');
    console.log(response);
    return response.data.success
      ? jobList(response.data)
      : (console.log('I am in first error'), onError(response.data));
    return result;
  } catch (e) {
    console.log('CATCHING STATE');
    console.log(e);
    onError(e);
  }
};

export const requestResume = async (userId, onError) => {
  try {
    const response = await Axios.get(`${resumeRequest}${userId}`);
    console.log('response result');
    return response.data.success ? response.data : onError(response.data);
  } catch (e) {
    console.log('error fetching');
    console.log(e);
    onError(e);
  }
};

// ========= cv builder

export const getAllResumeReuqests = async (userId, onError) => {
  // console.log('userId')
  // console.log(userId)
  try {
    const response = await Axios.get(`${seeAllResumeRequest}${userId}`);
    return response.data.success
      ? response.data.requests
      : onError(response.data);
  } catch (e) {
    console.log('error fetching');
    // console.log(e)
    onError(e);
  }
};

export const checkProgress = async (userId, onError) => {
  // console.log('userId')
  // console.log(userId)
  try {
    const response = await Axios.get(`${viewProgress}${userId}`);
    // console.log('response')
    // console.log(response)
    return response.data.success ? response.data : onError(response.data);
  } catch (e) {
    console.log('error fetching');
    console.log(e);
    onError(e);
  }
};

export const draftLiked = async (draftDetails, onError) => {
  console.log('draftDetails');
  console.log(draftDetails);

  try {
    const {req_id, draft_id} = draftDetails;
    const response = await Axios.get(`${draftLike}${req_id}/draft/${draft_id}`);
    console.log('response api');
    console.log(response);
    return response.data.success
      ? response.data.feedback
      : onError(response.data);
  } catch (e) {
    console.log('cating state');
    console.log(e);
    onError(e);
  }
};

export const draftDisliked = async (userDetails, onError) => {
  // console.log('userDetails')
  // console.log(userDetails)
  const {id, req_id, comments} = userDetails;
  // console.log(id)
  // console.log(req_id)
  // console.log(comments)

  try {
    const response = await Axios.post(draftDislike, {
      draft_id: id,
      req_id: req_id,
      comments: comments,
    });
    console.log('dislike response api');
    console.log(response);
    // return response.data.success ? response.data : onError(response.data)
  } catch (e) {
    console.log('cating state');
    console.log(e);
    onError(e);
  }
};

//  to check if feedback comments and like disklike
export const checkResponse = async (id, onError) => {
  try {
    const response = await Axios.get(`${callResponseApi}${id}`);
    console.log('response api');
    console.log(response.data.draft.feedback);
    return response.data.success ? response.data.draft : onError(response.data);
  } catch (e) {
    console.log('error');
    onError(e);
  }
};

// messages

export const GetAllUserChat = async (id, onError) => {
  // console.log("&&&&&&&&&&&&&&&&&&&&&&");
  // console.log(id);
  try {
    const response = await Axios.get(`${GetAllChat}${id}`);
    // console.log('API RESPONSE');
    // console.log(response.data)
    return response.data.success
      ? chatList(response.data.contacts)
      : onError(response.data);
  } catch (e) {
    console.log('Catching Error');
    console.log(e)
    if (e.message === 'Network Error') {
      onError(e.message);
    } else {
      
      onError(e);
    }
  }
};

export const getSpecificUserChat = async (me, other, onError) => {
  // console.log("========API START")
  // console.log(me)
  // console.log(other)
  // console.log("========API ENDED")
  try {
    const response = await Axios.get(`${GetOneChat}${me}/get/${other}`);
    // console.log("API RESPONSE")
    // console.log(response.data.chats)
    return response.data.success ? response.data.chats : onError(response.data);
  } catch (e) {
    console.log('Cating Error--getSpecificUserChat ');
    console.log(e);
    onError(e);
  }

  // return true
};

export const SaveChat = async (sender, reciever, message, onError) => {
  // console.log("============Save CHAT API==========")
  // console.log(sender)
  // console.log(reciever)
  // console.log(message)
  try {
    const response = await Axios.post(`${ChatWith}`, {
      sender: sender,
      reciever: reciever,
      message: message,
    });

    // console.log('response');
    // console.log(response);
    return response.data.success
      ? response.data.message
      : response.data.message;
    // : onError(response.data);
  } catch (e) {
    console.log('CATCH ERROR');
    console.log(e);
    onError(e);
  }
};

export const SendFile = async (Sender, Reciever, FileDetail, onError) => {
  // console.log("Req Recieved")
  const formData = new FormData();
  formData.append('file', {
    name: FileDetail.name,
    uri: FileDetail.uri,
    type: FileDetail.type,
  });
  formData.append('sender', `${Sender}`);
  formData.append('reciever', `${Reciever}`);

  try {
    const response = await Axios.post(`${StoreFile}`, formData);
    // console.log("API RESPONSE")
    // console.log(response)
    return response.data.success
      ? response.data.message
      : onError(response.data);
  } catch (error) {
    console.log('Error');
    console.log(error);
    onError(error);
  }
  // return (
  //   true
  // )
};

// ======================================================
// GET TRAINING LIST
export const getAllTrainings = async onError => {
  try {
    const response = await Axios.get(`${GetTrainingList}`);
    console.log('------API RESPONSE------TRaings')
    console.log(response)
    return response.data.success
      // ? response.data.trainings
      ? {type: TRAININGS, payload: response.data.trainings}
      : onError(response.data);
  } catch (e) {
    console.log('-----FETCH ERRRO API');
    console.log(e);
    onError(e);
  }
};

// GET TRAINING DETAILS
export const getTrainingDetails = async (id, onError) => {
  try {
    const response = await Axios.get(`${GetTrainingDetails}/${id}/view`);
    // console.log('------API RESPONSE------')
    // console.log(response)
    return response.data.success
      ? response.data.training
      : onError(response.data);
  } catch (e) {
    console.log('-----FETCH ERRRO API');
    console.log(e);
    onError(e);
  }
};

// Apply Job
export const ApplyForJob = async (jobId, userId, onError) => {
  try {
    const response = await Axios.get(`${ApplyJob}${jobId}/user/${userId}`);
    // console.log('------API RESPONSE------')
    // console.log(response)
    return response.data.success
      ? response.data.training
      : onError(response.data);
  } catch (e) {
    console.log('-----FETCH ERRRO API');
    console.log(e);
    onError(e);
  }
};

// SAVE JOB
export const SavingJob = async (jobId, userId, onError) => {
  try {
    const response = await Axios.get(`${SaveJob}${jobId}/user/${userId}`);
    // console.log('------API RESPONSE------')
    // console.log(response)
    return response.data.success
      ? response.data.training
      : onError(response.data);
  } catch (e) {
    console.log('-----FETCH ERRRO API');
    console.log(e);
    onError(e);
  }
};

// FOLLOW - JOB
export const FollowingJob = async (companyId, userId, onError) => {
  try {
    const response = await Axios.get(
      `${FollowCompany}${companyId}/user/${userId}`,
    );
    // console.log('------API RESPONSE------')
    // console.log(response)
    return response.data.success
      ? response.data.follow_status
      : onError(response.data);
  } catch (e) {
    console.log('-----FETCH ERRRO API');
    console.log(e);
    onError(e);
  }
};

// check job status applied or not
export const AuthUserViewJob = async (jobId, userId, onError) => {
  // console.log("JOB AND USER ID")
  // console.log(jobId)
  // console.log(userId)
  try {
    const response = await Axios.get(
      `${CheckJobStatus}${jobId}/view/${userId}`,
    );
    // console.log('---------------------------------------------');
    // console.log(response);
    return response.data.success ? response.data.job : onError(response.data);
  } catch (error) {
    console.log('-----FETCH ERROR API-------');
    console.log(error);
    onError(error);
  }
};

export const FavoriteRequest = async (jobId, userId, onError) => {
  try {
    const response = await Axios.get(`${SaveJob}${jobId}/user/${userId}`);
    return response.data.success
      ? SaveIsFav(response.data)
      : onError(response.data);
  } catch (error) {
    console.log('=============');
    console.log(error);
    onError(error);
  }
};
// APPLY JOB FROM JOB NOT CATELOGUE
export const ApplyToJob = async (jobId, userId, onError) => {
  try {
    const response = await Axios.get(`${ApplyJob}${jobId}/user/${userId}`);
    // console.log('API RESPONSE');
    // console.log(response.data);
    return response.data.success
      ? jobApplied(response.data.success)
      : onError(response.data);
  } catch (error) {
    console.log('Error');
    console.log(error);
    onError(error);
  }
};

// SEARCH JOB WITH LOCATION
export const ApplySearch = async (title, location, onError) => {
  try {
    const response = await Axios.post(`${HomePageSearch}`, {
      title: title,
      location: location,
    });
    console.log('SEARCH RESULT API');
    console.log(response.data.jobs);
    return response.data.success ? response.data.jobs : onError(response.data);
  } catch (error) {
    console.log('FETCH ERRRO API');
    console.log(error);
    onError(error);
  }
};

// =========FILTERS

export const FilterForCompanies = async (details, onError) => {
  const data = {
    category: details.category,
    industory: details.industory,
    open_jobs: details.openJobs,
  };
  // console.log('=====BEFORE=======API========COMPAINES=====');
  // console.log(data);
  try {
    const response = await Axios.post(`${CompanyFilter}`, data);
    console.log('--------------------------API REPONSE');
    console.log(response);
    return response.data.success
      ? response.data.companies
      : onError(response.data);
  } catch (e) {
    console.log('ERROR STATE');
    console.log(e);
    onError(e);
  }
};
// ===============================================
export const FilterForjobs = async (details, onError) => {
  const data = {
    category: details.category,
    // qualification: details.qualification,
    experience: details.experience,
    job_type: details.jobType,
    country: details.country,
    gender: details.gender,
  };
  // console.log('==========BEFORE=====API==========JOBS====');
  // console.log(data);
  try {
    const response = await Axios.post(`${JobFilter}`, data);
    console.log('--------------------------API REPONSE');
    console.log(response);
    return response.data.success ? response.data.jobs : onError(response.data);
  } catch (e) {
    console.log('ERROR STATE');
    console.log(e);
    onError(e);
  }
};

export const FilterForTraining = async (details, onError) => {
  const data = {
    category: details.category,
    type: details.type,
    language: details.lang,
    level: details.level,
  };
  // console.log('========BREFORE ======API=======TRAINING');
  // console.log(data);
  try {
    const response = await Axios.post(`${TrainingFilter}`, data);
    // console.log('--------------------------API REPONSE');
    // console.log(response);
    return response.data.success
      ? response.data.trainings
      : onError(response.data);
  } catch (e) {
    console.log('ERROR STATE');
    console.log(e);
    onError(e);
  }
};

export const ViewCompanyDetailAsAuth = async (companyId, id, onError) => {
  try {
    // console.log('--------------------------API REPONSE');
    // console.log(response);
    const response = await Axios.get(`${ViewCompany}${companyId}/view/${id}`);
    return response.data.success
      ? response.data.company
      : onError(response.data);
  } catch (error) {
    console.log('ERROR STATE');
    console.log(error);
    onError(error);
  }
};

export const ViewCompanyDetailAsNormal = async (companyId, err) => {
  try {
    const response = await Axios.get(`${ViewCompany}${companyId}/view/`);
    console.log('--------------------------API REPONSE');
    console.log(response);
    return response.data.success
      ? response.data.company
      : onError(response.data);
  } catch (error) {
    console.log('ERROR STATE');
    console.log(error);
    onError(error);
  }
};

//=========FETCH FAVORITES============
export const FavoriteJob = async (id, onError) => {
  // console.log('=============');
  // console.log(id);
  try {
    const response = await Axios.get(`${FavJobs}${id}`);
    // console.log('--------------------------API REPONSE');
    // console.log(response);

    return response.data.success ? response.data.jobs : onError(response.data);
  } catch (error) {
    console.log('ERROR STATE');
    console.log(error);
    onError(error);
  }
};
export const FavoriteComp = async (id, onError) => {
  try {
    // console.log('--------------------------API REPONSE');
    // console.log(response);
    const response = await Axios.get(`${FavComps}${id}`);
    return response.data.success
      ? response.data.companies
      : onError(response.data);
  } catch (error) {
    console.log('ERROR STATE');
    console.log(error);
    onError(error);
  }
};
export const AppliedJobs = async (id, onError) => {
  try {
    // console.log('--------------------------API REPONSE');
    // console.log(response);
    const response = await Axios.get(`${AppJobs}${id}`);
    return response.data.success ? response.data.jobs : onError(response.data);
  } catch (error) {
    console.log('ERROR STATE');
    console.log(error);
    onError(error);
  }
};

// contact us

export const ContactUs = async (formData, onError) => {
  const userForm = {
    name: formData.name,
    email: formData.email,
    phone: formData.phone,
    company: formData.company,
    message: formData.message,
  };
  try {
    const response = await Axios.post(`${Contact}`, `${userForm}`);
    console.log('--------------------------API REPONSE');
    console.log(response);
    return response.data.success ? response.data : onError(response.data);
  } catch (error) {
    console.log('ERROR STATE');
    console.log(error);
    onError(error);
  }
};

export const AboutUs = async onError => {
  try {
    const response = await Axios.get(`${About}`);
    console.log('--------------------------API REPONSE');
    console.log(response);
    return response.data.success ? response.data : onError(response.data);
  } catch (error) {
    console.log('ERROR STATE');
    console.log(error);
    onError(error);
  }
};

export const GetCompInfo = async onError => {
  try {
    const response = await Axios.get(`${CompamyContact}`);
    return response.data.success
      ? response.data.contact
      : onError(response.data);
  } catch (e) {
    console.log('Error in Contact Info');
    onError(e);
  }
};
