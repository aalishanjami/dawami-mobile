import React from 'react';
import {
  GetAllUserChat,
  GetCompaniesList,
  GetJobsList,
  getAllTrainings,
  AuthUserViewJob,
  FavoriteComp,
} from './getRequests';
import {CustomAlert} from 'components';
import {
  AlertTiming,
  COMPAINES_LIST_STATE,
  LOADING,
  SAVE_JOB_STATUS,
  SET_LOADING,
  TRAININGS,
} from 'redux/constants';
import {NAV} from 'utils';
import {batch} from 'react-redux';
import {colors} from 'styles/theme';

export const loading = setLoading => {
  return {type: SET_LOADING, payload: setLoading};
};

// ===================================>> GET COMPANIES
export const getCompanies = async (action, loading) => {
  const {dispatch, messageRef, companies, navigate} = action;
  // console.log(companies);
  loading(true);
  const result = await GetCompaniesList(err => {
    messageRef.current?.show(
      <CustomAlert text={err?.message || ee} />,
      AlertTiming,
    );
    loading(false);
  });
  result && (dispatch(result), loading(false));
  navigate && navigate(NAV.COMPANY);
  return;
};
// ===================================>> GET JOBS
export const getJobs = async (action, loading) => {
  const {dispatch, messageRef, joblist, navigate} = action;
  loading(true);
  const result = await GetJobsList(err => {
    messageRef.current?.show(
      <CustomAlert text={err?.messsage || err} />,
      AlertTiming,
    );
    loading(false);
  });
  result && dispatch(result), loading(false);
  navigate && navigate(NAV.JOB);
  return;
};
// ===================================>> GET TRAININGS
export const getTrainings = async (action, loading) => {
  const {dispatch, messageRef, trainingslist, navigate} = action;
  loading(true);

  const result = await getAllTrainings(err => {
    messageRef.current?.show(
      <CustomAlert text={err?.messsage || err} />,
      AlertTiming,
    );
    loading(false);
  });
  result && (dispatch(result), loading(false));
  navigate && navigate(NAV.TRAININGS);
};

// ===================================>> GET MESSAGE
export const fetchMessage = async (action, loading) => {
  const {dispatch, messageRef, navigate, id, chats} = action;
  loading(true);
  const result = await GetAllUserChat(id, err => {
    messageRef.current?.show(
      <CustomAlert textColor={colors.primary} text={err} />,
      AlertTiming,
    ),
      loading(false);
  });
  result && (dispatch(result), loading(false));
  navigate && navigate(NAV.MESSAGES);
  return result.payload;
};

// nned to set
export const viewJobs = async (action, loading) => {
  const {dispatch, status, jobId, id, messageRef} = action;
  if (status) {
    loading(true);
    const result = await AuthUserViewJob(jobId, id, err => {
      console.log('error');
      messageRef.current?.show(<CustomAlert text={err?.message || err} />);
      loading(false);
    });
    if (result) {
      console.log('auth user');
      dispatch({type: SAVE_JOB_STATUS, payload: result});
      loading(false);
    }
  } else {
    console.log('non-auth user');
    dispatch({type: SAVE_JOB_STATUS, payload: jobId});
    loading(false);
  }
};

// export const followingCompany = async ({
//   dispatch,
//   id,
//   messageRef,
//   navigation,
// }) => {
//   const {navigate} = navigation;
//   // dispatch(loading(true));
//   const result_favComp = await FavoriteComp(id, err => {
//     messageRef.current?.show(err?.message || err, 2000);
//     // dispatch(loading(false));
//   });
//   result &&
//     dispatch({
//       type: COMPAINES_LIST_STATE,
//       payload: {listOfCompanies: result_favComp},
//     });
//   // dispatch(loading(false));
//   navigate(NAV.COMPANY, {favCompList: true, title: favComp});
// };
