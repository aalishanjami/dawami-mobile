import {
  loginUser,
  registerUser,
  saveUser,
  forgetPassword,
  verifyCode,
  resetPassword,
  logout,
  goWithgoogle,
  goWithFacebook,
  checkUserNameAvailability,
  RemoveRegisteredAccount,
  VerifyUser,
  loggedout,
} from './auth';

import {
  userDetails,
  language,
  GetProfileInfo,
  UpdateUserInfo,
  UpdateContactInfo,
  AddJobTitle,
  AddEducation,
} from './user';

import {
  fetchMessage,
  getCompanies,
  getJobs,
  getTrainings,
  viewJobs,
  loading,
  followingCompany,
} from './SimpleAction';

export {
  fetchMessage,
  getCompanies,
  getJobs,
  getTrainings,
  viewJobs,
  loading,
  followingCompany,
};

import {
  GetCompaniesList,
  GetJobsList,
  companyLogo,
  draftLiked,
  draftDisliked,
  GetAllUserChat,
  getSpecificUserChat,
  SaveChat,
  SendFile,
  getAllTrainings,
  getTrainingDetails,
  AuthUserViewJob,
  FavoriteRequest,
  ApplyToJob,
  ApplySearch,
  FilterForTraining,
  FilterForCompanies,
  FilterForjobs,
  FollowingJob,
  ViewCompanyDetailAsAuth,
  AppliedJobs,
  FavoriteComp,
  FavoriteJob,
  ViewCompanyDetailAsNormal,
  ContactUs,
  AboutUs,
  GetCompInfo,
} from './getRequests';

export {
  loginUser,
  loggedout,
  registerUser,
  forgetPassword,
  saveUser,
  userDetails,
  verifyCode,
  resetPassword,
  logout,
  goWithgoogle,
  goWithFacebook,
  GetCompaniesList,
  checkUserNameAvailability,
  GetJobsList,
  RemoveRegisteredAccount,
  VerifyUser,
  companyLogo,
  draftLiked,
  draftDisliked,
  GetProfileInfo,
  UpdateUserInfo,
  UpdateContactInfo,
  AddJobTitle,
  AddEducation,
  GetAllUserChat,
  getSpecificUserChat,
  SaveChat,
  SendFile,
  getAllTrainings,
  getTrainingDetails,
  AuthUserViewJob,
  FavoriteRequest,
  ApplyToJob,
  ApplySearch,
  FilterForTraining,
  FilterForCompanies,
  FilterForjobs,
  FollowingJob,
  ViewCompanyDetailAsAuth,
  AppliedJobs,
  FavoriteJob,
  FavoriteComp,
  ViewCompanyDetailAsNormal,
  ContactUs,
  AboutUs,
  GetCompInfo,
};
