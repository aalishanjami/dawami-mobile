import {
  USER_LOGIN,
  USER_LOGOUT,
  REGISTER_USER,
  FORGET_TOKEN,
  REMOVE_FORGET_TOKEN,
  ADD_USER_INFO,
  LOADING,
  SET_LOADING,
} from '../constants';

const initialState = {
  isloading: false,
  status: false,
  reset_confirmation_code: null,
  requestedUserDetails: null,
  userBasicProfile:null,
};

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LOADING:
      return {...state, isloading: action.payload};
    case USER_LOGIN:
      return {...state, status: action.payload.success};
    case ADD_USER_INFO:
      return {...state, userBasicProfile: action.payload};
    case USER_LOGOUT:
      return {
        ...state,
        status: action.payload.success,
        userBasicProfile: null,
      };
    case REGISTER_USER:
      return {...state, status: action.payload.success};
    case FORGET_TOKEN:
      return {
        ...state,
        reset_confirmation_code: action.payload.code.confirmation_code,
        requestedUserDetails: action.payload.code,
      };
    case REMOVE_FORGET_TOKEN:
      return {...state, reset_confirmation_code: null};
    default:
      return state;
  }
};

export default authReducer;
