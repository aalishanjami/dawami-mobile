import {
  COMPAINES_LIST_STATE,
  JOB_LIST_STATE,
  USER_DRAFTS,
  CURRENT_VIEW_PROG,
  ADDING_FEEDBACK,
  ALL_CHATS,
  SAVE_JOB_STATUS,
  IS_FAV,
  IS_APPLIED,
  USER_LOGOUT,
  SEARCH_RESULTS,
  FOLLOW,
  TRAININGS,
  CURRENT_COMP,
  ADD_FAV_JOB,
  ADD_FAV_COMP,
  APPLIED_JOB,
} from 'redux/constants';

const initialState = {
  all_chats: [],
  listOfCompanies: [],
  Trainings: [],
  listOfJobs: [],
  currentCompStatus:null,
};

export const getRequestReducer = (state = initialState, action) => {
  switch (action.type) {
    case COMPAINES_LIST_STATE:
      return {...state, listOfCompanies: action.payload.listOfCompanies};
    case JOB_LIST_STATE:
      return {...state, listOfJobs: action.payload.listOfJobs};
    case USER_DRAFTS:
      return {...state, allDrafts: action.payload};
    case CURRENT_VIEW_PROG:
      return {...state, currentViewProg: action.payload};
    case ADDING_FEEDBACK:
      return {...state.currentViewProg, feedback: action.payload.feedback};
    case ALL_CHATS:
      return {...state, all_chats: action.payload};
    case SAVE_JOB_STATUS:
      return {...state, currentJobStatus: action.payload};
    case TRAININGS:
      return {...state, Trainings: action.payload};
    case IS_FAV: {
      return {
        ...state,
        currentJobStatus: {
          ...state.currentJobStatus,
          saved_status: action.payload.current_status,
        },
      };
    }
    case CURRENT_COMP:
      return {...state, currentCompStatus: action.payload};
    case FOLLOW: {
      return {
        ...state,
        currentCompStatus: {
          ...state.currentCompStatus,
          follow_status: action.payload,
        },
      };
    }
    case IS_APPLIED: {
      return {
        ...state,
        currentJobStatus: {...state.currentJobStatus, applied_status: 1},
      };
    }
    case USER_LOGOUT:
      return {...state, all_chats: [], currentJobStatus: null};
    case SEARCH_RESULTS:
      return {...state, searchResult: action.payload};

    case ADD_FAV_JOB:
      return {...state, favJobs: action.payload};
    case ADD_FAV_COMP:
      return {...state, favComps: action.payload};
    case APPLIED_JOB:
      return {...state, appliedJobs: action.payload};
    default:
      return state;
  }
};
