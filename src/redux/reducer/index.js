import {authReducer} from './auth'
import {userInfoReducer} from './user'
import {getRequestReducer} from './getRequests'

export {authReducer,userInfoReducer,getRequestReducer}