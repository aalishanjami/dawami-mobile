import {
  ADD_USER_INFO,
  REMOVE_USER_INFO,
  LANG_EN,
  SET_LANGUAGE,
  ADD_PROFILE_INFO,
  UPDATE_PROFILE_INFO,
  ADD_PREFERRED_JOB,
  ADD_EDUCATION,
  TEMP_LANG,
} from 'redux/constants';

const initialState = {
  currentLN: LANG_EN,
  userData: {
    profile: {
      experiences: [],
      pereferred_job: {},
      educations: [],
      skills: [],
      hobbies: [],
      references: [],
      languages: [],
      certificates: [],
    },
    cities: [],
    countries: [],
  },
};

export const userInfoReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LANGUAGE:
      return {...state.userData, currentLN: action.payload};
     
    case UPDATE_PROFILE_INFO:
      return {
        ...state,
        userData: {
          ...state.userData,
          profile: {
            ...state.userData.profile,
            ...action.payload,
          },
        },
      };
    case REMOVE_USER_INFO:
      return {...state, userInfo: action.payload};
    case ADD_PROFILE_INFO:
      return {...state, ...state.userInfo, userData: action.payload};
    case ADD_PREFERRED_JOB:
      return {
        ...state,
        userData: {
          ...state.userData,
          profile: {
            ...state.userData.profile,
            pereferred_job: action.payload,
          },
        },
      };
    case ADD_EDUCATION:
      return {
        ...state,
        userData: {
          ...state.userData,
          profile: {
            ...state.userData.profile,
            educations: [...state.userData.profile.educations, action.payload],
          },
        },
      };

    default:
      return state;
  }
};
