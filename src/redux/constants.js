import {WORDS} from 'utils';

export const AlertTiming = 4000

export const LANGUAGE_SELECTED = 'SELECTED_LANGUAGE';
export const TEMP_LANG = "TEMPORARY_SELECTION"
// export const SPLASH_LANGUAGE = 'SPLASH_LANGUAGE';
export const USER_LOGIN = 'USER_LOGIN';
export const REGISTER_USER = 'REGISTER_USER';
export const ADD_USER_INFO = 'ADD_USER_INFO';
export const REMOVE_USER_INFO = 'REMOVE_ADD_USER_INFO';
export const USER_LOGOUT = 'USER_LOGOUT';
export const FORGET_TOKEN = 'FORGET_TOKEN';
export const REMOVE_FORGET_TOKEN = 'REMOVE_FORGET_TOKEN';

// GET LIST OF
export const GET_COMPANIES = 'GET_COMPANIES';
export const GET_JOBS = 'GET_JOBS';

// SAVE TO IT IN STATE
export const COMPAINES_LIST_STATE = 'COMPAINES_LIST_STATE';
export const JOB_LIST_STATE = 'JOB_LIST_STATE';

//Language control
export const LANG_EN = 'AR';
export const LANG_AR = 'EN';
export const SET_LANGUAGE = 'SET_LANGUAGE';
// export const SET_AR = 'SET_AR'

export const USER_DRAFTS = 'USER_DRAFTS';
export const CURRENT_VIEW_PROG = 'CURRENT_VIEW_PROGRESS_DETAILS';
export const ADDING_FEEDBACK = 'ADDING_FEEDBACK';
export const ADD_PROFILE_INFO = 'ADD_PROFILE_INFO';
export const UPDATE_PROFILE_INFO = 'UPDATE_PROFILE_INFO';
export const ADD_PREFERRED_JOB = 'ADD_PREFERRED_JOB';
export const ADD_EDUCATION = 'ADD_EDUCATION';

// Messages
export const ALL_CHATS = 'ALL_CHATS';
export const UPDATE_CHAT = "UPDATE_CHAT"

export const SAVE_JOB_STATUS = 'SAVE_JOB_STATUS';
export const IS_FAV = 'SAVE_AS_FAVORITE';

export const IS_APPLIED = 'IS_APPLIED';

export const SEARCH_RESULTS = 'SEARCH_RESULTS';

export const FOLLOW = 'FOLLOW';

export const TRAININGS = 'TRAININGS';

export const CURRENT_COMP = 'CURRENT_VIEWING_COMPANY_DETAILS';
export const CURRENT_COMP_UPDATE = 'CURRENT_VIEWING_COMP_UPDATE';

export const ADD_FAV_JOB = 'ADD_FAV_JOBS';
export const ADD_FAV_COMP = 'ADD_FAV_COMP';
export const APPLIED_JOB = 'ADD_APPLIED_JOBs';
export const SET_LOADING = "SET_LOADING"

