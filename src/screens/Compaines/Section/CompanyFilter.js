import React, {useReducer, useState} from 'react';
import {Block, Text, Button, FilterOptions} from 'components';
import {sizes, colors} from 'styles/theme';
import {
  Image,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import * as icons from 'assets/icons';
import {FilterForCompanies} from 'redux/actions';
import {useSelector} from 'react-redux';
import {LANG_AR} from 'redux/constants';
import {WORDS} from 'utils';

const CompanyFilter = props => {
  const {closePopup, onSearch, resetFilters} = props;
  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;
  // -------------------------------------------
  const SELECT = AR ? WORDS.PleaseSelect_AR : WORDS.PleaseSelect;
  const CIVIL = AR ? WORDS.Civil_AR : WORDS.Civil;
  const OPERATION = AR ? WORDS.Operation_AR : WORDS.Operation;
  const OIL = AR ? WORDS.Oil_AR : WORDS.Oil;
  const MEDIA = AR ? WORDS.MEDIA_AR : WORDS.MEDIA;
  const BANKING = AR ? WORDS.Banking_AR : WORDS.Banking;
  const MEDICAL = AR ? WORDS.Medical_AR : WORDS.Medical;
  const IT = AR ? WORDS.InfomationTec_AR : WORDS.InfomationTec;
  const EE = AR ? WORDS.ElecEng_AR : WORDS.ElecEng;
  const SAFETY = AR ? WORDS.Safety_AR : WORDS.Safety;
  const CONSTRUCTION = AR ? WORDS.construct_AR : WORDS.construct;
  const D_JOBS = AR ? WORDS.defaultJob_AR : WORDS.defaultJob;
  const GOVT = AR ? WORDS.Govt_AR : WORDS.Govt;
  const S_GOVT = AR ? WORDS.SemiGovt_AR : WORDS.SemiGovt;
  const PRIVATE = AR ? WORDS.PrivateJob_AR : WORDS.PrivateJob;
  const OPENJOBS = AR ? WORDS.OpenJobsOpt_AR : WORDS.OpenJobsOpt;
  const RESET = AR ? WORDS.Reset_AR : WORDS.Reset;
  const SEARCH = AR ? WORDS.SEARCH_AR : WORDS.SEARCH;
  // -------------------------------------------

  // ========STATE VARIANTS STARTED
  const CATE = AR ? WORDS.CATE_AR : WORDS.CATE;
  const INDUS = AR ? WORDS.INDUSTRY_AR : WORDS.INDUSTRY;
  // ========STATE VARIANTS ENDED

  const dataToSend = {
    industry: [
      {name: SELECT, value: 0},
      {name: CIVIL, value: 1},
      {name: OIL, value: 2},
      {name: MEDIA, value: 3},
      {name: BANKING, value: 4},
      {name: MEDICAL, value: 5},
      {name: IT, value: 6},
      {name: EE, value: 7},
      {name: SAFETY, value: 8},
      {name: CONSTRUCTION, value: 9},
    ],
    category: [
      {name: SELECT, value: 0},
      {name: D_JOBS, value: 1},
      {name: GOVT, value: 2},
      {name: S_GOVT, value: 3},
      {name: PRIVATE, value: 4},
    ],
    open: [
      // {name: 'Please Select', value: 0},
      {name: SELECT, value: 0},
      {name: OPENJOBS, value: 1},
    ],
  };

  const defaultData = {
    industry: {
      defaultSelection: {name: SELECT, value: 0},
      civil: {name: CIVIL, value: 1},
      oil: {name: OIL, value: 2},
      media: {name: MEDIA, value: 3},
      bank: {name: BANKING, value: 4},
      medical: {name: MEDICAL, value: 5},
      it: {name: IT, value: 6},
      ee: {name: EE, value: 7},
      safety: {name: SAFETY, value: 8},
      construction: {name: CONSTRUCTION, value: 9},
    },
    category: {
      defaultSelection: {name: SELECT, value: 0},
      default: {name: D_JOBS, value: 1},
      govt: {name: GOVT, value: 2},
      sGovt: {name: S_GOVT, value: 3},
      private: {name: PRIVATE, value: 4},
    },
    openJobs: {
      defaultSelection: {name: SELECT, value: 0},
      openJobs: {name: OPENJOBS, value: 1},
    },
  };

  const initialState = {
    industory: defaultData.industry.defaultSelection.value,
    category: defaultData.category.defaultSelection.value,
    openJobs: defaultData.openJobs.defaultSelection.value,
  };

  const reducer = (state, action) => {
    switch (action.type) {
      case RESET: {
        return {...initialState};
      }
      case CATE:
        return {...state, category: action.payload};
      case INDUS:
        return {...state, industory: action.payload};
      case OPENJOBS:
        return {...state, openJobs: action.payload};
      default:
        return state;
    }
  };
  const [state, localDispatch] = useReducer(reducer, initialState);

  // const search = async () => {
  //   // console.log(state);

  //   const result = await FilterForCompanies(state, err => {
  //     console.log(err);
  //   });
  //   console.log('GETTING AND SENDING RESULT');
  //   console.log(result);
  //   result && onSearch(result);
  //   closePopup();
  // };
  const [showLoading, setShowLoading] = useState(false);
  const search = async () => {
    setShowLoading(true);
    const result = await FilterForCompanies(state, err => {
      console.log(err);
      setShowLoading(false);
    });
    if (result) {
      console.log(result);
      onSearch(result);
      setShowLoading(false);
      closePopup();
    }
  };

  console.log('================STATE FILTER COMPANIES=========');
  console.log(state);

  const cate = AR ? WORDS.CATE_AR : WORDS.CATE;
  const ind = AR ? WORDS.INDUSTRY_AR : WORDS.INDUSTRY;
  const jobs = AR ? WORDS.OpenJobs_ar : WORDS.OpenJobs;

  return (
    <Block style={styles.mainCon}>
      <Block>
        {/* ---------------------------------------------------------------------------- */}
        {/* category */}
        <FilterOptions
          title={cate}
          values={dataToSend.category}
          selectedValue={state.category}
          changedItem={e => localDispatch({type: CATE, payload: e})}
        />
        <FilterOptions
          title={ind}
          values={dataToSend.industry}
          selectedValue={state.industory}
          changedItem={e => localDispatch({type: INDUS, payload: e})}
        />
        <FilterOptions
          title={jobs}
          values={dataToSend.open}
          selectedValue={state.openJobs}
          changedItem={e => localDispatch({type: OPENJOBS, payload: e})}
        />

        {/* ---------------------------------------------------------------------------- */}
      </Block>
      {!showLoading ? (
        <Block row middle center style={styles.btnCon}>
          <Button
            onPress={search}
            center
            middle
            style={{...styles.btn, width: '62%'}}>
            <Text color={colors.primary}>{SEARCH}</Text>
          </Button>
          <Button
            onPress={() => localDispatch({type: RESET})}
            center
            middle
            style={{
              ...styles.btn,
              borderWidth: 0.3,
              borderStyle: 'dashed',
              width: sizes.getWidth('25%'),
              backgroundColor: 'transparent',
            }}>
            {/* <Image
              source={icons.roundClose}
              style={{
                resizeMode: 'contain',
                width: sizes.getWidth(20),
                height: sizes.getHeight(13),
                tintColor: colors.gray,
              }}
            /> */}
            <Text color={colors.red}>{RESET}</Text>
          </Button>
        </Block>
      ) : (
        <Block row middle center style={styles.btnCon}>
          <ActivityIndicator color={colors.customRed} size="large" />
        </Block>
      )}
      <Button
        onPress={closePopup}
        center
        middle
        style={{
          ...styles.btn,
          // borderWidth: 1,
          backgroundColor: colors.primary,
          borderStyle: 'dashed',
          width: sizes.screenSize * 0.04,
          height: sizes.screenSize * 0.04,
          // backgroundColor: 'transparent',
          position: 'absolute',
          right: 0,
          right: !AR ? -sizes.getWidth(4) : null,
          left: AR ? -sizes.getWidth(4) : null,
          top: -sizes.getHeight(5),
          zIndex: 50,
        }}>
        <Image
          source={icons.roundClose}
          style={{
            resizeMode: 'contain',
            // flex: 1,
            // borderWidth: 1,
            width: sizes.getWidth(15),
            height: sizes.getHeight(13),
            tintColor: colors.spaceGreen,
          }}
        />
      </Button>
    </Block>
  );
};

export {CompanyFilter};

const styles = StyleSheet.create({
  mainCon: {
    flex: 0,
    height: sizes.getHeight(50),
    // borderWidth: 2,
    // backgroundColor: colors.customRed,
  },
  // textField: {
  //   flex: 0,
  //   height: sizes.getHeight(10),
  //   // borderWidth: 1,
  //   justifyContent: 'center',
  //   borderRadius: sizes.getWidth(1),
  //   marginVertical: sizes.getWidth(0.5),

  // },
  btnCon: {
    // borderWidth: 1,
    flex: 0,
    height: sizes.getHeight(8),
  },
  btn: {
    // width: '40%',
    height: '100%',
    marginHorizontal: sizes.getWidth(2),
    backgroundColor: colors.customRed,
    borderRadius: sizes.getWidth(1),
  },
});
