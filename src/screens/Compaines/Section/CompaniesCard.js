import React from 'react';
import {StyleSheet, Image, Platform, Dimensions} from 'react-native';
import {Button, Block, Text} from 'components';
import {sizes, colors} from 'styles/theme';
import * as icons from 'assets/icons';
import {companyLogo} from 'redux/apiConstants';
import {useSelector} from 'react-redux';
import {LANG_EN, LANG_AR} from 'redux/constants';
import {theme} from 'styles';

const CompaniesCard = props => {
  const {onSelected, item} = props;
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width === 375;

  // console.log('=========Companies Card============');
  // console.log(item);
  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;

  const jobCount = item?.jobs?.length;

  //========================================================
  const companyName = AR ? item?.name_ar : item?.name_en;
  let post = jobCount > 1 ? 'Posts' : 'Post';
  const jobPosted = AR ? `${jobCount} تم نشر الوظيفة` : `${jobCount} ${post}`;
  //========================================================
  return (
    <Button
      onPress={() => onSelected(item)}
      // onPress={() => navigation.navigate('AboutCompany')}
      center
      style={{
        height: sizes.getHeight(iphone6s ? 12 : 10),
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: colors.gray,
      }}>
      <Block center middle flex={false} width={sizes.getWidth('20')}>
        <Image
          source={{
            uri: companyLogo + item?.logo,
          }}
          style={styles.img}
        />
      </Block>
      <Block
        style={{
          // borderWidth: 1,
          height: '100%',
          paddingTop: sizes.getHeight(2),
        }}
        flex={false}
        width={sizes.getWidth('65')}>
        <Text
          style={{
            ...styles.companyName,
            fontSize: sizes.customFont(iphone6s ? (AR ? 8 : 12) : 14),
          }}>
          {companyName}
        </Text>
        <Block
          style={{
            ...styles.detailBox,
            width: sizes.getWidth(AR ? 18 : 13),
            textAlign: AR ? 'right' : 'left',
            // borderWidth: 1,
            minHeight: sizes.getHeight(3),
          }}
          padding={
            AR ? [0, sizes.getWidth(2), 0, 0] : [0, 0, 0, sizes.getWidth(1)]
          }>
          <Text
            h3
            color={colors.primary}
            style={{
              // borderWidth: 1,
              textAlign: AR ? 'right' : 'left',
              lineHeight: 18,
              fontSize: sizes.customFont(iphone6s ? 7 : AR ? 8 : 14),
            }}>
            {jobPosted}
          </Text>
        </Block>
      </Block>
      <Image
        source={icons.forward_arrow}
        style={{
          width: sizes.getWidth(1.5),
          resizeMode: 'contain',
        }}
      />
    </Button>
  );
};

export {CompaniesCard};

const styles = StyleSheet.create({
  detailBox: {
    marginLeft: sizes.getWidth(0.7),
    justifyContent: 'center',
    // alignItems: 'center',
    flex: 0,
    // width: sizes.getWidth(20),
    height: sizes.getWidth(4),
    paddingHorizontal: sizes.getWidth2,
    backgroundColor: '#BD9571',

    // borderRadius: sizes.withScreen(0.004),
  },
  img: {
    resizeMode: 'contain',
    width: sizes.getWidth(18),
    height: sizes.getHeight(7),
  },
  companyName: {
    fontSize: sizes.customFont(13.5),
    // fontWeight: 'bold',
    textAlign: 'left',
    lineHeight: 28,
    ...theme.fonts.GMR,
  },
});
