import React, {useState, useEffect, useRef} from 'react';
import {
  Block,
  Text,
  Searchbar,
  Button,
  CustomHeader,
  FilterOptions,
  FilterPopup,
  ActivitySign,
  CustomAlert,
} from 'components';
import {FlatList, Image, RefreshControl, Platform} from 'react-native';
import {sizes, colors} from 'styles/theme';
import {useSelector, useDispatch} from 'react-redux';
import {companyLogo} from 'redux/apiConstants';
import {CompanyFilter} from './Section/CompanyFilter';
import {CompaniesCard} from './Section/CompaniesCard';
import Toast from 'react-native-easy-toast';
import {
  CURRENT_COMP,
  COMPAINES_LIST_STATE,
  LANG_AR,
  LANG_EN,
} from 'redux/constants';
import {
  GetCompaniesList,
  ViewCompanyDetailAsAuth,
  ViewCompanyDetailAsNormal,
  FavoriteComp,
} from 'redux/actions';
import {NAV, sortByProperty, WORDS} from 'utils';
import {SafeAreaView} from 'react-native-safe-area-context';
import * as lang from 'utils/LanguageHandler'
import { language } from 'redux/actions/user';
import { company } from 'assets/icons';

const CompainesList = props => {
  const ios = Platform.OS === 'ios';
  const {navigation, route} = props;
  const alertTiming = 3500;
  const listOfCompanies = useSelector(
    state => state?.getLists?.listOfCompanies,
  );

  const {status} = useSelector(state => state.auth);
  const id = useSelector(state => state.auth?.userBasicProfile?.id);
  const dispatch = useDispatch();
  const [showFilter, setShowFilter] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const goToDetails = async companyDetail => {
    // console.log(companyDetail);
    // console.log(id);
    // console.log(sender)
    // setIsLoading(true);
    if (status) {
      const result = await ViewCompanyDetailAsAuth(
        companyDetail.id,
        id,
        err => {
          console.log('err');
          console.log(err);
          messageRef.current?.show(err?.message || err, 2000);
          setIsLoading(fale);
        },
      );
      result && (
      dispatch({
        type: CURRENT_COMP,
        payload: result,
      }),
      setIsLoading(false),
      navigation.navigate(NAV.COMPANY_DETAILS))
    } 
    else {
      console.log(companyDetail);
      const result = await ViewCompanyDetailAsNormal(companyDetail.id, err => {
        // console.log('err');
        // console.log(err);
        messageRef.current?.show(err?.message||err, 2000)
        setIsLoading(false);
      });
      // console.log('=====================================');
      // console.log('**************NON AUTH USER********************');
      // console.log(result);
      // console.log('=====================================');
      result &&
        (dispatch({
          type: CURRENT_COMP,
          payload: result,
        }),
        setIsLoading(false),
        navigation.navigate(NAV.COMPANY_DETAILS));
    }
  };

  useEffect(() => {
    // setCompList(listOfCompanies);
    // console.log('Mount');
    // console.log(compList);
    // console.log(route.params?.favCompList);
    // console.log('Mounting Compaines Screen');
    setCompList(listOfCompanies);
    return () => setCompList(listOfCompanies);
  }, [listOfCompanies]);

  const [compList, setCompList] = useState(listOfCompanies);
  const searchHandler = word => {
    var filteredList = [];
    listOfCompanies.forEach(element => {
      var lowerTitle = element.name_en.toLowerCase();
      var lowerWord = word.toLowerCase();
      if (lowerTitle.startsWith(lowerWord)) {
        filteredList.push(element);
      }
      // console.log(filteredList);
      setCompList(filteredList);
    });
  };
  // =============FILTEReD DATA ----------------
  const search = filteredCompaniesList => {
    // console.log('--------FILTERED LIST');
    // console.log(filteredCompaniesList);
    setCompList(filteredCompaniesList);
  };
  const messageRef = useRef();
  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = async () => {
    setIsLoading(true);
    if (!route.params?.favCompList) {
      const result = await GetCompaniesList(err => {
        return (
          messageRef.current?.show(<CustomAlert text={'err'} />, alertTiming),
          setIsLoading(false)
        );
      });
      result && dispatch(result);
    } else {
      const result_favComp = await FavoriteComp(id, err => {
        console.log(err);
        setIsLoading(false);
      });
      console.log(result_favComp);
      dispatch({
        type: COMPAINES_LIST_STATE,
        payload: {listOfCompanies: result_favComp},
      });
    }
    setIsLoading(false);
  };

  // console.log('==============');
  // console.log(compList);
  const {currentLN} = useSelector(state => state.userInfo);

  const company_sortBy = compList?.sort(sortByProperty('created_at'));
  const AR = currentLN === LANG_AR;
  let searchPL = AR
    ? WORDS.searchPlaceholderInComp_ar
    : WORDS.searchPlaceholderInComp;

  return (
    <>
      <SafeAreaView
        style={{flex: 0}}
        style={{backgroundColor: colors.spaceGreen}}
      />
      <Block>
        <CustomHeader navigation={navigation} />
        <Searchbar
          // bg={colors.spaceGreen}
          onIconPress={() => setShowFilter(true)}
          onChangeText={searchHandler}
          title={AR?lang.COMPANY_AR:lang.COMPANY}
          textStyle={{textAlign: 'left'}}
          placeholder={searchPL}
        />
        {compList && compList.length ? (
          <Block padding={[0, sizes.getWidth(5)]}>
            <FlatList
              refreshControl={
                <RefreshControl onRefresh={onRefresh} refreshing={refreshing} />
              }
              showsVerticalScrollIndicator={false}
              keyExtractor={(item, index) => {
                return index.toString();
              }}
              // data={listOfCompanies}
              data={company_sortBy}
              renderItem={({index, item}) => {
                // console.log('====index');
                // console.log(index);
                return (
                  <CompaniesCard item={item} onSelected={e => goToDetails(e)} />
                );
              }}
            />
          </Block>
        ) : (
          <Block middle center>
            <Text  color={colors.gray}>
              Nothing Found in Companies.
            </Text>
          </Block>
        )}
        {showFilter && (
          <FilterPopup>
            <CompanyFilter
              onSearch={e => search(e)}
              closePopup={() => setShowFilter(false)}
            />
          </FilterPopup>
        )}
        {isLoading && <ActivitySign />}
        <Toast
          ref={messageRef}
          style={{
            backgroundColor: colors.customRed,
            width: sizes.getWidth(100),
            borderRadius: 2,
          }}
          positionValue={sizes.getDimensions.height}
          fadeInDuration={200}
          fadeOutDuration={100}
          opacity={0.8}
        />
      </Block>
    </>
  );
};

export default CompainesList;
