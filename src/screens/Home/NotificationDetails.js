export const chatDetails = (data, sender) => {
  return {
    title: sender.username,
    message: `Messages: ${data.message}`,
    subText: 'A New Message has arrived',
    data: {data, sender: sender},
    type: 'CHAT',
  };
};




export const newJob = (data, sender) => {
  return {
    title: `New Job Posted by ${sender.name_en}`,
    message: data.description_en,
    subText: data.country_id,
    data: {data, sender: sender},
    type: 'JOB',
  };
};






// ===============================================================

  const enumStatus = {
    Pending: '0',
    Assigned: '1',
    InProgress: '2',
    WaitingForFeedBack: '3',
    Completed: '4',
    Cancelled: '5',
    Closed: '6',
    DraftAccepted: '7',
  };

  const getStatus = status => {
    let currentStatus = '';
    switch (status) {
      case '0':
        return (currentStatus = 'Pending'); //0
      case enumStatus.Assigned:
        return (currentStatus = 'Assiged'); // 1
      case enumStatus.InProgress:
        return (currentStatus = 'In-Progress'); //2
      case enumStatus.WaitingForFeedBack:
        return (currentStatus = 'Waiting For FeedBack'); //3
      case enumStatus.Completed:
        return (currentStatus = 'Completed'); //4
      case enumStatus.Cancelled:
        return (currentStatus = 'Cancelled'); //5
      case enumStatus.Closed:
        return (currentStatus = 'Closed'); //6
      case enumStatus.DraftAccepted:
        return (currentStatus = 'Draft Accepted'); //7
      default:
        return (currentStatus = 'Update On Request'); //8
    }
  };


export const cvStatus = (data, sender) => {
  return {
    title: `Status: ${getStatus(data.status)}`,
    message: 'Update on submitted Request',
    subText: `From: ${sender.email}`,
    data: {data, sender: sender},
    type: 'CV',
  };
};
