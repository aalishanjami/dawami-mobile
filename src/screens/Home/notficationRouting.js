import {SAVE_JOB_STATUS, CURRENT_COMP} from 'redux/constants';
import {NAV} from 'utils';
import {ViewCompanyDetailAsAuth} from 'redux/actions';

// CHAT ROUTING

export const ChatRouting = (userData, details) => {
  const {data, sender} = details;
  const {navigation, userInfo, dispatch} = userData;
  console.log('Chat Routing perform here ');
  console.log(data);
  console.log(sender);
  // navigation.navigate(NAV.MESSAGES, {recall:true})
  navigation.navigate(NAV.MESSAGES, {
    me: data.reciever_id,
    other: data.sender_id,
    byNofitication: true,
  });
};

//  CV BUILDER ROUTING

export const CVRouting = (userData, details) => {
  const {data, sender} = details;
  const {navigation, userInfo, dispatch} = userData;
  console.log('CV Routing perform here ');
  console.log(data);
  console.log(sender);
  navigation.navigate(NAV.CV)
};

// JOB ROUTING

export const JobRouting = async (userData, details) => {
  const {data, sender} = details;
  const {navigation, userInfo, dispatch, setIsLoading} = userData;
  console.log('job Routing perform here ');
  console.log(data);
  console.log(sender);
  setIsLoading(true);
  const result = await ViewCompanyDetailAsAuth(sender.id, userInfo.id, err => {
    console.log('err');
    console.log(err);
    setIsLoading(fale);
  });
  dispatch({
    type: CURRENT_COMP,
    payload: result,
  });
  setIsLoading(false);
  navigation.navigate(NAV.COMPANY_DETAILS);
};

// ----------------------------------------------------------------
// foregrounn maange

export const foregroundMsgOpenApp = () => {
  let data = JSON.parse(remoteMessage.data);
  let sender = JSON.parse(remoteMessage?.sender);

  //chat
  if (data.message) {
    PushNotification.localNotification(chatDetails(data, sender));
  }
  // cv builder statsu changed
  else if (data.staff && data.status) {
    PushNotification.localNotification(cvStatus(data, sender));
  }
  // new job posted
  else if (data.company && (data.title_en || data.title_ar)) {
    PushNotification.localNotification(newJob(data, sender));
  } else {
    console.log('Default');
    console.log(data);
    console.log(sender);
  }
};





// 
export const routingManager  = (value,userData) => {
    // console.log("From Routing")
    // console.log(value);
    switch (value.type) {
      case 'CHAT':
        // console.log("ROUTING MANAGER IS CALLED")
        // console.log(value.data);
        // console.log(userData);
        // console.log('---------------------')
        return ChatRouting(userData, value.data);
      case 'JOB':
        return JobRouting(userData, value.data);
      case 'CV':
        return CVRouting(userData, value.data);
      default:
        return console.log('Do Nothing No Data In Notification');
    }
}
