import {Button, Text} from 'components';
import React from 'react';
import {StyleSheet} from 'react-native';
import {Image} from 'react-native-animatable';
import {useSelector} from 'react-redux';
import {theme} from 'styles';
import {colors, iphone6s, sizes} from 'styles/theme';
import {LANG_AR} from 'utils/LanguageHandler';

const {withScreen, getWidth, getHeight,screenSize} = sizes;
const {spaceGreen} = colors;
export const Blocks = props => {
  const {item, goTo} = props;
  const currentLN = useSelector(state => state.userInfo.currentLN);
  const status = useSelector(state => state.auth.status);
  const AR = currentLN === LANG_AR;
  return (
    <Button
      onPress={() => goTo(item.route)}
      style={{
        ...styles.blocks,
        backgroundColor: item.color,
        height:status ? screenSize * 0.155 : screenSize * 0.117
      }}>
      <Image source={item.image} style={styles.blockImg} />
      <Text
        style={{
          ...styles.title,
          fontSize: iphone6s ? (AR ? 8 : 12) : 18,
        }}>
        {item.name}
      </Text>
    </Button>
  );
};

const styles = StyleSheet.create({
  blocks: {
    borderRadius: withScreen(0.003),
    // borderWidth: 1,
    width: getWidth(46),
    marginHorizontal: getWidth(1),
    justifyContent: 'center',
    alignItems: 'center',
  },
  blockImg: {
    tintColor: colors.primary,
    resizeMode: 'contain',
    flex: 0.25,
    marginBottom: getHeight(1),
  },
  title: {
    // fontSize: sizes.customFont(),
    color: colors.primary,
    ...theme.fonts.GMSB,
    // fontFamily: 'Arial',
  },
});
