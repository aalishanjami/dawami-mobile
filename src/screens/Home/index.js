import React, {useEffect, useState, useRef} from 'react';
import {
  Block,
  Text,
  CustomHeader,
  CustomInput,
  SquareButton,
  Button,
  ActivitySign,
  CustomAlert,
  Searchbar,
  Loader,
  TextField,
} from 'components';
import {colors, iphone6s, sizes} from 'styles/theme';
import * as icons from 'assets/icons';
import {
  Image,
  FlatList,
  ScrollView,
  SafeAreaView,
  Modal,
  Platform,
  StatusBar,
  StyleSheet,
  Alert,
  Dimensions,
  AppState,
  Linking,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {
  GetCompaniesList,
  GetJobsList,
  fetchMessage,
  getCompanies,
  getJobs,
  getTrainings,
  loading,
} from 'redux/actions';
import {getAllTrainings, ApplySearch} from 'redux/actions/getRequests';
import Toast from 'react-native-easy-toast';
import messaging, {
  AuthorizationStatus,
  firebase,
} from '@react-native-firebase/messaging';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import PushNotification from 'react-native-push-notification';
import DeviceInfo, {isEmulator} from 'react-native-device-info';
import {
  SEARCH_RESULTS,
  SAVE_JOB_STATUS,
  TRAININGS,
  JOB_LIST_STATE,
  LANG_AR,
  LANG_EN,
  SET_LANGUAGE,
  SET_LOADING,
  AlertTiming,
} from 'redux/constants';
import {WORDS, NAV} from 'utils';
import {theme} from 'styles';
import {chatDetails, cvStatus, newJob} from './NotificationDetails';
import {
  ChatRouting,
  CVRouting,
  JobRouting,
  foregroundMsgOpenApp,
  routingManager,
} from './notficationRouting';
import {Notifications} from 'react-native-notifications';
import {Blocks} from './MainButtons';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {__} from 'utils/LanguageHandler';
// import { COMPANY } from 'utils/RouteConstants';
const {primary, red, customRed, spaceGreen, newGray} = colors;
const {
  header,
  h4,
  h3,
  h2,
  h1,
  customFont,
  getWidth,
  getHeight,
  withScreen,
  screenSize,
} = sizes;
const HomePage = ({route, navigation}) => {
  const {navigate} = navigation;
  const ios = Platform.OS === 'ios';
  const auth = useSelector(state => state.auth);
  const searchResult = useSelector(state => state.getLists?.searchResult);
  const {currentLN} = useSelector(state => state.userInfo);
  const userInfo = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;

  const generateNotfication = (remoteMessage, isForeground) => {
    let data = JSON.parse(remoteMessage.data);
    let sender = JSON.parse(remoteMessage?.sender);
    // chat
    if (data.message) {
      let dataToPresent = chatDetails(data, sender);
      return isForeground
        ? ios
          ? (PushNotification.configure({
              onNotification: notification => {
                pushSettingsIOS(remoteMessage);
              },
            }),
            PushNotification.localNotification(dataToPresent))
          : PushNotification.localNotification(dataToPresent)
        : chatDetails(data, sender);
    }
    // cv builder statsu changed
    else if (data.staff && data.status) {
      let dataToPresent = cvStatus(data, sender);
      return ios
        ? (PushNotification.configure({
            onNotification: notification => {
              pushSettingsIOS(remoteMessage);
            },
          }),
          PushNotification.localNotification(dataToPresent))
        : PushNotification.localNotification(dataToPresent);
    }
    // new job posted
    else if (data.company && (data.title_en || data.title_ar)) {
      let dataToPresent = newJob(data, sender);
      return isForeground
        ? ios
          ? (PushNotification.configure({
              onNotification: notification => {
                pushSettingsIOS(remoteMessage);
              },
            }),
            PushNotification.localNotification(dataToPresent))
          : PushNotification.localNotification(dataToPresent)
        : newJob(data, sender);
    } else {
      console.log('Default');
      console.log(data);
      console.log(sender);
    }
  };
  //
  //
  //
  const pushSettings = async () => {
    const userData = {
      navigation,
      userInfo,
      dispatch,
      setIsLoading,
    };
    PushNotification.configure({
      onNotification: function(notification) {
        if (notification.foreground) {
          return routingManager(notification, userData);
        } else {
          let data = JSON.stringify(notification.data);
          let sender = notification.sender;
          let remoteMessage = {
            data,
            sender,
          };
          const incomingData = generateNotfication(
            remoteMessage,
            notification.foreground,
          );
          routingManager(incomingData, userData);
        }
        notification.finish(PushNotificationIOS.FetchResult.NoData);
      },
      onAction: function(notification) {},
      onRegistrationError: function(err) {
        Alert.alert(
          'Dawami',
          'You wont able to get Notfications,\nIf you really want get job update please enable from Setting -> Dawami -> Notfications',
        );
      },
      permissions: {
      alert: true,
      badge: true,
      sound: true,
    }
    });
    PushNotification.cancelAllLocalNotifications();
  };
  //
  //
  //
  // for ios routing
  const pushSettingsIOS = msg => {
    const userData = {
      navigation,
      userInfo,
      dispatch,
      setIsLoading,
    };
    console.log('msg.data');
    console.log(msg.data);
    const data = JSON.parse(msg?.data);
    const sender = JSON.parse(msg?.sender);

    console.log('Push setting ios is called');
    console.log(data);
    console.log(sender);
    // chat
    if (data.message) {
      return routingManager(chatDetails(data, sender), userData);
    }
    // cv builder statsu changed
    else if (data.staff && data.status) {
      return routingManager(cvStatus(data, sender), userData);
    }
    // new job posted
    else if (data.company && (data.title_en || data.title_ar)) {
      return routingManager(newJob(data, sender), userData);
    } else {
      console.log('Default');
      console.log(data);
      console.log(sender);
    }
    PushNotificationIOS.cancelAllLocalNotifications();
  };

  //-----------------------------------

  useEffect(() => {
    checkDeviceType();
    return () => null;
  }, ['']);

  const checkDeviceType = async () => {
    // console.log('============');
    let isEmulator = await DeviceInfo.isEmulator();
    if (!isEmulator) {
      // settings
      !ios
        ? pushSettings()
        : PushNotification.configure({
            onAction: notification => {
              console.log('OPENING HERE ');
            },

            permissions: {
              alert: true,
              badge: true,
              sound: true,
            },
            popInitialNotification: true,
            requestPermissions: true,
          });
    }
  };

  useEffect(() => {
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      generateNotfication(remoteMessage.data, true);
    });
    messaging().onNotificationOpenedApp(async remoteMessage => {
      console.log('on notification open app');
      console.log(remoteMessage);
      ios
        ? pushSettingsIOS(remoteMessage.data)
        : generateNotfication(remoteMessage.data);
    });
    messaging().setBackgroundMessageHandler(async remoteMessage => {
      ios
        ? pushSettingsIOS(remoteMessage.data)
        : generateNotfication(remoteMessage.data);
    });
    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          ios
            ? pushSettingsIOS(remoteMessage.data)
            : generateNotfication(remoteMessage.data, true);
          PushNotification.cancelAllLocalNotifications();
        }
      });
    return unsubscribe;
  }, ['']);

  //
  //
  //
  //
  //
  //
  //
  //

  //
  //
  //
  //
  //
  //
  //
  //

  //
  //
  //
  //
  //
  //
  //
  //

  //
  //
  //
  //
  //
  //
  //
  //
  const messageRef = useRef();
  const dispatch = useDispatch();
  const id = useSelector(state => state.auth.userBasicProfile?.id);
  const chats = useSelector(state => state.getLists.all_chats);
  const compaines = useSelector(state => state.getLists.listOfCompanies);
  const jobslist = useSelector(state => state.getLists.listOfJobs);
  const trainingslist = useSelector(state => state.getLists.Trainings);
  let clicked = false;
  const authSection = useSelector(state => state.auth);
  const action = {
    dispatch,
    messageRef,
    navigate,
    compaines,
    jobslist,
    trainingslist,
    chats,
    id,
  };
  let firstTimeAction = {
    dispatch,
    messageRef,
    compaines,
    jobslist,
    trainingslist,
    chats,
    id,
  };

  // useEffect(() => {
  //   if (id) {
  //     if (!chats?.length) {
  //       fetchMessage(firstTimeAction, setIsLoading);
  //     }
  //   }
  //   getCompanies(firstTimeAction, setIsLoading);
  //   getJobs(firstTimeAction, setIsLoading);
  //   if (!trainingslist?.length) {
  //     getTrainings(firstTimeAction, setIsLoading);
  //   }
  //   if (isLoading) {
  //     setTimeout(() => {
  //       setIsLoading(false);
  //     }, 3000);
  //   }
  //   return () => null;
  // }, ['']);

  const alertTiming = 3000;
  const SignUp = 'Sign Up';
  const SignUp_AR = 'سجل';

  // const company = {name: WORDS.COMPANY, name_ar: WORDS.COMPANY_AR};
  // const jobs = {name: WORDS.JOB, name_ar: WORDS.JOB_AR};
  // const cv = {name: WORDS.CV_BUILDER, name_ar: WORDS.CV_BUILDER_AR};
  // const training = {
  //   name: WORDS.TRAINING_CATELOG,
  //   name_ar: WORDS.TRAINING_CATE_AR,
  // };
  const company = AR ? WORDS.COMPANY_AR : 'Company';
  const jobs = AR ? WORDS.JOB_AR : 'Jobs';
  const cv = AR ? WORDS.CV_BUILDER_AR : WORDS.CV_BUILDER;
  const _training = AR ? WORDS.TRAINING_CATE_AR : WORDS.TRAINING_CATELOG;

  const searchPlaceHolder = AR
    ? WORDS.Search_placeholder_ar
    : WORDS.Search_placeholder;
  const areaPlaceHolder = AR
    ? WORDS.Area_placeholder_ar
    : WORDS.Area_placeholder;
  const search = AR ? WORDS.SEARCH_AR : WORDS.SEARCH;
  const noResultFound = AR ? WORDS.NO_RESULT_FOUND_AR : WORDS.NO_RESULT_FOUND;
  const home_title = AR ? WORDS.HOME_TITLE_AR : WORDS.HOME_TITLE;
  const login = AR ? WORDS.LOGIN_AR : WORDS.LOGIN;
  const signup = AR ? SignUp_AR : SignUp;
  const data = [
    // {key: '1', name: 'Company', color: colors.brown, image: icons.company},
    // {key: '2', name: 'Job', color: colors.darkBrown, image: icons.job},
    // {key: '3', name: 'CVBuilder', color: colors.darkBrown, image: icons.cv},
    // {
    //   key: '4',
    //   name: 'TrainingCatalogue',
    //   color: colors.brown,
    //   image: icons.training_catalog,
    // },
    {
      key: '1',
      name: company,
      color: '#BD9571',
      route: NAV.COMPANY,
      image: icons.company,
    },
    {
      key: '2',
      name: jobs,
      color: colors.darkBrown,
      route: NAV.JOB,
      image: icons.job,
    },
    {
      key: '3',
      name: cv,
      color: colors.darkBrown,
      route: NAV.CV,
      image: icons.cv,
    },
    {
      key: '4',
      name: _training,
      route: NAV.TRAININGS,
      color: '#BD9571',
      image: icons.training_catalog,
    },
  ];
  const {listOfCompanies, listOfJobs} = useSelector(state => state.getLists);

  // ===================================================================
  const getRelaventData = async name => {
    switch (name) {
      case NAV.COMPANY:
        getCompanies(action, setIsLoading);
        return;

      case NAV.JOB:
        getJobs(action, setIsLoading);
        return;
      case NAV.CV:
        status
          ? navigation.navigate(NAV.CV)
          : messageRef.current?.show(
              <CustomAlert
                textColor={colors.primary}
                text="Login Is Required"
              />,
              alertTiming,
            );
        return;
      case NAV.TRAININGS:
        if (!trainingslist?.length) {
          // console.log(trainingslist);
          // getTrainings(action, setIsLoading);
        } else {
          navigate(NAV.TRAININGS);
        }
        return;
      default:
        return console.log('ops');
    }
  };

  const {status} = authSection;
  // useEffect(() => messageRef.current?.show('', 2000));
  useEffect(() => {
    route.params?.fetchError &&
      messageRef.current?.show(
        <CustomAlert
          textColor={colors.primary}
          text="Cannot Load Profile. Check your internet connectivity first"
        />,
        3500,
      );
    route.params?.messageSent &&
      messageRef.current?.show(
        <CustomAlert
          textColor={colors.primary}
          textAlign={'center'}
          text={AR ? WORDS.MessageSent_AR : WORDS.MessageSent}
        />,
        4000,
      );
  }, [route.params?.fetchError, route.params?.messageSent]);

  // ==================================================
  // const [btnDisable, setBtnDisable] = useState(false);
  const [title, setTitle] = useState(null);
  const [location, setLocation] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const inputHander = ({name, text}) => {
    switch (name) {
      case 'title':
        return setTitle(text);
      case 'location':
        return setLocation(text);
      default:
        return console.log('default');
    }
  };
  // =============================
  useEffect(() => {
    if (isloading) {
      setTimeout(() => {
        dispatch(loading(false));
      }, 3000);
    }
    return () => null;
  }, ['']);
  const locationRef = useRef();
  const titleRef = useRef();
  const [isLoading, setIsLoading] = useState(false);
  const applySearch = async () => {
    if ((title && title !== '') || (location && location !== '')) {
      setIsLoading(true);
      const result = await ApplySearch(title, location, err => {
        messageRef.current.show(
          <CustomAlert text={err?.message || err} />,
          AlertTiming,
        );
        setIsLoading(false);
      });
      result && result.length
        ? (setIsLoading(false),
          dispatch({
            type: JOB_LIST_STATE,
            payload: {
              listOfJobs: result,
            },
          }),
          locationRef.current?.clear(),
          titleRef.current?.clear(),
          setTitle(null),
          setLocation(null),
          navigation.navigate(NAV.JOB))
        : // console.log(' NOTHIN FOUND I AM IN ELSE'),
          (messageRef.current?.show(
            <CustomAlert
              textColor={colors.primary}
              textAlign="center"
              text={noResultFound}
            />,
            3500,
          ),
          locationRef.current?.clear(),
          titleRef.current?.clear(),
          setTitle(null),
          setLocation(null)),
        locationRef.current.focus();
      setIsLoading(false);
    } else {
      messageRef.current?.show(
        <CustomAlert textColor={colors.primary} text={noResultFound} />,
        AlertTiming,
      );
      setIsLoading(false);
    }
  };
  // ==================================================
  const isloading = useSelector(state => state.auth.isloading);
  // <ActivitySign visible={isloading} />;

  return (
    <>
      {isLoading && <ActivitySign />}
      <SafeAreaView style={styles.firstSafeArea} />

      <SafeAreaView style={styles.mainSafeArea}>
        <StatusBar backgroundColor={colors.primary} barStyle="dark-content" />
        {/* ScrollView */}

        <KeyboardAwareScrollView>
          <Block style={styles.mainCon}>
            <Block center style={{borderWidth: 0}}>
              {/* ================================================= */}
              <CustomHeader
                bg={primary}
                tint={customRed}
                logoColor={red}
                navigation={navigation}
              />
              {/* ================================================= HOME TITLE  */}
              <Block style={styles.titleCon}>
                <Text
                  style={{
                    fontSize: AR
                      ? customFont(iphone6s ? h4 : header)
                      : iphone6s
                      ? h1
                      : header,
                    ...theme.fonts.GMB,
                    // borderWidth:1,
                  }}
                  color={spaceGreen}>
                  {home_title}
                </Text>
              </Block>
              {/* ================================================= SEARCH INPUTS */}

              <Block flex={1.3}>
                <Block style={styles.search}>
                  <Image source={icons.briefCase} style={styles.searchImage} />
                  <TextField
                    blurOnSubmit={false}
                    onChangeText={e => inputHander(e)}
                    placeholder={searchPlaceHolder}
                    ref={titleRef}
                    name={'title'}
                    returnKeyType="next"
                    inputStyling={styles.inputStyle}
                    onSubmitEditing={() => {
                      locationRef.current?.focus();
                    }}
                  />
                </Block>
                <Block style={styles.search}>
                  <Image
                    source={icons.red_location}
                    style={styles.searchImage}
                  />
                  <TextField
                    blurOnSubmit={true}
                    returnKeyType={'send'}
                    onChangeText={e => inputHander(e)}
                    placeholder={areaPlaceHolder}
                    ref={locationRef}
                    name={'location'}
                    inputStyling={styles.inputStyle}
                    onSubmitEditing={applySearch}
                  />
                </Block>
              </Block>
              {/* ================================================= */}
              <Block
                style={{borderWidth: 0, width: '100%'}}
                margin={[getHeight(1), 0, 0, 0]}
                padding={[0, getWidth(3)]}>
                {/* =================================================  IMAGE AND SEARCH BUTTON*/}
                <Block row>
                  <SquareButton
                    onPress={applySearch}
                    textColor={colors.primary}
                    width={sizes.getWidth(34)}
                    // height={sizes.getHeight(ios ? (iphone6s ? 6 : 4.5) : 5)}
                    textStyle={{
                      fontSize: iphone6s ? 12 : 22,
                    }}
                    style={{
                      // backgroundColor: btnDisable ? 'transparent' : colors.customRed,
                      borderWidth: 0,
                      backgroundColor: colors.spaceGreen,
                      borderRadius: sizes.withScreen(0.03),
                    }}>
                    {search}
                  </SquareButton>
                  <Block
                    center
                    middle
                    style={
                      {
                        // position: 'absolute',
                        // //   borderWidth: 1,
                        // height: '60%',
                        // width: '70%',
                        // right: 0,
                        // zIndex: -10,
                        // bottom: -sizes.getHeight(ios ? 2 : status ? 5 : 2),
                      }
                    }>
                    <Image
                      source={icons.background_beautify}
                      style={{
                        resizeMode: 'contain',
                        width: screenSize * 0.24,
                        height: screenSize * 0.1,
                      }}
                    />
                  </Block>
                </Block>
              </Block>
              {/* SEARCH AREA END */}
            </Block>
            {/* =================================================  BLOCKS*/}
            <Block flex={1.3} middle>
              <Block
                middle
                center
                flex={2.3}
                style={{
                  // borderWidth: 2,
                  paddingHorizontal: getWidth(2),
                  paddingTop: sizes.getHeight(status ? 1 : 0),
                  height: !status ? screenSize * 0.246 : screenSize * 0.33,
                }}>
                <FlatList
                  showsVerticalScrollIndicator={false}
                  data={data}
                  numColumns={2}
                  key={!status ? 1 : 0}
                  renderItem={({item, index}) => {
                    return (
                      <Blocks goTo={e => getRelaventData(e)} item={item} />
                    );
                  }}
                />
              </Block>
              {/* =================================================  Buttons Start*/}
              {!status && (
                <Block padding={[0, sizes.getWidth(3)]}>
                  <SquareButton
                    onPress={() => navigation.navigate(NAV.LOGIN)}
                    withImage
                    source={icons.login}
                    imgSyling={{
                      tintColor: spaceGreen,
                    }}
                    bgColor={newGray}
                    textColor={spaceGreen}
                    textStyle={{
                      fontSize: iphone6s ? 12 : 18,
                    }}
                    height={getHeight(6)}>
                    {login}
                  </SquareButton>
                  <SquareButton
                    // onPress={() => navigation.navigate('Signup')}
                    onPress={() => navigation.navigate(NAV.SIGNUP)}
                    withImage
                    imgSyling={{
                      tintColor: primary,
                    }}
                    source={icons.white_user}
                    bgColor={colors.spaceGreen}
                    textColor={colors.primary}
                    textStyle={{
                      fontSize: iphone6s ? 12 : 18,
                    }}
                    height={sizes.getHeight(6)}>
                    {signup}
                  </SquareButton>
                </Block>
              )}
              {/* =================================================  Buttons End*/}
            </Block>
            {/* =================================================  TOAST AND SEARCH MODAL*/}

            <Toast
              ref={messageRef}
              style={{
                backgroundColor: colors.customRed,
                width: sizes.getWidth(100),
                borderRadius: 0,
              }}
              positionValue={sizes.getDimensions.height}
              fadeInDuration={200}
              fadeOutDuration={100}
              opacity={1}
            />
            {showModal && (
              <Modal
                animationType="slide"
                visible={showModal}
                onRequestClose={() => setShowModal(false)}
                onDismiss={() => alert('dismissed')}>
                <Block
                  middle
                  center
                  style={{
                    // backgroundColor: colors.customRed,
                    borderWidth: 0,
                  }}>
                  <CustomHeader bg={colors.darkBrown} />
                  <Searchbar
                    // onIconPress={fillterPopup}
                    // onChangeText={inputHandler}
                    bg={colors.darkBrown}
                    title={'Jobs'}
                    placeholder={'Find for companies here'}
                  />
                  <FlatList
                    data={searchResult}
                    keyExtractor={(item, index) => {
                      return index.toString();
                    }}
                    renderItem={({item, index}) => {
                      return (
                        <SearchResults
                          onClick={item => {
                            console.log(item);
                            dispatch({
                              type: SAVE_JOB_STATUS,
                              payload: item,
                            }),
                              setTitle(null),
                              setLocation(null),
                              titleRef.current.clear(),
                              locationRef.current.clear(),
                              setShowModal(false),
                              navigation.navigate('jobDetails');
                          }}
                          item={item}
                        />
                      );
                    }}
                  />

                  {/* <SearchResult
              onClick={
                item => console.log(item)
                // dispatch({type: SAVE_JOB_STATUS, payload: item}),
                // setTitle(null),
                // setLocation(null),
                // titleRef.current.clear(),
                // locationRef.current.clear(),
                // setShowModal(false),
                // navigation.navigate('jobDetails')
              }
            /> */}
                  {/* =================CLOSE MODAL BUTTON================ */}
                  {/* <Block
              bottom
              style={{
                position: 'absolute',
                bottom: sizes.getHeight(3),
                zIndex: 10,
                backgroundColor: colors.brown,
                flex: 0,
              }}>
              <Button
                opacity={0.3}
                onPress={() => setShowModal(false)}
                middle
                center
                style={{
                  height: sizes.screenSize * 0.05,
                  width: sizes.screenSize * 0.05,
                  backgroundColor: colors.customRed,
                  borderRadius: sizes.screenSize * 3,
                  opacity: 0.8,
                }}>
                <Image
                  source={icons.roundClose}
                  style={{
                    resizeMode: 'contain',
                    width: sizes.getWidth(30),
                    height: sizes.getHeight(13),
                    tintColor: colors.primary,
                  }}
                />
              </Button>
            </Block> */}
                  {/* =================CLOSE MODAL BUTTON================ */}
                </Block>
              </Modal>
            )}

            {/*  */}
            {/*  */}
            {/*  */}
            {/*  */}
            {/*  */}
            {/*  */}
            {/*  */}
            {/*  */}
          </Block>
        </KeyboardAwareScrollView>
        {/* ScrollView */}
      </SafeAreaView>

      {/* </KeyboardAwareScrollView> */}
    </>
  );
};

const styles = StyleSheet.create({
  firstSafeArea: {
    flex: 0,
    backgroundColor: colors.primary,
  },
  mainSafeArea: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  mainCon: {
    // backgroundColor: colors.primary,
  },
  titleCon: {
    flex: 0,
    alignItems: 'center',
    justifyContent: 'center',
    height: getHeight(iphone6s ? 9 : 7),
  },
  search: {
    flexDirection: 'row',
    borderWidth: 0.6,
    borderColor: spaceGreen,
    flex: 0,
    height: screenSize * 0.035,
    marginVertical: getHeight(0.5),
    marginHorizontal: getWidth(5),
    alignItems: 'center',
    borderRadius: screenSize * 0.2,
    paddingHorizontal: getWidth(3),
  },
  searchImage: {
    resizeMode: 'contain',
    width: screenSize * 0.018,
    tintColor: spaceGreen,
  },
  inputStyle: {
    // borderWidth:1,
    width: '95%',
    paddingVertical: 0,
    paddingHorizontal: getWidth(1),
    height: '100%',
    color: spaceGreen,
    textAlign: 'left',
    fontSize: customFont(iphone6s ? 8 : 14),
  },
  //
  //
  //
});

export default HomePage;
