import React, {useState, useEffect} from 'react';
import {Block, Text, TextField, Button, ActivitySign} from 'components';
import {colors, sizes} from 'styles/theme';
import {TextInput} from 'react-native-gesture-handler';
import {PasswordFormState, validateField, validateFields} from 'utils';
import {resetPassword} from 'redux/actions';
import {useSelector} from 'react-redux';
const SetNewPassword = props => {
  const {resetCompleted, unmountMe} = props;
  const authSection = useSelector(state => state.auth);
  const [inidicatorStatus, setInidicatorStatus] = useState(false);
  const [state, setState] = useState({
    fields: {...PasswordFormState},
  });

  const handleInput = ({text, name}) => {
    const newField = validateField(state.fields[name], text);
    setState({fields: {...state.fields, [name]: newField}});
  };

  const handleSubmit = async () => {
    setInidicatorStatus(prev => (prev = !prev));
    const result = validateFields(state.fields);
    if (result.validity) {
      // CALL API
      const result = await resetPassword(
        {
          email: authSection.requestedUserDetails.email,
          password: state.fields.password.value,
        },
        err =>{
          // alert(state.fields.password.errorMessage)
          console.log(err)
        },
      );
      console.log('PASSWORD RESET RESULT HANDLER GOING BACK');

      result.success && ( 
          unmountMe(), 
          resetCompleted() 
        )
    } else {
      // alert("Password Doesn't meet policy length. ");
      // setState(state=> ({...state.fields.password, errorMessage:'sss'}))
    }
    setState(state => ({...state, fields: result.newFields}));
    setInidicatorStatus(prev => (prev = !prev));
  };
  useEffect(() => {
    console.log("SET NEW PASSWORD IS UNMOUNTED RENDER")
  },[unmountMe])


  return (
    <Block
      center
      flex={false}
      padding={[
        sizes.getHeight(3),
        sizes.getWidth(5),
        sizes.getHeight(2),
        sizes.getWidth(5),
      ]}
      style={{
        // borderWidth: 1,
        borderRadius: sizes.withScreen(0.004),
        backgroundColor: colors.primary,
        elevation: 20,
      }}
      height={sizes.getHeight(25)}
      width={sizes.getWidth(95)}>
      <Text h3 bold color={colors.customRed}>
        Great ! You've been verifed now
      </Text>
      <Text h4 color={colors.customRed}>
        You can set here your NEW PASSWORD here.{' '}
      </Text>
      {/* <TextInput secureTextEntry */}
      <Block row>
        <Block middle flex={3} margin={[0, sizes.getWidth(2), 0, 0]}>
          <TextField
            onChangeText={handleInput}
            name={'password'}
            secureTextEntry = {true}
            textContentType="password"
            placeholder={'SET NEW PASSWORD'}
            inputStyling={{
              borderBottomWidth: 1,
              height: sizes.getHeight(8),
              width: '90%',
            }}
          />
          {state.fields.password.errorMessage ? (
            <Text color={colors.customRed} h4>
              {state.fields.password.errorMessage}
            </Text>
          ) : null}
        </Block>
        <Block middle padding={[20, 0, 0, 0]}>
          <Button
            onPress={handleSubmit}
            center
            middle
            style={{
              backgroundColor: colors.customRed,
              borderRadius: sizes.withScreen(0.004),
              height: sizes.getHeight(5),
            }}>
            <Text color={colors.primary}>SET</Text>
          </Button>
        </Block>
      </Block>
      {inidicatorStatus && <ActivitySign />}
    </Block>
  );
};

export default SetNewPassword;
