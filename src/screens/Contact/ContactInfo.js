import React from 'react';
import {Text, Block} from 'components';
import {Image, Platform, Dimensions} from 'react-native';
import * as icons from 'assets/icons';
import {sizes, colors} from 'styles/theme';
import {useSelector} from 'react-redux';
import {LANG_AR} from 'redux/constants';

const ContactInfo = info => {
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width <= 375;
  const {currentLN} = useSelector(state => state?.userInfo);
  const AR = currentLN === LANG_AR;

  const email = AR ? info.email : info.email2;

  console.log('=====info======');
  console.log(info);
  return (
    <Block
      flex={false}
      padding={[0, sizes.getWidth(5)]}
      style={{
        //   borderWidth: 1,
        backgroundColor: '#AB7264',
        width: '100%',
        // backgroundColor: colors.brown,
        height: sizes.getHeight(28),
      }}>
      <Text h2 bold color={colors.primary}>
        Get In Touch
      </Text>
      <Text
        style={{
          marginTop: sizes.getHeight(2),
          fontSize: sizes.customFont(iphone6s ? 10 : ios ? 19 : 15),
        }}
        color={colors.primary}>
        There are many variations of passage of Lorem Ipsum avaiable, but the
        majority have suffered alteration in some form, by inject humour
      </Text>
      <Block row center flex={false} style={{marginTop: sizes.getHeight(1)}}>
        <Image
          source={icons.location}
          style={{
            resizeMode: 'contain',
            marginRight: sizes.getWidth(2),
            tintColor: colors.primary,
            width: sizes.getWidth(4),
          }}
        />
        <Text
          color={colors.primary}
          style={{
            fontSize: sizes.customFont(iphone6s ? 10 : ios ? 19 : 15),
          }}>
          {/* 123 - Al Shamal Road, Ghaffar Doha,Qatar */}
          {info.address}
        </Text>
      </Block>

      <Block
        row
        center
        flex={false}
        style={{
          // borderWidth: 1
          height: sizes.getHeight(4),
          marginTop: sizes.getHeight(1),
        }}>
        <Block center row flex={false} style={{borderWidth: 0}}>
          <Image
            source={icons.message}
            style={{
              resizeMode: 'contain',
              marginRight: sizes.getWidth(2),
              tintColor: colors.primary,
              width: sizes.getWidth(4),
            }}
          />
          <Text
            color={colors.primary}
            style={{
              fontSize: sizes.customFont(iphone6s ? 10 : ios ? 17 : 15),
            }}>
            {/* info@dawami.com */}
            {email}
          </Text>
        </Block>
        <Block>
          <Block center row bottom>
            <Image
              source={icons.phone}
              style={{
                resizeMode: 'contain',
                marginRight: sizes.getWidth(2),
                tintColor: colors.primary,
                width: sizes.getWidth(4),
              }}
            />
            <Text
              color={colors.primary}
              style={{
                fontSize: sizes.customFont(iphone6s ? 10 : ios ? 19 : 15),
              }}>
              {/* +7412345987 */}
              {info.phone}
            </Text>
          </Block>
        </Block>
      </Block>
    </Block>
  );
};

export default ContactInfo;
