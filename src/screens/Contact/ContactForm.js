import React, {useReducer, useRef, useState, useEffect} from 'react';
import {
  Text,
  Block,
  CustomInput,
  Button,
  CustomAlert,
  ActivitySign,
} from 'components';
import {sizes, colors} from 'styles/theme';
import {ScrollView, ActivityIndicator, StyleSheet, Platform, Dimensions} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import {ContactUs, GetCompInfo} from 'redux/actions';
import Toast from 'react-native-easy-toast';
import * as EmailValidator from 'email-validator';
import {CustomTextArea} from 'components/CustomInput';
import {useSelector} from 'react-redux';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {WORDS, NAV} from 'utils';
import {LANG_AR} from 'redux/constants';
import ContactInfo from './ContactInfo';

const ContactForm = props => {
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width <= 375;

  const {navigation, route, onError} = props;
  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;

  const name_placeHolder = AR ? 'اسم' : 'Name';
  const email_placeholder = AR ? WORDS.EMAIL_AR : WORDS.EMAIL;
  const company_palceholder = AR ? 'شركة' : 'Company';
  const contact_placeholder = AR ? 'هاتف' : 'Phone No';
  const message_placeholder = AR
    ? 'فضلا أكتب رسالتك هنا'
    : 'Please Type Your Message Here.';
  const sendBtnText = AR ? 'إرسال' : 'Send';
  const messageSent = AR ? WORDS.MessageSent_AR : WORDS.MessageSent;
  const NAME = 'NAME';
  const EMAIL = 'EMAIL';
  const COMPANY = 'COMPANY';
  const PHONE = 'PHONE';
  const MESSAGE = 'MESSAGE';
  const initailState = {
    name: null,
    email: null,
    phone: null,
    company: null,
    message: null,
  };
  const reducer = (state, action) => {
    switch (action.type) {
      case NAME:
        return {...state, name: action.payload};
      case EMAIL:
        return {...state, email: action.payload};
      case PHONE:
        return {...state, phone: action.payload};
      case COMPANY:
        return {...state, company: action.payload};
      case MESSAGE:
        return {...state, message: action.payload};
      default:
        return state;
    }
  };
  const [state, dispatch] = useReducer(reducer, initailState);
  const [isLoading, setIsLoading] = useState(false);

  const inputHandler = ({name, text}) => {
    switch (name) {
      case NAME:
        dispatch({
          type: NAME,
          payload: text,
        });
        return;
      case EMAIL:
        return dispatch({
          type: EMAIL,
          payload: text,
        });
      case COMPANY:
        return dispatch({
          type: COMPANY,
          payload: text,
        });
      case PHONE:
        return dispatch({
          type: PHONE,
          payload: text,
        });
      case MESSAGE:
        return dispatch({
          type: MESSAGE,
          payload: text,
        });
      default:
        // console.log(name);
        // console.log(text);
        return console.log('Default Input Handler');
    }
  };
  const nameRef = useRef();
  const emailRef = useRef();
  const compRef = useRef();
  const phoneRef = useRef();
  const messageRef = useRef();

  const onSubmit = async () => {
    setIsLoading(true);
    if (state.name && state.phone && state.company && state.message) {
      if (EmailValidator.validate(state.email)) {
        const result = await ContactUs(state, err => {
          onError(err);
          setIsLoading(false);
        });
        result &&
          (console.log('RESULT'),
          setIsLoading(false),
          nameRef.current?.clear(),
          emailRef.current?.clear(),
          compRef.current?.clear(),
          phoneRef.current?.clear(),
          messageRef.current?.clear(),
          // onError(messageSent)
          navigation.navigate(NAV.HOME, {messageSent: true}));
      } else {
        onError('Email is not valid.');
        setIsLoading(false);
      }
    } else {
      onError('All Fields are Required.');
      setIsLoading(false);
    }
  };

  // console.log('============================');
  useEffect(() => {
    fetchCompInfo();
  }, ['']);

  const [info, setInfo] = useState();
  const fetchCompInfo = async () => {
    const result = await GetCompInfo(err => {
      // showError(err);
    });
    result && setInfo(result);
  };

  // console.log('============================');
  return (
    // <ScrollView style={{flex: 1}}>

    <Block
      style={{
        backgroundColor: colors.primary,
        width: '100%',
        // borderWidth: 2,
      }}>
      {/* <ScrollView> */}
      <KeyboardAwareScrollView style={{flex: 1}}>
        <ContactInfo {...info} />

        <Block center style={styles.formCon}>
          <CustomInput
            // forwardRef={nameRef}
            ref={nameRef}
            editable={!isLoading}
            name={NAME}
            onChangeText={inputHandler}
            noRounded
            noIcon={AR ? false : true}
            bb
            // inputyStyle={{paddingHorizontal: 0}}
            blockStyle={{justifyContent: 'flex-start'}}
            placeholder={name_placeHolder}
            bc={colors.gray}
          />
          <CustomInput
            // forwardRef={emailRef}
            ref={emailRef}
            editable={!isLoading}
            name={EMAIL}
            onChangeText={inputHandler}
            noRounded
            noIcon={AR ? false : true}
            bb
            blockStyle={{justifyContent: 'flex-start'}}
            placeholder={email_placeholder}
            bc={colors.gray}
          />
          <CustomInput
            // forwardRef={compRef}
            ref={compRef}
            editable={!isLoading}
            onChangeText={inputHandler}
            noRounded
            noIcon={AR ? false : true}
            bb
            name={COMPANY}
            blockStyle={{justifyContent: 'flex-start'}}
            placeholder={company_palceholder}
            bc={colors.gray}
          />
          <CustomInput
            editable={!isLoading}
            // forwardRef={phoneRef}
            ref={phoneRef}
            onChangeText={inputHandler}
            noRounded
            noIcon={AR ? false : true}
            bb
            name={PHONE}
            keyboardType={'phone-pad'}
            blockStyle={{justifyContent: 'flex-start'}}
            placeholder={contact_placeholder}
            numeric
            bc={colors.gray}
          />
          <CustomTextArea
            ref={messageRef}
            editable={!isLoading}
            placeholder={message_placeholder}
            name={MESSAGE}
            onChangeText={inputHandler}
          />
          {/* </ScrollView> */}
          {!isLoading ? (
            <Button onPress={onSubmit} center middle style={styles.submitBtn}>
              <Text
                style={{fontSize: sizes.customFont(iphone6s ? 12 : 22)}}
                color={colors.primary}>
                {sendBtnText}
              </Text>
            </Button>
          ) : (
            // <Block middle center style={{borderWidth: 1, width: '100%'}}>
            <ActivityIndicator size={'large'} color={'#AB7264'} />
            // </Block>
          )}
        </Block>
        {/* </ScrollView> */}
      </KeyboardAwareScrollView>
    </Block>
  );
};

export default ContactForm;

const styles = StyleSheet.create({
  formCon: {
    // borderWidth: 1,
    // paddingHorizontal: sizes.getWidth(10),
    backgroundColor: colors.primary,
    width: sizes.getWidth(86),
    height: sizes.getHeight(54),
    marginLeft: sizes.getWidth(7),
    position: 'relative',
    bottom: sizes.getHeight(4),
    elevation: 10,
    zIndex: 1000,
    shadowRadius: 19,
    shadowColor: colors.gray3,
    shadowOpacity: 0.5,
    paddingHorizontal: sizes.getWidth(2),
  },
  submitBtn: {
    backgroundColor: colors.customRed,
    width: '80%',
    position: 'absolute',
    bottom: 10,
  },
});
