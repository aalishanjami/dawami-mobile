import React, {useEffect, useRef, useState} from 'react';
import {
  Block,
  Text,
  Button,
  CustomInput,
  CustomHeader,
  CustomAlert,
  ActivitySign,
} from 'components';
import {sizes, colors} from 'styles/theme';
import ContactInfo from './ContactInfo';
import ContactForm from './ContactForm';
import {SafeAreaView} from 'react-native';
import Toast from 'react-native-easy-toast';
import {GetCompInfo} from 'redux/actions';

const Contact = ({navigation, route}) => {
  const messageRef = useRef();

  useEffect(() => {
    fetchCompInfo();
  }, ['']);

  const [info, setInfo] = useState();
  const fetchCompInfo = async () => {
    const result = await GetCompInfo(err => {
      showError(err?.message || err);
    });
    result && setInfo(result);
  };

  const showError = e => {
    messageRef.current?.show(
      <CustomAlert textColor={colors.primary} text={e} />,
      3500,
    );
  };

  return (
    <>
      <SafeAreaView style={{flex: 0}} style={{backgroundColor: '#AB7264'}} />
      {/* <StatusBar barStyle="dark-content" translucent={true} /> */}
      <Block style={{broderWidth: 4}}>
        {/* <CustomHeader bg={'#AB7264'} navigation={navigation} backBtn /> */}
        <Block flex={false} style={{backgroundColor: 'transparent'}}>
          <Block
            flex={false}
            style={
              {
                // marginTop: sizes.getHeight(Platform.OS === 'android' ? 3 : 0),
              }
            }>
            <CustomHeader
              tint={colors.reds}
              // bg={colors.brown}
              bg={'#AB7264'}
              navigation={navigation}
              backBtn
            />
          </Block>
        </Block>
        <ContactForm
          navigation={navigation}
          route={route}
          onError={e => showError(e)}
        />

        <Toast
          ref={messageRef}
          style={{
            backgroundColor: colors.customRed,
            width: sizes.getWidth(100),
            borderRadius: 2,
          }}
          positionValue={sizes.getDimensions.height / 1}
          fadeInDuration={200}
          fadeOutDuration={100}
          opacity={1}
        />
      </Block>
    </>
  );
};

export default Contact;
