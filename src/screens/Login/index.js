import React, {useState, useEffect, createRef, useRef} from 'react';
import {
  Block,
  Text,
  Button,
  SquareButton,
  TextField,
  SimpleToast,
  ActivitySign,
  CustomAlert,
} from 'components';
import {
  Image,
  TextInput,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
  Keyboard,
  SafeAreaView,
  Platform,
  Dimensions,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import * as icons from 'assets/icons';
import {colors, sizes} from 'styles/theme';
import {LoginFormState, validateField, validateFields, WORDS, NAV} from 'utils';
import Toast from 'react-native-easy-toast';
import {fbHandler, googleSigning} from 'utils';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';
import {useSelector, useDispatch, batch} from 'react-redux';
import {
  loginUser,
  saveUser,
  goWithgoogle,
  forgetPassword,
} from 'redux/actions/auth';
import {USER_LOGIN, ADD_USER_INFO, LANG_AR} from 'redux/constants';
import {goWithFacebook, GetProfileInfo} from 'redux/actions';
import messaging, {AuthorizationStatus} from '@react-native-firebase/messaging';
// ========================CODE STARTEd=================

export const Login = ({route, navigation}) => {
  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width === 375;

  const REG_SUCC = 'Registration Successfully Completed';
  const REG_SUCC_AR = 'تم التسجيل بنجاح.';

  const remember = AR ? WORDS.REMEMBER_AR : WORDS.REMEMBER;
  const email = AR ? WORDS.EMAIL_AR : WORDS.EMAIL;
  const passwd = AR ? WORDS.PASSWORD_AR : WORDS.PASSWORD;
  const login = AR ? WORDS.LOGIN_AR : WORDS.LOGIN;
  const forget = AR ? WORDS.FORGET_PASSWORD_AR : WORDS.FORGET_PASSWORD;
  const orConnectWith = AR ? WORDS.OR_CONNECT_AR : WORDS.OR_CONNECT;
  const dontHav = AR ? WORDS.DONT_HAVE_AC_AR : WORDS.DONT_HAVE_AC;
  const reg_done = AR ? REG_SUCC_AR : REG_SUCC;
  const fb = AR ? WORDS.FB_AR : WORDS.FB;
  const google = AR ? WORDS.GOOGLE_AR : WORDS.GOOGLE;

  const alertTiming = 3000;
  const [state, setState] = useState({
    loginBtnDisable: false,
    fields: {...LoginFormState},
  });
  const [isLoading, setIsLoading] = useState(false);
  const [checkbox, setCheckbox] = useState(false);
  // useSelector(state=> console.log(state))
  const messageRef = useRef();
  useEffect(() => {
    GoogleSignin.configure({
      offlineAccess: false,

      // webClientId:
      //   '206246080380-8rev8p06u07f1mm8e1u4s24k4an8g0s3.apps.googleusercontent.com',
      webClientId:
        '206246080380-lf8jg6q039gova27eph82gbh4983k4q9.apps.googleusercontent.com',
    });
  });
  const handleInput = ({text, name}) => {
    const newField = validateField(state.fields[name], text);
    setState({fields: {...state.fields, [name]: newField}});
  };
  function toggleLoginButton() {
    setState(state => ({...state, loginBtnDisable: !state.loginBtnDisable}));
  }
  const dispatch = useDispatch();

  // ==================================LOGIN WITH FACEBOOK========================================
  const loginWithFacebook = async () => {
    setIsLoading(true);
    try {
      const userInfo = await fbHandler();
      if (userInfo === null) {
        messageRef.current?.show(
          <CustomAlert
            textColor={colors.primary}
            text={'Please check INTERNET CONNECTIVITY..'}
          />,
          3500,
        );
        setIsLoading(false);
      }
      // User Founded but
      else {
        // check if email of user is not available
        if (!userInfo.email) {
          messageRef.current?.show(
            <CustomAlert
              textColor={colors.primary}
              text="No Email detected with this Facebook Account.."
            />,
            alertTiming,
          );
        }
        // if email found with this facebook account ---- proceeded to owm API
        else {
          const result = await goWithFacebook(userInfo, err => {
            // FACEBOOK ERROR FROM OWNED API WILL BE HERE--------
            setIsLoading(false);
          });
          result &&
            batch(() => {
              dispatch({
                type: USER_LOGIN,
                payload: {success: result.payload.success},
              });
              dispatch({type: ADD_USER_INFO, payload: result.payload.user});
            });
          setIsLoading(false);
        }
      }
    } catch (e) {
      // console.log('catch error state');
      // console.log(e);
      messageRef.current?.show(
        <CustomAlert
          textColor={colors.primary}
          // color={colors.primary}
          text={'Request Aborted.'}
        />,
        alertTiming,
      );
      setIsLoading(false);
    }
    setIsLoading(false);
  };
  // ============================GOOGE LOGIN========================================////////
  const loginWithGoogle = async () => {
    // messageRef.current?.show(<CustomAlert text={"Google Service is Unavailable yet. Please wait for next update."} />,alertTiming)
    setIsLoading(true);
    try {
      const user = await googleSigning();
      console.log('==========GOOGLE USER=======');
      console.log(user);
      if (user.name) {
        // console.log('**user');
        // console.log(user);
        const result = await goWithgoogle(user, err => {
          messageRef.current?.show(
            <CustomAlert textColor={colors.primary} text={err} />,
          );
          setIsLoading(false);
        });
        console.log('========GOOGLE');
        console.log(result);
        result &&
          (batch(() => {
            dispatch({
              type: USER_LOGIN,
              payload: {success: result.payload.success},
            }),
              dispatch({type: ADD_USER_INFO, payload: result.payload.user});
          }),
          setIsLoading(false));
        await GoogleSignin.revokeAccess();
      } else {
        messageRef.current?.show(
          <CustomAlert
            textColor={colors.primary}
            text={
              'No USERNAME associated with GOOGLE ACCOUNT \nPlease Try Again..'
            }
          />,
          2500,
        );
        setIsLoading(false), await GoogleSignin.revokeAccess();
      }
    } catch (e) {
      messageRef.current?.show(
        <CustomAlert
          textColor={colors.primary}
          text={"We couldn't process your request right now.."}
        />,
        alertTiming,
      );
      setIsLoading(false), await GoogleSignin.revokeAccess();
    }
  };
  // ===============================HANDLE SUBMIT=======================
  // const _handleSubmit = async () => {
  //   const token = await messaging().getToken();
  //   console.log(token);
  // };
  const handleSubmit = async () => {
    Keyboard.dismiss();
    const result = validateFields(state.fields);
    const {
      fields: {email, password},
    } = state;
    if (result.validity) {
      setIsLoading(true);
      toggleLoginButton();
      // firebase token
      const token = await messaging().getToken();

      const login_result = await loginUser(
        {email: email.value, password: password.value, token: token},
        // {email: email.value, password: password.value},
        err => {
          messageRef.current?.show(
            <CustomAlert textColor={colors.primary} text={err?.message || err} />,
            alertTiming,
          ),
            // if (err) {
            //   if (err.error) {
            //     messageRef.current?.show(
            //       <CustomAlert color={colors.primary} text={err.error} />,
            //       alertTiming,
            //     ),
            //       setIsLoading(false);
            //   } else {
            //     messageRef.current?.show(
            //       <CustomAlert
            //         color={colors.primary}
            //         text="Check your INTERNET CONNECTIVITY"
            //       />,
            //       alertTiming,
            //     );
            //     setIsLoading(false);
            //   }
            // }
            // alert(err)
            setIsLoading(false);
        },
      );
      if (login_result) {
        // console.log('========login_result========');
        // console.log(login_result.payload.user.id);
        const profileInfo = await GetProfileInfo(
          login_result.payload.user.id,
          err => {
            console.log('cant find profile and unable to add in redux');
            messageRef.current?.show(
              <CustomAlert
                textColor={colors.primary}
                text={err?.message || err}
                // Due to Some Error, We couldn't get your info, please contact Application owner.
              />,
              alertTiming,
            );
            setIsLoading(false);
          },
        );
        // setIsLoading(false);
        // const savingUserInfo = saveUser(login_result.payload.user)
        if (profileInfo) {
          const savingUserInfo = saveUser(profileInfo.payload.user);
          // console.log(savingUserInfo)
          batch(() => {
            dispatch(login_result);
            dispatch(savingUserInfo);
            profileInfo && dispatch(profileInfo);
          }),
            toggleLoginButton(),
            setIsLoading(false);
          // navigation.navigate('Home');
        }
      }
      // IF VALIDITIY
    } else {
      // when user put something fishy
      console.log('ERROR ');
      messageRef.current?.show(
        <CustomAlert
          textColor={colors.primary}
          text="Please Follow Instructions"
        />,
        alertTiming,
      );
      toggleLoginButton();
      setIsLoading(false);
    }
    setState(state => ({...state, fields: result.newFields}));
    setIsLoading(false);
  };

  // ========================================================================
  useEffect(() => {
    setState(state => ({
      ...state,
      fields: LoginFormState,
      loginBtnDisable: false,
    }));
  }, [navigation]);
  // ========================================================================
  const passwordRef = useRef();
  const EmailRef = useRef();
  const [navParams, setNavParams] = useState(route.params);

  useEffect(() => {
    passwordRef.current?.clear(), EmailRef.current?.clear();
    setState(state => ({
      ...state,
      fields: LoginFormState,
      loginBtnDisable: false,
    }));
    // console.log('i am coming fro mregistration please set popup here ');
    navParams?.registerStatus &&
      (messageRef.current?.show(
        <CustomAlert textColor={colors.primary} text={reg_done} />,
        alertTiming,
      ),
      setNavParams(route.params));
    return () => {
      const statusbar = route.params;
      setNavParams(statusbar);
      navParams?.resetCompleted &&
        (passwordRef.current?.clear(),
        EmailRef.current?.clear(),
        setState(state => ({
          ...state,
          fields: LoginFormState,
          loginBtnDisable: false,
        })));
      // if user is coming from register and get successed
      // console.log('REGISTRATION STATUS');
      // console.log(navParams);
      // navParams?.registerStatus &&
      //   (messageRef.current?.show(
      //     <CustomAlert text="Registration Successful. Please Login" />,
      //     alertTiming,
      //   ),
      //   setNavParams(route.params));
    };
  }, [route.params, navigation]);
  // =====================================

  // function _configureGoogleSignIn() {
  //   GoogleSignin.configure({
  //     offlineAccess: false,
  //   });
  // }
  return (
    <>
      <SafeAreaView style={{flex: 1}}>
        <Block style={{borderWidth: 0, backgroundColor: colors.primary}}>
          {/* Image */}
          <Block flex={false} height={sizes.getHeight(25)} center middle>
            <Image
              source={icons.red_logo}
              style={{resizeMode: 'contain', flex: 0.6}}
            />
          </Block>
          {/* {state.loginBtnDisable && (
        <ActivityIndicator size="large" color={colors.customRed} />
      )} */}

          <ScrollView
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}>
            {/* Inputs */}
            <Block
              middle
              // center
              flex={false}
              height={sizes.getHeight(25)}
              padding={[0, sizes.getWidth(7)]}
              style={{borderWidth: 0}}>
              <TextField
                inputRef={EmailRef}
                onChangeText={handleInput}
                name={'email'}
                inputStyling={styles.inputStyle}
                autoCompleteType={'email'}
                keyboardType={'email-address'}
                placeholder={email}
              />
              {!state.fields.email.isValid && (
                <Text h4 color={colors.red}>
                  {state.fields.email.errorMessage}
                </Text>
              )}
              <TextField
                inputRef={passwordRef}
                secureTextEntry={true}
                onChangeText={handleInput}
                name={'password'}
                inputStyling={styles.inputStyle}
                autoCompleteType={'password'}
                placeholder={passwd}
              />
              {!state.fields.password.isValid && (
                <Text h4 color={colors.red}>
                  {state.fields.password.errorMessage}
                </Text>
              )}
              {/* remember Me */}
              <Block
                padding={[0, sizes.getWidth(6), 0, sizes.getWidth(6)]}
                center
                middle
                // style={{borderWidth:1}}
                flex={false}>
                <SquareButton
                  disabled={state.loginBtnDisable}
                  onPress={handleSubmit}
                  // onPress={() => navigation.navigate('Home')}
                  bgColor={
                    state.loginBtnDisable ? colors.gray : colors.spaceGreen
                  }
                  textStyle={{
                    fontSize: iphone6s ? 12 : 18,
                  }}
                  textColor={colors.primary}>
                  {login}
                </SquareButton>
              </Block>
            </Block>

            <Block
              margin={[sizes.getHeight(3), 0, 0, 0]}
              padding={[0, sizes.getWidth(5), 0, sizes.getWidth(5)]}
              row
              style={{justifyContent: 'space-between'}}>
              <Button
                center
                row
                style={{width: sizes.getWidth(50), borderWidth: 0}}>
                <Block
                //  style={{borderWidth: 1, width: sizes.getWidth(3),flex:0}}
                >
                  <CheckBox
                    lineWidth={1}
                    hideBox={false}
                    boxType={'square'}
                    tintColor={colors.gray2}
                    onCheckColor={colors.primary}
                    onFillColor={colors.customRed}
                    onTintColor={'transparent'}
                    animationDuration={0.5}
                    // disabled={false}
                    onAnimationType={'bounce'}
                    offAnimationType={'stroke'}
                    // value={state.acceptTerm}
                    value={checkbox}
                    // style={{width:sizes.getWidth(2), height:sizes.getHeight(5)}}
                    onChange={() => setCheckbox(!checkbox)}
                  />
                </Block>
                <Block flex={4}>
                  <Text h4 color={colors.gray2}>
                    {remember}
                  </Text>
                </Block>
              </Button>
              <Button
                onPress={() => navigation.navigate(NAV.RESET)}
                center
                middle>
                <Text
                  h4
                  // style={{
                  //   // fontSize: sizes.customFont(Platform.OS === 'android' ? 17 : 23),
                  // }}
                  color={colors.gray2}>
                  {forget}
                </Text>
              </Button>
            </Block>

            {/* Social Login */}
            <Block
              margin={[sizes.getHeight(3), 0, 0, 0]}
              padding={[0, sizes.getWidth(7), 0, sizes.getWidth(7)]}
              // style={{borderWidth:1}}
              flex={4}>
              {/* or connet with */}
              <Block>
                <Text
                  style={{
                    fontSize: iphone6s ? 12 : 18,
                  }}
                  color={colors.gray2}>
                  {orConnectWith}
                </Text>
              </Block>
              {/* social buttons */}
              <Block
                middle
                margin={[sizes.getHeight(2), 0, 0, 0]}
                row
                style={{justifyContent: 'space-between'}}>
                {/* ========================== */}
                <SquareButton
                  bw
                  onPress={loginWithFacebook}
                  height={sizes.getHeight('7')}
                  withImage
                  source={icons.facebook}
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderWidth: 1,
                    // width="100%",
                  }}
                  width={'46%'}>
                  <Text
                    style={{
                      fontSize: iphone6s ? 12 : 18,
                    }}>
                    {fb}
                  </Text>
                </SquareButton>
                <SquareButton
                  onPress={loginWithGoogle}
                  height={sizes.getHeight('7')}
                  withImage
                  source={icons.google}
                  style={{justifyContent: 'center', alignItems: 'center'}}
                  width={'46%'}>
                  <Text
                    style={{
                      fontSize: iphone6s ? 12 : 18,
                    }}>
                    {google}
                  </Text>
                </SquareButton>
              </Block>
              {/* ========================== */}
              <Block center>
                <SquareButton
                  onPress={() => (
                    navigation.navigate(NAV.SIGNUP),
                    passwordRef.current?.clear(),
                    EmailRef.current?.clear(),
                    setState(state => ({
                      ...state,
                      fields: LoginFormState,
                      // loginBtnDisable: false,
                    }))
                  )}
                  noBorder
                  width={'90%'}>
                  <Text color={colors.gray}>{dontHav}</Text>
                </SquareButton>
              </Block>
              {/* ============================ */}
            </Block>
          </ScrollView>
          <Toast
            ref={messageRef}
            style={{
              backgroundColor: colors.customRed,
              width: sizes.getWidth(100),
              borderRadius: 0,
            }}
            positionValue={sizes.getDimensions.height}
            fadeInDuration={200}
            fadeOutDuration={100}
            opacity={1}
          />
          {isLoading && <ActivitySign />}
        </Block>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  inputStyle: {
    borderBottomColor: colors.gray,
    borderBottomWidth: 2,
    width: '100%',
    textAlign: 'left',
    // borderWidth: 1,
    padding: 0,
    marginBottom: sizes.getHeight(2),
    height: sizes.getHeight(6),
  },
});
