import React, {useState, useEffect} from 'react';
import {Block, Text, ChatButton, Button} from 'components';
import {TabView, SceneMap, TabBar, ScrollPager} from 'react-native-tab-view';
import {About, Vacancies, Media, Contact} from 'screens/CompanyProfile';
import {colors, sizes} from 'styles/theme';
import {useSelector} from 'react-redux';
import {LANG_AR} from 'redux/constants';
import {WORDS} from 'utils';
import {StyleSheet, Dimensions, Platform} from 'react-native';
import {VACANCIES, MEDIA, CONTACT} from 'utils/LanguageHandler';

const TabNavigatorWrapper = props => {
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width <= 375;
  const {navigation, loader} = props;
  const {currentLN} = useSelector(state => state.userInfo);
  const job_status = useSelector(
    state => state.getLists?.currentJobStatus?.saved_status,
  );
  // console.log('////////////////////////////');
  // console.log(loader);
  // console.log('////////////////////////////');
    const [update,setUpdate] = useState(false)
  // useEffect(() => {
    
  // }, [s]);

  const AR = currentLN === LANG_AR;

  const ab = AR ? WORDS.ABOUT_AR : WORDS.ABOUT;
  const cn = AR ? WORDS.CONTACT_AR : WORDS.CONTACT;
  const va = AR ? WORDS.VACANCIES_AR : WORDS.VACANCIES;
  const med = AR ? WORDS.MEDIA_AR : WORDS.MEDIA;
  const [name, setName] = useState({about: ab, vacancies: va, contact: cn});

  useEffect(() => {
    setName({about: ab, vacancies: va, contact: cn});
    return () => setName({about: ab, vacancies: va, contact: cn});
  }, [currentLN]);

  const [index, setIndex] = React.useState(0);
  const [routes] = useState([
    {key: 'about', title: name.about},
    {key: 'vacancies', title: va},
    {key: 'media', title: med},
    {key: 'contacts', title: cn},
  ]);

  // const renderScene = SceneMap({
  //   about: () => <About />,
  //   contacts: () => <Contact />,
  //   media: () => <Media />,
  //   vacancies: () => <Vacancies navigation={navigation} />,
  // });

  // const renderTabBar = props => (
  //   <TabBar
  //     {...props}
  //     indicatorStyle={{backgroundColor: colors.brown}}
  //     style={{
  //       backgroundColor: 'transparent',
  //       elevation: 0,
  //     }}
  //     indicatorStyle={{
  //       borderWidth: 2.2,
  //       borderColor: colors.spaceGreen,
  //       width: '10%',
  //       alignSelf: 'center',
  //       marginLeft: '5%',
  //       borderTopLeftRadius: sizes.getWidth(10),
  //       borderTopRightRadius: sizes.getWidth(10),
  //     }}
  //     activeColor={colors.spaceGreen}
  //     inactiveColor={colors.black}
  //     labelStyle={{
  //       textTransform: 'capitalize',
  //       fontSize: sizes.withScreen(0.01),
  //     }}
  //   />
  // );

  // ====
  let AB = AR ? 'حول' : WORDS.ABOUT;
  let VAC = AR ? WORDS.VACANCIES_AR : VACANCIES;
  let MED = AR ? WORDS.MEDIA_AR : MEDIA;
  let CON = AR ? WORDS.CONTACT_AR : CONTACT;
  let [currentTab, setCurrentTab] = useState(AB);

  useEffect(() => {
    setCurrentTab(AB);
    return () => currentTab;
  }, [currentLN]);

  const changeTab = current => {
    switch (current) {
      case AB:
        return setCurrentTab(AB);
      // return;
      case VAC:
        return setCurrentTab(VAC);
      // return;
      case MED:
        return setCurrentTab(MED);
      // return;
      case CON:
        return setCurrentTab(CON);
      // return;
      default:
        return setCurrentTab(AB);
    }
  };
  // console.log('=====');
  // console.log(currentTab);
  return (
    <Block>
      {/* <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        // initialLayout={()=><Contact />}
        // renderTabBar={renderTabBar}
        renderTabBar={() => (
          <TabBar
            {...props}
            indicatorStyle={{backgroundColor: colors.brown}}
            style={{
              backgroundColor: 'transparent',
              elevation: 0,
            }}
            indicatorStyle={{
              borderWidth: 2.2,
              borderColor: colors.spaceGreen,
              width: '10%',
              alignSelf: 'center',
              marginLeft: '5%',
              borderTopLeftRadius: sizes.getWidth(10),
              borderTopRightRadius: sizes.getWidth(10),
            }}
            activeColor={colors.spaceGreen}
            inactiveColor={colors.black}
            labelStyle={{
              textTransform: 'capitalize',
              fontSize: sizes.withScreen(0.01),
            }}
          />
        )}
      /> */}
      {/* <ChatButton onPress={() => navigation.navigate('Messages')} /> */}
      <Block style={styles.headingCon}>
        <Button
          onPress={() => changeTab(AB)}
          style={{
            ...styles.tabBtn,
            borderBottomWidth: currentTab === AB ? 5 : 0,
          }}>
          <Text
            style={{
              ...styles.tabBtnText,
              fontSize: sizes.customFont(iphone6s ? 8 : 13),
            }}>
            {AB}
          </Text>
        </Button>
        <Button
          onPress={() => changeTab(VAC)}
          style={{
            ...styles.tabBtn,
            borderBottomWidth: currentTab === VAC ? 5 : 0,
          }}>
          <Text
            style={{
              ...styles.tabBtnText,
              fontSize: sizes.customFont(iphone6s ? 8 : 13),
            }}>
            {VAC}
          </Text>
        </Button>
        <Button
          onPress={() => changeTab(MED)}
          style={{
            ...styles.tabBtn,
            borderBottomWidth: currentTab === MED ? 5 : 0,
          }}>
          <Text
            style={{
              ...styles.tabBtnText,
              fontSize: sizes.customFont(iphone6s ? 8 : 13),
            }}>
            {MED}
          </Text>
        </Button>
        <Button
          onPress={() => changeTab(CON)}
          style={{
            ...styles.tabBtn,
            borderBottomWidth: currentTab === CON ? 5 : 0,
          }}>
          <Text
            style={{
              ...styles.tabBtnText,
              fontSize: sizes.customFont(iphone6s ? 8 : 13),
            }}>
            {CON}
          </Text>
        </Button>
      </Block>
      <Block>
        {currentTab === AB && <About />}
        {currentTab === VAC && (
          <Vacancies update={setUpdate} loader={loader} navigation={navigation} />
        )}
        {currentTab === MED && <Media {...navigation} />}
        {currentTab === CON && <Contact />}
      </Block>
    </Block>
  );
};

const styles = StyleSheet.create({
  headingCon: {
    flex: 0,
    height: sizes.getHeight(5),
    width: '100%',
    borderBottomWidth: 0.4,
    alignItems: 'center',
    marginTop: sizes.getHeight(0.3),
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: sizes.getWidth(3),
  },
  tabBtn: {
    // borderWidth: 1,

    height: '100%',
    justifyContent: 'center',
    // flex: 1,
    alignItems: 'center',
    paddingHorizontal: sizes.getWidth(3),
    borderBottomColor: colors.spaceGreen,
  },
  tabBtnText: {
    color: colors.spaceGreen,
    fontSize: sizes.h4,
  },
});

export default TabNavigatorWrapper;
