import React, {useState, useRef} from 'react';
import {Block, Text, CustomHeader, Button, CustomAlert} from 'components';
import {colors, sizes} from 'styles/theme';
import {Image, StyleSheet, ActivityIndicator, Linking} from 'react-native';
import * as icons from 'assets/icons';
import {useSelector, useDispatch} from 'react-redux';
import {companyLogo} from 'redux/apiConstants';
import {FollowingJob} from 'redux/actions';
import {FOLLOW, LANG_AR} from 'redux/constants';
import Toast from 'react-native-easy-toast';
import {WORDS} from 'utils';
import {FB} from 'utils/LanguageHandler';

const PortfolioHeader = navigation => {
  const LINKEDIN = 'LINKEDIN';
  const TWITTER = 'TWITTER';
  const FACEBOOK = 'FACEBOOK';
  const details = useSelector(state => state.getLists?.currentCompStatus);
  const {status} = useSelector(state => state.auth);
  const {currentLN} = useSelector(state => state?.userInfo);
  const AR = currentLN === LANG_AR;
  const dispatch = useDispatch();
  const id = useSelector(state => state?.auth?.userBasicProfile?.id);

  // console.log('========********************==========');
  // console.log(details);
  // console.log('========********************==========');

  const [isLoading, setIsLoading] = useState(false);
  const followComp = async details => {
    setIsLoading(true);
    const result = await FollowingJob(details.id, id, err => {
      console.log('ERROR IN FAV');
      console.log(err);
      setIsLoading(false);
    });
    console.log('===========RESULT Follow');
    console.log(result.toString());

    const isFollowing = result === 1 ? true : false;
    result && dispatch({type: FOLLOW, payload: result});
    setIsLoading(false);
  };
  const messageRef = useRef();
  const compProfile = (name, link) => {
    // console.log(name);
    console.log(link);

    let TwitterApplink = name === TWITTER && `twitter://timeline/qatargas`;
    let fbAppLink = name === FACEBOOK && `fb://qatargas/`;
    let linkedInApplink = name === LINKEDIN && `linkedin://company/qatargas`;

    // -----------
    const fb = `fb://${link}`;
    Linking.canOpenURL(link)
      .then(supported =>
        Linking.openURL(
          supported
            ? (name === FACEBOOK && link) ||
                (name === TWITTER && link) ||
                (name === LINKEDIN && link)
            : link,
        ),
      )
      .catch(err => {
        messageRef.current?.show(
          <CustomAlert
            textColor={colors.spaceGreen}
            text="Unable to Reached. Try Again later."
          />,
          3500,
        );
      });

    // ------------------------------------------------
    // Linking.canOpenURL(twitter).then(supported => {
    //   if (supported) {
    //     Linking.openURL(twitter);
    //   } else {
    //     // console.log('not');
    //     messageRef.current?.show(
    //       <CustomAlert
    //         textColor={colors.spaceGreen}
    //         text="Unable to Reached. Try Again later."
    //       />,
    //       3500,
    //     );
    //   }
    // });
  };

  // const follow = AR ? WORDS.FOLLOW_AR : WORDS.FOLLOW;
  // const unfollow = AR ? WORDS.UNFOLLOW_AR : WORDS.UNFOLLOW;

  return (
    <Block
      center
      padding={[0, 0, 10, 0]}
      // flex={2}
      flex={false}
      height={sizes.getHeight(27)}
      style={{backgroundColor: colors.spaceGreen}}>
      {/* <CustomHeader /> */}

      <Toast
        ref={messageRef}
        style={{
          backgroundColor: colors.primary,
          width: sizes.getWidth('100%'),
          borderRadius: 0,
          // borderWidth: 1,
          elevation: 5,
          // borderColor: colors.sp,
          zIndex: 100,
        }}
        positionValue={sizes.getDimensions.height}
        fadeInDuration={200}
        fadeOutDuration={100}
        opacity={1}
      />
      <Block
        center
        middle
        style={{
          // borderWidth: 1,
          backgroundColor: colors.primary,
          padding: sizes.screenSize * 0.005,
          marginTop: sizes.getHeight(2),
          flex: 0,
          height: sizes.screenSize * 0.09,
          width: sizes.screenSize * 0.09,
          borderRadius: sizes.screenSize * 3,
          overflow: 'hidden',
        }}>
        <Image
          source={{uri: companyLogo + details?.logo}}
          style={{
            resizeMode: 'contain',
            // borderWidth: 1,
            flex: 1,
            width: '100%',
            height: '100%',
            // width: sizes.getWidth(13),
            // height: sizes.getHeight(1),
          }}
        />
      </Block>
      <Block center middle style={{borderWidth: 0}}>
        <Text h2 color={colors.primary}>
          {/* {currentLN === 'EN' ? details?.name_en : details?.name_ar} */}
          {currentLN === LANG_AR ? details?.name_ar : details?.name_en}
        </Text>
      </Block>
      <Block
        center
        // middle
        style={{
          // flexDirection: status ? 'row' : '',
          // borderWidth: 1,
          width: '100%',
        }}>
        <Block
          row
          // flex={2}
          center
          middle
          // bottom
          style={{
            // borderWidth: 1,
            height: sizes.getHeight(10),
            width: sizes.getWidth(100),
          }}>
          {details?.facebook && (
            <Button
              onPress={() => compProfile(FACEBOOK, details?.facebook)}
              center
              middle
              style={styles.btnStyle}>
              <Image
                source={icons.facebookIcon}
                style={{resizeMode: 'contain', flex: 0.4}}
              />
            </Button>
          )}
          {details?.twitter && (
            <Button
              disabled={!details.linkedin}
              onPress={() => compProfile(TWITTER, details?.twitter)}
              center
              middle
              style={styles.btnStyle}>
              <Image source={icons.twitter} style={styles.logoStyle} />
            </Button>
          )}
          {details?.linkedin && (
            <Button
              onPress={() => compProfile(LINKEDIN, details?.linkedin)}
              center
              middle
              style={styles.btnStyle}>
              <Image source={icons.linkedin} style={styles.logoStyle} />
            </Button>
          )}
        </Block>
      </Block>

      {/* -------------------------------------------------------- */}
      {status && (
        <Block middle center style={styles.followCon}>
          {!isLoading ? (
            <Button
              // disabled={details.follow_status === }
              middle
              center
              onPress={() => followComp(details)}
              style={{
                ...styles.followBtn,
                backgroundColor:
                  details?.follow_status === 1 ? colors.newGray : 'transparent',
                borderWidth: details?.follow_status === 1 ? 0 : 1,
                borderColor:
                  details?.follow_status === 1 ? 'transparent' : colors.newGray,
              }}>
              {/* <Text h4 color={colors.brown} bold>
                {details?.follow_status === 1 ? unfollow : follow}
              </Text> */}
              <Image
                source={icons.bell}
                style={{
                  resizeMode: 'contain',
                  flex: 1,

                  tintColor:
                    details?.follow_status === 1
                      ? colors.primary
                      : colors.newGray,
                }}
              />
            </Button>
          ) : (
            <Block
              flex={false}
              middle
              center
              style={{
                borderWidth: 0.7,
                borderColor: colors.primary,
                borderStyle: 'dashed',
                width: '70%',
                height: '80%',
                borderRadius: sizes.getWidth(1.5),
              }}>
              <ActivityIndicator color={colors.newGray} />
            </Block>
          )}
        </Block>
      )}
      {/* -------------------------------------------------------- */}
      {/* <Block
        style={{
          // borderWidth: 1,
          position: 'absolute',
          // top: -sizes.getHeight(5),
          width: '100%',
          zIndex: 1000,
        }}> */}
      {/* <Toast
        ref={messageRef}
        style={{
          backgroundColor: colors.customRed,
          width: sizes.getWidth(100),
          borderRadius: 2,
        }}
        positionValue={sizes.getHeight('100%') / 4.7}
        fadeInDuration={200}
        fadeOutDuration={100}
        opacity={1}
      /> */}
    </Block>
    // </Block>
    // <Block />
  );
};

const styles = StyleSheet.create({
  btnStyle: {
    borderWidth: 0.7,
    marginHorizontal: sizes.getWidth(1.5),
    borderColor: colors.primary,
    width: sizes.screenSize * 0.03,
    height: sizes.screenSize * 0.03,

    borderRadius: sizes.withScreen(0.07),
  },
  logoStyle: {
    resizeMode: 'contain',
    flex: 0.9,
    tintColor: colors.primary,
  },
  followCon: {
    // borderWidth: 1,
    width: sizes.getWidth(15),
    height: sizes.getHeight(6.5),
    position: 'absolute',
    bottom: sizes.getHeight(2),
    right: sizes.getWidth(2),
  },
  followBtn: {
    // borderWidth: 1,
    height: sizes.screenSize * 0.04,
    width: sizes.screenSize * 0.04,
    // width: sizes.getWidth(30),
    borderRadius: sizes.screenSize * 0.04,
    // backgroundColor: colors.primary,
    borderWidth: 1,
    paddingHorizontal: sizes.getWidth(4),
  },
});

export default PortfolioHeader;
