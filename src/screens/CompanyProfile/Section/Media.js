import React, {useEffect, useState} from 'react';
import {Block, Text, Button} from 'components';
import {media, NAV} from 'utils';
import {Image, FlatList, Modal} from 'react-native';
import {sizes, colors} from 'styles/theme';
import {useSelector} from 'react-redux';
import {LANG_AR} from 'redux/constants';
import * as images from 'assets/images';
import {ImageViewer} from './ImageViewer';

const Media = navigation => {
  const {currentLN} = useSelector(state => state.userInfo);
  const details = useSelector(state => state.getLists?.currentCompStatus);
  const AR = currentLN === LANG_AR;
  const id = useSelector(state => state.auth?.userBasicProfile?.id);
  const stateMedia = useSelector(
    state => state.getLists?.currentCompStatus?.gallery,
  );
  const imgPath = 'https://dawami.wedigits.dev/images/company_images/';
  const noMedia = 'No Media Found';
  const noMedia_ar = 'لم يتم العثور على وسائط';
  const nothingFound = AR ? noMedia_ar : noMedia;

  const [media, setMedia] = useState(stateMedia);
  console.log('=======MEDIA======');
  console.log(media);

  useEffect(() => {
    return () => setMedia(stateMedia);
  }, [details.jobs]);
  const [showPic, setShowPic] = useState({isShowing: false, path: null});

  const goForView = imagePath => {
    setShowPic({isShowing: true, path: imagePath});
  };

  return (
    <Block
      style={{marginTop: sizes.getHeight(3), borderWidth: 0}}
      padding={[0, sizes.getWidth(2)]}>
      {media && media.length ? (
        <FlatList
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          data={media}
          numColumns={3}
          renderItem={({item}) => {
            return (
              <Button
                onPress={() => goForView(imgPath + item.name)}
                // onPress={() => {
                //   navigation.navigate(NAV.IMAGE_VIEWER, {
                //     image: imgPath + item.name,
                //   });
                // }}
                flex={false}
                margin={[5, 5]}
                center
                middle
                height={sizes.getHeight(13)}
                style={{
                  height: sizes.getHeight(12),
                  marginHorizontal: sizes.getWidth(1),
                  borderWidth: 0.4,
                  borderStyle: 'dashed',
                  borderRadius: sizes.screenSize * 0.011,
                  overflow: 'hidden',
                  width: sizes.getWidth(29),
                }}>
                <Image
                  source={{uri: imgPath + item.name}}
                  style={{
                    resizeMode: 'contain',
                    flex: 1,
                    width: sizes.getWidth(30),
                    height: sizes.getHeight(7),
                    borderRadius: sizes.screenSize * 0.011,
                  }}
                />
              </Button>
            );
          }}
        />
      ) : (
        <Block center middle>
          <Text h4 color={colors.gray}>
            {nothingFound}
          </Text>
        </Block>
      )}

      <Modal
        onRequestClose={() => setShowPic({isShowing: false, path: null})}
        visible={showPic.isShowing}>
        <ImageViewer
          closeModal={() => setShowPic({isShowing: false, path: null})}
          path={showPic.path}
        />
      </Modal>
    </Block>
  );
};

export default Media;
