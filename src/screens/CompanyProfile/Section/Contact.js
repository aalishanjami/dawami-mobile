import React from 'react';
import {Block, Text, Button, ChatButton} from 'components';
import * as images from 'assets/images';
import * as icons from 'assets/icons';
import {Image, Platform, Dimensions} from 'react-native';
import {sizes, colors} from 'styles/theme';
import {useSelector} from 'react-redux';
import {LANG_AR} from 'redux/constants';
import {WORDS} from 'utils';

const Addressbar = props => {
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width <= 375;
  const {image, text} = props;
  return (
    <Block
      center
      middle
      flex={false}
      height={sizes.getHeight(9)}
      style={{
        // borderWidth: 1,
        width: '100%',
        marginVertical: sizes.getHeight(0.7),
        borderRadius: sizes.withScreen(0.004),
        elevation: 10,
        backgroundColor: colors.primary,
      }}
      row
      center
      middle>
      <Block center middle margin={[0, sizes.getWidth(2), 0, 0]} flex={false}>
        <Image
          source={image}
          style={{
            tintColor: colors.gray3,
            resizeMode: 'contain',
            width: sizes.getWidth(3),
          }}
        />
      </Block>
      <Text
        numberOfLines={1}
        ellipsizeMode="tail"
        style={{
          fontWeight: '200',
          fontSize: sizes.customFont(iphone6s ? 13 : 18),
        }}
        color={colors.gray3}>
        {text}
      </Text>
    </Block>
  );
};

const Contact = () => {
  // console.log('============CONTACTS============');
  // console.log(details.phone);
  const details = useSelector(state => state.getLists?.currentCompStatus);
  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;
  const noDataFound = AR ? WORDS.NoDataFound_AR : WORDS.NoDataFound;
  return (
    <Block>
      <Block
        center
        middle
        flex={false}
        height={sizes.getHeight(20)}
        style={{overflow: 'hidden'}}>
        <Image
          source={images.map}
          style={{resizeMode: 'cover', width: '100%', height: '100%'}}
        />
      </Block>
      {/* Contact Info block started */}
      <Block
        flex={false}
        style={{
          // borderWidth: 1,
          position: 'absolute',
          top: sizes.getHeight(12),
          left: sizes.getWidth(7.5),
          zIndex: 10,
          width: sizes.getWidth(85),
          // height: sizes.getHeight(35),
          // backgroundColor: 'red',
        }}>
        <Block center>
          {details?.address && (
            <Addressbar
              image={icons.location}
              // text={'123 Al Shamal Road Gharafah, Doha QATAR'}
              text={details?.address}
            />
          )}
          {details?.phone && (
            <Addressbar image={icons.phone} text={details?.phone} />
          )}
          {details.website && (
            <Addressbar
              image={icons.message}
              text={details?.website || noDataFound}
            />
          )}
          {!details.address && !details.phone && !details.website && (
            <Addressbar text={noDataFound} />
          )}
        </Block>
      </Block>
      {/* message */}
    </Block>
  );
};

export default Contact;
