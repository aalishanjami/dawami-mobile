import React, {useRef, useState, useEffect} from 'react';
import {
  Block,
  Text,
  Button,
  ChatButton,
  CustomAlert,
  ActivitySign,
} from 'components';
import {vacancies, WORDS, sortByProperty} from 'utils';
import {sizes, colors} from 'styles/theme';
import {
  Image,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  RefreshControl,
  Platform,
  Dimensions,
} from 'react-native';
import * as icons from 'assets/icons';
import {useSelector, useDispatch} from 'react-redux';
import {SAVE_JOB_STATUS, LANG_AR, CURRENT_COMP} from 'redux/constants';
import {
  AuthUserViewJob,
  FavoriteRequest,
  ViewCompanyDetailAsNormal,
  ViewCompanyDetailAsAuth,
} from 'redux/actions';
import Toast from 'react-native-easy-toast';
import {VacanciesCard} from './VacanciesCard';
const Vacancies = props => {
  
  const {navigation, loader,update} = props;
  // console.log('////////////////////////////');
  // console.log(props);
  // console.log('////////////////////////////');
  const details = useSelector(state => state.getLists?.currentCompStatus);

  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;
  const {status} = useSelector(state => state.auth);
  const id = useSelector(state => state.auth?.userBasicProfile?.id);
  const dispatch = useDispatch();
  const messageRef = useRef();
  // console.log('==============Vacancies=============');
  // console.log(details.jobs);

  const [isWaiting, setIsWaiting] = useState(false);
  // const doFav = async item => {
  //   setIsWaiting(true);
  //   const result = await FavoriteRequest(item.id, id, err => {
  //     console.log('ERROR IN FAV');
  //     console.log(err);
  //     setIsWaiting(false);
  //   });
  //   console.log('====RESULT===');
  //   console.log(result);
  //   // dispatch(result)
  //   // setIsWaiting(false);
  // };
  const updated_by_list = details?.jobs?.sort(sortByProperty('updated_at'));
  console.log('=====VACANCIES=======');
  console.log(details.jobs);

  const [refreshing, setRefreshing] = useState(false);

  const onRefresh = async () => {
    loader(true);
    if (status) {
      const result = await ViewCompanyDetailAsAuth(details.id, id, err => {
        console.log('err');
        console.log(err);
        loader(fale);
      });
      dispatch({
        type: CURRENT_COMP,
        payload: result,
      });
      loader(false);

      // navigation.navigate(NAV.COMPANY_DETAILS);
    } else {
      // console.log(companyDetail);
      const result = await ViewCompanyDetailAsNormal(details.id, err => {
        console.log('err');
        console.log(err);
        loader(false);
      });
      // console.log('=====================================');
      // console.log('**************NON AUTH USER********************');
      // console.log(result);
      // console.log('=====================================');
      dispatch({
        type: CURRENT_COMP,
        payload: result,
      });
      loader(false);

      // navigation.navigate(NAV.COMPANY_DETAILS);
    }
  };
  return (
    <Block padding={[20, 0]}>
      {details.jobs && details.jobs.length ? (
        <FlatList
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          // data={listOfJobs}
          data={updated_by_list.reverse()}
          keyExtractor={(item, index) => {
            return index.toString();
          }}
          renderItem={(item, index) => {
            // console.log('item');
            // console.log(item);
            return (
              <VacanciesCard
                showLoading={e => loader(e)}
                // key={index}
                item={item}
                navigation={navigation}
              />
            );
          }}
        />
      ) : (
        <Block middle center>
          <Text h3 color={colors.gray}>
            {AR ? WORDS.NO_JOB_AR : WORDS.NO_JOB}
          </Text>
        </Block>
      )}
      {/* {isWaiting && <ActivitySign />} */}
      {/* message */}
    </Block>
  );
};

const styles = StyleSheet.create({
  cardBtn: {
    // borderWidth: 1,
    height: sizes.getHeight(10),
    justifyContent: 'center',
  },
  loader: {
    height: '100%',
    width: '100%',
    position: 'absolute',
    top: 0,
    zIndex: 50,
    backgroundColor: colors.lightPink,
  },
});
export default Vacancies;
