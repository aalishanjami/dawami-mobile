import React, {useState, useRef, useEffect} from 'react';
import {StyleSheet, ActivityIndicator, Image, Dimensions, Platform} from 'react-native';
import * as icons from 'assets/icons';
import * as images from 'assets/images';
import {Block, Text, Button, CustomAlert} from 'components';
import {useSelector, useDispatch} from 'react-redux';
import {
  SAVE_JOB_STATUS,
  LANG_AR,
  JOB_LIST_STATE,
  CURRENT_VIEW_PROG,
  IS_FAV,
  CURRENT_COMP,
  CURRENT_COMP_UPDATE,
} from 'redux/constants';
import {
  FavoriteRequest,
  AuthUserViewJob,
  ViewCompanyDetailAsAuth,
} from 'redux/actions';
import Toast from 'react-native-easy-toast';
import {sizes, colors} from 'styles/theme';
import {NAV, WORDS} from 'utils';
import {color} from 'react-native-reanimated';

const VacanciesCard = props => {
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width <= 375;
  const {navigation, route, item, showLoading} = props;
  // const details = useSelector(state => state.getLists?.currentCompStatus);
  const currentCachedCompanyDetails = useSelector(
    state => state.getLists?.currentCompStatus,
  );
  const insideCurrentCompanyJobDetails = currentCachedCompanyDetails.jobs;

  const {currentLN} = useSelector(state => state?.userInfo);
  const AR = currentLN === LANG_AR;
  const {status} = useSelector(state => state.auth);
  const id = useSelector(state => state.auth?.userBasicProfile?.id);
  const dispatch = useDispatch();
  const [isWaiting, setIsWaiting] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const messageRef = useRef();

  //==============================================================
  // const [jobsArray, setJobsArray] = useState(vacanciesList);
  const [jobsArray, setJobsArray] = useState(insideCurrentCompanyJobDetails);
  const doFav = async item => {
    setIsWaiting(true);
    const result = await FavoriteRequest(item.id, id, err => {
      console.log('ERROR IN FAV');
      console.log(err);
      setIsWaiting(false);
    });
    console.log('result from api is ');
    console.log(result.payload);
    console.log('COmpany Job Array');
    console.log(insideCurrentCompanyJobDetails);
    let indexNumber = insideCurrentCompanyJobDetails.indexOf(item);
    console.log(indexNumber);

    let newArray = {...item, saved_status: result.payload.current_status};

    insideCurrentCompanyJobDetails.splice(
      indexNumber,
      1,
      newArray,
      //    {
      //   ...item,
      //   saved_status: result.payload.current_status,
      // }
    );
    dispatch({
      type: CURRENT_COMP,
      payload: {
        ...currentCachedCompanyDetails,
        job: insideCurrentCompanyJobDetails,
      },
    });
    setIsWaiting(false);
    // navigation.navigate(NAV.JOB_DETAILS);
  };

  // useEffect(() => {
  //   console.log('Mount');
  // }, [currentJob]);

  // console.log('=============VACANCIES CARD========');
  // console.log(item.item);

  //==============================================================

  const viewJob = async item => {
    console.log(item);
    // console.log(companyDetail);

    if (status) {
      showLoading(true);
      const result = await AuthUserViewJob(item.id, id, err => {
        console.log('error');
        // messageRef.current?.show(
        //   <CustomAlert text="Error Please Try Again." />,
        // );
        showLoading(false);
      });
      result &&
        (dispatch({
          type: SAVE_JOB_STATUS,
          payload: result,
        }),
        showLoading(false),
        navigation.navigate(NAV.JOB_DETAILS));
    } else {
      console.log('user is logged out');
      dispatch({
        type: SAVE_JOB_STATUS,
        payload: item.item,
      });
      setIsWaiting(false);
    }
  };

  const Freelancer = {name: 'Freelancer', value: '1'};
  const FullTime = {name: 'Full Time', value: '2'};
  const PartTime = {name: 'Part Time', value: '3'};

  const job_type = item.item?.employee_type;
  const freelance = 'freelance';
  const freelance_ar = 'مستقل';
  const fullTime = 'Full Time';
  const fulltime_ar = 'وقت كامل';
  const parttime = 'Part Time';
  const parttime_ar = 'دوام جزئى';

  const free = {name: freelance, name_ar: freelance_ar};
  const full = {name: fullTime, name_ar: fulltime_ar};
  const part = {name: parttime, name_ar: parttime_ar};

  const getEmployeeType = empType => {
    console.log('EMP TYPE');
    console.log(empType);
    switch (empType) {
      case Freelancer.value:
        return getEmployeeTypeByLanguage(Freelancer.name);
      case FullTime.value:
        return getEmployeeTypeByLanguage(Freelancer.name);
      case PartTime.value:
        return getEmployeeTypeByLanguage(Freelancer.name);
      default:
        return console.log('default');
    }
  };

  const getEmployeeTypeByLanguage = jobType => {
    switch (jobType) {
      case Freelancer.name:
        return free;
      case PartTime.name:
        return part;
      case FullTime.name:
        return full;
      default:
        return console.log('default');
    }
  };
  console.log('item.item.employee_type');
  console.log(item.item.employee_type);
  // console.log(getEmployeeType(item.item.employee_type));
  const empType = AR
    ? getEmployeeType(item.item.employee_type).name_ar
    : getEmployeeType(item.item.employee_type).name;
  // const jobType = getEmployeeType(Freelancer);

  // console.log('jobType');
  // console.log(jobType);

  // const likeJob = async item => {
  //   setIsWaiting(true);
  //   dispatch({type: CURRENT_VIEW_PROG, payload: item});
  //   const result = await FavoriteRequest(item.id, id, err => {
  //     messageRef.current?.show(
  //       <CustomAlert text={err} textColor={colors.primary} />,
  //       3500,
  //     );
  //     setIsWaiting(false);
  //   });
  //   console.log('RESULT OF FETCH ');
  //   console.log(result);
  //   // result && dispatch(result);
  //   setIsWaiting(false);
  // };

  return (
    <Block
      // middle
      style={{
        borderBottomWidth: 0.6,
        // borderWidth: 1,
        justifyContent: status ? 'center' : null,
        height: sizes.getHeight(iphone6s?14:11),
        marginBottom: sizes.getHeight(1),
        flex: 0,
        borderBottomColor: colors.newGray,
      }}
      row
      center
      padding={[0, sizes.getWidth(5)]}>
      <Button
        onPress={() => viewJob(item.item)}
        style={{...styles.cardBtn, borderWidth: 0, height: '100%'}}>
        <Block row middle>
          <Block
            style={{borderWidth: 0, height: sizes.getHeight(10)}}
            padding={[sizes.getHeight(1), 0, sizes.getHeight(1), 0]}
            flex={false}
            width={sizes.getWidth(80)}>
            <Text
              h3
              style={{
                textAlign: 'left',
                fontSize: sizes.customFont(iphone6s ? 13 : 18),
              }}>
              {/* {item.title} */}
              {currentLN === LANG_AR
                ? item.item.title_ar
                : item.item.title_en}
            </Text>
            <Text
              numberOfLines={1}
              ellipsizeMode="tail"
              h4
              style={{
                textTransform: 'capitalize',
                textAlign: 'left',
                borderWidth: 0,
                color: colors.gray,
              }}>
              {/* {item.item.employee_type} */}
              {empType}
              {/* {item.subTitle} */}
              {/* {currentLN === LANG_AR
                ? item.item.description_ar
                : item.item.description_en} */}
            </Text>
            <Text h4 color={colors.gray}>
              {new Date(
                item.item.updated_at.replace(/\s+/g, 'T'),
              ).toDateString()}
            </Text>
          </Block>
        </Block>
      </Button>
      {status && (
        <Button
          center
          middle
          style={{
            // borderWidth: 1,
            height: '100%',
            width: sizes.getWidth(10),
          }}
          onPress={() => doFav(item.item)}
          disabled={isWaiting}
          // padding={[10, 0, 0, 0]}
        >
          {!isWaiting ? (
            <Image
              source={
                // ? // item.item?.saved_status === 1
                item.item.saved_status === 1
                  ? icons.filledHeart
                  : icons.unfilledHeart
                // icons.unfilledHeart
              }
              style={{
                resizeMode: 'contain',
                width: sizes.getWidth(9),
                height: sizes.getHeight(5),
                tintColor: colors.spaceGreen,
              }}
            />
          ) : (
            <ActivityIndicator color={colors.customRed} />
          )}
        </Button>
      )}
      <Toast
        ref={messageRef}
        style={{
          backgroundColor: colors.customRed,
          width: sizes.getWidth(100),
          borderRadius: 2,
        }}
        positionValue={sizes.getDimensions.height}
        fadeInDuration={200}
        fadeOutDuration={100}
        opacity={1}
      />
    </Block>
  );
};

export {VacanciesCard};

const styles = StyleSheet.create({});
