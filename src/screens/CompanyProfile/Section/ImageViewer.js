import React, {useEffect} from 'react';
import {StyleSheet, Image} from 'react-native';
import {Block, Text, Button} from 'components';
import {sizes, colors} from 'styles/theme';
import * as icons from 'assets/icons';
import {NAV} from 'utils';

const ImageViewer = props => {
  // useEffect(() => {
  //   const path = route.params?.image;
  //   console.log(path);
  // });

  return (
    <Block middle center color={colors.black}>
      <Button
        onPress={props.closeModal}
        style={{
          position: 'absolute',
          right: sizes.getWidth(2),
          top: sizes.getHeight(2),
          justifyContent: 'center',
          width: sizes.screenSize * 0.04,
          height: sizes.screenSize * 0.04,
          alignItems: 'center',
          // borderWidth: 1,
          borderColor: colors.primary,
          zIndex: 1000,
        }}>
        <Image
          source={icons.close_white}
          style={{
            resizeMode: 'contain',
            width: sizes.getWidth(3),
            height: sizes.getHeight(6),
          }}
        />
      </Button>
      <Image
        source={{uri: props.path}}
        style={{
          width: '100%',
          height: sizes.getHeight('100%'),
          resizeMode: 'contain',
        }}
      />
    </Block>
  );
};

export {ImageViewer};

const styles = StyleSheet.create({});
