import React, {useEffect} from 'react';
import {Block, Text} from 'components';
import {Image, ScrollView, Platform, Dimensions} from 'react-native';
import * as icons from 'assets/icons';
import * as images from 'assets/images';
import {sizes, colors} from 'styles/theme';
import {useSelector} from 'react-redux';
import {LANG_AR} from 'redux/constants';

const About = () => {
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width <= 375;
  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;
  const details = useSelector(state => state.getLists?.currentCompStatus);

  // console.log('===========ABOUT');
  // console.log(details);

  useEffect(() => {
    return () => null;
  }, [details.jobs]);

  return (
    <Block padding={[0, sizes.getWidth(5)]}>
      <Block
        center
        row
        style={{
          borderBottomWidth: 0.5,
          borderBottomColor: colors.gray,
        }}>
        <Image
          source={icons.location}
          style={{
            resizeMode: 'contain',
            marginRight: sizes.getWidth(1),
            width: sizes.getWidth(3),
            marginTop: sizes.getHeight(2),
            tintColor: colors.gray,
          }}
        />
        <Text
          color={colors.gray}
          style={{
            fontSize: sizes.customFont(iphone6s ? 10 : 14),
            marginTop: sizes.getHeight(2),
            marginLeft:sizes.getWidth(1)
          }}>
          {/* Al Sadd */}
          {details.address || 'No Address Provided '}
        </Text>
      </Block>
      <Block margin={[sizes.getHeight(2), 0, 0, 0]} flex={9}>
        <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
          <Text
            style={{
              fontSize: sizes.customFont(iphone6s ? 10 : 14),
            }}
            color={colors.gray}>
            {AR ? details?.about_us_ar : details?.about_us_en}
          </Text>
        </ScrollView>
      </Block>
    </Block>
  );
};

export default About;
