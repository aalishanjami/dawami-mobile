import React, {useState, useEffect} from 'react';
import {
  Block,
  Text,
  CustomHeader,
  ActivitySign,
} from 'components';
import {Header, TabWrapper} from 'screens/CompanyProfile';
import {colors} from 'styles/theme';
import {SafeAreaView} from 'react-native';
const CompanyPortfolio = props => {
  const {navigation, route} = props;

  
  
  const [showLoader, setShowLoader] = useState(false);
  return (
    <>
      <SafeAreaView
        style={{flex: 0}}
        style={{backgroundColor: colors.spaceGreen}}
      />
      <Block>
        <CustomHeader navigation={navigation} />
        <Header />
        <Block flex={5}>
          <TabWrapper loader={e => setShowLoader(e)} navigation={navigation} />
        </Block>
        {showLoader && <ActivitySign />}
      </Block>
    </>
  );
};

export default CompanyPortfolio;
