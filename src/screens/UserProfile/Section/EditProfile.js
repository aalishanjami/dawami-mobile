import React, {useState, useRef, forwardRef, useEffect} from 'react';
import {
  Block,
  Text,
  Button,
  TextField,
  CustomAlert,
  ActivitySign,
} from 'components';
import {sizes, colors} from 'styles/theme';
import {
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  Animated,
  StyleSheet,
  Alert,
} from 'react-native';
import * as images from 'assets/images';
import * as icons from 'assets/icons';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import {
  ContactInfo,
  PreferredJobs,
  BasicInfo,
  WorkInfo,
  EducationalInfo,
  Skills,
  LangInfo,
  CertInfo,
  ReferencesInfo,
  Hobbies,
} from './Merger';
import Animator from '../Animator';
import Toast from 'react-native-easy-toast';
import {useSelector, useDispatch} from 'react-redux';
import {GetProfileInfo} from 'redux/actions';
import {SafeAreaView} from 'react-native-safe-area-context';

const EditProfile = props => {
  const {navigation} = props;
  const dispatch = useDispatch();
  const userInfo = useSelector(state => state.auth.userBasicProfile);
  const userData = useSelector(state => state.userInfo?.userData);
  const compRef = React.createRef();
  const [viewBasicInfo, setViewBasicInfo] = useState(false);
  // console.log(userData.user ? userData.user + 'sab p' : 'KSFKLSJDS');
  const {id, name} = userInfo;
  const messageRef = useRef();

  var uri = '';
  var imagetype = '';

  const [showMessage, setShowMessage] = useState(false);
  const [message, setMessage] = useState('');
  const [isWaiting, setIsWaiting] = useState(false);
  // const [name, setName] = useState(false);
  // const [imagetype, setImagetype] = useState(false);
  // const [uri, setUri] = useState(false);
  const options = {
    title: 'Select Avatar',
    customButtons: [{name: 'fb', title: 'Choose Photo from Facebook'}],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  const messageHandler = message => {
    messageRef.current?.show(
      <CustomAlert textColor={colors.primary} text={message} />,
      3500,
    );
  };
  const ActivityHandler = status => {
    // console.log(status)
    setIsWaiting(status);
  };

  useEffect(() => {
    async function fetchProfileRecords() {
      setIsWaiting(true);
      const result = await GetProfileInfo(userInfo.id, err => {
        return (
          console.log('error'),
          setIsWaiting(false),
          Alert.alert(
            'Error In Fetch Records',
            'Sorry We Cannot Go Further Please Check You Internet Connectivity first',
            [
              {
                text: 'Go To Profile',
                onPress: () =>
                  navigation.navigate('Profile', {fetchError: true}),
              },
              {
                text: 'Back To Home',
                onPress: () => navigation.navigate('Home', {fetchError: true}),
              },
            ],
          )
        );
      });
      result && (dispatch(result), setIsWaiting(false));
      setIsWaiting(false);
    }
    if (!userData?.success) {
      fetchProfileRecords();
    }

    return () => null;
  }, ['']);

  const pickImage = async () => {
    ImagePicker.launchImageLibrary(options, response => {
      console.log('Response=', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        imagetype = response.type;
        name = response.fileName;
        uri = response.uri;
        addImage();
      }
    });
  };

  async function fetchProfileRecords() {
    setIsWaiting(true);
    const result = await GetProfileInfo(id, err => {
      return (
        console.log('error'),
        setIsWaiting(false),
        Alert.alert(
          'Error In Fetch Records',
          'Sorry We Cannot Go Further Please Check You Internet Connectivity first',
          [
            {
              text: 'Go To Profile',
              onPress: () => navigation.navigate('Profile', {fetchError: true}),
            },
            {
              text: 'Back To Home',
              onPress: () => navigation.navigate('Home', {fetchError: true}),
            },
          ],
        )
      );
    });
    result && (dispatch(result), setIsWaiting(false));
    setIsWaiting(false);
  }

  const addImage = async () => {
    try {
      setIsWaiting(false);
      let form = new FormData();

      var file = {
        uri: uri,
        type: imagetype,
        name: name,
      };
      form.append('image', file);

      console.log('DATA BEFORE SUBMITTION========');
      console.log(form);
      console.log(file);
      const response = await axios
        .post(
          'https://dawami.wedigits.dev/api/user/' +
            id +
            '/update/profile_image',
          form,
        )
        .then(function(res, err) {
          if (res.data) {
            fetchProfileRecords();
          }
        });
      console.log('API =========RESPONSE IS ================');
      console.log('yess');
    } catch (e) {
      console.log('error catching');
      console.log(e);
      //  onError(e);
    }
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <Block middle>
        <Block
          center
          middle
          flex={false}
          height={sizes.getHeight(25)}
          style={{
            backgroundColor: 'transparent',
            elevation: 1,
            width: '100%',
          }}>
          <Block
            center
            middle
            flex={false}
            height={sizes.withScreen(0.1)}
            width={sizes.withScreen(0.1)}
            style={{overflow: 'hidden', borderRadius: sizes.withScreen(3)}}>
            {userData ? (
              userData.user ? (
                <Image
                  source={{
                    uri:
                      'https://dawami.wedigits.dev/images/user_avatars/' +
                      userData.user.avatar,
                  }}
                  style={{
                    resizeMode: 'contain',
                    flex: 1,
                    height: 100,
                    width: 100,
                  }}
                />
              ) : (
                <Image
                  source={icons.person}
                  style={{
                    resizeMode: 'contain',
                    flex: 1,
                    height: 100,
                    width: 100,
                  }}
                />
              )
            ) : null}
          </Block>
          <Button
            center
            middle
            style={{
              //   borderWidth:1,
              position: 'absolute',
              bottom: sizes.getHeight(7),
              right: sizes.getWidth(36),
              height: sizes.screenSize * 0.024,
              width: sizes.screenSize * 0.024,
              borderRadius: sizes.screenSize * 3,
              backgroundColor: colors.customRed,
            }}>
            <TouchableOpacity
              onPress={() => {
                pickImage();
              }}>
              <Image
                source={icons.camera}
                style={{
                  resizeMode: 'contain',
                  width: sizes.getWidth(4),
                }}
              />
            </TouchableOpacity>
          </Button>

          {/* ============================ */}
          <Block center middle flex={false}>
            <Text h3 color={colors.customRed}>
              {name || null}
            </Text>
            {/* <Text h4 color={colors.customRed}>
            UI / UX Designer
          </Text> */}
          </Block>
        </Block>
        <Block padding={[0, sizes.getWidth(2)]}>
          {/* ================================================ */}
          <ScrollView
            style={{
              width: '100%',
              paddingTop: sizes.getHeight(3),
              // borderWidth:1,
            }}
            showsVerticalScrollIndicator={false}>
            <Animator title="Basic Info">
              <BasicInfo
                message={message => messageHandler(message)}
                showWaiting={status => ActivityHandler(status)}
              />
            </Animator>

            <Animator title="Contact Info">
              <ContactInfo
                message={message => messageHandler(message)}
                showWaiting={status => ActivityHandler(status)}
              />
            </Animator>

            <Animator title="Preffered Jobs">
              <PreferredJobs
                message={message => messageHandler(message)}
                showWaiting={status => ActivityHandler(status)}
              />
            </Animator>
            <Animator title="Work Info">
              <WorkInfo
                message={message => messageHandler(message)}
                showWaiting={status => ActivityHandler(status)}
              />
            </Animator>

            <Animator title="Education">
              <EducationalInfo
                message={message => messageHandler(message)}
                showWaiting={status => ActivityHandler(status)}
              />
            </Animator>

            <Animator title="Skills">
              <Skills />
            </Animator>

            <Animator title="Languages">
              <LangInfo />
            </Animator>

            <Animator title="Certifications">
              <CertInfo />
            </Animator>

            <Animator title="References">
              <ReferencesInfo />
            </Animator>

            <Animator title="Hobbies">
              <Hobbies />
            </Animator>

            {/* <ContactInfo />
          <PreferredJobs />
          <WorkInfo />
          <EducationalInfo />
          <Skills />
          <LangInfo />
          <CertInfo />
          <ReferencesInfo />
          <Hobbies/> */}
          </ScrollView>
        </Block>

        <Toast
          ref={messageRef}
          style={{
            backgroundColor: colors.customRed,
            width: sizes.getWidth(100),
            borderRadius: 2,
          }}
          positionValue={sizes.getDimensions.height}
          fadeInDuration={200}
          fadeOutDuration={100}
          opacity={1}
        />

        {isWaiting && <ActivitySign />}
      </Block>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  headingStyle: {
    // borderWidth:1,
    height: sizes.getHeight(7),
    justifyContent: 'center',
    borderRadius: sizes.getWidth(1),
    paddingHorizontal: sizes.getWidth(2),
    backgroundColor: colors.primary,
  },
});

export {EditProfile};
