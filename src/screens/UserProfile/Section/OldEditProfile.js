// import React from 'react';
// import {Block, Text, Button} from 'components';
// import {sizes, colors} from 'styles/theme';
// import {ScrollView, Image, TextInput} from 'react-native';
// import * as images from 'assets/images';
// import * as icons from 'assets/icons';

// const EditProfile = props => {
//   return (
//     <Block middle>
//       <Block
//         center
//         middle
//         flex={false}
//         height={sizes.getHeight(25)}
//         style={{backgroundColor: colors.customRed, width: '100%'}}>
//         <Block
//           center
//           middle
//           flex={false}
//           height={sizes.withScreen(0.1)}
//           width={sizes.withScreen(0.1)}
//           style={{overflow: 'hidden', borderRadius: sizes.withScreen(3)}}>

//           <Image
//             source={images.person}
//             style={{resizeMode: 'contain', flex: 1}}
//         />
//         </Block>
//         <Button
//           center
//           middle
//           style={{
//             //   borderWidth:1,    
//               position: 'absolute',
//             bottom:sizes.getHeight(7),
//             right:sizes.getWidth(36),
//             height:sizes.screenSize*0.024,
//             width:sizes.screenSize*0.024,
//             borderRadius:sizes.screenSize*3,
//             backgroundColor:colors.brown

//           }}>
//           <Image
//             source={icons.camera}
//             style={{
//               resizeMode: 'contain',
//               width: sizes.getWidth(4),
//             }}
//           />
//         </Button>

//         {/* ============================ */}
//         <Block center middle flex={false}>
//           <Text h3 color={colors.primary}>
//             Ibrahim Muhammad
//           </Text>
//           <Text h4 color={colors.primary}>
//             UI / UX Designer
//           </Text>
//         </Block>
//       </Block>
//       <Block padding={[0, sizes.getWidth(13)]}>
//         {/* ================================================ */}
//         <ScrollView
//           style={{width: '100%', paddingTop:sizes.getHeight(3)}}
//           showsVerticalScrollIndicator={false}>
//           <Block flex={false} middle height={sizes.getHeight(8)}>
//             <Block>
//               <Text h3 bold>
//                 Full Name
//               </Text>
//             </Block>
//             <Block flex={2} row space={'between'}>
//               <TextInput editable={true} />
//             </Block>
//           </Block>
//           <Block flex={false} middle height={sizes.getHeight(8)}>
//             <Block>
//               <Text h3 bold>
//                 Username
//               </Text>
//             </Block>
//             <Block flex={2} row space={'between'}>
//               <TextInput value={'great'} />
//             </Block>
//           </Block>
//           <Block flex={false} middle height={sizes.getHeight(8)}>
//             <Block>
//               <Text h3 bold>
//                 Email Address
//               </Text>
//             </Block>
//             <Block flex={2} row space={'between'}>
//               <TextInput value={'great'} />
//             </Block>
//           </Block>
//           <Block flex={false} middle height={sizes.getHeight(8)}>
//             <Block>
//               <Text h3 bold>
//                 Phone
//               </Text>
//             </Block>
//             <Block flex={2} row space={'between'}>
//               <TextInput value={'great'} />
//             </Block>
//           </Block>
//           <Block flex={false} middle height={sizes.getHeight(8)}>
//             <Block>
//               <Text h3 bold>
//                 Industry Expertise
//               </Text>
//             </Block>
//             <Block flex={2} row space={'between'}>
//               <TextInput value={'great'} />
//             </Block>
//           </Block>
//           <Block flex={false} middle height={sizes.getHeight(10)}>
//             <Block>
//               <Text h3 bold>
//                 About Me
//               </Text>
//             </Block>
//             <Block flex={2} row space={'between'}>
//               <TextInput multiline={true} numberOfLines={20} value={'great'} />
//             </Block>
//           </Block>
//           <Block center middle height={sizes.getHeight(7.5)}>
//             <Button
//               center
//               middle
//               style={{
//                 width: '100%',
//                 backgroundColor: colors.brown,
//                 borderRadius: sizes.getWidth(10),
//               }}>
//               <Text h2 color={colors.customRed}>
//                 Done
//               </Text>
//             </Button>
//           </Block>
//         </ScrollView>
//       </Block>
//     </Block>
//   );
// };

// export default EditProfile;
