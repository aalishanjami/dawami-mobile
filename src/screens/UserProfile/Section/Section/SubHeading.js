import React from 'react';
import {Block, Text} from 'components';
import {Image, Platform, Dimensions} from 'react-native';
import * as icons from 'assets/icons';
import {colors, sizes} from 'styles/theme';

const SubHeading = props => {
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width <= 375;
  const {width} = props;
  return (
    <Block
      flex={false}
      //  middle
      //  bottom
      middle
      //    margin={[sizes.getHeight(3),0,0,0]}
      height={sizes.getHeight(5)}
      width={width || sizes.getWidth('25')}
      //  style={{borderWidth:1}}
      style={{
        alignContent: 'center',
      }}>
      <Text style={{
        fontSize:iphone6s?sizes.customFont(9):sizes.customFont(12)
      }} bold>
        {props.children}
      </Text>
    </Block>
  );
};

export {SubHeading};
