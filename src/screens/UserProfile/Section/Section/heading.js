import React from 'react'
import { Block, Text } from 'components'
import { Image, Platform, Dimensions } from 'react-native'
import * as icons from 'assets/icons'
import { colors, sizes } from 'styles/theme'

const Heading = props => {
    const ios = Platform.OS === 'ios';
    const iphone6s = Dimensions.get('screen').width <= 375;
    return (
       <Block row  
    //    flex={false}
    //    style={{borderWidth:1}}
       >
           <Block flex={false} width={sizes.getWidth(10)}  middle>
           <Image source={icons.white_user} style={{resizeMode:'contain',width:sizes.getWidth(5), tintColor:colors.black}} />
           </Block>
           <Block middle flex={4}>
           <Text 
           style={{fontSize:sizes.customFont(iphone6s?11:16)}}
           bold>{props.children}</Text>
           </Block>
           </Block>
    )
}

export  {Heading}
