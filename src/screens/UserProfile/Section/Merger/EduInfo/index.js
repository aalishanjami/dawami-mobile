import React, {useReducer, useState, useEffect, useRef} from 'react';
import {Block, Text, TextField, TextFieldOne,  SquareButton} from 'components';
import {SubHeading} from '../../Section';
import {sizes, colors} from 'styles/theme';
import {StyleSheet} from 'react-native';
import {Picker} from '@react-native-community/picker';
import {useDispatch, useSelector} from 'react-redux';
import {AddEducation} from 'redux/actions';
import { View } from 'react-native-animatable';
import DatePicker from 'react-native-datepicker';
import * as icons from 'assets/icons';
import axios from 'axios';
import {GetProfileInfo} from 'redux/actions';
import {TouchableOpacity, TouchableWithoutFeedback, TouchableNativeFeedback} from 'react-native-gesture-handler'
import {
  ActivityIndicator,
  Image,
  Modal,
  TouchableHighlight,
  ToastAndroid,
  KeyboardAvoidingView,
  Button} from 'react-native';

const EducationalInfo = props => {
  const {showWaiting, message} = props;
  const auth = useSelector(state => state.userInfo);
  const countries = useSelector(state => state.userInfo.userData.countries);
  const id = useSelector(state => state.userInfo.userData.user.id);
  const dispatch = useDispatch();
  const GV = {
    PAK: {name: 'PAKISTAN', value: '1'},
    QATAR: {name: 'QATAR', value: '2'},

    year: [
      '2009',
      '2010',
      '2011',
      '2012',
      '2014',
      '2015',
      '2016',
      '2017',
      '2018',
      '2019',
      '2020',
    ],
    month: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
    DG :[
     {name:'High school or equivalent', value: "1"},
    {name:'Diploma',value:"2"},
     {name:'Bachelors Degree',value:"3"},
     {name:'Higher Diploma',value:"4"},
      {name:'Masters Degree',value:"5"},
     {name:'Doctrate',value:"6"},
   //   NV: {name: 'No Visa', value: '7'},
  ],
  };
  
  const UNI_NAME = 'UNIVERSITY_NAME';
  const DEGREE = 'DEGREE';
  const COUNTRY = 'COUNTRY';
  const MAJOR = 'MAJOR';
  const SUMMARY = 'SUMMARY';
  const YEAR = 'YEAR';
  const MONTH = 'MONTH';
  
  const nameRef = useRef()
  const degreeRef = useRef()
  const majorRef = useRef()
  const summaryRef = useRef()
  const [isWaiting, setIsWaiting] = useState(false);
  
  
  const initailState = {
    // disableSaveBtn:true,
    uniName: '',
    degree: '',
    country: GV.PAK.value,
    major: '',
    summary: '',
    year: GV.year[5],
    month: GV.month[2],
  };

  const [disableSaveBtn, setDisableSaveBtn] = useState(true);
  const reducer = (state, action) => {
    switch (action.type) {
      case UNI_NAME:
        return {...state, uniName: action.payload};
      case DEGREE:
        return {...state, degree: action.payload};
      case MAJOR:
        return {...state, major: action.payload};
      case MONTH:
        return {...state, month: action.payload};
      case YEAR:
        return {...state, year:action.payload};
      case COUNTRY:
        return {...state, country: action.payload};
      case SUMMARY:
        return {...state, summary: action.payload};
      default:
        return state;
    }
  };
  const [state, localDispatch] = useReducer(reducer, initailState);
  const [modalVisible, setModalVisible] = useState(false);
  const [Univ, setUniv] = useState('');
  const [Degree, setDegree] = useState('');
  const [coun, setCoun] = useState('');
  const [grad_time, setGradTime] = useState('');
  const [maj, setMaj] = useState('');
  const [summar, setSummar] = useState();
  const [ed_id, setEdId] = useState();

  async function fetchProfileRecords() {
    setIsWaiting(true);
    const result = await GetProfileInfo(id, err => {
      return (
        setIsWaiting(false)

      );
    });
    console.log(JSON.stringify(result) + "sasfsadsasad");
    result && (dispatch(result));
  }

console.log("counnn",coun)
  const updateEdu = async () => {
    try {
       let form = new FormData();
       form.append("university_name",Univ)
       form.append("degree",Degree)
       form.append("graduate_time",grad_time)
       form.append("major",maj)
       form.append("country_id",coun)
       form.append("description",summar)

       
      console.log('DATA BEFORE SUBMITTION========');
      if (Univ !== '' && Degree !=='' && grad_time!=='' && maj!=='') {
        const response = await axios.post(
          'https://dawami.wedigits.dev/api/profile/'+ed_id+'/update/education',
          form,
        ).then(function(res,err){
          if (res.data) {
            setEdId('')
            fetchProfileRecords()
           setModalVisible(false)
           console.log(res +" updated")
          }

        })
       console.log('API =========RESPONSE IS ================');
          console.log("yess")
     }
     else {
       ToastAndroid.show("Fill",ToastAndroid.LONG)
       console.log("no")
     }
      }
      catch (e) {
        console.log('error catching');
        console.log(e);
      //  onError(e);
      }  
  };

  const addEdu = async () => {
    try {
       let form = new FormData();
       form.append("university_name",Univ)
       form.append("degree",Degree)
       form.append("graduate_time",grad_time)
       form.append("major",maj)
       form.append("country_id",coun)
       form.append("description",summar)

       
      console.log('DATA BEFORE SUBMITTION========');
      if (Univ !== '' && Degree !=='' && grad_time!=='' && maj!=='') {
        const response = await axios.post(
          'https://dawami.wedigits.dev/api/profile/'+id+'/add/education',
          form,
        ).then(function(res,err){
          if (res.data) {
            
            fetchProfileRecords()
           setModalVisible(false)
           console.log(res +" updated")
          }

        })
       console.log('API =========RESPONSE IS ================');
          console.log("yess")
     }
     else {
       ToastAndroid.show("Fill",ToastAndroid.LONG)
       console.log("no")
     }
      }
      catch (e) {
        console.log('error catching');
        console.log(e);
      //  onError(e);
      }  
  };

  const removeEdu = async (eduid) => {
    try {
     console.log('DATA BEFORE deletion========' + eduid);
        const response = await axios.get(
          'https://dawami.wedigits.dev/api/profile/remove/education/'+eduid,
        ).then(function(res,err){
          if (res.data) {
            
            fetchProfileRecords()
           console.log(res +" updated")
          }

        })
        fetchProfileRecords()
       console.log('API =========RESPONSE IS ================');
          console.log("yess")
     
      }
      catch (e) {
        console.log('error catching');
        console.log(e);
      //  onError(e);
      }  
  };




  // ======================SAVE
  const saveInfoHandler = async () => {
    showWaiting(true);
    const currentState = {...state, id: auth.id.toString()};
    const result = await AddEducation(currentState, err => {
      alert(err), showWaiting(false);
    });
    console.log('RESULT ==========');
    console.log(result);
    dispatch(result)
    nameRef?.current.clear()
    degreeRef?.current.clear()
    majorRef?.current.clear()
    summaryRef?.current.clear()
    showWaiting(false);
    message('INFORMATION UPDATED')
  };

  useEffect(() => {
    if (
      state.uniName !== '' &&
      state.degree !== '' &&
      state.major !== '' &&
      state.summary !== ''
    ) {
      setDisableSaveBtn(false);
    } else {
      setDisableSaveBtn(true);
    }
  });

  const inputHandler = ({name, text}) => {
    switch (name) {
      case UNI_NAME:
        return localDispatch({type: UNI_NAME, payload: text});
      case DEGREE:
        return localDispatch({type: DEGREE, payload: text});
      case MAJOR:
        return localDispatch({type: MAJOR, payload: text});
      case SUMMARY:
        return localDispatch({type: SUMMARY, payload: text});
      default:
        console.log('default');
        return;
    }
  };

  console.log('===============================');
  console.log(JSON.stringify(GV.DG) + "yooo");
  console.log('===============================');

  return (
    <View>
        <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setEdId('')
          setModalVisible(false);
          
        }}>
          <KeyboardAvoidingView style={{flex:1}} behavior="padding" enabled>
        <View
          style={{
          //  justifyContent: 'center',
            alignItems: 'center',
            margin:10,
           // flex:0.5,
           height:sizes.getHeight(65),
            marginTop:sizes.getHeight(10),
            padding:sizes.getWidth(5),
            backgroundColor: '#fff', elevation: 1,borderRadius:sizes.withScreen(0.006)
          }}>
          <Block
            center
            margin={[sizes.getHeight(5), 10, 10, 10]}
            flex={false}
            row
            center
            style={styles.recordKeeperCon}>
            <Block>
              <SubHeading>University Name:</SubHeading>
            </Block>
            <Block
              padding={[
                0,
                sizes.getWidth(0),
                sizes.getWidth(0),
                sizes.getWidth(0),
              ]}
              flex={3}
              style={{borderWidth: 0}}>

              <TextFieldOne
                onChangeText={(text)=>{
               setUniv(text.text)
               console.log(Univ)
                  
                }}
               value={Univ}
                inputStyling={{
                  borderBottomWidth: 1,
                  borderBottomColor: colors.gray2,
                  color:'#000',
                  width: '100%',
                  padding:0,
                  paddingBottom: 0,
                }}
              />
            </Block>
          </Block>

          <Block flex={false} row center style={styles.recordKeeperCon}
           margin={[sizes.getHeight(1), 10, 10, 10]}
                      >
            <Block>
              <SubHeading>Degree:</SubHeading>
            </Block>
            <Block
              padding={[
                0,
                sizes.getWidth(0),
                sizes.getWidth(0),
                sizes.getWidth(0),
              ]}
              flex={3}
              style={{borderWidth: 0}}>

              <Picker
                selectedValue={Degree}
            onValueChange={(itemValue, index) =>{
              setDegree(itemValue)
            }
            }
                style={styles.pickerStyle}>
                {GV.DG.map(itemm => (
                  <Picker.Item label={itemm.name} value={itemm.value} />
                  ))}
                  </Picker>
                  
            </Block>
            
          </Block>

          <Block flex={false} row center style={styles.recordKeeperCon}
           margin={[sizes.getHeight(1), 10, 10, 10]}
                      >
            <Block>
              <SubHeading>Country:</SubHeading>
            </Block>
            <Block
              padding={[
                0,
                sizes.getWidth(0),
                sizes.getWidth(0),
                sizes.getWidth(0),
              ]}
              flex={3}
              style={{borderWidth: 0}}>

              <Picker
                selectedValue={coun}
                onValueChange={(itemValue, index) =>{
                  setCoun(itemValue)
                }
                }
                style={styles.pickerStyle}>
                {countries.map(itemm=>(

                  <Picker.Item label={itemm.country_name} value={itemm.id.toString()}  />
                ))}
                  </Picker>
            </Block>  
          </Block>
          
          <Block flex={false} row center style={styles.recordKeeperCon}>
        <Block>
          <SubHeading>Graduate Time:</SubHeading>
        </Block>
        <Block
          padding={[0, sizes.getWidth(0), sizes.getWidth(0), sizes.getWidth(0)]}
          flex={2}
          style={{borderWidth: 0}}>
          <DatePicker
          style={{
            marginTop:sizes.getHeight(1),
            width: sizes.getWidth(55),
            justifyContent: 'center',
          }}
          placeholder="Select Date"
          mode="date"
          format="YYYY-MM"
          date={grad_time}
          onDateChange={date => {
            setGradTime(date)
          }}
        />
        </Block>
      </Block>
      <Block flex={false} row center style={styles.recordKeeperCon}>
        <Block>
          <SubHeading> Major:</SubHeading>
        </Block>

        <Block
          padding={[0, sizes.getWidth(0), sizes.getWidth(2), sizes.getWidth(0)]}
          flex={2}
          
          style={{borderWidth: 0}}>
          <TextFieldOne  
            value={maj}
            onChangeText={(text)=>{
              setMaj(text.text)
              }}
            inputStyling={{
              borderBottomWidth: 1,
              width: '100%',
              borderBottomColor: colors.gray2,
              
              paddingBottom: 0,
            }}
          />
        </Block>
      </Block>

      <Block
        center
        flex={false}
        style={{
          ...styles.recordKeeperCon,
      //    marginBottom: 10,
          height: sizes.getHeight(10),
        }}>
        <Block style={{width: '100%'}}>
          <SubHeading center width={'100%'}>
            Personal Summary
          </SubHeading>
        </Block>
        <Block
          padding={[0, sizes.getWidth(2), 0, 0]}
          style={{
            borderWidth: 0,
            height: '100%',
          }}>
          <TextFieldOne
            onChangeText={(text)=>{
              setSummar(text.text)
             
            }}
            placeholder="Description"
            multilines={true}
            value={summar}
            numberOfLines={5}
            textAlignVertical="top"
            inputStyling={{
              borderWidth: 0.4,
              borderStyle: 'dotted',
              borderRadius: sizes.getWidth(1),
              width: '100%',
              height: '100%',
            }}
          />
        </Block>
      </Block>

          <View style={{flexDirection:'row',marginTop:sizes.getHeight(10)}}>
          <View style={{flex:0.4,margin:5}}>
          <SquareButton 
        textColor={colors.primary}
        // bgColor={colors.customRed}
        width={sizes.getWidth(30)}
        height={sizes.getHeight(6)}
        onPress={()=>
          ed_id ? updateEdu() : addEdu()
          }
        style={{
          // backgroundColor: btnDisable ? 'transparent' : colors.customRed,
          backgroundColor: colors.customRed,
          borderWidth: 0,
          borderRadius: sizes.withScreen(0.01),
        }}>
        Submit
          </SquareButton>
          </View>
          <View style={{flex:0.4,margin:5}}>
          <SquareButton 
        textColor={colors.primary}
        // bgColor={colors.customRed}
        width={sizes.getWidth(25)}
        height={sizes.getHeight(6)}
        onPress={()=>
          setModalVisible(false)}
        style={{
          // backgroundColor: btnDisable ? 'transparent' : colors.customRed,
          backgroundColor: '#B22222',
          borderWidth: 0,
          borderRadius: sizes.withScreen(0.015),
        }}>
        Cancel
          </SquareButton>
          </View>
          </View>
        
        </View>
        </KeyboardAvoidingView>
      </Modal>

       <TouchableOpacity
        onPress={() => {
          setModalVisible(true);
          setUniv('')
          setDegree('')
          setCoun('')
          setGradTime('')
          setMaj('')
          setSummar('')
        }}>
        <Image
          source={icons.add}
          style={{
            height: sizes.getHeight(4),
            resizeMode: 'contain',
            width: sizes.getWidth(10),
            alignSelf:'flex-end',
            marginTop:10
          }}
        />
      </TouchableOpacity>
    {auth.userData.profile.educations.map((item)=>(

      <Block
      padding={[0, sizes.getWidth(2)]}
      margin={[0, 0, sizes.getHeight(4), 0]}>
     
      <Block
        center
        margin={[sizes.getHeight(2), 0, 0, 0]}
        flex={false}
        row
        center
        style={styles.recordKeeperCon}>
        <Block>
          <SubHeading>University Name</SubHeading>
        </Block>
        <Block
          padding={[0, sizes.getWidth(2), sizes.getWidth(2), sizes.getWidth(2)]}
          flex={2}
          style={{borderWidth: 0}}>
          <TextFieldOne
            editable = {false}
             value={item.university_name}
            inputStyling={{
              borderBottomWidth: 1,
              borderBottomColor: colors.gray2,
              width: '90%',
              margin: 0,
              paddingBottom: 0
            }}
          />
        </Block>
      </Block>
      <Block flex={false} row center style={styles.recordKeeperCon}>
        <Block>
          <SubHeading>Degree</SubHeading>
        </Block>
        <Block
          padding={[0, sizes.getWidth(2), sizes.getWidth(2), sizes.getWidth(2)]}
          flex={2}
          style={{borderWidth: 0}}>
          <Picker
          enabled={false}
            selectedValue={item.degree}
            style={styles.pickerStyle}>
              {GV.DG.map((itemm)=>(
            <Picker.Item label={itemm.name} value={itemm.value} />
              ))}
          </Picker>
        </Block>
      </Block>

      <Block flex={false} row center style={styles.recordKeeperCon}>
        <Block>
          <SubHeading>Country</SubHeading>
        </Block>
        <Block
          padding={[0, sizes.getWidth(2), sizes.getWidth(2), sizes.getWidth(2)]}
          flex={2}
          style={{borderWidth: 0}}>
          <Picker
            selectedValue={item.country_id}
            enabled={false}
            style={styles.pickerStyle}>
              {countries.map((itemm)=>(
            <Picker.Item label={itemm.country_name} value={itemm.id.toString()} />
              ))}
          </Picker>
        </Block>
      </Block>

      <Block flex={false} row center style={styles.recordKeeperCon}>
        <Block>
          <SubHeading>Graduate Time</SubHeading>
        </Block>
        <Block
          padding={[0, sizes.getWidth(2), sizes.getWidth(2), sizes.getWidth(2)]}
          flex={2.1}
          style={{borderWidth: 0}}>
          <DatePicker
          style={{
            width: sizes.getWidth(65),
            justifyContent: 'center',
            height: sizes.getHeight(5),
          }}
          placeholder="Select Date"
          mode="date"
          format="YYYY-MM"
          date={item.graduate_time}
          enabled={false}
          

        />
        </Block>
      </Block>


      {/* <Block flex={false} row center style={styles.recordKeeperCon}>
        <Block>
          <SubHeading>Graduate Month</SubHeading>
        </Block>
        <Block
          padding={[0, sizes.getWidth(2), sizes.getWidth(2), sizes.getWidth(2)]}
          flex={2}
          style={{borderWidth: 0}}>
          <Picker
            selectedValue={state.month}
            onValueChange={(itemValue, index) =>
              localDispatch({type: MONTH, payload: itemValue})
            }
            style={styles.pickerStyle}>
            {GV.month.map(el => {
              return <Picker.Item label={el} value={el} />;
            })}
          </Picker>
        </Block>
      </Block> */}

      <Block flex={false} row center style={styles.recordKeeperCon}>
        <Block>
          <SubHeading>Major</SubHeading>
        </Block>

        <Block
          padding={[0, sizes.getWidth(2), sizes.getWidth(2), sizes.getWidth(2)]}
          flex={2}
          style={{borderWidth: 0}}>
          <TextFieldOne
            value={item.major}
            editable={false}
            onChangeText={inputHandler}
            inputStyling={{
              borderBottomWidth: 1,
              width: '100%',
              borderBottomColor: colors.gray2,
              margin: 0,
              paddingBottom: 0   
            }}
          />
        </Block>
      </Block>

      <Block
        center
        flex={false}
        style={{
          ...styles.recordKeeperCon,
          marginBottom: 10,
          height: sizes.getHeight(15),
        }}>
        <Block center style={{width: '100%'}}>
          <SubHeading center width={'100%'}>
            Personal Summary
          </SubHeading>
        </Block>
        <Block
          padding={[0, sizes.getWidth(2), 0, 0]}
          style={{
            borderWidth: 0,
            height: '100%',
          }}>
          <TextFieldOne
           editable={false}
            placeholder="Description"
            multilines={true}
            value={item.description}
            numberOfLines={5}
            textAlignVertical="top"
            inputStyling={{
              borderWidth: 0.4,
              borderStyle: 'dotted',
              borderRadius: sizes.getWidth(1),
              width: '100%',
              height: '100%'
            }}
          />
        </Block>
      </Block>
      {/* <Block
        row
        bottom
        crossRight
        flex={false}
        height={sizes.getHeight(8)}
        padding={[0, sizes.padding]}
        margin={[sizes.getHeight(7), 0, 0, 0]}>
           <Button
          onPress={saveInfoHandler}
          disabled={isWaiting || disableSaveBtn}
          activeOpacity={0.6}
          center
          middle
          style={{
            marginRight:sizes.getWidth(1),
            borderRadius: sizes.getWidth(1),
            backgroundColor:
              disableSaveBtn || isWaiting ? colors.gray : colors.customRed,
            width: '40%',
          }}>
          <Text color={colors.primary}>Add More </Text>
        </Button>

        <Button
          onPress={saveInfoHandler}
          disabled={isWaiting || disableSaveBtn}
          activeOpacity={0.6}
          center
          middle
          style={{
            borderRadius: sizes.getWidth(1),
            backgroundColor:
              disableSaveBtn || isWaiting ? colors.gray : colors.customRed,
            width: '40%',
          }}>
          {isWaiting ? (
            <ActivityIndicator size={'large'} color={colors.primary} />
          ) : (
            <Text color={colors.primary}>Save</Text>
          )}
        </Button>
      </Block> */}
      <View   
           style={{flexDirection:'row',alignSelf:'center',marginTop:sizes.getHeight(7)}}>
          <TouchableOpacity
          style={{width:100,alignSelf:'center'}}
        onPress={() => {
          setModalVisible(true);
          setEdId(item.id)
          setUniv(item.university_name)
          setDegree(item.degree)
          setCoun(item.country_id)
          setGradTime(item.graduate_time)
          setMaj(item.major)
          setSummar(item.description)
          console.log(item.country_id + "country")
          }}>
        <View style={styles.editbtn}>
          <Text style={{textAlign: 'center',fontSize: 16,color:'white'}} 
          >Edit</Text>

            </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={()=>{
          removeEdu(item.id)
        }}
        >
        <Image
          source={icons.remove}
          style={{
            height: sizes.getHeight(4),
            resizeMode: 'contain',
            width: sizes.getWidth(10),
            alignSelf:'flex-end',
            marginLeft:15
          }}
        />
      </TouchableOpacity>
      </View>
    </Block>
    ))}
    </View>
    
  );
};

const styles = StyleSheet.create({
  recordKeeperCon: {
    height: sizes.getHeight(4),
    marginBottom: sizes.getHeight(1),
    marginTop:sizes.getHeight(1),
//    margin:sizes.getHeight(0)
  },
  pickerStyle: {
    width: sizes.getWidth(65),
    marginTop:sizes.getHeight(0.4),
    transform: [{scaleX: 0.9}, {scaleY: 0.9}],
    height: sizes.getHeight(3),
  },
  editbtn:{
    backgroundColor:colors.customRed,alignSelf:'center',width:100,padding:6,borderRadius:5
  }
});

export {EducationalInfo};
