import React, { useState, useEffect } from 'react';
import { Block, Text, Button } from 'components';
import { SubHeading } from 'screens/UserProfile/Section/Section';
import { Picker } from '@react-native-community/picker';
import { sizes, colors } from 'styles/theme';
import { StyleSheet, Dimensions, Platform } from 'react-native';
import { VisaSelection } from './visa';
import { CustomBtn } from './button';

const JobLocation = props => {
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width <= 375;
  const ADD_JOB_LOC = 'ADD_JOB_LOC';
  const REMOVE_JOB_LOC = 'REMOVE_JOB_LOC';
  const CHANGE_JOB_LOC = 'CHANGE_JOB_LOC';
  const VISA = 'VISA';

  const { value, currentIndex, localDispatch, jobLocation, visaType } = props;
  // console.log('=====Job Location Props========');
  // console.log(props);
  const GV = {
    CNTRY: {
      PAK: { name: 'PAKISTAN', value: '1' },
      QTR: { name: 'QATAR', value: '2' },
    },
    RC: {
      CTZ: { name: 'Citizen', value: '1' },
      RVT: { name: 'Residence Visa (Transferable)', value: '2' },
      RVNT: { name: 'Residence Visa (Non-Transferable)', value: '3' },
      STD: { name: 'Student', value: '4' },
      TV: { name: 'Transit Visa', value: '5' },
      VV: { name: 'Visit Visa', value: '6' },
      NV: { name: 'No Visa', value: '7' },
    },
  };


  const addHandler = (value, index) => {
    localDispatch({ type: ADD_JOB_LOC, payload: [...jobLocation, value] })
    localDispatch({ type: VISA, payload: [...visaType, value] })
  }
  const removeHandler = (value, index) => {
    let newLocationArray = jobLocation.filter((v, i) => i !== index)
    let newVisaArray = visaType.filter((v, i) => i !== index)
    localDispatch({ type: ADD_JOB_LOC, payload: newLocationArray })
    localDispatch({ type: VISA, payload: newVisaArray })
  }

  const changeLocationHanlder = (currentPos, value) => {
    jobLocation[currentPos] = value
    localDispatch({ type: ADD_JOB_LOC, payload: jobLocation })
  }

  const getVisaValues = () => {
    let values
    visaType.forEach(element => {
      values = element
    });
    return values
  }

  const [visaValue, setVisaValue] = useState(0)

  useEffect(()=>{
    setVisaValue(prev => prev + 1)
  },[])

  console.log('=================////')
  // console.log(visaValue)
  // console.log(props);

  return (
    <Block>
      <Block row>
        <Block
          // middle center
          flex={false}
          width={sizes.getWidth(55)}
          style={styles.jobLocation}>
          <Picker
            itemStyle={{height: sizes.getHeight(iphone6s ? 6 : 5)}}
            selectedValue={value}
            onValueChange={(itemValue, index) =>
              changeLocationHanlder(currentIndex, itemValue)
            }
            style={styles.pickerStyle}>
            <Picker.Item
              label={GV.CNTRY.PAK.name}
              value={GV.CNTRY.PAK.value}
            />
            <Picker.Item
              label={GV.CNTRY.QTR.name}
              value={GV.CNTRY.QTR.value}
            />
          </Picker>

          {/* <VisaSelection value={visaType[visaValue]} {...props} /> */}
        </Block>

        {/* {props.jobLocation.length > 1 ? (
          currentIndex !== 0 && (
            <CustomBtn onPress={() => removeHandler(value, currentIndex)} />
          )
        ) : (
          <CustomBtn
            sign={'+'}
            onPress={() => addHandler(value, currentIndex)}
          />
        )} */}
      </Block>
    </Block>
  );
};

const styles = StyleSheet.create({
  pickerStyle: {
    width: sizes.getWidth(40),
    transform: [{ scaleX: 0.8 }, { scaleY: 0.8 }],
    height: sizes.getHeight(5),
    backgroundColor: '#EEEEEE99'
  },
  recordKeeperCon: {
    height: sizes.getHeight(12),
    // borderWidth:1,
    width: '100%',
    // backgroundColor: 'red',
    // marginBottom: sizes.getHeight(2),
  },
  jobLocation: {
    //     height: sizes.getHeight(10),
    // width: '100%',
    marginBottom: sizes.getHeight(1),
    borderWidth: 0.4,
    marginRight: sizes.getWidth(3),
    borderRadius: sizes.getWidth(0.7),
    borderStyle: 'dotted'

  },
});

export { JobLocation };
