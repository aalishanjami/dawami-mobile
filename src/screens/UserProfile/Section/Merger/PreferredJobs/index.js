import React, {
  useState,
  useRef,
  forwardRef,
  useReducer,
  useEffect,
} from 'react';
import {Block, Text, TextField, Button} from 'components';
import {
  StyleSheet,
  ScrollView,
  ActivityIndicator,
  Alert,
  Platform,
  Dimensions,
} from 'react-native';
import {Picker} from '@react-native-community/picker';
import {sizes, colors} from 'styles/theme';
import {Heading, SubHeading} from '../../Section';
import {
  JobTitle,
  JobLocation,
  DisplayValues,
  VisaSelection,
  CustomBtn,
} from './Section';
import {useSelector, useDispatch} from 'react-redux';
import {AddJobTitle} from 'redux/actions';
import {JobLocationComp} from './jobLocation';
import {LANG_AR} from 'redux/constants';
import {WORDS} from 'utils';
import {
  addJobTitle,
  BAKERS,
  BANK,
  BANK_AR,
  Beginner,
  CITIZEN,
  Freelancer,
  internship,
  MANAGEMENT,
  NON_RES_VISA,
  No_VISA,
  Pakistan,
  Security,
  STDUENT,
  Transit_Visa,
  TYRES,
  Visit_VISA,
  Volunteer,
  __,
} from 'utils/LanguageHandler';

const PreferredJobs = props => {
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width <= 375;
  const {showWaiting, message} = props;
  // =========================================================================
  let {currentLN} = useSelector(state => state.userInfo);
  let userInfo = useSelector(state => state.userInfo.userData);
  const pereferred_job = useSelector(state => state.userInfo?.userData);
  const {id} = useSelector(state => state.auth.userBasicProfile);
  const AR = currentLN === LANG_AR;
  // -----------------
  const JOB_CATE = 'JOB_CATE';
  const ADD_JOB_LOC = 'ADD_JOB_LOC';
  const VISA = 'VISA';
  const REMOVE_JOB_LOC = 'REMOVE_JOB_LOC';
  const CHANGE_JOB_LOC = 'CHANGE_JOB_LOC';

  const REMOVE_VISA_TYPE = 'REMOVE_VISA_TYPE';
  const JOB_LVL = 'JOB_LVL';
  const JOB_IND = 'JOB_IND';
  const JOB_TYPE = 'JOB_TYPE';
  const CURRNCY = 'CURRENCY';
  const NPRIOD = 'NPERIOD';
  const SUMRY = 'SUMMARY';
  const JOB_TITLE = 'JOB_TITLE';
  const SALARY = 'TARGET SALARY';
  const SAVE_BTN_DIS = 'DISABLE_SAVE_BUTTON';
  // const TITLE = 'TITLE';
  const REMOVE_JOB_TITLE = 'REMOVE_JOB_TITLE';
  // AR?WORDS.NON_RES_VISA__AR:WORDS.NON_RES_VISA,
  const GV = {
    CNTRY: {
      PAK: {
        name: __(WORDS.Pakistan),
        value: '1',
      },
      QTR: {
        name: __(WORDS.Qarat),
        value: '2',
      },
      JOR: {
        name: __(WORDS.JORDAN),
        value: '3',
      },
    },
    RC: {
      CTZ: {
        name: __(WORDS.CITIZEN),
        value: '1',
      },
      RVT: {
        name: __(WORDS.RES_VISA),
        value: '2',
      },
      RVNT: {
        name: __(WORDS.NON_RES_VISA),
        value: '3',
      },
      STD: {
        name: __(WORDS.STDUENT),
        value: '4',
      },
      TV: {
        name: __(WORDS.Transit_Visa),
        value: '5',
      },
      VV: {
        name: __(WORDS.Visit_VISA),
        value: '6',
      },
      NV: {
        name: __(WORDS.No_VISA),
        value: '7',
      },
    },
    //generic values+ initial values
    CATE: {
      MGMNT: {
        name: __(WORDS.MANAGEMENT),
        value: '1',
      },
      BNK: {
        name: __(WORDS.BANK),
        value: '2',
      },
      SEC: {
        name: __(WORDS.Security),
        value: '3',
      },
    },
    lvl: {
      BGNR: {
        name: __(WORDS.Beginner),
        value: '1',
      },
      INTR: {
        name: __(WORDS.Intermediate),
        value: '2',
      },
    },
    CRNCY: {
      DLR: {
        name: '$',
        value: '1',
      },
      ERO: {
        name: '£',
        value: '2',
      },
    },
    TYPE: {
      FREE: {
        name: __(WORDS.Freelancer),
        value: '1',
      },
      INTRN: {
        name: __(WORDS.internship),
        value: '2',
      },
      VOLN: {
        name: __(WORDS.Volunteer),
        value: '3',
      },
    },
    IND: {
      BNK: {
        name: __(WORDS.BANK),
        value: '1',
      },
      BKRS: {
        name: __(WORDS.BAKERS),
        value: '2',
      },
      TYR: {
        name: __(WORDS.TYRES),
        values: '3',
      },
    },
    NPRD: {
      im: {
        name: __(WORDS.IMMEDIATELY),
        value: '1',
      },
      mo: {
        name: __(WORDS.MORE_THAN_ONE_MONTH),
        value: '2',
      },
      mt: {
        name: __(WORDS.MORE_THAN_THREE_MONTHS),
        value: '3',
      },
      ms: {
        name: __(WORDS.MORE_THAN_SIX_MONTHS),
        value: '4',
      },
      my: {
        name: __(WORDS.MORE_THAN_ONE_YEAR),
        value: '5',
      },
    },
  };

  let initialState = {
    disableSaveBtn: true,
    title: userInfo.profile.pereferred_job.title || [],
    jobCategory: userInfo.profile.pereferred_job.category_id,
    jobLocation: userInfo.profile.pereferred_job.country || ['1'],
    jobLevel: userInfo.profile.pereferred_job.level,
    jobIndustry: userInfo.profile.pereferred_job.industry_id,
    targetSalary: userInfo.profile.pereferred_job.target_salary,
    targetCurrency: userInfo.profile.pereferred_job.target_currency,
    summary: userInfo.profile.pereferred_job.summary,
    jobType: userInfo.profile.pereferred_job.type,
    visaType: userInfo.profile.pereferred_job.visa || ['1'],
    noticePeriod: userInfo.profile.pereferred_job.notice_period,
  };
  const reducer = (state, action) => {
    switch (action.type) {
      case SAVE_BTN_DIS:
        return {...state, disableSaveBtn: action.payload};
      case JOB_TITLE:
        if (state.title.length <= 0) {
          return {...state, title: action.payload};
        } else {
          return {...state, title: [...state.title, action.payload]};
        }
      case REMOVE_JOB_TITLE:
        return {...state, title: action.payload};
      case JOB_CATE:
        return {
          ...state,
          jobCategory: action.payload,
          disableSaveBtn: action.payload === pereferred_job.category_id,
        };
      case ADD_JOB_LOC:
        return {
          ...state,
          jobLocation: action.payload,
          disableSaveBtn: action.payload === pereferred_job.country,
        };
      case VISA:
        return {
          ...state,
          visaType: action.payload,
          disableSaveBtn: action.payload === state.jobType,
        };
      case JOB_LVL:
        return {
          ...state,
          jobLevel: action.payload,
          disableSaveBtn: action.payload === pereferred_job.level,
        };
      case JOB_IND:
        return {
          ...state,
          jobIndustry: action.payload,
          disableSaveBtn: action.payload === pereferred_job.industry_id,
        };
      case JOB_TYPE:
        return {
          ...state,
          jobType: action.payload,
          disableSaveBtn: action.payload === pereferred_job.type,
        };
      case CURRNCY:
        return {
          ...state,
          targetCurrency: action.payload,
          disableSaveBtn: action.payload === pereferred_job.target_currency,
        };
      case SALARY:
        return {
          ...state,
          targetSalary: action.payload,
          disableSaveBtn: action.payload === pereferred_job.target_salary,
        };
      case SUMRY:
        return {
          ...state,
          summary: action.payload,
          disableSaveBtn: action.payload === pereferred_job.summary,
        };
      case NPRIOD:
        return {
          ...state,
          noticePeriod: action.payload,
          disableSaveBtn: action.payload === pereferred_job.notice_period,
        };
      default:
        console.log('default reducer');
        return state;
    }
  };

  const [state, localDispatch] = useReducer(reducer, initialState);

  // =============JOB ADD HANDLER ==============
  const addJobTitleHandler = async () => {
    setAddJobTitle(null);
    showWaiting(true);
    const currentStatus = recordHandler();
    const currentState = { ...currentStatus, id: auth.id };
    const result = await AddJobTitle(currentState, err => {
      message('Error. we cannot move forward. ');
      showWaiting(false);
      localDispatch({ type: SAVE_BTN_DIS, payload: true });
      setAddJobTitle(null);
    });
    if (result) {
      dispatch(result);
      setAddJobTitle(null);
      localDispatch({ type: SAVE_BTN_DIS, payload: true }), showWaiting(false);
      message('INFORMATION UPDATED');
    }
    inputRef.current?.clear();
  };
  // ==================================>>>>>>>>>>>>

  console.log('==========>> INITIAL STATE');
  console.log(initialState.title);
  // console.log(pereferred_job);


  return (
    <>
      <Block>
        <Block style={styles.mainCon}>
          {/* ============================================== CLICK TO REMOVE */}
          <Block>
            {state.title?.length && (
              <Block margin={[10, 0, 0, 5]} flex={false}>
                <Text color={colors.gray4}>Click to Remove</Text>
              </Block>
            )}
          </Block>
          <ScrollView horizontal>
            {initialState.title?.length &&
              state.title?.map((el, index) => {
                return (
                  <DisplayValues
                    onPress={() => jobRemoveHandler(el)}
                    el={el}
                    key={index}
                  />
                );
              })}
          </ScrollView>
          {/* =============== ADD JOB TITLE=================== */}
          <Block style={styles.jobTitle}>
            <Block style={styles.addJobInput}>
              <TextField
                placeholder={__(addJobTitle)}
                inputStyling={styles.addInput}
              />
            </Block>
            <Block middle center>
              <Button style={styles.addBtn}>
                <Text color={colors.gray}>+</Text>
              </Button>
            </Block>
          </Block>
          {/* ============================================== */}
        </Block>
      </Block>
    </>
  );
};

const styles = StyleSheet.create({
  mainCon: {
    paddingHorizontal: sizes.getWidth(2),
    marginBottom: sizes.getHeight(3),
  },
  jobTitle: {
    borderWidth: 1,
    flex: 0,
    flexDirection: 'row',
    height:sizes.getHeight(7)
  },
  addJobInput: {
    borderWidth: 1,
    flex: 4,
    padding:0,
    margin:0,
  },
  addInput: {
    backgroundColor: 'red',
    width:'100%',
  },
  addBtn: {
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor:colors.gray,
    borderRadius:sizes.screenSize * 0.007,
    width:sizes.screenSize * 0.04
  },
});

export {PreferredJobs};
