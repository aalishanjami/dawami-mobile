 <Block
      padding={[0, sizes.getWidth(2)]}
      margin={[0, 0, sizes.getHeight(3), 0]}>
      <Block>
        {pereferred_job.title?.length > 0 && (
          <Block margin={[10, 0, 0, 5]} flex={false}>
            <Text
              style={{
                fontSize: sizes.customFont(iphone6s ? 10 : 18),
              }}
              color={colors.gray}>
              Click to Remove
            </Text>
          </Block>
        )}
        <ScrollView scrollEnabled={true} horizontal={true}>
          {pereferred_job.title?.length > 0 &&
            pereferred_job.title?.map((el, index) => {
              return (
                <DisplayValues
                  onPress={() => jobRemoveHandler(el)}
                  el={el}
                  key={index}
                />
              );
            })}
        </ScrollView>
      </Block>

      <JobTitle
        placeholder={'Add Job Title Here...'}
        // showAddBtn= {
        //   jobTitleArray.length,addJobTitle.length
        //     // ? true
        //     // : false || addJobTitle?.length >= 1
        //     // ? true
        //     // : false
        // }
        showAddBtn={jobTitleArray?.length > 0 || addJobTitle?.length > 1}
        ref={inputRef}
        name={JOB_TITLE}
        onChangeText={inputHandler}
        onPress={addJobTitleHandler}
        sign={'+'}
      />
      {/* JOB CATE */}
      <Block row style={styles.recordKeeperCon}>
        <SubHeading>Job Catagory</SubHeading>
        <Picker
          itemStyle={{height: sizes.getHeight(iphone6s ? 6 : 5)}}
          selectedValue={state.jobCategory}
          onValueChange={(itemValue, index) =>
            localDispatch({type: JOB_CATE, payload: itemValue})
          }
          // style={{width: sizes.getWidth(60), hieght: sizes.getHeight(5)}}
          style={{...styles.pickerStyle, width: sizes.getWidth(70)}}>
          <Picker.Item
            label={GV.CATE.MGMNT.name}
            value={GV.CATE.MGMNT.value}
          />
          <Picker.Item label={GV.CATE.BNK.name} value={GV.CATE.BNK.value} />
          <Picker.Item label={GV.CATE.SEC.name} value={GV.CATE.SEC.value} />
        </Picker>
      </Block>
      {/* =========================JOB LOCATION========================= */}

      <Block row>
        <Block middle flex={2} style={{borderWidth: 0}}>
          <SubHeading>Job Location</SubHeading>
        </Block>
        <Block
          flex={false}
          width={sizes.getWidth(64)}
          style={{
            borderWidth: 0.4,
            borderRadius: sizes.getWidth(1),
            borderStyle: 'dashed',
          }}>
          {state.jobLocation.map((v, i) => {
            return (
              <JobLocation
                el={v}
                countryChange={(v, i) => localDispatch({type: CHANGE_JOB_LOC, payload: v})}
              />
            );
          })}
        </Block>
        {/* <Block
          flex={false}
          width={sizes.getWidth(64)}
          style={{
            borderWidth: 0.4,
            borderRadius: sizes.getWidth(1),
            borderStyle: 'dashed',
          }}>
          {state.jobLocation.map((v, i) => {
            return (
              <JobLocation
                el={v}
                countryChange={(v, i) => localDispatch({type: CHANGE_JOB_LOC, payload: v})}
              />
            );
          })}
        </Block>
      </Block>
      <Block  margin={[10,0,0,0]} row style={{borderWidth: 0}}>
        <Block>
          <SubHeading>Visa Type</SubHeading>
        </Block>
        <Block
          flex={false}
          width={sizes.getWidth(64)}
          style={{
            borderWidth: 0.4,
            borderRadius: sizes.getWidth(1),
            borderStyle: 'dashed',
          }}>
          {state.visaType.map((v, i) => {
            return (
              <VisaSelection
                el={v}
                visaChange={(v, i) => localDispatch({type: VISA, payload: v})}
              />
            );
          })}
        </Block> */}

        <Block flex={6} style={{borderWidth: 0}}>
          {state.jobLocation.map((value, index) => {
            return (
              <JobLocation
                key={index}
                value={value}
                currentIndex={index}
                localDispatch={localDispatch}
                {...state}
              />
            );
          })}
        </Block>
      </Block>

      {/* =========================JOB LOCATION========================= */}

      {/* JOB LEVEL */}
      <Block row center middle style={styles.recordKeeperCon}>
        <SubHeading>Job Level</SubHeading>
        <Picker
          itemStyle={{height: sizes.getHeight(iphone6s ? 6 : 5)}}
          // selectedValue={userInfo.userInfo?.profile.pereferred_job.jobLevel}
          selectedValue={pereferred_job.level}
          onValueChange={(itemValue, index) =>
            localDispatch({type: JOB_LVL, payload: itemValue})
          }
          style={{...styles.pickerStyle, width: sizes.getWidth(70)}}>
          <Picker.Item label={GV.lvl.BGNR.name} value={GV.lvl.BGNR.value} />
          <Picker.Item label={GV.lvl.INTR.name} value={GV.lvl.INTR.value} />
        </Picker>
      </Block>
      {/* JOB INDUSTRY */}
      <Block row center middle style={styles.recordKeeperCon}>
        <SubHeading>Job Industry</SubHeading>
        <Picker
          itemStyle={{height: sizes.getHeight(iphone6s ? 6 : 5)}}
          selectedValue={state.jobIndustry}
          onValueChange={(itemValue, index) =>
            localDispatch({type: JOB_IND, payload: itemValue})
          }
          style={{...styles.pickerStyle, width: sizes.getWidth(70)}}>
          <Picker.Item label={GV.IND.BNK.name} value={GV.IND.BNK.value} />
          <Picker.Item label={GV.IND.BKRS.name} value={GV.IND.BKRS.value} />
          <Picker.Item label={GV.IND.TYR.name} value={GV.IND.TYR.values} />
        </Picker>
      </Block>
      {/* JOB TYPE */}
      <Block row center middle style={styles.recordKeeperCon}>
        <SubHeading>Job Type</SubHeading>
        <Picker
          itemStyle={{height: sizes.getHeight(iphone6s ? 6 : 5)}}
          selectedValue={state.jobType}
          onValueChange={(itemValue, index) =>
            localDispatch({type: JOB_TYPE, payload: itemValue})
          }
          // style={{width: sizes.getWidth(60), hieght: sizes.getHeight(5)}}
          style={{...styles.pickerStyle, width: sizes.getWidth(70)}}>
          <Picker.Item label={GV.TYPE.FREE.name} value={GV.TYPE.FREE.value} />
          <Picker.Item
            label={GV.TYPE.INTRN.name}
            value={GV.TYPE.INTRN.value}
          />
          <Picker.Item label={GV.TYPE.VOLN.name} value={GV.TYPE.VOLN.value} />
        </Picker>
      </Block>
      {/* TARGET SALARY */}
      <Block
        row
        center
        middle
        style={{
          ...styles.recordKeeperCon,
          // borderWidth: 1,
          height: sizes.getHeight(9),
        }}>
        <SubHeading>Target Salary</SubHeading>

        <Block
          center
          middle
          style={{borderWidth: 0, paddingBottom: sizes.getHeight(2)}}>
          <TextField
            value={state.targetSalary}
            placeholder={`Prev:${pereferred_job?.target_salary}`}
            name={SALARY}
            keyboardType={'numeric'}
            onChangeText={inputHandler}
            inputStyling={{
              width: '80%',
              borderBottomWidth: 1,
              marginTop: sizes.getHeight(2),
            }}
          />
        </Block>

        <Block>
          <Picker
            itemStyle={{height: sizes.getHeight(iphone6s ? 6 : 5)}}
            selectedValue={pereferred_job.target_currency}
            onValueChange={(itemValue, index) =>
              localDispatch({type: CURRNCY, payload: itemValue})
            }
            style={{...styles.pickerStyle, width: sizes.getWidth(22)}}>
            <Picker.Item
              label={GV.CRNCY.DLR.name}
              value={GV.CRNCY.DLR.value}
            />
            <Picker.Item
              label={GV.CRNCY.ERO.name}
              value={GV.CRNCY.ERO.value}
            />
          </Picker>
        </Block>
      </Block>
      {/* summary */}
      <Block
        row
        center
        flex={false}
        style={{
          ...styles.recordKeeperCon,
          marginBottom: 10,
          height: sizes.getHeight(15),
        }}>
        <SubHeading>Summary</SubHeading>
        <Block
          style={{
            // borderWidth: 1,
            marginTop: sizes.getHeight(5),
            // minHeight:sizes.getHeight(30),
            height: '100%',
            // overflow: 'hidden',
          }}>
          <TextField
            name={SUMRY}
            onChangeText={inputHandler}
            value={state.summary}
            placeholder="summary"
            multilines={true}
            numberOfLines={2}
            textAlignVertical="top"
            inputStyling={{
              borderStyle: 'dashed',
              elevation: 0.3,
              borderWidth: 0.6,
              borderRadius: sizes.getWidth(1),
              width: '100%',
              height: sizes.getHeight(13),
              padding: sizes.getWidth(2),
              // height: '100%',
            }}
          />
        </Block>
      </Block>
      {/* notice period */}
      <Block row center middle style={styles.recordKeeperCon}>
        <SubHeading>Notice Period</SubHeading>

        <Block>
          <Picker
            itemStyle={{height: sizes.getHeight(iphone6s ? 6 : 5)}}
            selectedValue={state.noticePeriod}
            onValueChange={(itemValue, index) =>
              localDispatch({type: NPRIOD, payload: itemValue})
            }
            style={{...styles.pickerStyle, width: sizes.getWidth(70)}}>
            <Picker.Item label={GV.NPRD.im.name} value={GV.NPRD.im.value} />
            <Picker.Item label={GV.NPRD.mo.name} value={GV.NPRD.mo.value} />
            <Picker.Item label={GV.NPRD.mt.name} value={GV.NPRD.mt.value} />
            <Picker.Item label={GV.NPRD.ms.name} value={GV.NPRD.ms.value} />
            <Picker.Item label={GV.NPRD.my.name} value={GV.NPRD.my.value} />
          </Picker>
        </Block>
      </Block>
      {/* ------------------------------------ */}

      <Block
        bottom
        crossRight
        flex={false}
        height={sizes.getHeight(8)}
        // style={{borderWidth: 1}}
      >
        <Button
          onPress={saveInfoHandler}
          disabled={isWaiting || state.disableSaveBtn}
          activeOpacity={0.6}
          center
          middle
          style={{
            borderRadius: sizes.getWidth(1),
            backgroundColor:
              state.disableSaveBtn || isWaiting
                ? colors.gray
                : colors.customRed,
            width: '40%',
          }}>
          {isWaiting ? (
            <ActivityIndicator size={'large'} color={colors.primary} />
          ) : (
            <Text color={colors.primary}>Save</Text>
          )}
        </Button>
      </Block>
    </Block>