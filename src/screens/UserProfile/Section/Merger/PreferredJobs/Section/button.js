import React from 'react'
import { Block, Button, Text } from 'components'
import { sizes, colors } from 'styles/theme'

const CustomBtn = props => {
    const { onPress, sign } = props
    return (
        <Block center middle>
            <Button
                activeOpacity={0.5}
                onPress={() => onPress()}
                center
                middle
                style={{
                    width: '100%',
                    borderRadius: sizes.getWidth(1),
                    borderWidth: 0.6,
                    borderColor: colors.gray,
                    borderStyle: 'dashed'
                }}>
                <Text bold h1 color={colors.gray}>{sign || "-"}</Text>
            </Button>
        </Block>
    )
}

export {CustomBtn}
