import React from 'react';
import {Heading} from '../../Section';
import {
  StyleSheet,
  ActivityIndicator,
  Modal,
  Image,
  Button,
  View,
  ToastAndroid,
} from 'react-native';
import {sizes} from 'styles/theme';
import {Block} from 'components';
import {useReducer, useState, useEffect, useRef} from 'react';
import {Text, TextField, TextFieldOne, SquareButton} from 'components';
import {SubHeading} from '../../Section';
import * as icons from 'assets/icons';
import {colors} from 'styles/theme';
import {useDispatch, useSelector} from 'react-redux';
import {TouchableOpacity} from 'react-native-gesture-handler';
import axios from 'axios';
import {GetProfileInfo} from 'redux/actions';

const ReferencesInfo = props => {
  const {showWaiting, message} = props;
  const auth = useSelector(state => state.userInfo.userData.profile.references);
  const dispatch = useDispatch();

  const [modalVisible, setModalVisible] = useState(false);
  const [name, setName] = useState('');
  const [level, setLevel] = useState('');
  const [companyName, setCompanyName] = useState('');
  const [email, setEmail] = useState('');
  const [phoneno, setPhoneno] = useState('');
  const [skillId, setSkilllId] = useState('');

  const id = useSelector(state => state.userInfo.userData.user.id);

  async function fetchProfileRecords() {
    const result = await GetProfileInfo(id, err => {
      return console.log(err);
    });
    result && dispatch(result);
  }

  const removeEdu = async eduid => {
    try {
      console.log('DATA BEFORE deletion========' + eduid);
      const response = await axios
        .get(
          'https://dawami.wedigits.dev/api/profile/remove/reference/' + eduid,
        )
        .then(function(res, err) {
          if (res.data) {
            fetchProfileRecords();
            console.log(res + ' updated');
          }
        });
      fetchProfileRecords();
      console.log('API =========RESPONSE IS ================');
      console.log('yess');
    } catch (e) {
      console.log('error catching');
      console.log(e);
      //  onError(e);
    }
  };

  const addHobby = async () => {
    try {
      let form = new FormData();
      form.append('name', name);
      form.append('job_title', level);
      form.append('email', email);
      form.append('company_name', companyName);
      form.append('number', phoneno);

      console.log('DATA BEFORE SUBMITTION========');
      if (name !== '' && level !== '') {
        const response = await axios
          .post(
            'https://dawami.wedigits.dev/api/profile/' + id + '/add/reference',
            form,
          )
          .then(function(res, err) {
            if (res.data) {
              GetProfileInfo(id, err => {
                return console.log(err);
              });
              setName('');
              setLevel('');
              setCompanyName('');
              setEmail('');
              setPhoneno('');
              setSkilllId('');
              setModalVisible(false);
              fetchProfileRecords();
              console.log(res);
            }
          });
        console.log('API =========RESPONSE IS ================');
        console.log('yess');
      } else {
        ToastAndroid.show('Fill', ToastAndroid.LONG);
        console.log('no');
      }
    } catch (e) {
      console.log('error catching');
      console.log(e);
      //  onError(e);
    }
  };

  const updateHobby = async () => {
    try {
      let form = new FormData();
      form.append('name', name);
      form.append('job_title', level);
      form.append('email', email);
      form.append('company_name', companyName);
      form.append('number', phoneno);
      //  setSkillapi(skills => [...skills, auth])
      console.log(JSON.stringify(id) + 'uess');
      console.log('DATA BEFORE SUBMITTION========');
      if (name !== '' && level !== '') {
        const response = await axios
          .post(
            'https://dawami.wedigits.dev/api/profile/' +
              skillId +
              '/update/reference',
            form,
          )
          .then(function(res, err) {
            if (res.data) {
              GetProfileInfo(id, err => {
                return console.log(err);
              });
              setName('');
              setLevel('');
              setSkilllId('');
              setModalVisible(false);
              fetchProfileRecords();
              console.log(res);
            }
          });
        console.log('API =========RESPONSE IS ================');
        console.log('yess');
      } else {
        ToastAndroid.show('Fill', ToastAndroid.LONG);
        console.log('no');
      }
    } catch (e) {
      console.log('error catching');
      console.log(e);
      //  onError(e);
    }
  };
  return (
    <View>
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
          setName('');
          setLevel('');
          setCompanyName('');
          setEmail('');
          setPhoneno('');
          setSkilllId('');
        }}>
        <View
          style={{
            //  justifyContent: 'center',
            alignItems: 'center',
            margin: 10,
            padding: 10,
            // flex:0.5,
            marginTop: sizes.getHeight(10),
            height: 430,
            width: '95%',
            backgroundColor: '#fff',
            elevation: 1,
            borderRadius: sizes.withScreen(0.006),
          }}>
          <Block
            center
            margin={[sizes.getHeight(2), 0, 0, 0]}
            flex={false}
            row
            center
            style={styles.recordKeeperCon}>
            <SubHeading>Name</SubHeading>
            <Block>
              <TextField
                value={name}
                
                onChangeText={text => {
                  setName(text.text);
                }}
                inputStyling={{
                  borderBottomWidth: 1,
                  width: '100%',
                  margin: 0,
                  paddingBottom: 0,
                }}
              />
            </Block>
          </Block>

          <Block
            center
            margin={[sizes.getHeight(2), 0, 0, 0]}
            flex={false}
            row
            center
            style={styles.recordKeeperCon}>
            <SubHeading>Job Title</SubHeading>
            <Block>
              <TextField
                
                value={level}
                onChangeText={text => {
                  setLevel(text.text);
                }}
                inputStyling={{
                  borderBottomWidth: 1,
                  width: '100%',
                  margin: 0,
                  paddingBottom: 0,
                }}
              />
            </Block>
          </Block>

          <Block
            center
            margin={[sizes.getHeight(2), 0, 0, 0]}
            flex={false}
            row
            center
            style={styles.recordKeeperCon}>
            <SubHeading>Company Name</SubHeading>
            <Block>
              <TextField
                
                value={companyName}
                onChangeText={text => {
                  setCompanyName(text.text);
                }}
                inputStyling={{
                  borderBottomWidth: 1,
                  width: '100%',
                  margin: 0,
                  paddingBottom: 0,
                }}
              />
            </Block>
          </Block>

          <Block
            center
            margin={[sizes.getHeight(2), 0, 0, 0]}
            flex={false}
            row
            center
            style={styles.recordKeeperCon}>
            <SubHeading>Email</SubHeading>
            <Block>
              <TextField
                value={email}
                
                onChangeText={text => {
                  setEmail(text.text);
                }}
                inputStyling={{
                  borderBottomWidth: 1,
                  width: '100%',
                  margin: 0,
                  paddingBottom: 0,
                }}
              />
            </Block>
          </Block>

          <Block
            center
            margin={[sizes.getHeight(2), 0, 0, 0]}
            flex={false}
            row
            center
            style={styles.recordKeeperCon}>
            <SubHeading>Phone Number</SubHeading>
            <Block>
              <TextField
                value={phoneno}
                
                onChangeText={text => {
                  setPhoneno(text.text);
                }}
                inputStyling={{
                  borderBottomWidth: 1,
                  width: '100%',
                  margin: 0,
                  paddingBottom: 0,
                }}
              />
            </Block>
          </Block>

          <View style={{flexDirection: 'row', marginTop: sizes.getHeight(4)}}>
            <View style={{flex: 0.4, margin: 5}}>
              <SquareButton
                textColor={colors.primary}
                // bgColor={colors.customRed}
                width={sizes.getWidth(30)}
                height={sizes.getHeight(6)}
                onPress={() => {
                  console.log(skillId);
                  if(skillId == ""){
                    console.log("add");
                    addHobby();
                  } else {
                    updateHobby();
                    console.log("update");
                  }
                  
                }}
                style={{
                  // backgroundColor: btnDisable ? 'transparent' : colors.customRed,
                  backgroundColor: colors.customRed,
                  borderWidth: 0,
                  borderRadius: sizes.withScreen(0.01),
                }}>
                Submit
              </SquareButton>
            </View>
            <View style={{flex: 0.4, margin: 5}}>
              <SquareButton
                textColor={colors.primary}
                // bgColor={colors.customRed}
                width={sizes.getWidth(25)}
                height={sizes.getHeight(6)}
                onPress={() => {
                  setModalVisible(false);
                  setName('');
                  setLevel('');
                  setCompanyName('');
                  setEmail('');
                  setPhoneno('');
                  setSkilllId('');
                }}
                style={{
                  // backgroundColor: btnDisable ? 'transparent' : colors.customRed,
                  backgroundColor: '#B22222',
                  borderWidth: 0,
                  borderRadius: sizes.withScreen(0.015),
                }}>
                Cancel
              </SquareButton>
            </View>
          </View>
       
        </View>
      </Modal>

      <TouchableOpacity
        style={{margin: 12}}
        onPress={() => {
          setModalVisible(true);
        }}>
        <Image
          source={icons.add}
          style={{
            height: sizes.getHeight(4),
            resizeMode: 'contain',
            width: sizes.getWidth(10),
            alignSelf: 'flex-end',
          }}
        />
      </TouchableOpacity>

      {auth.map(item => (
        <Block padding={[0, 0, sizes.getHeight(3), 0]}>
          <Block
            center
            margin={[sizes.getHeight(2), 0, 0, 0]}
            flex={false}
            row
            center
            style={styles.recordKeeperCon}>
            <SubHeading>Name</SubHeading>
            <Block>
              <TextField
                value={item.name}
                inputStyling={{
                  borderBottomWidth: 1,
                  width: '100%',
                  margin: 0,
                  paddingBottom: 0,
                }}
              />
            </Block>
          </Block>

          <Block
            center
            margin={[sizes.getHeight(2), 0, 0, 0]}
            flex={false}
            row
            center
            style={styles.recordKeeperCon}>
            <SubHeading>Job Title</SubHeading>
            <Block>
              <TextField
                value={item.job_title}
                inputStyling={{
                  borderBottomWidth: 1,
                  width: '100%',
                  margin: 0,
                  paddingBottom: 0,
                }}
              />
            </Block>
          </Block>

          <Block
            center
            margin={[sizes.getHeight(2), 0, 0, 0]}
            flex={false}
            row
            center
            style={styles.recordKeeperCon}>
            <SubHeading>Company Name</SubHeading>
            <Block>
              <TextField
                value={item.company_name}
                inputStyling={{
                  borderBottomWidth: 1,
                  width: '100%',
                  margin: 0,
                  paddingBottom: 0,
                }}
              />
            </Block>
          </Block>

          <Block
            center
            margin={[sizes.getHeight(2), 0, 0, 0]}
            flex={false}
            row
            center
            style={styles.recordKeeperCon}>
            <SubHeading>Email</SubHeading>
            <Block>
              <TextField
                value={item.email}
                inputStyling={{
                  borderBottomWidth: 1,
                  width: '100%',
                  margin: 0,
                  paddingBottom: 0,
                }}
              />
            </Block>
          </Block>

          <Block
            center
            margin={[sizes.getHeight(2), 0, 0, 0]}
            flex={false}
            row
            center
            style={styles.recordKeeperCon}>
            <SubHeading>Phone Number</SubHeading>
            <Block>
              <TextField
                value={item.number}
                inputStyling={{
                  borderBottomWidth: 1,
                  width: '100%',
                  margin: 0,
                  paddingBottom: 0,
                }}
              />
            </Block>
          </Block>

          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              marginTop: sizes.getHeight(3),
            }}>
            <TouchableOpacity
              style={{width: 100, alignSelf: 'center'}}
              onPress={() => {
                setModalVisible(true);
              }}>
              <View style={styles.editbtn}>
                <Text
                  style={{textAlign: 'center', fontSize: 16, color: 'white'}}
                  onPress={() => {
                    setSkilllId(item.id);
                    setModalVisible(true);
                    setName(item.name);
                    setLevel(item.job_title);
                    setCompanyName(item.company_name);
                    setEmail(item.email);
                    setPhoneno(item.number);
                  }}>
                  Edit
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                removeEdu(item.id);
              }}>
              <Image
                source={icons.remove}
                style={{
                  height: sizes.getHeight(4),
                  resizeMode: 'contain',
                  width: sizes.getWidth(10),
                  alignSelf: 'flex-end',
                  marginLeft: 15,
                }}
              />
            </TouchableOpacity>
          </View>
        
        </Block>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  recordKeeperCon: {
    height: sizes.getHeight(5),
    marginBottom: sizes.getHeight(2),
  },
  modalDesign: {
    backgroundColor: colors.black,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  editbtn: {
    backgroundColor: colors.customRed,
    alignSelf: 'center',
    width: 100,
    padding: 6,
    borderRadius: 5,
  },
});

export {ReferencesInfo};
