import React from 'react';
import {Heading} from '../../Section';

import {
  StyleSheet,
  ActivityIndicator,
  Modal,
  Image,
  Button,
  View,
  ToastAndroid,
} from 'react-native';
import {sizes} from 'styles/theme';
import {Block} from 'components';
import {useReducer, useState, useEffect, useRef} from 'react';
import {Text, TextField, TextFieldOne, SquareButton} from 'components';
import {SubHeading} from '../../Section';
import * as icons from 'assets/icons';
import {colors} from 'styles/theme';
import {useDispatch, useSelector} from 'react-redux';
import {TouchableOpacity} from 'react-native-gesture-handler';
import axios from 'axios';
import {GetProfileInfo} from 'redux/actions';

const Hobbies = props => {
  const {showWaiting, message} = props;
  const auth = useSelector(state => state.userInfo.userData.profile.hobbies);
  const dispatch = useDispatch();

  const [modalVisible, setModalVisible] = useState(false);
  const [name, setName] = useState('');
  const [level, setLevel] = useState('');
  const [skillId, setSkilllId] = useState('');

  const id = useSelector(state => state.userInfo.userData.user.id);

  async function fetchProfileRecords() {
    const result = await GetProfileInfo(id, err => {
      return console.log(err);
    });
    result && dispatch(result);
  }

  const addHobby = async () => {
    try {
      let form = new FormData();
      form.append('name', name);
      form.append('accomplishment', level);
      //  setSkillapi(skills => [...skills, auth])
      console.log(JSON.stringify(id) + 'uess');
      console.log('DATA BEFORE SUBMITTION========');
      if (name !== '' && level !== '') {
        const response = await axios
          .post(
            'https://dawami.wedigits.dev/api/profile/' + id + '/add/hobby',
            form,
          )
          .then(function(res, err) {
            if (res.data) {
              GetProfileInfo(id, err => {
                return console.log(err);
              });
              setName('');
              setLevel('');
              setSkilllId('');
              setModalVisible(false);
              fetchProfileRecords();
              console.log(res);
            }
          });
        console.log('API =========RESPONSE IS ================');
        console.log('yess');
      } else {
        ToastAndroid.show('Fill', ToastAndroid.LONG);
        console.log('no');
      }
    } catch (e) {
      console.log('error catching');
      console.log(e);
      //  onError(e);
    }
  };

  const updateHobby = async () => {
    try {
      let form = new FormData();
      form.append('name', name);
      form.append('accomplishment', level);
      //  setSkillapi(skills => [...skills, auth])
      console.log(JSON.stringify(id) + 'uess');
      console.log('DATA BEFORE SUBMITTION========');
      if (name !== '' && level !== '') {
        const response = await axios
          .post(
            'https://dawami.wedigits.dev/api/profile/' +
              skillId +
              '/update/hobby',
            form,
          )
          .then(function(res, err) {
            if (res.data) {
              GetProfileInfo(id, err => {
                return console.log(err);
              });
              setName('');
              setLevel('');
              setSkilllId('');
              setModalVisible(false);
              fetchProfileRecords();
              console.log(res);
            }
          });
        console.log('API =========RESPONSE IS ================');
        console.log('yess');
      } else {
        ToastAndroid.show('Fill', ToastAndroid.LONG);
        console.log('no');
      }
    } catch (e) {
      console.log('error catching');
      console.log(e);
      //  onError(e);
    }
  };

  const removeEdu = async eduid => {
    try {
      console.log('DATA BEFORE deletion========' + eduid);
      const response = await axios
        .get('https://dawami.wedigits.dev/api/profile/remove/hobby/' + eduid)
        .then(function(res, err) {
          if (res.data) {
            fetchProfileRecords();
            console.log(res + ' updated');
          }
        });
      fetchProfileRecords();
      console.log('API =========RESPONSE IS ================');
      console.log('yess');
    } catch (e) {
      console.log('error catching');
      console.log(e);
      //  onError(e);
    }
  };

  return (
    <View>
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
          setName('');
          setLevel('');
          setSkilllId('');
        }}>
        <View
          style={{
            //  justifyContent: 'center',
            alignItems: 'center',
            margin: 10,
            // flex:0.5,
            marginTop: sizes.getHeight(10),
            height: '40%',
            width: '95%',
            backgroundColor: '#fff',
            elevation: 1,
            borderRadius: sizes.withScreen(0.006),
          }}>
          <Block
            center
            margin={[sizes.getHeight(5), 10, 10, 10]}
            flex={false}
            row
            center
            style={styles.recordKeeperCon}>
            <Block>
              <SubHeading>Hobby Name:</SubHeading>
            </Block>
            <Block
              padding={[
                0,
                sizes.getWidth(0),
                sizes.getWidth(0),
                sizes.getWidth(0),
              ]}
              flex={3}
              style={{borderWidth: 0}}>
              <TextFieldOne
                //    ref={nameRef}
                onChangeText={text => {
                  setName(text.text);
                  //  setSkillName(text.text)
                }}
                value={name}
                inputStyling={{
                  borderBottomWidth: 1,
                  borderBottomColor: colors.gray2,
                  width: '100%',
                  padding: 0,
                  paddingBottom: 0,
                }}
              />
            </Block>
          </Block>

          <Block
            flex={false}
            row
            center
            style={styles.recordKeeperCon}
            margin={[sizes.getHeight(3), 10, 10, 10]}>
            <Block>
              <SubHeading>Accomplishment</SubHeading>
            </Block>
            <Block
              padding={[
                0,
                sizes.getWidth(0),
                sizes.getWidth(0),
                sizes.getWidth(0),
              ]}
              flex={3}
              style={{borderWidth: 0}}>
              <TextFieldOne
                //    ref={nameRef}
                onChangeText={text => {
                  setLevel(text.text);
                }}
                value={level}
                inputStyling={{
                  borderBottomWidth: 1,
                  borderBottomColor: colors.gray2,
                  width: '100%',
                  padding: 0,
                  paddingBottom: 0,
                }}
              />
            </Block>
          </Block>
          <View style={{flexDirection: 'row', marginTop: sizes.getHeight(4)}}>
            <View style={{flex: 0.4, margin: 5}}>
              <SquareButton
                textColor={colors.primary}
                // bgColor={colors.customRed}
                width={sizes.getWidth(30)}
                height={sizes.getHeight(6)}
                onPress={() => {
                  if (skillId != '') {
                    updateHobby();
                  } else {
                    addHobby();
                  }
                }}
                style={{
                  // backgroundColor: btnDisable ? 'transparent' : colors.customRed,
                  backgroundColor: colors.customRed,
                  borderWidth: 0,
                  borderRadius: sizes.withScreen(0.01),
                }}>
                Submit
              </SquareButton>
            </View>
            <View style={{flex: 0.4, margin: 5}}>
              <SquareButton
                textColor={colors.primary}
                // bgColor={colors.customRed}
                width={sizes.getWidth(25)}
                height={sizes.getHeight(6)}
                onPress={() => {
                  setModalVisible(false);
                  setName('');
                  setLevel('');
                  setSkilllId('');
                }}
                style={{
                  // backgroundColor: btnDisable ? 'transparent' : colors.customRed,
                  backgroundColor: '#B22222',
                  borderWidth: 0,
                  borderRadius: sizes.withScreen(0.015),
                }}>
                Cancel
              </SquareButton>
            </View>
          </View>
        </View>
      </Modal>

      <TouchableOpacity
        style={{margin: 12}}
        onPress={() => {
          setModalVisible(true);
        }}>
        <Image
          source={icons.add}
          style={{
            height: sizes.getHeight(4),
            resizeMode: 'contain',
            width: sizes.getWidth(10),
            alignSelf: 'flex-end',
          }}
        />
      </TouchableOpacity>
      {auth.map(item => (
        <Block
          style={{borderWidth: 0}}
          margin={[0, 3, sizes.getHeight(0), 10]}
          padding={[sizes.getHeight(1), 0, sizes.getHeight(0), 0]}>
          <Block
            center
            margin={[sizes.getHeight(0), 0, 0, 0]}
            flex={false}
            row
            center
            style={styles.recordKeeperCon}>
            <Block>
              <SubHeading>Hobby Name</SubHeading>
            </Block>
            <Block
              padding={[
                0,
                sizes.getWidth(2),
                sizes.getWidth(2),
                sizes.getWidth(2),
              ]}
              flex={2}
              style={{borderWidth: 0}}>
              <TextFieldOne
                //    ref={nameRef}
                // onChangeText={}
                // name={}
                editable={false}
                value={item.name}
                inputStyling={{
                  borderBottomWidth: 1,
                  borderBottomColor: colors.gray2,
                  width: '100%',
                  margin: 0,
                  paddingBottom: 0,
                }}
              />
            </Block>
          </Block>

          <Block
            center
            margin={[sizes.getHeight(0), 0, 0, 0]}
            flex={false}
            row
            center
            style={styles.recordKeeperCon}>
            <Block>
              <SubHeading>Accomplishment</SubHeading>
            </Block>
            <Block
              padding={[
                0,
                sizes.getWidth(2),
                sizes.getWidth(2),
                sizes.getWidth(2),
              ]}
              flex={2}
              style={{borderWidth: 0}}>
              <TextFieldOne
                //    ref={nameRef}
                // onChangeText={}
                // name={}
                editable={false}
                value={item.accomplishment}
                inputStyling={{
                  borderBottomWidth: 1,
                  borderBottomColor: colors.gray2,
                  width: '100%',
                  margin: 0,
                  paddingBottom: 0,
                }}
              />
            </Block>
          </Block>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              style={{flex: 1}}
              onPress={() => {
                setSkilllId(item.id);
                setModalVisible(true);
                setName(item.name);
                setLevel(item.accomplishment);
              }}>
              <View style={styles.editbtn}>
                <Text
                  style={{textAlign: 'center', fontSize: 16, color: 'white'}}>
                  Edit
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={{flex: 1}}
              onPress={() => {
                removeEdu(item.id);
              }}>
              <Image
                source={icons.remove}
                style={{
                  height: sizes.getHeight(4),
                  resizeMode: 'contain',
                  width: sizes.getWidth(10),
                  alignSelf: 'flex-end',
                  marginLeft: 15,
                }}
              />
            </TouchableOpacity>
          </View>
        </Block>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  recordKeeperCon: {
    height: sizes.getHeight(5),
    marginBottom: sizes.getHeight(2),
  },
  modalDesign: {
    backgroundColor: colors.black,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  editbtn: {
    backgroundColor: colors.customRed,
    alignSelf: 'center',
    width: 100,
    padding: 6,
    borderRadius: 5,
  },
});

export {Hobbies};
