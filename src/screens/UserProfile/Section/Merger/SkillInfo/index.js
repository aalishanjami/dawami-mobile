import React, { useState } from 'react';

import {
  StyleSheet,
  ActivityIndicator,
  Image,
  Modal,
  TouchableHighlight,
  Button,
  ToastAndroid,
  KeyboardAvoidingView
} from 'react-native';
import { TouchableOpacity, TouchableWithoutFeedback, TouchableNativeFeedback } from 'react-native-gesture-handler'
import axios from 'axios';
import { sizes, fonts } from 'styles/theme';
import { Block, SquareButton } from 'components';
import * as icons from 'assets/icons';
import { Text, TextField, TextFieldOne } from 'components';
import { SubHeading } from '../../Section';
import { colors } from 'styles/theme';
import { Picker } from '@react-native-community/picker';
import { useDispatch, useSelector } from 'react-redux';
import { View } from 'react-native-animatable';
import { GetProfileInfo } from 'redux/actions';


const Skills = props => {

  const dispatch = useDispatch();
  const { showWaiting, message } = props;
  const skillData = useSelector(state => state.userInfo.userData.profile.skills);
  const id = useSelector(state => state.userInfo.userData.user.id);

  const Skill = {
    level: [
      { name: 'Beginner', value: '1' },
      { name: 'intermediate', value: '2' },
      { name: 'expert', value: '3' },
    ],
    showModal: false,
    //  skill_name: '',
    //  skill_level: '',
  };
  const [modalVisible, setModalVisible] = useState(false);
  //const [skills, setskills] = useState(skillData);
  const [skill_id, setskillId] = useState('');
  const [name, setName] = useState('');
  const [level, setLevel] = useState('');

  async function fetchProfileRecords() {
    const result = await GetProfileInfo(id, err => {
      return (
        console.log("error")
      );
    });
    console.log(JSON.stringify(result) + "sasfsadsasad");
    result && (dispatch(result));
  }

  const addSkill = async () => {
    try {
      let form = new FormData();
      form.append("name", name)
      form.append("level", level)
      console.log(JSON.stringify(id) + "uess")
      console.log('DATA BEFORE SUBMITTION========');
      if (name !== '' && level !== '') {
        const response = await axios.post(
          'https://dawami.wedigits.dev/api/profile/' + id + '/add/skills',
          form,
        ).then(function (res, err) {
          if (res.data) {
            setModalVisible(false)
            fetchProfileRecords();
            //console.log(res)

          }
        })
        console.log('API =========RESPONSE IS ================');
        console.log("yess")
      }
      else {
        ToastAndroid.show("Fill", ToastAndroid.LONG)
        console.log("no")
      }
    }
    catch (e) {
      console.log('error catching');
      console.log(e);
      //  onError(e);
    }
  };

  const removeEdu = async (eduid) => {
    try {
     console.log('DATA BEFORE deletion========' + eduid);
        const response = await axios.get(
          'https://dawami.wedigits.dev/api/profile/remove/skill/'+eduid,
        ).then(function(res,err){
          if (res.data) {
            
            fetchProfileRecords()
           console.log(res +" updated")
          }

        })
        fetchProfileRecords()
       console.log('API =========RESPONSE IS ================');
          console.log("yess")
     
      }
      catch (e) {
        console.log('error catching');
        console.log(e);
      //  onError(e);
      }  
  };

  const updateSkill = async () => {
    try {
      console.log(skill_id + "skill id")
      let form = new FormData();
      form.append("name", name)
      form.append("level", level)
      console.log(JSON.stringify(id) + "uess")
      const url = 'https://dawami.wedigits.dev/api/profile/' + skill_id + '/update/skills'
      console.log('DATA BEFORE SUBMITTION========');
      if (name !== '' && level !== '') {
        const response = await axios.post(url,
          form,
        ).then(function (res, err) {
          if (res.data) {
            setskillId('')
            setModalVisible(false)
            fetchProfileRecords()
            console.log(res)
          }
        })
        console.log('API =========RESPONSE IS ================');
        console.log("yess")
      }
      else {
        ToastAndroid.show("Fill", ToastAndroid.LONG)
        console.log("no")
      }
    }
    catch (e) {
      console.log('error catching');
      console.log(e);
      //  onError(e);
    }
  };


  return (
    <View>
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
          skill_id ?
            setskillId('')
            : null
        }}>
        <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled>
          <View
            style={{
              //  justifyContent: 'center',
              alignItems: 'center',
              margin: 10,
              // flex:0.5,
              marginTop: sizes.getHeight(10),
              height: sizes.getHeight(40),
              padding: sizes.getWidth(5),
              backgroundColor: '#fff', elevation: 1, borderRadius: sizes.withScreen(0.006)
            }}>
            <Block
              center
              margin={[sizes.getHeight(5), 10, 10, 10]}
              flex={false}
              row
              center
              style={styles.recordKeeperCon}>
              <Block>
                <SubHeading>Skill Name:</SubHeading>
              </Block>
              <Block
                padding={[
                  0,
                  sizes.getWidth(0),
                  sizes.getWidth(0),
                  sizes.getWidth(0),
                ]}
                flex={3}
                style={{ borderWidth: 0 }}>

                <TextFieldOne
                  //    ref={nameRef}
                  onChangeText={text => {
                    setName(text.text);
                    //  setSkillName(text.text) 
                    console.log(JSON.stringify(Skill.skill_name) + "inside");
                  }}
                  value={name}
                  inputStyling={{
                    borderBottomWidth: 1,
                    borderBottomColor: colors.gray2,
                    width: '100%',
                    padding: 0,
                    paddingBottom: 0,
                  }}
                />
              </Block>
            </Block>

            <Block flex={false} row center style={styles.recordKeeperCon}
              margin={[sizes.getHeight(3), 10, 10, 10]}
            >
              <Block>
                <SubHeading>Skill level</SubHeading>
              </Block>
              <Block
                padding={[
                  0,
                  sizes.getWidth(0),
                  sizes.getWidth(0),
                  sizes.getWidth(0),
                ]}
                flex={3}
                style={{ borderWidth: 0 }}>

                <Picker
                  selectedValue={level}
                  onValueChange={(itemValue, index) =>
                    setLevel(itemValue)
                  }
                  style={styles.pickerStyle}>
                  {Skill.level.map(itemm => (
                    <Picker.Item label={itemm.name} value={itemm.value} />
                  ))}
                </Picker>
              </Block>
            </Block>
            <View style={{flexDirection:'row',marginTop:sizes.getHeight(4)}}>
              <View style={{ flex: 0.4, margin: 5 }}>
                <SquareButton
                  textColor={colors.primary}
                  // bgColor={colors.customRed}
                  width={sizes.getWidth(30)}
                  height={sizes.getHeight(6)}
                  onPress={() =>
                    skill_id == '' ? addSkill : updateSkill()
                  }
                  style={{
                    // backgroundColor: btnDisable ? 'transparent' : colors.customRed,
                    backgroundColor: colors.customRed,
                    borderWidth: 0,
                    borderRadius: sizes.withScreen(0.01),
                  }}>
                  Submit
          </SquareButton>
              </View>
              <View style={{ flex: 0.4, margin: 5 }}>
                <SquareButton
                  textColor={colors.primary}
                  // bgColor={colors.customRed}
                  width={sizes.getWidth(25)}
                  height={sizes.getHeight(6)}
                  onPress={() =>
                    setModalVisible(false)}
                  style={{
                    // backgroundColor: btnDisable ? 'transparent' : colors.customRed,
                    backgroundColor: '#B22222',
                    borderWidth: 0,
                    borderRadius: sizes.withScreen(0.015),
                  }}>
                  Cancel
          </SquareButton>
              </View>
            </View>
          </View>
        </KeyboardAvoidingView>
      </Modal>

      <TouchableOpacity
        onPress={() => {
          setModalVisible(true);
          setName('')
          setskillId('')
          setLevel('')
        }}>
        <Image
          source={icons.add}
          style={{
            height: sizes.getHeight(4),
            resizeMode: 'contain',
            width: sizes.getWidth(10),
            alignSelf: 'flex-end',
            marginTop: 10
          }}
        />
      </TouchableOpacity>
      {skillData.map((item, index) => (
        <Block
          style={{ borderWidth: 0 }}
          margin={[7, 2, sizes.getHeight(0), 10]}
          padding={[sizes.getHeight(0.1), 0, sizes.getHeight(0), 0]}>

          <Block
            center
            margin={[sizes.getHeight(0), 0, 0, 0]}
            flex={false}
            row
            center
            style={styles.recordKeeperCon}>
            <Block>
              <SubHeading>Skill Name</SubHeading>
            </Block>
            <Block
              padding={[
                0,
                sizes.getWidth(2),
                sizes.getWidth(2),
                sizes.getWidth(2),
              ]}
              flex={2}
              style={{ borderWidth: 0 }}>
              <TextFieldOne
                //    ref={nameRef}
                // onChangeText={}
                // name={}
                editable={false}
                value={item.name}
                inputStyling={{
                  borderBottomWidth: 1,
                  borderBottomColor: colors.gray2,
                  width: '100%',
                  margin: 0,
                  paddingBottom: 3,
                }}
              />
            </Block>
          </Block>
          <Block flex={false} row center style={styles.recordKeeperCon}>
            <Block>
              <SubHeading>
                Skill level
              </SubHeading>
            </Block>
            <Block
              padding={[
                0,
                sizes.getWidth(2),
                sizes.getWidth(2),
                sizes.getWidth(2),
              ]}
              flex={2}
              style={{ borderWidth: 0 }}>
              <Picker
                selectedValue={item.level}
                enabled={false}
                // onValueChange={(itemValue, index) =>
                // localDispatch({type: COUNTRY, payload: itemValue})
                //  }
                style={styles.pickerStyle}>
                {Skill.level.map(itemm => (
                  <Picker.Item label={itemm.name} value={itemm.value} />
                ))}
              </Picker>
            </Block>
          </Block>
          <View
           style={{flexDirection:'row',alignSelf:'center',marginTop:sizes.getHeight(3)}}>
           <TouchableOpacity
              style={{ width: 100, alignSelf: 'center' }}
              onPress={() => {
                setModalVisible(true);

              }}><View style={styles.editbtn}>
                <Text style={{ textAlign: 'center', fontSize: 16, color: 'white' }} onPress={() => {
                  setName(item.name)
                  setLevel(item.level)
                  setskillId(item.id)
                }}>Edit</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
        onPress={()=>{
          removeEdu(item.id)
        }}
        >
        <Image
          source={icons.remove}
          style={{
            height: sizes.getHeight(4),
            resizeMode: 'contain',
            width: sizes.getWidth(10),
            alignSelf:'flex-end',
            marginLeft:15
          }}
        />
      </TouchableOpacity>
          </View>
          {/* {index % 1 == 0 ?
          <View style={{backgroundColor:'grey',height:sizes.getHeight(0.2)}}></View> : null
          } */}
        </Block>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  recordKeeperCon: {
    height: sizes.getHeight(5)
  },
  pickerStyle: {
    width: sizes.getWidth(60),
    marginTop: sizes.getHeight(1),
    transform: [{ scaleX: 0.9 }, { scaleY: 0.9 }],
    height: sizes.getHeight(5),
    // alignSelf: 'stretch',
    // flex: 1,
  },
  modalDesign: {
    backgroundColor: colors.black,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  editbtn: {
    backgroundColor: colors.customRed, alignSelf: 'center', width: 100, padding: 6, borderRadius: 5
  }
});

export { Skills };
