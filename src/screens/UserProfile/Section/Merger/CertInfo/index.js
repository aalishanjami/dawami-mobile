import React from 'react';
import {Heading} from '../../Section';
import {theme} from 'styles';
import {
  StyleSheet,
  ActivityIndicator,
  Modal,
  Image,
  Button,
  View,
  ToastAndroid,
  TouchableHighlight,
  TouchableHighlightBase,
  TouchableNativeFeedback,
  TouchableNativeFeedbackBase,
  TouchableOpacity,
} from 'react-native';
import {sizes} from 'styles/theme';
import {Block} from 'components';
import {useReducer, useState, useEffect, useRef} from 'react';
import {Text, TextField, TextFieldOne, SquareButton} from 'components';
import {SubHeading} from '../../Section';
import * as icons from 'assets/icons';
import {colors} from 'styles/theme';
import {useDispatch, useSelector} from 'react-redux';
//import {TouchableOpacity} from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';
import axios from 'axios';
import {GetProfileInfo} from 'redux/actions';
import DatePicker from 'react-native-datepicker';
import {Picker} from '@react-native-community/picker';

const CertInfo = props => {
  const {showWaiting, message} = props;
  const auth = useSelector(
    state => state.userInfo.userData.profile.certificates,
  );
  const dispatch = useDispatch();

  const Type = {
    type: [{name: 'Training', value: '0'}, {name: 'Certificate', value: '1'}],
  };

  const options = {
    title: 'Select Avatar',
    customButtons: [{name: 'fb', title: 'Choose Photo from Facebook'}],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  const removeEdu = async eduid => {
    try {
      console.log('DATA BEFORE deletion========' + eduid);
      const response = await axios
        .get(
          'https://dawami.wedigits.dev/api/profile/remove/certificate/' + eduid,
        )
        .then(function(res, err) {
          if (res.data) {
            fetchProfileRecords();
            console.log(res + ' updated');
          }
        });
      fetchProfileRecords();
      console.log('API =========RESPONSE IS ================');
      console.log('yess');
    } catch (e) {
      console.log('error catching');
      console.log(e);
      //  onError(e);
    }
  };

  const [modalVisible, setModalVisible] = useState(false);
  const [name, setName] = useState('');
  const [type, setType] = useState('');
  const [certificatefile, setCertificatefile] = useState('');
  const [institutionname, setInstitutionname] = useState('');
  const [startdate, setStartdate] = useState('');
  const [expirydate, setExpirydate] = useState('');
  const [totalhours, setTotalhours] = useState('');
  const [uri, setUri] = useState('');
  const [imagetype, setImagetype] = useState('');
  const [skillId, setSkilllId] = useState('');

  const id = useSelector(state => state.userInfo.userData.user.id);

  async function fetchProfileRecords() {
    const result = await GetProfileInfo(id, err => {
      return console.log(err);
    });
    result && dispatch(result);
  }
  const pickImage = async () => {
    ImagePicker.launchImageLibrary(options, response => {
      console.log('Response=', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        setImagetype(response.type);
        setCertificatefile(response.fileName);
        setUri(response.uri);
      }
    });
  };

  const addHobby = async () => {
    try {
      let form = new FormData();
      form.append('certificate_file', certificatefile);
      form.append('type', type);
      form.append('name', name);
      form.append('institution_name', institutionname);
      form.append('start_date', startdate);
      form.append('total_hours', totalhours);
      form.append('expiry_date', expirydate);
      var file = {};
      if (uri != '') {
        var ext = certificatefile.substring(
          certificatefile.lastIndexOf('.') + 1,
        );
        file = {
          uri: uri,
          type: imagetype,
          name: certificatefile,
        };
        form.append('certificate_file', file);
      }

      console.log(JSON.stringify(id) + 'uess');
      console.log('DATA BEFORE SUBMITTION========');
      console.log(form);
      console.log(file);
      if (name !== '' && type !== '') {
        const response = await axios
          .post(
            'https://dawami.wedigits.dev/api/profile/' +
              id +
              '/add/certificate',
            form,
          )
          .then(function(res, err) {
            if (res.data) {
              GetProfileInfo(id, err => {
                return console.log(err);
              });
              setSkilllId('');
              setName('');
              setType('');
              setInstitutionname('');
              setCertificatefile('');
              setStartdate('');
              setExpirydate('');
              setTotalhours('');
              setUri('');
              setModalVisible(false);
              fetchProfileRecords();
              console.log(res);
            }
          });
        console.log('API =========RESPONSE IS ================');
        console.log('yess');
      } else {
        ToastAndroid.show('Fill', ToastAndroid.LONG);
        console.log('no');
      }
    } catch (e) {
      console.log('error catching');
      console.log(e);
      //  onError(e);
    }
  };

  const updateHobby = async () => {
    try {
      let form = new FormData();
      form.append('type', type);
      form.append('name', name);
      form.append('institution_name', institutionname);
      form.append('start_date', startdate);
      form.append('total_hours', totalhours);
      form.append('expiry_date', expirydate);

      if (uri != '') {
        var ext = certificatefile.substring(
          certificatefile.lastIndexOf('.') + 1,
        );
        const file = {
          uri: uri,
          type: imagetype,
          name: certificatefile,
        };
        form.append('certificate_file', file);
      }

      console.log(JSON.stringify(id) + 'uess');
      console.log('DATA BEFORE SUBMITTION UPDATE========');
      console.log(form);
      if (name !== '' && type !== '') {
        const response = await axios
          .post(
            'https://dawami.wedigits.dev/api/profile/' +
              skillId +
              '/update/certificate',
            form,
          )
          .then(function(res, err) {
            if (res.data) {
              GetProfileInfo(id, err => {
                return console.log(err);
              });
              setSkilllId('');
              setName('');
              setType('');
              setInstitutionname('');
              setUri('');
              setCertificatefile('');
              setStartdate('');
              setExpirydate('');
              setTotalhours('');
              setModalVisible(false);
              fetchProfileRecords();
              console.log(res);
            }
          });
        console.log('API =========RESPONSE IS ================');
        console.log('yess');
      } else {
        ToastAndroid.show('Fill', ToastAndroid.LONG);
        console.log('no');
      }
    } catch (e) {
      console.log('error catching');
      console.log(e);
      //  onError(e);
    }
  };

  return (
    <View>
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
          setSkilllId('');
          setName('');
          setType('');
          setInstitutionname('');
          setCertificatefile('');
          setStartdate('');
          setExpirydate('');
          setTotalhours('');
          setUri('');
        }}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <View
            style={{
              margin: 12,
              padding: 12,
              height: 500,
              backgroundColor: '#fff',
              width: sizes.getWidth(90),
            }}>
            <View style={styles.modalRow}>
              <Text style={styles.heading}>Type</Text>
              <View style={{flex: 1}}>
                <Picker
                  selectedValue={type}
                  onValueChange={(itemValue, index) => {
                    setType(itemValue.toString());
                    setStartdate('');
                    setExpirydate('');
                    setTotalhours('');
                  }}
                  style={styles.pickerStyle}>
                  {Type.type.map(itemm => (
                    <Picker.Item label={itemm.name} value={itemm.value} />
                  ))}
                </Picker>
              </View>
            </View>

            {type == '1' ? (
              <View>
                <View style={styles.modalRow}>
                  <Text style={styles.heading}>Name</Text>
                  <View style={{flex: 1}}>
                    <TextField
                      value={name}
                      onChangeText={text => {
                        setName(text.text);
                      }}
                      inputStyling={{
                        borderBottomWidth: 1,
                        width: '100%',
                        margin: 0,
                        paddingBottom: 0,
                      }}
                    />
                  </View>
                </View>

                <View style={styles.modalRow}>
                  <Text style={styles.heading}>Date Issue</Text>
                  <View style={{flex: 1}}>
                    <DatePicker
                      style={{
                        width: sizes.getWidth(50),
                        // borderWidth: 1,
                        justifyContent: 'center',
                        height: sizes.getHeight(5),
                      }}
                      placeholder="Select Date"
                      mode="date"
                      format="YYYY-MM-DD"
                      date={startdate}
                      onDateChange={data => setStartdate(data)}
                    />
                  </View>
                </View>

                <View style={styles.modalRow}>
                  <Text style={styles.heading}>Date Expiry</Text>
                  <View style={{flex: 1}}>
                    <DatePicker
                      style={{
                        width: sizes.getWidth(50),
                        // borderWidth: 1,
                        justifyContent: 'center',
                        height: sizes.getHeight(5),
                      }}
                      placeholder="Select Date"
                      mode="date"
                      format="YYYY-MM-DD"
                      date={expirydate}
                      onDateChange={data => setExpirydate(data)}
                    />
                  </View>
                </View>

                <View style={styles.modalRow}>
                  <Text style={styles.heading}>Attachment</Text>
                  <View style={{flex: 1}}>
                    <TouchableOpacity
                      onPress={() => {
                        pickImage();
                      }}>
                      <View
                        style={{
                          // borderWidth: 1,
                          backgroundColor: colors.darkBrown,
                          borderRadius: 10,
                          padding: 12,
                          width: 150,
                        }}>
                        <Text h4 color={colors.primary}>
                          {certificatefile
                            ? certificatefile
                            : 'No Attachment Found'}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            ) : (
              <View>
                <View style={styles.modalRow}>
                  <Text style={styles.heading}>Name</Text>
                  <View style={{flex: 1}}>
                    <TextField
                      onChangeText={text => {
                        setName(text.text);
                      }}
                      value={name}
                      inputStyling={{
                        borderBottomWidth: 1,
                        width: '100%',
                        margin: 0,
                        paddingBottom: 0,
                      }}
                    />
                  </View>
                </View>
                <View style={styles.modalRow}>
                  <Text style={styles.heading}>Institution Name</Text>
                  <View style={{flex: 1}}>
                    <TextField
                      value={institutionname}
                      onChangeText={text => {
                        setInstitutionname(text.text);
                      }}
                      inputStyling={{
                        borderBottomWidth: 1,
                        width: '100%',
                        margin: 0,
                        paddingBottom: 0,
                      }}
                    />
                  </View>
                </View>

                <View style={styles.modalRow}>
                  <Text style={styles.heading}>Start Date</Text>
                  <View style={{flex: 1}}>
                    <DatePicker
                      style={{
                        width: sizes.getWidth(50),
                        // borderWidth: 1,
                        justifyContent: 'center',
                        height: sizes.getHeight(5),
                      }}
                      placeholder="Select Date"
                      mode="date"
                      format="YYYY-MM-DD"
                      date={startdate}
                      onDateChange={data => setStartdate(data)}
                    />
                  </View>
                </View>

                <View style={styles.modalRow}>
                  <Text style={styles.heading}>Total Hours</Text>
                  <View style={{flex: 1}}>
                    <TextField
                      value={totalhours}
                      onChangeText={text => {
                        setTotalhours(text.text);
                      }}
                      inputStyling={{
                        borderBottomWidth: 1,
                        width: '100%',
                        margin: 0,
                        paddingBottom: 0,
                      }}
                    />
                  </View>
                </View>

                <View style={styles.modalRow}>
                  <Text style={styles.heading}>Attachment</Text>

                  <View style={{flex: 1}}>
                    <TouchableOpacity onPress={pickImage}>
                      <View
                        style={{
                          // borderWidth: 1,
                          backgroundColor: colors.darkBrown,
                          borderRadius: 10,
                          padding: 12,
                          width: 150,
                        }}>
                        <Text h4 color={colors.primary}>
                          {certificatefile
                            ? certificatefile
                            : 'No Attachment Found'}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            )}
            <View style={{flexDirection: 'row', marginTop: sizes.getHeight(4)}}>
              <View style={{flex: 0.4, margin: 5}}>
                <SquareButton
                  textColor={colors.primary}
                  // bgColor={colors.customRed}
                  width={sizes.getWidth(30)}
                  height={sizes.getHeight(6)}
                  onPress={() => {
                    if (skillId != '') {
                      updateHobby();
                    } else {
                      addHobby();
                    }
                  }}
                  style={{
                    // backgroundColor: btnDisable ? 'transparent' : colors.customRed,
                    backgroundColor: colors.customRed,
                    borderWidth: 0,
                    borderRadius: sizes.withScreen(0.01),
                  }}>
                  Submit
                </SquareButton>
              </View>
              <View style={{flex: 0.4, margin: 5}}>
                <SquareButton
                  textColor={colors.primary}
                  // bgColor={colors.customRed}
                  width={sizes.getWidth(25)}
                  height={sizes.getHeight(6)}
                  onPress={() => {
                    setModalVisible(false);
                    setSkilllId('');
                    setName('');
                    setType('');
                    setInstitutionname('');
                    setCertificatefile('');
                    setStartdate('');
                    setExpirydate('');
                    setTotalhours('');
                    setUri('');
                  }}
                  style={{
                    // backgroundColor: btnDisable ? 'transparent' : colors.customRed,
                    backgroundColor: '#B22222',
                    borderWidth: 0,
                    borderRadius: sizes.withScreen(0.015),
                  }}>
                  Cancel
                </SquareButton>
              </View>
            </View>
          </View>
        </View>
      </Modal>

      <TouchableOpacity
        style={{margin: 12}}
        onPress={() => {
          setModalVisible(true);
        }}>
        <Image
          source={icons.add}
          style={{
            height: sizes.getHeight(4),
            resizeMode: 'contain',
            width: sizes.getWidth(10),
            alignSelf: 'flex-end',
          }}
        />
      </TouchableOpacity>
      {auth.map(item => (
        <Block
          margin={[0, 0, sizes.getHeight(4), 0]}
          padding={[0, 0, sizes.getHeight(4), 0]}>
          <Block flex={false} row center style={styles.recordKeeperCon}>
            <SubHeading>Type</SubHeading>
            <Block
              padding={[
                0,
                sizes.getWidth(2),
                sizes.getWidth(2),
                sizes.getWidth(2),
              ]}
              flex={2}
              style={{borderWidth: 0}}>
              <Picker
                selectedValue={item.type}
                enabled={false}
                // onValueChange={(itemValue, index) =>
                //   localDispatch({type: COUNTRY, payload: itemValue})
                // }
                style={styles.pickerStyle}>
                {Type.type.map(itemm => (
                  <Picker.Item label={itemm.name} value={itemm.value} />
                ))}
              </Picker>
            </Block>
          </Block>

          {item.type == '1' ? (
            <View>
              <Block flex={false} row center style={styles.recordKeeperCon}>
                <SubHeading>Name</SubHeading>
                <TextField
                  editable={false}
                  value={item.name}
                  inputStyling={{
                    borderBottomWidth: 1,
                    width: '100%',
                    margin: 0,
                    paddingBottom: 0,
                  }}
                />
              </Block>

              <Block flex={false} row center style={styles.recordKeeperCon}>
                <SubHeading>Date Issue</SubHeading>

                <DatePicker
                  style={{
                    width: sizes.getWidth(50),
                    // borderWidth: 1,
                    justifyContent: 'center',
                    height: sizes.getHeight(5),
                  }}
                  placeholder="Select Date"
                  mode="date"
                  format="YYYY-MM-DD"
                  date={item.start_date}
                  // onDateChange={data => setDateState({date:date})}
                />
              </Block>

              <Block flex={false} row style={styles.recordKeeperCon}>
                <SubHeading>Expiry Date (If any)</SubHeading>
                <DatePicker
                  style={{
                    width: sizes.getWidth(50),
                    // borderWidth: 1,
                    justifyContent: 'center',
                    height: sizes.getHeight(5),
                  }}
                  placeholder="Select Date"
                  mode="date"
                  format="YYYY-MM-DD"
                  date={item.expiry_date}
                  // onDateChange={data => setDateState({date:date})}
                />
              </Block>

              <Block
                center
                flex={false}
                row
                style={{...styles.recordKeeperCon, height: sizes.getHeight(7)}}>
                <SubHeading>Attachment</SubHeading>
                <Block
                  middle
                  center
                  style={{
                    // borderWidth: 1,
                    backgroundColor: colors.darkBrown,
                    borderRadius: sizes.getWidth(2),
                    width: '40%',
                    height: '80%',
                  }}
                  flex={false}>
                  <Text h4 color={colors.primary}>
                    {item.certificate_file
                      ? item.certificate_file
                      : 'No Attachment Found'}
                  </Text>
                </Block>
              </Block>
            </View>
          ) : (
            <View>
              <Block flex={false} row center style={styles.recordKeeperCon}>
                <SubHeading>Name</SubHeading>
                <TextField
                  editable={false}
                  value={item.name}
                  inputStyling={{
                    borderBottomWidth: 1,
                    width: '100%',
                    margin: 0,
                    paddingBottom: 0,
                  }}
                />
              </Block>

              <Block flex={false} row center style={styles.recordKeeperCon}>
                <SubHeading>Institution Name</SubHeading>
                <TextField
                  editable={false}
                  value={item.institution_name}
                  inputStyling={{
                    borderBottomWidth: 1,
                    width: '100%',
                    margin: 0,
                    paddingBottom: 0,
                  }}
                />
              </Block>

              <Block flex={false} row center style={styles.recordKeeperCon}>
                <SubHeading>Start Date</SubHeading>

                <DatePicker
                  style={{
                    width: sizes.getWidth(50),
                    // borderWidth: 1,
                    justifyContent: 'center',
                    height: sizes.getHeight(5),
                  }}
                  placeholder="Start Date"
                  mode="date"
                  format="YYYY-MM-DD"
                  date={item.start_date}
                  // onDateChange={data => setDateState({date:date})}
                />
              </Block>

              <Block flex={false} row center style={styles.recordKeeperCon}>
                <SubHeading>Total Hours</SubHeading>
                <TextField
                  value={item.total_hours}
                  inputStyling={{
                    borderBottomWidth: 1,
                    width: '100%',
                    margin: 0,
                    paddingBottom: 0,
                  }}
                />
              </Block>

              <Block
                center
                flex={false}
                row
                style={{...styles.recordKeeperCon, height: sizes.getHeight(7)}}>
                <SubHeading>Attachment</SubHeading>
                <Block
                  middle
                  center
                  style={{
                    // borderWidth: 1,
                    backgroundColor: colors.darkBrown,
                    borderRadius: sizes.getWidth(2),
                    width: '60%',
                    height: '80%',
                  }}
                  flex={false}>
                  <Text h4 color={colors.primary}>
                    {item.certificate_file
                      ? item.certificate_file
                      : 'No Attachment Found'}
                  </Text>
                </Block>
              </Block>
            </View>
          )}

          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              marginTop: sizes.getHeight(3),
            }}>
            <TouchableOpacity
              style={{width: 100, alignSelf: 'center'}}
              onPress={() => {
                setModalVisible(true);
              }}>
              <View style={styles.editbtn}>
                <Text
                  style={{textAlign: 'center', fontSize: 16, color: 'white'}}
                  onPress={() => {
                    setSkilllId(item.id);
                    setModalVisible(true);
                    setName(item.name);
                    setType(item.type);
                    setInstitutionname(item.institution_name);
                    setCertificatefile(item.certificate_file);
                    setStartdate(item.start_date);
                    setExpirydate(item.expiry_date);
                    setTotalhours(item.total_hours);
                  }}>
                  Edit
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                removeEdu(item.id);
              }}>
              <Image
                source={icons.remove}
                style={{
                  height: sizes.getHeight(4),
                  resizeMode: 'contain',
                  width: sizes.getWidth(10),
                  alignSelf: 'flex-end',
                  marginLeft: 15,
                }}
              />
            </TouchableOpacity>
          </View>
        </Block>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  recordKeeperCon: {
    height: sizes.getHeight(5),
    marginBottom: sizes.getHeight(2),
    // borderWidth:1,
  },
  modalRow: {
    flexDirection: 'row',
    marginTop: 12,
  },
  textInput: {
    color: theme.colors.gray5,
    width: '100%',
    // placeholderTextColor:colors.darkBrown
    ...theme.fonts.h1,
  },
  heading: {
    fontWeight: 'bold',
    ...theme.fonts.h4,
    width: 100,
    marginTop: 12,
  },
  modalDesign: {
    backgroundColor: colors.black,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  editbtn: {
    backgroundColor: colors.customRed,
    alignSelf: 'center',
    width: 100,
    padding: 6,
    borderRadius: 5,
  },
});

export {CertInfo};
