import React, {useState} from 'react';
import {Block, TextField, Text} from 'components';
import {
  StyleSheet,
  ToastAndroid,
  Image,
  View,
  Modal,
  Button,
  KeyboardAvoidingView,
} from 'react-native';
import {sizes, colors} from 'styles/theme';
import {SubHeading, Heading} from '../../Section';
import {Picker} from '@react-native-community/picker';
import Checkbox from '@react-native-community/checkbox';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import {SquareButton} from 'components';
import DatePicker from 'react-native-datepicker';
import {useDispatch, useSelector} from 'react-redux';
import {TouchableOpacity} from 'react-native-gesture-handler';
import axios from 'axios';
import * as icons from 'assets/icons';
import {GetProfileInfo} from 'redux/actions';
import {exp} from 'react-native-reanimated';

const WorkInfo = props => {
  const {showWaiting, message} = props;
  const auth = useSelector(
    state => state.userInfo.userData.profile.experiences,
  );
  const dispatch = useDispatch();

  const [modalVisible, setModalVisible] = useState(false);
  const [selectedGender, setSelectedGender] = useState('');
  const [title, setTitle] = useState('');
  const [category, setCategory] = useState('');
  const [country, setCountry] = useState('');
  const [startdate, setStartDate] = useState('');
  const [enddate, setEndDate] = useState('');
  const [iworkhere, setIWorkHere] = useState(false);
  const [desciption, setDescription] = useState('');
  const [companyname, setCompanyName] = useState('');
  const [companyindustry, setCompanyIndustry] = useState('');
  const [salarylastmonth, setSalaryLastMonth] = useState('');
  const [salarytype, setSalaryType] = useState('');
  const [expid, setExpid] = useState('');

  const id = useSelector(state => state.userInfo.userData.user.id);

  async function fetchProfileRecords() {
    const result = await GetProfileInfo(id, err => {
      return console.log(err);
    });
    result && dispatch(result);
  }

  const add = async () => {
    try {
      let form = new FormData();
      form.append('job_title', title);
      form.append('country_id', country);
      form.append('company_industry_id', companyindustry);
      form.append('job_category_id', category);
      if (iworkhere) {
        form.append('active', '1');
      }
      form.append('start_date', startdate);
      form.append('end_date', enddate);
      form.append('job_description', desciption);
      form.append('company_name', companyname);
      form.append('last_salary', salarylastmonth);
      form.append('last_salary_currency', salarytype);

      console.log(JSON.stringify(id) + 'uess');
      console.log('DATA BEFORE SUBMITTION========');

      console.log(form);

      const response = await axios
        .post(
          'https://dawami.wedigits.dev/api/profile/' + id + '/add/experience',
          form,
        )
        .then(function(res, err) {
          console.log(err);
          if (res.data) {
            setTitle('');
            setCategory('');
            setCountry('');
            setStartDate('');
            setEndDate('');
            setIWorkHere(false);
            setDescription('');
            setCompanyName('');
            setCompanyIndustry('');
            setSalaryLastMonth('');
            setSalaryType('');
            setExpid('');
            setModalVisible(false);
            fetchProfileRecords();
            console.log(res);
          }
        });
      console.log('API =========RESPONSE IS ================');
      console.log('yess');
    } catch (e) {
      console.log('error catching');
      console.log(e);
      setTitle('');
      setCategory('');
      setCountry('');
      setStartDate('');
      setEndDate('');
      setIWorkHere(false);
      setDescription('');
      setCompanyName('');
      setCompanyIndustry('');
      setSalaryLastMonth('');
      setSalaryType('');
      setExpid('');
      setModalVisible(false);
      fetchProfileRecords();
    }
  };
  const update = async () => {
    try {
      let form = new FormData();
      form.append('job_title', title);
      form.append('country_id', country);
      form.append('company_industry_id', companyindustry);
      form.append('job_category_id', category);
      if (iworkhere) {
        form.append('active', '1');
      }
      form.append('start_date', startdate);
      form.append('end_date', enddate);
      form.append('job_description', desciption);
      form.append('company_name', companyname);
      form.append('last_salary', salarylastmonth);
      form.append('last_salary_currency', salarytype);

      console.log(JSON.stringify(id) + 'uess');
      console.log('DATA BEFORE SUBMITTION========');

      const response = await axios
        .post(
          'https://dawami.wedigits.dev/api/profile/' +
            expid +
            '/update/experience',
          form,
        )
        .then(function(res, err) {
          console.log(err);
          if (res.data) {
            setTitle('');
            setCategory('');
            setCountry('');
            setStartDate('');
            setEndDate('');
            setIWorkHere(false);
            setDescription('');
            setCompanyName('');
            setCompanyIndustry('');
            setSalaryLastMonth('');
            setSalaryType('');
            setExpid('');
            setModalVisible(false);
            fetchProfileRecords();
            console.log(res);
          }
        });
      console.log('API =========RESPONSE IS ================');
      console.log('yess');
    } catch (e) {
      console.log('error catching');
      console.log(e);
      //  onError(e);
    }
  };
  let radio_props = [
    {label: 'Civil Engineering', value: 1},
    {label: 'Operations', value: 2},
    {label: 'Oil and Gas', value: 3},
    {label: 'Media', value: 4},
    {label: 'Banking', value: 5},
    {label: 'Information Technology', value: 6},
    {label: 'Electrical Engineering', value: 7},
    {label: 'Safety', value: 8},
    {label: 'Construction', value: 9},
  ];

  const removeEdu = async eduid => {
    try {
      console.log('DATA BEFORE deletion========' + eduid);
      const response = await axios
        .get(
          'https://dawami.wedigits.dev/api/profile/remove/experience/' + eduid,
        )
        .then(function(res, err) {
          if (res.data) {
            fetchProfileRecords();
            console.log(res + ' updated');
          }
        });
      fetchProfileRecords();
      console.log('API =========RESPONSE IS ================');
      console.log('yess');
    } catch (e) {
      console.log('error catching');
      console.log(e);
      //  onError(e);
    }
  };
  return (
    <Block padding={[0, 0, sizes.getHeight(2), 0]}>
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
          setTitle('');
          setCategory('');
          setCountry('');
          setStartDate('');
          setEndDate('');
          setIWorkHere(false);
          setDescription('');
          setCompanyName('');
          setCompanyIndustry('');
          setSalaryLastMonth('');
          setSalaryType('');
          setExpid('');
        }}>
        <KeyboardAvoidingView
          style={{
            //  justifyContent: 'center',
            alignItems: 'center',
            margin: 10,
            padding: 10,
            // flex:0.5,
            marginTop: sizes.getHeight(2),
            flex: 1,
            backgroundColor: '#fff',
            elevation: 1,
            borderRadius: sizes.withScreen(0.006),
          }}>
          <Block row center style={[styles.recordKeeperCon, {marginTop: 8}]}>
            <SubHeading>Job Title</SubHeading>
            <Block flex={9}>
              <TextField
                inputStyling={{borderBottomWidth: 1, width: '100%'}}
                value={title}
                onChangeText={text => {
                  console.log(text);
                  setTitle(text.text);
                }}
              />
            </Block>
          </Block>

          <Block row center style={styles.recordKeeperCon}>
            <SubHeading>JOB CATEGORY</SubHeading>
            <Block>
              <Picker
                selectedValue={category}
                onValueChange={item => setCategory(item)}
                style={{width: sizes.getWidth(60), hieght: sizes.getHeight(5)}}>
                {radio_props.map(item => (
                  <Picker.Item
                    label={item.label}
                    value={item.value.toString()}
                  />
                ))}
              </Picker>
            </Block>
          </Block>

          <Block row center style={styles.recordKeeperCon}>
            <SubHeading>Country</SubHeading>
            <Block>
              <Picker
                onValueChange={item => setCountry(item)}
                selectedValue={country}
                style={{width: sizes.getWidth(60), hieght: sizes.getHeight(5)}}>
                <Picker.Item label="Select Country" value="CITIZEN" />
                <Picker.Item label="QATAR" value="1" />
                <Picker.Item label="JORDAN" value="2" />
                <Picker.Item label="UNITED KINGDOM" value="3" />
              </Picker>
            </Block>
          </Block>

          <Block row center style={styles.recordKeeperCon}>
            <SubHeading>Job Duration</SubHeading>

            <Block>
              <DatePicker
                style={{
                  width: sizes.getWidth(35),
                  // borderWidth: 1,
                  justifyContent: 'center',
                  height: sizes.getHeight(5),
                }}
                placeholder="Select Date"
                mode="date"
                format="YYYY-MM-DD"
                date={startdate}
                onDateChange={data => {
                  console.log(data);
                  setStartDate(data);
                }}
              />
            </Block>

            <Block>
              <DatePicker
                style={{
                  width: sizes.getWidth(35),
                  // borderWidth: 1,
                  justifyContent: 'center',
                  height: sizes.getHeight(5),
                }}
                placeholder="Select Date"
                mode="date"
                format="YYYY-MM-DD"
                date={enddate}
                onDateChange={data => setEndDate({data})}
              />
            </Block>
          </Block>

          <Block row center>
            <Checkbox
              value={iworkhere}
              onValueChange={() =>
                iworkhere ? setIWorkHere(false) : setIWorkHere(true)
              }
            />
            <Text h3>I Still Work Here</Text>
          </Block>

          <Block row>
            <SubHeading>Description</SubHeading>
            <Block>
              <TextField
                multilines={true}
                numberOfLines={5}
                onChangeText={text => setDescription(text.text)}
                value={desciption}
                inputStyling={{borderWidth: 0.6, width: '100%'}}
              />
            </Block>
          </Block>

          <Block row margin={[sizes.getHeight(2), 0, 0, 0]}>
            <SubHeading>Company Name</SubHeading>
            <Block>
              <TextField
                value={companyname}
                onChangeText={text => setCompanyName(text.text)}
                inputStyling={{borderWidth: 0.6, width: '100%'}}
              />
            </Block>
          </Block>

          <Block row center style={styles.recordKeeperCon}>
            <SubHeading>Company Industry</SubHeading>
            <Block>
              <Picker
                value={companyindustry}
                onValueChange={item => setCompanyIndustry(item)}
                style={{width: sizes.getWidth(60), hieght: sizes.getHeight(5)}}>
                {radio_props.map(item => (
                  <Picker.Item
                    label={item.label}
                    value={item.value.toString()}
                  />
                ))}
              </Picker>
            </Block>
          </Block>

          <Block row margin={[sizes.getHeight(2), 0, 0, 0]}>
            <SubHeading>Salary Last Month</SubHeading>
            <Block>
              <TextField
                value={salarylastmonth}
                onChangeText={text => setSalaryLastMonth(text.text)}
                keyboardType={'numeric'}
                inputStyling={{borderWidth: 0.6, width: '100%'}}
              />
            </Block>
          </Block>

          <Block row center style={styles.recordKeeperCon}>
            <SubHeading>Salary Type</SubHeading>
            <Block>
              <Picker
                value={salarytype}
                onValueChange={item => {
                  console.log(item);
                  setSalaryType(item);

                  console.log(salarytype, 'salarytype');
                }}
                style={{width: sizes.getWidth(60), hieght: sizes.getHeight(5)}}>
                <Picker.Item label="$" value="1" />
                <Picker.Item label="QTR" value="2" />
              </Picker>
            </Block>
          </Block>

          <View style={{flexDirection: 'row', marginTop: sizes.getHeight(4)}}>
            <View style={{flex: 0.4, margin: 5}}>
              <SquareButton
                textColor={colors.primary}
                // bgColor={colors.customRed}
                width={sizes.getWidth(30)}
                height={sizes.getHeight(6)}
                onPress={() => {
                  expid == '' ? add : update;
                }}
                style={{
                  // backgroundColor: btnDisable ? 'transparent' : colors.customRed,
                  backgroundColor: colors.customRed,
                  borderWidth: 0,
                  borderRadius: sizes.withScreen(0.01),
                }}>
                Submit
              </SquareButton>
            </View>
            <View style={{flex: 0.4, margin: 5}}>
              <SquareButton
                textColor={colors.primary}
                // bgColor={colors.customRed}
                width={sizes.getWidth(25)}
                height={sizes.getHeight(6)}
                onPress={() => {
                  setModalVisible(false);
                  setTitle('');
                  setCategory('');
                  setCountry('');
                  setStartDate('');
                  setEndDate('');
                  setIWorkHere(false);
                  setDescription('');
                  setCompanyName('');
                  setCompanyIndustry('');
                  setSalaryLastMonth('');
                  setSalaryType('');
                  setExpid('');
                }}
                style={{
                  // backgroundColor: btnDisable ? 'transparent' : colors.customRed,
                  backgroundColor: '#B22222',
                  borderWidth: 0,
                  borderRadius: sizes.withScreen(0.015),
                }}>
                Cancel
              </SquareButton>
            </View>
          </View>
        </KeyboardAvoidingView>
      </Modal>
      <Heading> WORK EXPERIENCE + </Heading>
      <TouchableOpacity
        style={{margin: 12}}
        onPress={() => {
          setModalVisible(true);
        }}>
        <Image
          source={icons.add}
          style={{
            height: sizes.getHeight(4),
            resizeMode: 'contain',
            width: sizes.getWidth(10),
            alignSelf: 'flex-end',
          }}
        />
      </TouchableOpacity>
      {auth.map(i => (
        <View>
          <Block row center style={[styles.recordKeeperCon, {marginTop: 8}]}>
            <SubHeading>Job Title</SubHeading>
            <Block flex={9}>
              <TextField
                inputStyling={{borderBottomWidth: 1, width: '100%'}}
                value={i.job_title}
              />
            </Block>
          </Block>

          <Block row center style={styles.recordKeeperCon}>
            <SubHeading>JOB CATEGORY</SubHeading>
            <Block>
              <Picker
                selectedValue={i.job_category_id}
                style={{width: sizes.getWidth(60), hieght: sizes.getHeight(5)}}>
                {radio_props.map(item => (
                  <Picker.Item
                    label={item.label}
                    value={item.value.toString()}
                  />
                ))}
              </Picker>
            </Block>
          </Block>

          <Block row center style={styles.recordKeeperCon}>
            <SubHeading>Country</SubHeading>
            <Block>
              <Picker
                selectedValue={i.country_id}
                style={{width: sizes.getWidth(60), hieght: sizes.getHeight(5)}}>
                <Picker.Item label="Select Country" value="CITIZEN" />
                <Picker.Item label="QATAR" value="1" />
                <Picker.Item label="JORDAN" value="2" />
                <Picker.Item label="UNITED KINGDOM" value="3" />
              </Picker>
            </Block>
          </Block>

          <Block row center style={styles.recordKeeperCon}>
            <SubHeading>Job Duration</SubHeading>

            <Block>
              <DatePicker
                style={{
                  width: sizes.getWidth(35),
                  // borderWidth: 1,
                  justifyContent: 'center',
                  height: sizes.getHeight(5),
                }}
                placeholder="Select Date"
                mode="date"
                format="YYYY-MM-DD"
                date={i.start_date}
                // onDateChange={data => setDateState({date:date})}
              />
            </Block>

            <Block>
              <DatePicker
                style={{
                  width: sizes.getWidth(35),
                  // borderWidth: 1,
                  justifyContent: 'center',
                  height: sizes.getHeight(5),
                }}
                placeholder="Select Date"
                mode="date"
                format="YYYY-MM-DD"
                date={i.end_date}
                // onDateChange={data => setDateState({date:date})}
              />
            </Block>
          </Block>

          <Block row center>
            <Checkbox disabled={true} value={i.active == '1' ? true : false} />
            <Text h3>I Still Work Here</Text>
          </Block>

          <Block row>
            <SubHeading>Description</SubHeading>
            <Block>
              <TextField
                multilines={true}
                numberOfLines={5}
                value={i.job_description}
                inputStyling={{borderWidth: 0.6, width: '100%'}}
              />
            </Block>
          </Block>

          <Block row margin={[sizes.getHeight(2), 0, 0, 0]}>
            <SubHeading>Company Name</SubHeading>
            <Block>
              <TextField
                value={i.company_name}
                inputStyling={{borderWidth: 0.6, width: '100%'}}
              />
            </Block>
          </Block>

          <Block row center style={styles.recordKeeperCon}>
            <SubHeading>Company Industry</SubHeading>
            <Block>
              <Picker
                selectedValue={i.company_industry_id}
                style={{width: sizes.getWidth(60), hieght: sizes.getHeight(5)}}>
                {radio_props.map(item => (
                  <Picker.Item
                    label={item.label}
                    value={item.value.toString()}
                  />
                ))}
              </Picker>
            </Block>
          </Block>

          <Block row margin={[sizes.getHeight(2), 0, 0, 0]}>
            <SubHeading>Salary Of Last Month</SubHeading>
            <Block>
              <TextField
                value={i.last_salary_currency + ' ' + i.last_salary}
                keyboardType={'numeric'}
                inputStyling={{borderWidth: 0.6, width: '100%'}}
              />
            </Block>
          </Block>

          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              marginTop: sizes.getHeight(3),
            }}>
            <TouchableOpacity
              style={{width: 100, alignSelf: 'center'}}
              onPress={() => {
                setModalVisible(true);
                setTitle(i.job_title);
                setCategory(i.job_category_id);
                setCountry(i.country_id);
                setStartDate(i.start_date);
                setEndDate(i.end_date);
                setIWorkHere(i.active == '1' ? true : false);
                setDescription(i.job_description);
                setCompanyName(i.company_name);
                setCompanyIndustry(i.company_industry_id);
                setSalaryLastMonth(i.last_salary);
                setSalaryType(i.last_salary_currency);
                setExpid(i.id);
              }}>
              <View style={styles.editbtn}>
                <Text
                  style={{textAlign: 'center', fontSize: 16, color: 'white'}}
                  onPress={() => {
                    setModalVisible(true);
                    setTitle(i.job_title);
                    setCategory(i.job_category_id);
                    setCountry(i.country_id);
                    setStartDate(i.start_date);
                    setEndDate(i.end_date);
                    setIWorkHere(i.active == '1' ? true : false);
                    setDescription(i.job_description);
                    setCompanyName(i.company_name);
                    setCompanyIndustry(i.company_industry_id);
                    setSalaryLastMonth(i.last_salary);
                    setSalaryType(i.last_salary_currency);
                    setExpid(i.id);
                  }}>
                  Edit
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                removeEdu(i.id);
              }}>
              <Image
                source={icons.remove}
                style={{
                  height: sizes.getHeight(4),
                  resizeMode: 'contain',
                  width: sizes.getWidth(10),
                  alignSelf: 'flex-end',
                  marginLeft: 15,
                }}
              />
            </TouchableOpacity>
          </View>
        </View>
      ))}
    </Block>
  );
};

const styles = StyleSheet.create({
  recordKeeperCon: {
    height: sizes.getHeight(5),
    marginBottom: sizes.getHeight(2),
  },
  modalDesign: {
    backgroundColor: colors.black,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  editbtn: {
    backgroundColor: colors.customRed,
    alignSelf: 'center',
    width: 100,
    padding: 6,
    borderRadius: 5,
  },
});

export {WorkInfo};
