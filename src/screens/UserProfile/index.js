import React, {useState, useEffect, useRef} from 'react';
import {
  Block,
  Text,
  Button,
  ActivitySign,
  CustomAlert,
  CustomHeader,
} from 'components';
import {sizes, colors} from 'styles/theme';
import {
  ScrollView,
  Image,
  Platform,
  SafeAreaView,
  Dimensions,
} from 'react-native';
import * as images from 'assets/images';
import * as icons from 'assets/icons';
import {useSelector, useDispatch} from 'react-redux';
import {GetProfileInfo} from 'redux/actions';
import Toast from 'react-native-easy-toast';
import {NAV} from 'utils';
import {LANG_AR} from 'redux/constants';
import {color} from 'react-native-reanimated';
import { NoDataFound, NoDataFound_AR } from 'utils/LanguageHandler';

const Profile = ({navigation, route}) => {
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width <= 375;
  const messageRef = useRef();

  const [isWaiting, setIsWaiting] = useState(false);
  const dispatch = useDispatch();
  // const userInfo = useSelector(state => state.userInfo.userData);
  // console.log(JSON.stringify(userInfo.user.avatar) + "userrrrrrrrrr");
  const userInfo = useSelector(state => state.auth.userBasicProfile);
  const userInfo_ar = useSelector(state => state.auth.userBasicProfile?.profile_ar);
  // const profile= useSelector(state => state.userInfo.userData.profile);
  const user = useSelector(state => state.userInfo?.userData?.user);


  const [userr, setUser] = useState('');
  const [profile, setProfile] = useState('');
  const [experiences, setExperiences] = useState('');
  const [experiencesAr, setExperiencesAr] = useState('');
  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;
  

  const editProfileHandler = () => {
    navigation.navigate(NAV.EDIT_PRO);
  };

  const {name,avatar,username,email,expertise} = userInfo

 
  useEffect(() => {
    console.log(route.params?.fetchError);
    route.params?.fetchError &&
      messageRef.current?.show(
        <CustomAlert
          textColor={colors.primary}
          // textColor={colors.primary}
          text="Cannot Load Profile. Check your internet connectivity first"
        />,
        2500,
      );
    async function fetchProfileRecords() {
      setIsWaiting(true);
      const result = await GetProfileInfo(userInfo.id, err => {
        return (
          console.log('error'),
          setIsWaiting(false),
          Alert.alert(
            'Error In Fetch Records',
            'Sorry We Cannot Go Further Please Check You Internet Connectivity first',
            [
              {
                text: 'Go To Profile',
                onPress: () =>
                  navigation.navigate(NAV.PROFILE, {fetchError: true}),
              },
              {
                text: 'Back To Home',
                onPress: () => navigation.navigate(NAV.HOME, {fetchError: true}),
              },
            ],
          )
        );
      });
      console.log('===============================');
      console.log(result.payload);
      result && (dispatch(result), setIsWaiting(false));
      setIsWaiting(false);
      // console.log(JSON.stringify(result.payload.user.avatar) + 'saaaaaa');
      // setUser(result.payload.user);
      // setProfile(result.payload.profile);
      // setExperiences(result.payload.profile.experiences[0]);
      // setExperiencesAr(result.payload.profile_ar.experiences[0]);
      // setArData(result.payload.profile_ar);
    }
    if (!user?.success) {
      fetchProfileRecords();
    }
  }, []);

  
  const usrImgPath = 'https://dawami.wedigits.dev/images/user_avatars/';
  const noData_ar = 'لايوجد بيانات';
  const noData = '--'
  return (
    <>
      {/* <SafeAreaView style={{flex: 0, backgroundColor: colors.spaceGreen}} /> */}

      <Block middle>
        <Block flex={false}>
          <CustomHeader
            navigation={navigation}
            tint={colors.primary}
            backBtn
          />
        </Block>
        <Block
          center
          middle
          flex={false}
          height={sizes.getHeight(25)}
          style={{backgroundColor: colors.customRed, width: '100%'}}>
          <Block
            center
            middle
            flex={false}
            style={{
              height: sizes.screenSize * 0.09,
              width: sizes.screenSize * 0.09,
              overflow: 'hidden',
              borderWidth: 0.2,
              borderColor: colors.primary,
              borderRadius: sizes.withScreen(3),
            }}>
            {/* <Image source={images.person} style={{resizeMode:'contain', flex:1 }} /> */}
            {avatar ? (
              <Image
                // source={icons.avatarDummy}
                source={{uri: usrImgPath + avatar}}
                style={{
                  resizeMode: 'center',
                  flex: 1,
                  height: sizes.screenSize * 0.09,
                  width: sizes.screenSize * 0.09,
                }}
              />
            ) : (
              <Image
                source={icons.avatarDummy}
                style={{
                  resizeMode: 'contain',
                  flex: 1,
                  height: sizes.screenSize * 0.09,
                  width: sizes.screenSize * 0.09,
                  // height: sizes.getHeight(20),
                  tintColor: colors.primary,
                }}
              />
            )}
            {/* <Image source={{uri:userInfo.avatar}} style={{resizeMode:'contain', flex:1 }} /> */}
          </Block>
          <Block center middle flex={false} margin={[10, 0, 0, 0]}>
            <Text
              h2
              color={colors.primary}
              style={{textTransform: 'capitalize'}}>
              {AR ? userInfo_ar.full_name || noData_ar : username || noData}
            </Text>
            <Text
              h4
              color={colors.primary}
              style={{textTransform: 'capitalize', marginTop: 10}}>
              {/* {experiences.job_title || '-'} */}
              {expertise || '--'}
            </Text>
          </Block>
        </Block>
        {/* -------------------- */}
        <Block padding={[0, sizes.getWidth(13)]}>
          <ScrollView
            style={{width: '100%'}}
            showsVerticalScrollIndicator={false}>
            <Block
              middle
              height={sizes.getHeight(7.5)}
              style={{marginTop: sizes.getHeight(iphone6s ? 5 : 3)}}>
              <Text
                bold
                style={{
                  textAlign: 'left',
                  fontSize: sizes.customFont(iphone6s ? 13 : 18),
                }}>
                {AR ? 'الاسم الكامل' : 'Full Name'}
              </Text>
              <Text
                h3
                color={colors.gray}
                style={{
                  textTransform: 'capitalize',
                  textAlign: 'left',
                  fontSize: sizes.customFont(iphone6s ? 13 : 18),
                }}>
                {/* Ibrahim Muhammad */}
                {AR ? userInfo_ar?.username || noData_ar : username || noData}
              </Text>
            </Block>
            <Block
              middle
              height={sizes.getHeight(7.5)}
              style={{marginTop: sizes.getHeight(iphone6s ? 5 : 3)}}>
              <Text
                h3
                bold
                style={{
                  textAlign: 'left',
                  fontSize: sizes.customFont(iphone6s ? 13 : 18),
                }}>
                {AR ? 'اسم المستخدم' : 'Username'}
              </Text>
              <Text
                color={colors.gray}
                style={{
                  textTransform: 'capitalize',
                  fontSize: sizes.customFont(iphone6s ? 13 : 18),
                  textAlign: 'left',
                }}>
                {/* @username */}
                {AR
                  ? userInfo_ar.full_name || noData_ar
                  : userInfo?.full_name || noData}
              </Text>
            </Block>
            <Block
              middle
              height={sizes.getHeight(7.5)}
              style={{marginTop: sizes.getHeight(iphone6s ? 5 : 3)}}>
              <Text
                bold
                style={{
                  textAlign: 'left',
                  fontSize: sizes.customFont(iphone6s ? 13 : 18),
                }}>
                {AR ? 'البريد الإلكتروني' : 'Email Address'}
              </Text>
              <Text
                color={colors.gray}
                style={{
                  textAlign: 'left',
                  fontSize: sizes.customFont(iphone6s ? 13 : 18),
                }}>
                {/* Ibrahimuhmmad@example.com */}
                {email || noData}
              </Text>
            </Block>
            {/* <Block middle height={sizes.getHeight(7.5)}>
            <Text h3 bold>
              Address
            </Text>
            <Text h3 color={colors.gray}>
              {user.address || 'Null'}
            </Text>
          </Block> */}
            <Block
              middle
              height={sizes.getHeight(7.5)}
              style={{marginTop: sizes.getHeight(iphone6s ? 5 : 3)}}>
              <Text
                bold
                style={{
                  textAlign: 'left',
                  fontSize: sizes.customFont(iphone6s ? 13 : 18),
                }}>
                {AR ? 'عنوان وظيفي' : 'Industry Expertise'}
              </Text>
              <Text
                color={colors.gray}
                style={{
                  fontSize: sizes.customFont(iphone6s ? 13 : 18),
                }}>
                {/* UI & UX Designer */}
                {/* {AR ? experiencesAr.job_title : experiences.job_title || '-'} */}
                {expertise || noData}

              </Text>
            </Block>
          </ScrollView>
          <Block
            center
            style={{
              position: 'absolute',
              bottom: sizes.getHeight(1),
              width: '100%',
              left: '17%',
            }}
            padding={[0, sizes.getWidth(8)]}
            middle
            height={sizes.getHeight(7.5)}>
            <Button
              activeOpacity={0.6}
              onPress={editProfileHandler}
              center
              middle
              style={{
                backgroundColor: colors.spaceGreen,
                borderRadius: sizes.screenSize * 4,
                width: sizes.getWidth(70),
                elevation: 5,
                shadowRadius: 3,
                shadowOffset: {
                  height: 2,
                  width: 0,
                },
                shadowColor: colors.customRed,
                shadowRadius: 10,
              }}>
              <Text h2 color={colors.primary}>
                {AR ? 'تعديل الملف الشخصي' : 'Edit Profile'}
              </Text>
            </Button>
          </Block>
        </Block>
        <Toast
          ref={messageRef}
          style={{
            backgroundColor: colors.primary,
            width: sizes.getWidth(100),
            borderRadius: 2,
          }}
          positionValue={sizes.getDimensions.height}
          fadeInDuration={200}
          fadeOutDuration={100}
          opacity={1}
        />
        {isWaiting && <ActivitySign />}
      </Block>
    </>
  );
};

export default Profile;
