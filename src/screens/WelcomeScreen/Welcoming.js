// import React, {useEffect} from 'react';
// import {Block, Text, Button} from 'components';
// import * as icons from 'assets/icons';
// import {Image, Platform, StatusBar} from 'react-native';
// import {colors, sizes} from 'styles/theme';
// import {NAV} from 'utils';
// import {LANG_EN, LANG_AR, LANGUAGE_SELECTED} from 'redux/constants';
// import {useDispatch} from 'react-redux';

// export const WelcomingScreen = ({navigation}) => {
//   const dispatch = useDispatch();

//   const setLanguage = selectedLanguage => {
//     dispatch({
//       type: LANGUAGE_SELECTED,
//       payload: selectedLanguage,
//     });
//     navigation.navigate(NAV.HOME);
//   };

//   useEffect(() => {
//     Platform.OS === 'android' &&
//       (StatusBar.setBarStyle('light-content'),
//       StatusBar.setBackgroundColor(colors.customRed));
//   });

//   return (
//     <>
//       <StatusBar barStyle="light-content" />

//       <Block center middle style={{backgroundColor: colors.customRed}}>
//         <Block flex={false} middle>
//           <Image
//             source={icons.white_logo}
//             style={{resizeMode: 'contain', width: sizes.getWidth(47)}}
//           />
//         </Block>
//         <Block
//           flex={false}
//           middle
//           row
//           style={{width: '35%', justifyContent: 'space-between'}}>
//           <Button
//             onPress={() => setLanguage(LANG_EN)}
//             center
//             middle
//             style={{width: '40%'}}>
//             <Image
//               source={icons.english_word}
//               style={{resizeMode: 'contain', width: sizes.getWidth('13')}}
//             />
//           </Button>
//           <Button
//             onPress={() => setLanguage(LANG_AR)}
//             middle
//             center
//             style={{width: '40%', paddingBottom: sizes.getHeight(1)}}>
//             <Image
//               source={icons.arabi_word}
//               style={{resizeMode: 'contain', width: sizes.getWidth('9')}}
//             />
//           </Button>
//         </Block>
//       </Block>
//     </>
//   );
// };
