import React, {useEffect, useState} from 'react';
import {Block, Text, Button} from 'components';
import * as icons from 'assets/icons';
import {Image} from 'react-native';
import {colors, sizes} from 'styles/theme';
import {NAV} from 'utils';
import {useDispatch, useSelector} from 'react-redux';
import {LANG_EN, LANG_AR, SET_LANGUAGE} from 'redux/constants';

export const WelcomeScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const {status} = useSelector(state => state.auth);
  const setLanguage = selectedLanguage => {
    dispatch({
      type: SET_LANGUAGE,
      payload: selectedLanguage,
    });

    navigation.navigate(NAV.HOME);
  };

  return (
    <>
      <StatusBar barStyle="light-content" />
      <Block center middle style={{backgroundColor: colors.customRed}}>
        <Block flex={false} middle>
          <Image
            source={icons.white_logo}
            style={{resizeMode: 'contain', width: sizes.getWidth(47)}}
          />
        </Block>
        <Block
          flex={false}
          middle
          row
          style={{width: '25%', justifyContent: 'space-between'}}>
          <Button
            onPress={() => {
              setLanguage(LANG_EN);
            }}
            center
            middle
            style={{width: '40%'}}>
            <Image
              source={icons.english}
              style={{resizeMode: 'contain', width: sizes.getWidth('10')}}
            />
          </Button>
          <Button
            onPress={() => {
              setLanguage(LANG_AR);
            }}
            middle
            center
            style={{width: '40%'}}>
            <Image
              source={icons.arabi}
              style={{resizeMode: 'contain', width: sizes.getWidth('10')}}
            />
          </Button>
        </Block>
      </Block>
    </>
  );
};
//
