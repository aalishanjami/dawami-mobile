import React, {useState, useRef, useEffect} from 'react';
import {
  Block,
  Text,
  CustomHeader,
  Button,
  CustomModal,
  CustomAlert,
  ActivitySign,
} from 'components';
import {colors, sizes} from 'styles/theme';
import {
  FlatList,
  Image,
  Picker,
  StyleSheet,
  RefreshControl,
  Platform,
  SafeAreaView,
  Dimensions,
} from 'react-native';
import {ResumeStatus} from './Sections/ResumeStatus';
import {ProfileStatus} from './Sections/ProfileStatus';
import * as icons from 'assets/icons';
import {cvData} from 'utils/dummyData';
import Toast from 'react-native-easy-toast';
import {CVBuilderCards} from './Sections/cvBuilderCard';
import {getAllResumeReuqests} from 'redux/actions/getRequests';
import {useSelector, useDispatch} from 'react-redux';
import {USER_DRAFTS, LANG_AR} from 'redux/constants';
import {SubHeading} from './Sections/MainPageSection/heading';
import {WORDS} from 'utils';

const CvBuilder = ({route, navigation}) => {
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width <= 375;
  // const {userInfo} = useSelector(state => state.userInfo);
  const id = useSelector(state => state.auth?.userBasicProfile?.id);
  const allDrafts = useSelector(state => state.getLists.allDrafts);
  const {status} = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const [FQscreen, setFQscreen] = useState(true);
  const [isWaiting, setIsWaiting] = useState(false);
  const [modalVisibility, setModalVisibility] = useState(false);
  const [btnDisable, setBtnDisable] = useState(true);
  const messageRef = useRef();
  const comingStatus = message => {
    messageRef.current?.show(
      <CustomAlert textColor={colors.customRed} text={message} />,
      4000,
    );
  };

  useEffect(() => {
    status && getAllRequest();
  }, [id]);

  // const [submittedReq, setSubmittedReq] = useState();
  const [errorMessage, setErrorMessage] = useState(false);
  let data = {};
  const getAllRequest = async () => {
    // console.log('==================');
    // console.log('Fetching Result');
    // console.log()
    setIsWaiting(true);
    const result = await getAllResumeReuqests(id, err => {
      messageRef.current?.show(
        <CustomAlert textColor={colors.customRed} text={err?.message || err} />,
        3000,
      ),
        setIsWaiting(false),
        setErrorMessage(true),
        setBtnDisable(true);
    });
    result &&
      (dispatch({type: USER_DRAFTS, payload: result}),
      setBtnDisable(false),
      setIsWaiting(false));
    setBtnDisable(false);
  };

  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = async () => {
    getAllRequest();
  };

  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;
  // console.log(currentLN);
  // console.log(AR);
  const reqBtn = AR ? WORDS.reqNewResume_ar : WORDS.reqNewResume;
  const noResult = AR ? WORDS.NO_RESULT_FOUND_AR : WORDS.NO_RESULT_FOUND;

  return (
    <>
      <SafeAreaView style={{flex: 1}}>
        <Block>
          <CustomHeader navigation={navigation} />
          <SubHeading />
          <Block style={{borderWidth: 0}}>
            <Block color={colors.spaceGreen}>
              {/* --------- */}
              <Block center middle>
                {allDrafts && allDrafts.length > 0 ? (
                  <FlatList
                    refreshControl={
                      <RefreshControl
                        onRefresh={onRefresh}
                        refreshing={refreshing}
                      />
                    }
                    keyExtractor={(item, index) => {
                      return index.toString();
                    }}
                    showsVerticalScrollIndicator={false}
                    data={allDrafts}
                    renderItem={({item}) => {
                      return (
                        <CVBuilderCards
                          isWaiting={status => setIsWaiting(status)}
                          data={item}
                          count={allDrafts.length}
                        />
                      );
                    }}
                  />
                ) : (
                  <Block center middle>
                    <Text h3 color={colors.primary}>
                      {noResult}
                    </Text>
                  </Block>
                )}
              </Block>
              {/* -------------------------------------------------- */}
              <Block center middle style={styles.reqBtnCon}>
                <Button
                  disabled={!status || btnDisable}
                  onPress={() => setModalVisibility(true)}
                  activeOpacity={0.3}
                  center
                  middle
                  style={{
                    borderRadius: sizes.screenSize * 0.007,
                    paddingHorizontal: sizes.getWidth(3.5),
                    backgroundColor:
                      !status || btnDisable ? colors.gray : colors.spaceGreen,
                  }}>
                  <Text h3 color={colors.primary}>
                    {reqBtn}
                  </Text>
                </Button>
              </Block>
              {/* -------------------------------------------------- */}
            </Block>
          </Block>
        </Block>
        <Toast
          ref={messageRef}
          style={{
            backgroundColor: colors.primary,
            width: sizes.getWidth(100),
            borderRadius: 2,
          }}
          positionValue={sizes.getDimensions.height}
          fadeInDuration={200}
          fadeOutDuration={100}
          opacity={1}
        />
        {/* MODAL */}
        <CustomModal
          
          closeModal={() => setModalVisibility(false)}
          animation={'slide'}
          isVisible={modalVisibility}>
          {FQscreen ? (
            <ResumeStatus
              goToNext={() => setFQscreen(false)}
              status={message => comingStatus(message)}
              closeModal={() => setModalVisibility(false)}
            />
          ) : (
            <ProfileStatus
              goToPrev={() => setFQscreen(true)}
              status={({message}) => comingStatus(message)}
              closeModal={() => {
                setModalVisibility(false), setFQscreen(true);
              }}
            />
          )}
        </CustomModal>
        {isWaiting && <ActivitySign />}
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  btnStyle: {
    width: sizes.getWidth(50),
    padding: sizes.getWidth(5),
    borderRadius: sizes.getHeight(0.7),
    borderColor: colors.customRed,
  },
  reqBtnCon: {
    backgroundColor: colors.primary,
    flex: 0,
    position: 'absolute',
    bottom: 0,
    // bottom: -sizes.getHeight(3),
    zIndex: 1,
    width: '100%',
  },
  reqBtn: {
    width: sizes.getWidth(55),
    padding: sizes.getWidth(5),
    borderRadius: sizes.getHeight(0.7),
    // borderWidth: 5,
    height: sizes.getHeight(5),
    borderColor: colors.customRed,
  },
});

export {CvBuilder};
