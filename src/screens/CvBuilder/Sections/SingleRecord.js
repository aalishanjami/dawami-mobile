import React, {useEffect, useState} from 'react';
import {Block, Text, Button, ActivitySign} from 'components';
import {sizes, colors} from 'styles/theme';
import {
  FlatList,
  Platform,
  SafeAreaView,
  Dimensions,
  StyleSheet,
  ScrollView,
  Animated
} from 'react-native';
import {Image} from 'react-native-animatable';
import * as icons from 'assets/icons';
import {ViewDrafts} from './viewDrafts';
import {useSelector, useDispatch} from 'react-redux';
import Axios from 'axios';
import {ADDING_FEEDBACK} from 'redux/constants';
import {checkResponse} from 'redux/actions/getRequests';
import {DraftDetails} from './draftSections/draftsDetails';


const SingleRecord = props => {

  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width <= 375;
  const {closeModal} = props;
  const {currentViewProg} = useSelector(state => state.getLists);
  const [isWaiting, setIsWaiting] = useState(false);
  const dispatch = useDispatch();

  // useEffect(() => {
  //   currentViewProg.drafts.forEach(async element => {
  //     setIsWaiting(true)
  //     const response = await checkResponse(element.id, err => {
  //       console.log('error is here ')
  //     })
  //     console.log('response===================')
  //     console.log(response)
  //     setIsWaiting(false)
  //     })
  // },[])


  let requestDetails = currentViewProg.final
    ? [currentViewProg.final]
    : currentViewProg.drafts;
  // console.log('-------------currentViewProg---------------');
  // console.log(currentViewProg);
  // console.log(props);

  return (
    <>
      <SafeAreaView
        style={{
          flex: 0,
        }}
      />
      <Block
        color={colors.spaceGreen}
        style={{
          // backgroundColor: currentViewProg?.drafts.length
          // ? colors.spaceGreen
          // : '#f0f0f099',
          width: '100%',
          height:sizes.screenSize * 0.6,
          flex:0
        }}
        center
        middle>
        {currentViewProg.drafts.length ? (
          <Block
            center
            padding={[sizes.padding, 0]}
            style={{
              width: '100%',
            }}>
            <ScrollView style={{flex:1}}>
              {requestDetails &&
                requestDetails.map((v, i) => {
                  console.log('--------');
                  console.log(currentViewProg.final);
                  return (
                    currentViewProg.final && (
                      <Block
                        center
                        middle
                        style={{
                          height:
                            v.length === 1 ? sizes.getHeight(90) : 'auto',
                        }}>
                        <DraftDetails {...currentViewProg} />
                      </Block>
                    )
                  );
                })}
              <Block
                center
                middle
                style={{
                  // height: sizes.getHeight(90),
                  // borderWidth:1
                }}
                >
                <ViewDrafts closeModal={closeModal} />
              </Block>
            </ScrollView>

            {/* <FlatList
              keyExtractor={index => {
                return index.toString();
              }}
              data={requestDetails}
              renderItem={({item}) => {
                // console.log(item);
                return (
                  <Block
                    center
                    middle
                    style={{
                      height: item.length === 1 ? sizes.getHeight(90) : 'auto',
                    }}>
                    {currentViewProg.final && (
                      <Block style={{borderWidth:0, borderColor:'red'}}>
                        <DraftDetails {...currentViewProg} />
                      </Block>
                    )}
                    <ViewDrafts closeModal={closeModal} />
                  </Block>
                );
              }}
            /> */}
          </Block>
        ) : (
          <Block
            center
            middle
            flex={false}
            height={sizes.getHeight(18)}
            width={sizes.getWidth(85)}
            style={{
              backgroundColor: colors.primary,
              borderRadius: sizes.getWidth(2),
              // borderWidth: 0.1,
              borderColor: 'red',
              elevation: 4,
            }}>
            <Text
              color={colors.spaceGreen}
              style={{
                textAlign: 'center',
              }}>
              No Draft Available Against This Request {'\n'}
              Please Wait ,{'\n'}
              Request is under observations.
            </Text>
          </Block>
        )}

        {isWaiting && <ActivitySign />}
      </Block>
    </>
  );
};

export {SingleRecord};
