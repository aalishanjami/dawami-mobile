import React from 'react';
import {Block, Text} from 'components';
import {Image, StyleSheet, Platform, Dimensions} from 'react-native';
import {colors, sizes} from 'styles/theme';
import * as icons from 'assets/icons';
import {WORDS} from 'utils';
import {useSelector} from 'react-redux';
import {LANG_AR} from 'redux/constants';

const SubHeading = () => {
  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;
const ios = Platform.OS === 'ios';
const iphone6s = Dimensions.get('screen').width <= 375;
  const title = AR ? WORDS.newReq_ar : WORDS.newReq;
  // console.log('////////////////');
  // console.log(title);
  // console.log('////////////////');
  return (
    <Block
      color={colors.spaceGreen}
      style={{borderWidth: 0}}
      flex={false}
      height={sizes.getHeight(6)}
      row
      padding={[0, sizes.padding]}
      middle>
      <Block center middle>
        <Image source={icons.resume} style={styles.menuSubLogo} />
      </Block>
      <Block flex={9} middle>
        <Text style={{textAlign: 'left',
        fontSize:sizes.customFont(iphone6s?10:18)
        }} bold color={colors.primary}>
          {title}
        </Text>
      </Block>
    </Block>
  );
};

const styles = StyleSheet.create({
  menuSubLogo: {
    // flex: 0.7,
    width: sizes.getWidth(13),
    tintColor: colors.primary,
    resizeMode: 'contain',
  },
});

export {SubHeading};
