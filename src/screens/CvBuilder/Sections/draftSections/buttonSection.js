import React, {useState, useEffect} from 'react';
import {Block, Button, Text} from 'components';
import {sizes, colors} from 'styles/theme';
import {
  ActivityIndicator,
  Image,
  StyleSheet,
  Platform,
  PermissionsAndroid,
  Alert,
} from 'react-native';
import * as icons from 'assets/icons';
import Toast from 'react-native-easy-toast';
import {ShowComments} from './showComments';
import {useSelector} from 'react-redux';
import RNFetchBlob from 'rn-fetch-blob';
import {draftLiked, draftDisliked} from 'redux/actions';
import {ResumeDownload} from 'redux/apiConstants';
// import { draftDislike } from 'redux/apiConstants';
import {iphone6s} from 'styles/theme';

const ActionButtons = props => {
  const {id, request_id, file, final, closeModal} = props;
  // const {final} = useSelector(state => state.getLists.currentViewProg);

  // console.log('button props======================');
  // console.log(props);
  const {dislike, like} = icons;
  const [showComments, setShowComments] = useState(false);
  const [isWaiting, setIsWaiting] = useState(false);

  const dislikedHandler = (draft_id, request_id) => {
    // id: draft_id, req_id: request_id
    setShowComments(prev => !prev);
  };

  const sendComments = async (id, req_id, comments) => {
    setIsWaiting(true);
    // console.log('id')
    // console.log(id)
    // console.log('req_id')
    // console.log(req_id)
    // console.log('comment')
    // console.log(comments)
    // //call api
    const result = draftDisliked({id, req_id, comments}, err => {
      console.log('err');
      console.log(err);
      alert(err);
      setIsWaiting(false);
    });
    Alert.alert(
      'Feedback Submitted',
      '  Thanks, Your feedback is important to us',
      [
        // {text:'Stay Here', onPress: () => { setIsWaiting(false) }},
        {
          text: 'Going Back',
          onPress: () => {
            setIsWaiting(false), closeModal();
          },
        },
      ],
    );
    // props.closeModal()
  };

  const [data, setData] = useState();

  const likedHandle = async ({id, request_id}) => {
    // console.log('like');
    // console.log(id);
    // console.log(request_id);
    setIsWaiting(true);
    const result = await draftLiked({req_id: request_id, draft_id: id}, err => {
      return messageRef.current?.show(
        <CustomAlert text="Can't Process Request. Please try again." />,
        3000,
      );
    });
    console.log('result');
    console.log(result);
    setIsWaiting(false);
    closeModal();
  };

  const downloadMe = (downloadType, fileName) => {
    const fullPath = `${ResumeDownload}/${downloadType}/${fileName}`;

    const isIOS = Platform.OS === 'ios';
    const {DocumentDir, DownloadDir} = RNFetchBlob.fs.dirs;
    const actualPath = Platform.select({
      ios: DocumentDir,
      android: DownloadDir,
    });
    const configOptions = Platform.select({
      ios: {
        fileCache: true,
        appendExt: 'pdf',
      },
      android: {
        fileCache: false,
        addAndroidDownloads: {
          useDownloadManager: true,
          title: fileName,
          description: 'Downloading...',
          mime: 'application/pdf',
          mediaScannable: true,
          notification: true,
          path: `${actualPath}/${fileName}`,
        },
      },
    });

    if (isIOS) {
      closeModal();
      const encodedUrl = encodeURI(fullPath);
      // console.log("IOS DEVICE")
      RNFetchBlob.config(configOptions)
        .fetch('GET', encodedUrl)
        .then(async res => {
          setIsWaiting(false);

          RNFetchBlob.ios.previewDocument(res.path());
          const options = {
            title: fileName,
            urls: [`file://${fullPath}`],
          };
        })
        .catch(error => {
          console.log('error', error);
          setIsWaiting(false);
        });
      return;
    } else {
      RNFetchBlob.config(configOptions)
        .fetch('GET', fullPath)
        .progress((received, total) => {
          console.log('progress : ' + received + ' / ' + total);
        })
        .then(resp => {
          RNFetchBlob.android.actionViewIntent(resp.path());
          Alert.alert('Success');
          setIsWaiting(false);
        });
    }
  };
  const downloadFile = async (downloadType, urlPath) => {
    // closeModal()
    // console.log(urlPath);
    setIsWaiting(true);
    if (Platform.OS === 'android') {
      console.log('Platform is android');
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message: `To save this Draft \nClick Allow at next pop up. \nThanks`,
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          downloadMe(downloadType, urlPath);
          setIsWaiting(false);
          // closeModal();
          console.log('completed');
        } else {
          Alert.alert(
            'Attension',
            'In order to download this draft \n\nPlease go to setting > app > permissions  and turn it on',
          );
          setIsWaiting(false);
        }
      } catch (e) {
        console.log('error');
        console.log(e);
        alert(e);
        setIsWaiting(false);
        closeModal();
      }
    } else {
      downloadMe(downloadType, urlPath);
    }
  };

  return (
    <Block
      center
      style={{
        width: '100%',
        height: showComments ? sizes.getHeight(35) : 'auto',
      }}>
      {final ? (
        <Block
          center
          middle
          style={{
            // backgroundColor: '#EEEEEE99',
            backgroundColor: 'transparent',
            width: '100%',
            height: sizes.getHeight(8),
          }}>
          {isWaiting ? (
            <Button
              disabled={true}
              center
              middle
              style={{
                ...styles.btnStyle,
                backgroundColor: '#EEEEEE99',
                // backgroundColor: colors.spaceGreen,
              }}>
              <ActivityIndicator
                size="small"
                size={22}
                color={colors.customRed}
              />
            </Button>
          ) : (
            <Button
              row
              onPress={() => downloadFile('finals', final.file)}
              activeOpacity={0.3}
              center
              middle
              style={{...styles.btnStyle, borderWidth: 0.9, width: '70%'}}>
              <Text style={styles.font} color={colors.spaceGreen}>
                Final
              </Text>
              <Image
                source={icons.downloadSign}
                style={{
                  resizeMode: 'contain',
                  width: sizes.getWidth(10),
                  tintColor: colors.gray2,
                }}
              />
            </Button>
          )}
        </Block>
      ) : (
        <Block flex={false} row center style={styles.mainCon}>
          <Block
            crossRight
            flex={false}
            style={{
              width: '68%',
              //   height:sizes.getHeight(10),
              //   backgroundColor: 'red',
              // borderWidth: 1,
            }}>
            {isWaiting ? (
              <Button
                disabled={true}
                center
                middle
                style={{
                  ...styles.btnStyle,
                  backgroundColor: '#EEEEEE99',
                }}>
                <ActivityIndicator
                  size="small"
                  size={22}
                  color={colors.customRed}
                />
              </Button>
            ) : (
              <Button
                row
                onPress={() => downloadFile('drafts', file)}
                activeOpacity={0.3}
                center
                middle
                style={styles.btnStyle}>
                <Text color={colors.spaceGreen} style={styles.font}>
                  Draft
                </Text>
                <Image
                  source={icons.downloadSign}
                  style={{
                    resizeMode: 'contain',
                    width: sizes.getWidth(10),
                    tintColor: colors.spaceGreen,
                  }}
                />
              </Button>
            )}
          </Block>
          {/* like dislike */}
          <Block
            center
            padding={[0, sizes.getWidth(2)]}
            row
            middle
            style={{borderWidth: 0}}
            space={'between'}>
            <Button
              disabled={isWaiting || props.feedback}
              activeOpacity={0.3}
              center
              middle
              onPress={() => likedHandle({id, request_id})}
              style={{
                ...styles.likeBtn,
                backgroundColor:
                  (props.feedback &&
                    props.feedback.feedback === '1' &&
                    colors.spaceGreen) ||
                  (props.feedback &&
                    props.feedback.feedback === '2' &&
                    colors.gray),
              }}>
              <Image
                source={like}
                style={{
                  ...styles.likeImg,
                  tintColor:
                    (props.feedback && colors.primary) || colors.spaceGreen,
                }}
              />
            </Button>
            <Button
              disabled={isWaiting || props.feedback}
              onPress={() => dislikedHandler(id, request_id)}
              activeOpacity={0.3}
              center
              middle
              style={{
                ...styles.dislikeBtn,
                // borderWidth: 1,
                backgroundColor:
                  (props.feedback &&
                    props.feedback.feedback === '2' &&
                    colors.red) ||
                  (props.feedback &&
                    props.feedback.feedback === '1' &&
                    colors.gray),
              }}>
              <Image
                source={dislike}
                style={{
                  ...styles.dislikeImg,
                  tintColor:
                    (props.feedback && colors.primary) || colors.customRed,
                }}
              />
            </Button>
          </Block>
        </Block>
      )}
      {showComments && (
        <ShowComments
          getComments={comments => sendComments(id, request_id, comments)}
          closeComments={() => setShowComments(false)}
        />
      )}
    </Block>
  );
};

const styles = StyleSheet.create({
  font: {
    fontSize: sizes.customFont(iphone6s?13:16),
  },
  mainCon: {
    borderWidth: 0,
    // backgroundColor: '#EEEEEE99',
    borderRadius: sizes.getWidth(0.9),
  },
  likeBtn: {
    borderWidth: 1,
    padding: 2,
    // borderColor: '#4E9A1D',
    borderColor: colors.spaceGreen,
    borderRadius: sizes.getWidth(1),
    height: sizes.getHeight(5.3),
    width: sizes.getWidth(10),
    // borderStyle: 'dotted',
  },
  likeImg: {
    resizeMode: 'contain',
    width: sizes.getWidth(8),
    tintColor: '#4E9A1D',
  },
  dislikeBtn: {
    borderWidth: 1,
    padding: 2,
    width: sizes.getWidth(10),

    borderRadius: sizes.getWidth(1),
    height: sizes.getHeight(5.3),
    borderStyle: 'dotted',
    borderColor: colors.customRed,
  },
  dislikeImg: {
    resizeMode: 'contain',
    width: sizes.getWidth(8),
    tintColor: colors.customRed,
  },
  btnStyle: {
    borderWidth: 1,
    borderColor: colors.spaceGreen,
    borderStyle: 'dashed',
    // backgroundColor: colors.gray,
    borderRadius: sizes.getWidth(1),
    width: '50%',
  },
});

export {ActionButtons};
