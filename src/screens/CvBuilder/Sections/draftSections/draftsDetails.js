import React, {useState, useEffect} from 'react';
import {Block, Text, Button} from 'components';
import {StyleSheet, FlatList, PermissionsAndroid} from 'react-native';
import {sizes, colors} from 'styles/theme';
import {ActionButtons} from './buttonSection';
import RNFetchBlob from 'rn-fetch-blob';
import {ShowComments} from './showComments';
import {useSelector} from 'react-redux';
import {iphone6s} from 'styles/theme'

const DraftDetails = props => {
  // const {final} = useSelector(state => state.getLists.currentViewProg)
  const {
    id,
    request_id,
    name,
    description,
    created_at,
    file,
    data,
    final,
    // isWaiting,
    closeModal,
  } = props;



  // const [showComments, setShowComments] = useState(true);
  const disliked = (id, req_id) => {
    // console.log(id);
    // console.log(req_id);
  };
  const [isWaiting, setIsWaiting] = useState(false);
  const [height, setHeight] = useState();

  const likedHandler = async (draft_id, req_id) => {
    setIsWaiting(true);
    // console.log(draft_id);
    // console.log(req_id);
    //     const result = await draftLiked({req_id, draft_id}, err => {
    //       return messageRef.current?.show(
    //         <CustomAlert text="Can't Process Request. Please try again." />,
    //         3000,
    //       );
    //     });
    //     console.log('result');
    setIsWaiting(false);
  };
  const {drafts} = useSelector(state => state.getLists.currentViewProg);

  // console.log('name');
  // console.log(drafts);


  console.log('----------------------------------');
  console.log(props)


  return (
    <>
      <Block center flex={false} height={height} style={styles.mainCon}>
        <Block
          row
          flex={false}
            // style={{height: sizes.getHeight(10)}}
        >
          <Block style={{borderWidth: 0}}>
            <Text style={styles.font} color={colors.spaceGreen}>
              Name
            </Text>
            <Text style={styles.font} color={colors.spaceGreen}>
              Description
            </Text>
            <Text style={styles.font} color={colors.spaceGreen}>
              Created At
            </Text>
          </Block>
          <Block flex={2} style={styles.detailsStyle}>
            <Text style={styles.font} color={colors.spaceGreen}>
              {(final && final.name) || name}
            </Text>
            <Text
              color={colors.spaceGreen}
              style={styles.font}
              ellipsizeMode="tail"
              numberOfLines={1}>
              {(final && final.description) || description}
            </Text>
            <Text
              color={colors.spaceGreen}
              h2
              style={{...styles.font, lineHeight: 27}}>
              {/* {new Date(final && final.created_at || created_at).toLocaleDateString()} */}
              {new Date(
                (final && final.created_at) ||
                  created_at.replace(/\s+/g, 'T'),
              ).toDateString()}
            </Text>
          </Block>
        </Block>
        <ActionButtons {...props} />
      </Block>
      {/* {props?.feedback && (
        <Block>
          <Text>123</Text>
        </Block>
      )} */}
    </>
  );
};

const styles = StyleSheet.create({
  font: {
    fontSize: sizes.customFont(iphone6s?12:16),
  },
  mainCon: {
    overflow: 'hidden',
    // backgroundColor: '#BD9571',
    borderRadius: sizes.getWidth(2),
    // borderWidth: 0.1,
    backgroundColor: colors.primary,
    borderColor: 'red',
    elevation: 1,
    padding: sizes.padding,
    margin: sizes.getHeight(0.7),
    width: sizes.getWidth(90),
  },
  detailsStyle: {
    // backgroundColor: '#EEEEEE99',
    // backgroundColor:,
    borderTopLeftRadius: sizes.getWidth(2),
    borderTopRightRadius: sizes.getWidth(2),
    paddingHorizontal: sizes.padding * 1,
  },
});

export {DraftDetails};
