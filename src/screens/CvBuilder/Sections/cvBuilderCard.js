import React, {useState, useRef, useEffect} from 'react';
import {
  Block,
  Text,
  Button,
  ActivitySign,
  CustomModal,
  CustomAlert,
} from 'components';
import {sizes, colors} from 'styles/theme';
import {checkProgress, checkResponse} from 'redux/actions/getRequests';
import {SingleRecord} from './SingleRecord';
import Toast from 'react-native-easy-toast';
import {useDispatch, useSelector} from 'react-redux';
import {CURRENT_VIEW_PROG, ADDING_FEEDBACK, LANG_AR} from 'redux/constants';
import {WORDS} from 'utils';
import {Image, StyleSheet, Platform, Dimensions, Animated, PanResponder} from 'react-native';
import * as icons from 'assets/icons';

const CVBuilderCards = props => {
  const {data, isWaiting} = props;
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width <= 375;
  const dispatch = useDispatch();
  const enumStatus = {
    Pending: '0',
    Assigned: '1',
    InProgress: '2',
    WaitingForFeedBack: '3',
    Completed: '4',
    Cancelled: '5',
    Closed: '6',
    DraftAccepted: '7',
  };

  const getStatus = status => {
    let currentStatus = '';
    switch (status) {
      case '0':
        return (currentStatus = 'Pending'); //0
      case enumStatus.Assigned:
        return (currentStatus = 'Assiged'); // 1
      case enumStatus.InProgress:
        return (currentStatus = 'In-Progress'); //2
      case enumStatus.WaitingForFeedBack:
        return (currentStatus = 'Waiting For FeedBack'); //3
      case enumStatus.Completed:
        return (currentStatus = 'Completed'); //4
      case enumStatus.Cancelled:
        return (currentStatus = 'Cancelled'); //5
      case enumStatus.Closed:
        return (currentStatus = 'Closed'); //6
      case enumStatus.DraftAccepted:
        return (currentStatus = 'Draft Accepted'); //7
      default:
        return (currentStatus = 'Please Wait..'); //8
    }
  };

  const [modalVisibility, setModalVisiblity] = useState(false);
  // const [draftData, setDraftData] = useState([]);
  const messageRef = useRef();
  const viewProgress = async id => {
    isWaiting(true);
    const result = await checkProgress(id, err => {
      messageRef.current?.show(
        <CustomAlert
          textColor={colors.primary}
          text="Request Could not be proceeded"
        />,
        3500,
      ),
        isWaiting(false);
    });
    if (result) {
      console.log(result), dispatch({type: CURRENT_VIEW_PROG, payload: result});
      isWaiting(false);
      setModalVisiblity(true);
    }
  };

  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;

  let viewProg = AR ? WORDS.viewProg_ar : WORDS.viewProg;

  // console.log(new Date(data.created_at).toLocaleDateString());
 
  return (
    <Block
      flex={false}
      padding={[sizes.getHeight(1), sizes.getWidth(1)]}
      margin={[sizes.getHeight(0.6), 0]}
      height={sizes.getHeight(iphone6s ? 28 : 25)}
      width={sizes.getWidth('93')}
      color={colors.primary}
      style={{
        elevation: 2,

        borderRadius: sizes.getWidth(0.6),
      }}>
      {/* ==============================================================*/}

      <Block flex={2}>
        <Block padding={[0, sizes.getWidth(3)]} row space={'between'}>
          <Text h4> Status</Text>

          <Block
            center
            middle
            flex={false}
            style={{
              borderWidth: 0.4,
              borderRadius: sizes.getWidth(1),
              width: '40%',
              height: '80%',
            }}>
            <Text
              style={{
                fontSize: sizes.customFont(iphone6s ? 8 : 11),
              }}>
              {getStatus(data.status)}
            </Text>
          </Block>
        </Block>

        <Block padding={[0, sizes.getWidth(3)]} row space={'between'}>
          <Text h4> Created</Text>

          <Block
            center
            middle
            flex={false}
            style={{
              borderWidth: 0.4,
              borderRadius: sizes.getWidth(1),
              width: '40%',
              height: '80%',
            }}>
            <Text
              style={{
                fontSize: sizes.customFont(iphone6s ? 8 : 11),
              }}>
              {new Date(
                data.created_at.replace(' ', 'T'),
              ).toLocaleDateString()}
            </Text>
          </Block>
        </Block>
        <Block padding={[0, sizes.getWidth(3)]} row space={'between'}>
          <Text h4> Closed</Text>

          <Block
            center
            middle
            flex={false}
            style={{
              borderWidth: 0.4,
              borderRadius: sizes.getWidth(1),
              width: '40%',
              height: '80%',
            }}>
            <Text
              style={{
                fontSize: sizes.customFont(iphone6s ? 8 : 11),
              }}>
              {new Date(data.closed_at).toLocaleDateString()}

              {/* {new Date(data?.closed_at.replace(/\s+/g, 'T')).toDateString()} */}
            </Text>
          </Block>
        </Block>

        <Block padding={[0, sizes.getWidth(3)]} row space={'between'}>
          <Text h4> Staff Name</Text>

          <Block
            center
            middle
            flex={false}
            style={{
              borderWidth: 0.4,
              borderRadius: sizes.getWidth(1),
              width: '40%',
              height: '80%',
            }}>
            <Text
              style={{
                fontSize: sizes.customFont(iphone6s ? 8 : 11),
              }}>
              {!data.staff ? 'Not Assigned yet' : data.staff.name}
            </Text>
          </Block>
        </Block>

        <Block padding={[0, sizes.getWidth(3)]} row space={'between'}>
          <Text h4> Drafts</Text>

          <Block
            center
            middle
            flex={false}
            style={{
              borderWidth: 0.4,
              borderRadius: sizes.getWidth(1),
              width: '40%',
              height: '80%',
            }}>
            <Text
              style={{
                fontSize: sizes.customFont(iphone6s ? 8 : 11),
              }}>
              {data.total_drafts}
            </Text>
          </Block>
        </Block>
      </Block>

      <Block padding={[0, sizes.getWidth(3)]}>
        <Button
          onPress={() => viewProgress(data.id)}
          activeOpacity={0.3}
          style={{
            borderWidth: 1,
            borderStyle: 'dashed',
            borderRadius: sizes.getWidth(1),
            borderColor: colors.spaceGreen,
            alignItems: 'center',
            // width:'30%'
          }}
          center
          middle>
          <Text
            style={{
              borderWidth: 0,
              textAlign: 'center',
              lineHeight: 27,
              fontSize: sizes.customFont(iphone6s ? 8 : 11),
            }}
            h2
            color={colors.spaceGreen}>
            {viewProg}
          </Text>
        </Button>

        <Block />
      </Block>

      <CustomModal
        color={colors.spaceGreen}
        closeModal={() => {
          setModalVisiblity(false);
        }}
        isVisible={modalVisibility}>
        <SingleRecord closeModal={() => setModalVisiblity(false)} />
        {Platform.OS === 'ios' && (
          // <Button style={styles.closeBtnCon}>
            <Button
              onPress={() => setModalVisiblity(false)}
              style={styles.closeBtn}>
              <Image
                source={icons.close_white}
                style={{
                  resizeMode: 'contain',
                  flex: 1,
                  tintColor: colors.spaceGreen,
                }}
              />
            </Button>
          // </Button>
        )}
      </CustomModal>
    </Block>
  );
};

export {CVBuilderCards};

let CIRCLE_RADIUS  = 30;
let styles = StyleSheet.create({
  circle: {
    backgroundColor: "skyblue",
    width: CIRCLE_RADIUS * 2,
    height: CIRCLE_RADIUS * 2,
    borderRadius: CIRCLE_RADIUS
  },
  closeBtnCon: {
    zIndex: 1000,
    height: sizes.screenSize * 0.03,
    // width: sizes.screenSize * 0.06,
    width: '100%',
    backgroundColor: colors.spaceGreen,
    position: 'absolute',
    bottom: 0,
    left:sizes.getWidth(0),
    justifyContent: 'center',
    alignItems: 'center',
    // borderRadius: sizes.screenSize * 0.007,
    borderWidth: 0.5,
    borderColor: colors.primary,
  },
  closeBtn: {
    // borderWidth: 1,
    position: 'absolute',
    zIndex: 1000,
    borderColor:colors.spaceGreen,
    borderWidth:1,
    width: sizes.screenSize * 0.05,
    height: sizes.screenSize * 0.05,
    borderRadius: sizes.screenSize * 4,
    backgroundColor: colors.primary,
    shadowColor: colors.gray5,
    shadowOffset: {width: 0, height: 0},
    shadowOpacity:0.8,
    bottom:sizes.getHeight(2),
    shadowRadius: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
