import React, {useState, useRef, useEffect} from 'react';
import {Block, Button, Text, ActivitySign, CustomAlert} from 'components';
import {sizes, colors} from 'styles/theme';
import {draftLiked} from 'redux/actions';
import {
  Image,
  FlatList,
  Platform,
  PermissionsAndroid,
  Alert,
  ActivityIndicator,
  StyleSheet,
  TextInput,
  Keyboard,
  Dimensions,
} from 'react-native';
import * as icons from 'assets/icons';
// import RNFS from 'react-native-fs'
import RNFetchBlob from 'rn-fetch-blob';
import Toast from 'react-native-easy-toast';
import {DraftDetails} from './draftSections/draftsDetails';
import {ActionButtons} from './draftSections/buttonSection';
import {useDispatch, useSelector} from 'react-redux';

const ViewDrafts = props => {
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width <= 375;
  const {data, closeModal} = props;
  const {currentViewProg} = useSelector(state => state.getLists);

  // console.log('currentViewProg.drafts------------');
  // console.log(currentViewProg.drafts);

  // useEffect(() => {
  //   currentViewProg.dra
  // })

  return (
    <Block>
      {currentViewProg.drafts.map((el, index) => {
        return (
          <DraftDetails
            key={index}
            closeModal={closeModal}
            {...el}
            key={el.id}
          />
        );
      })}
    </Block>
  );
};

export {ViewDrafts};
