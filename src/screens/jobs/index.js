import React, {useState, useEffect, useRef} from 'react';
import {
  Block,
  Text,
  Searchbar,
  Button,
  CustomHeader,
  ActivitySign,
  CustomAlert,
} from 'components';
import {vacancies, sortByProperty, NAV, WORDS} from 'utils';
import {
  FlatList,
  Image,
  ScrollView,
  RefreshControl,
  SafeAreaView,
} from 'react-native';
import * as icons from 'assets/icons';
import {sizes, colors} from 'styles/theme';
import {useSelector, useDispatch} from 'react-redux';
import {LANG_AR, SAVE_JOB_STATUS, JOB_LIST_STATE} from 'redux/constants';
import {companyLogo} from 'redux/apiConstants';
import {
  AuthUserViewJob,
  GetJobsList,
  AppliedJobs,
  FavoriteJob,
} from 'redux/actions';
import Toast from 'react-native-easy-toast';
import {FilterPopup} from 'components/FilterPopup';
import JobFilterOptions from './Section/JobFilterOptions';
import {JobCard} from './Section/JobCard';
import {findJobHere_ar} from 'utils/LanguageHandler';
// ============================================================================
const JobsList = props => {
  const {navigation, route} = props;
  const UPDATED_AT = 'updated_at';
  const CREATED_AT = 'created_at';

  const {listOfJobs} = useSelector(state => state.getLists);
  const {currentLN} = useSelector(state => state.userInfo);
  const [isWaiting, setIsWaiting] = useState(false);
  const messageRef = useRef();

  // useEffect(() => {
  //   setTimeout(() => fetchListOfJobs, 3000);
  // });
  const {status} = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const id = useSelector(state => state.auth?.userBasicProfile?.id);
  const jobDetails = useSelector(state => state.getLists?.currentJobStatus);
  const [showFilter, setShowFilter] = useState(false);
  const fetchListOfJobs = () => {};

  // console.log(listOfJobs)

  const goToJobDetails = async item => {
    if (status) {
      setIsWaiting(true);
      const result = await AuthUserViewJob(item.id, id, err => {
        console.log('error');
        messageRef.current?.show(
          <CustomAlert text="Error Please Try Again." />,
        );
        setIsWaiting(false);
      });
      console.log('*******RESULT------');
      console.log(result);
      result &&
        (dispatch({type: SAVE_JOB_STATUS, payload: result}),
        navigation.navigate(NAV.JOB_DETAILS),
        setIsWaiting(false));
    } else {
      console.log('user is logged out');
      console.log(item);
      dispatch({type: SAVE_JOB_STATUS, payload: item});
      navigation.navigate(NAV.JOB_DETAILS);
      setIsWaiting(false);
    }
  };
  useEffect(() => {
    const res = setJobList(listOfJobs);
    return res;
  }, [listOfJobs]);
  const [joblist, setJobList] = useState(listOfJobs);
  const inputHandler = word => {
    var filteredList = [];

    listOfJobs.forEach(element => {
      var lowerTitle = element.title_en.toLowerCase();
      var lowerWord = word.toLowerCase();
      console.log(lowerWord);
      if (lowerTitle.startsWith(lowerWord)) {
        filteredList.push(element);
      }
      setJobList(filteredList);
      // setTimeout(() => setJobList(filteredList), 250);
    });
  };
  const fillterPopup = () => {
    setShowFilter(true);
  };

  const search = filteredJobList => {
    // console.log('--------FILTERED LIST');
    // console.log(filteredJobList);
    setJobList(filteredJobList);
  };

  const [refreshing, setRefreshing] = useState(false);

  const onRefresh = async () => {
    setIsWaiting(true);
    if (!route.params?.appJobList || !route.params?.favJobList) {
      const result = await GetJobsList(err => {
        console.log('err'),
          console.log(err),
          messageRef.current?.show(
            <CustomAlert text={'Unable To Connect. Please Try Again'} />,
            3500,
          );
        console.log('========FETCHING=======');
        console.log(result);
        result && dispatch(result);
        setIsWaiting(false);
      });
    } else if (route.params?.appJobList) {
      const result_appliedJobs = await AppliedJobs(id, err => {
        console.log(err);
        setIsWaiting(false);
      });
      result_appliedJobs &&
        dispatch({
          type: JOB_LIST_STATE,
          payload: {listOfJobs: result_appliedJobs},
        });
      setIsWaiting(false);
    } else if (route.params?.favJobList) {
      const result_favJobs = await FavoriteJob(id, err => {
        console.log(err);
        setIsWaiting(false);
      });
      dispatch({
        type: JOB_LIST_STATE,
        payload: {listOfJobs: result_favJobs},
      });
    }
    setIsWaiting(false);
  };
  // console.log('TITLE CURRENT');
  // console.log(route.params?.title);
  // const sortBy_updated_at = joblist.sort(sortByProperty('updated_at'));

  const AR = currentLN === LANG_AR;
  const searchPlaceHolder = AR ? WORDS.findJobHere_ar : WORDS.findJobHere;

  // console.log('===============');
  // console.log(joblist);
  return (
    <>
      <SafeAreaView
        style={{flex: 0}}
        style={{backgroundColor: colors.spaceGreen}}
      />
      <Block>
        <CustomHeader navigation={navigation} />
        {/*  */}
        <Searchbar
          onIconPress={fillterPopup}
          onChangeText={inputHandler}
          title={!route.params?.title && AR?WORDS.JOB_AR:WORDS.JOB}
          placeholder={searchPlaceHolder}
        />
        {joblist && joblist.length ? (
          <Block padding={[0, sizes.getWidth(5)]}>
            <FlatList
              refreshControl={
                <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
              }
              showsVerticalScrollIndicator={false}
              keyExtractor={(item, index) => {
                return index.toString();
              }}
              data={joblist}
              // data={sortBy_updated_at}
              renderItem={({item}) => {
                // console.log(item.company.logo);
                return (
                  <JobCard item={item} onSelected={e => goToJobDetails(e)} />
                );
              }}
            />
          </Block>
        ) : (
          <Block middle center>
            <Text h1 color={colors.gray}>
              {AR ? WORDS.NoDataFound_AR : WORDS.NoDataFound}
            </Text>
          </Block>
        )}
        <Toast
          ref={messageRef}
          style={{
            backgroundColor: colors.customRed,
            width: sizes.getWidth(100),
            borderRadius: 2,
          }}
          positionValue={sizes.getDimensions.height}
          fadeInDuration={200}
          fadeOutDuration={100}
          opacity={0.8}
        />
        {isWaiting && <ActivitySign />}
        {showFilter && (
          <FilterPopup>
            <JobFilterOptions
              onSearch={e => search(e)}
              closePopup={() => setShowFilter(false)}
            />
          </FilterPopup>
        )}
      </Block>
    </>
  );
};

export default JobsList;
