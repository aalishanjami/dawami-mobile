import React, {useState, useEffect, useRef} from 'react';
import {
  Block,
  Text,
  Searchbar,
  Button,
  CustomHeader,
  ActivitySign,
  CustomAlert,
} from 'components';
import {getJobPosition, WORDS} from 'utils';
import {FlatList, Image, StyleSheet, Platform, Dimensions} from 'react-native';
import * as icons from 'assets/icons';
import {sizes, colors} from 'styles/theme';
import {useSelector, useDispatch} from 'react-redux';
import {companyLogo} from 'redux/apiConstants';
import {LANG_AR} from 'redux/constants';

const JobCard = props => {
  const {item, onSelected} = props;
const ios = Platform.OS === 'ios';
const iphone6s = Dimensions.get('screen').width <= 375;

  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;

  const Freelancer = AR ? WORDS.Freelancer_AR : WORDS.Freelancer;
  const FullTime = AR ? WORDS.FullTime_AR : WORDS.FullTime;
  const PartTime = AR ? WORDS.PartTime_AR : WORDS.PartTime;

  const getEmpType = id => {
    switch (id) {
      case '1':
        return Freelancer;
      case '2':
        return FullTime;
      case '3':
        return PartTime;
      default:
        return 'Unknown';
    }
  };

  console.log('=========JOB LIST CARD');
  console.log(item.employee_type);

  const typeOfEmployee = getEmpType(item.employee_type);
  const jobTitle = AR ? item?.title_ar : item?.title_en;
  const TypeOFEmployee = AR
    ? item?.company?.name_ar + ` - ${typeOfEmployee} `
    : item?.company?.name_en + ` -  ${typeOfEmployee}`;
  const date = new Date(`${item?.company?.updated_at}`.replace(/\s+/g, 'T'))
    .toDateString()
    .split(' ')
    .slice(1)
    .join(' ');
  const imgRef = companyLogo + item?.company?.logo;
  return (
    <Button
      onPress={() => onSelected(item)}
      center
      style={{
        height: sizes.getHeight(iphone6s ? 14 : 10),
        flexDirection: 'row',
        borderBottomWidth: 1,
        marginVertical: sizes.getHeight(1),
        borderBottomColor: colors.gray,
      }}>
      <Block
        center
        middle
        flex={false}
        width={sizes.getWidth('20')}
        style={{
          // borderWidth: 1,
          height: '100%',
          marginRight: '3%',
          // backgroundColor: 'red',
        }}>
        <Image source={{uri: imgRef}} style={styles.img} />
      </Block>
      <Block style={styles.detailCon}>
        <Text
          style={{
            fontSize: iphone6s ? 16 : 18,
            textAlign: 'left',
          }}>
          {jobTitle}
        </Text>
        <Text
          h4
          color={colors.gray2}
          style={{
            fontSize: iphone6s ? AR?8:10 : 18,
            textAlign: 'left',
          }}>
          {TypeOFEmployee}
        </Text>
        <Text h4 color={colors.gray2}>
          {date}
        </Text>
      </Block>
      <Image
        source={icons.forward_arrow}
        style={{width: sizes.getWidth(1.5), resizeMode: 'contain'}}
      />
    </Button>
  );
};

export {JobCard};

const styles = StyleSheet.create({
  img: {
    resizeMode: 'contain',
    width: sizes.getWidth(18),
    height: sizes.getHeight(7),
  },
  detailCon: {
    // height: '70%',
    // borderWidth: 1,
    // backgroundColor: 'red',
    justifyContent: 'center',
    // padding={[4, 0, 0, 0]}
    flex: 0,
    height: sizes.getHeight(10),
    width: sizes.getWidth('65'),
  },
});
