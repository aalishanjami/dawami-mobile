import React, {useReducer, useState} from 'react';
import {Block, Text, Button, FilterOptions} from 'components';
import {sizes, colors} from 'styles/theme';
import {
  Image,
  TextInput,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import * as icons from 'assets/icons';
import {Picker} from '@react-native-community/picker';
import {FilterForjobs} from 'redux/actions';
import {useSelector} from 'react-redux';
import {LANG_AR} from 'redux/constants';
import {WORDS} from 'utils';

const JobFilterOptions = props => {
  const {closePopup, onSearch} = props;
  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;
  // -------------------------------------------
  const SELECT = AR ? WORDS.PleaseSelect_AR : WORDS.PleaseSelect;
  const MALE = AR ? WORDS.Male_AR : WORDS.Male;
  const FEMALE = AR ? WORDS.Female_AR : WORDS.Female;
  const PAK = AR ? WORDS.Pakistan_AR : WORDS.Pakistan;
  const QTR = AR ? WORDS.Qatar_AR : WORDS.Qarat;
  const LESS = AR ? WORDS.LessThanOne_AR : WORDS.LessThanOne;
  const ONE = AR ? WORDS.OneYear_AR : WORDS.OneYear;
  const TWO = AR ? WORDS.TwoYears_AR : WORDS.TwoYears;
  const THREE = AR ? WORDS.ThreeYears_AR : WORDS.ThreeYears;
  const FOUR = AR ? WORDS.FourYears_AR : WORDS.FourYears;
  const FIVE = AR ? WORDS.FiveYears_AR : WORDS.FiveYears;
  const SIX = AR ? WORDS.SixYears_AR : WORDS.SixYears;
  const SEVEN = AR ? WORDS.SevenYear_AR : WORDS.SevenYears;
  const EIGHT = AR ? WORDS.EightYears_AR : WORDS.EightYears;
  const NINE = AR ? WORDS.NineYears_AR : WORDS.NineYears;
  const TEN = AR ? WORDS.TenYears_AR : WORDS.TenYears;
  const MORE = AR ? WORDS.MoreThanTen_AR : WORDS.MoreThanTen;
  const FULL_TIME = AR ? WORDS.FullTime_AR : WORDS.FullTime;
  const PART_TIME = AR ? WORDS.PartTime_AR : WORDS.PartTime;
  const FREELANCE = AR ? WORDS.Freelancer_AR : WORDS.Freelancer;
  const CIVIL = AR ? WORDS.Civil_AR : WORDS.Civil;
  const OPERATION = AR ? WORDS.Operation_AR : WORDS.Operation;
  const OIL = AR ? WORDS.Oil_AR : WORDS.Oil;
  const MEDIA = AR ? WORDS.MEDIA_AR : WORDS.MEDIA;
  const BANKING = AR ? WORDS.Banking_AR : WORDS.Banking;
  const MEDICAL = AR ? WORDS.Medical_AR : WORDS.Medical;
  const IT = AR ? WORDS.ElecEng_AR : WORDS.ElecEng;
  const EE = AR ? WORDS.ElecEng_AR : WORDS.ElecEng;
  const SAFETY = AR ? WORDS.Safety_AR : WORDS.Safety;
  const CONSTRUCTION = AR ? WORDS.construct_AR : WORDS.construct;
  const RESET = AR ? WORDS.Reset_AR : WORDS.Reset;
  const SEARCH = AR ? WORDS.SEARCH_AR : WORDS.SEARCH;

  const category_title = AR ? WORDS.CATE_AR : WORDS.CATE;
  const Experience = AR ? WORDS.Experience_AR : WORDS.Experience;
  const jobType = AR ? WORDS.jobType_AR : WORDS.jobType;
  const Country = AR ? WORDS.Country_AR : WORDS.Country;
  const Gender = AR ? WORDS.Gender_AR : WORDS.Gender;

  // -------------------------------------------

  // ========STATE VARIANTS STARTED
  const CATE = 'category';
  const EXP = 'experience';
  const JOBTYPE = 'JOBTYPE';
  const COUNTRY = 'COUNTRY';
  const GENDER = 'GENDER';
  // ========STATE VARIANTS ENDED

  const dataToSend = {
    gender: [
      {name: SELECT, value: 0},
      {name: MALE, value: 1},
      {name: FEMALE, value: 2},
    ],
    exp: [
      {name: SELECT, value: 0},
      {name: LESS, value: 1},
      {name: ONE, value: 2},
      {name: TWO, value: 3},
      {name: THREE, value: 4},
      {name: FOUR, value: 5},
      {name: FIVE, value: 6},
      {name: SIX, value: 7},
      {name: SEVEN, value: 8},
      {name: EIGHT, value: 9},
      {name: NINE, value: 10},
      {name: TEN, value: 11},
      {name: MORE, value: 12},
    ],
    jobType: [
      {name: SELECT, value: 0},
      {name: PART_TIME, value: 1},
      {name: FULL_TIME, value: 2},
      {name: FREELANCE, value: 3},
    ],
    country: [
      {name: SELECT, value: 0},
      {name: PAK, value: 1},
      {name: QTR, value: 2},
    ],
    category: [
      {name: SELECT, value: 0},
      {name: CIVIL, value: 1},
      {name: OIL, value: 2},
      {name: MEDIA, value: 3},
      {name: BANKING, value: 4},
      {name: MEDICAL, value: 5},
      {name: IT, value: 6},
      {name: EE, value: 7},
      {name: SAFETY, value: 8},
      {name: CONSTRUCTION, value: 9},
    ],
  };

  const defaultData = {
    gender: {
      selection: {name: SELECT, value: 0},
      male: {name: MALE, value: 1},
      female: {name: FEMALE, value: 2},
    },
    country: {
      selection: {name: SELECT, value: 0},
      Pakistan: {name: PAK, value: 1},
      QATAR: {name: QTR, value: 1},
    },
    exp: {
      selection: {name: SELECT, value: 0},
      less: {name: LESS, value: 1},
      one: {name: ONE, value: 2},
      two: {name: TWO, value: 3},
      three: {name: THREE, value: 4},
      four: {name: FOUR, value: 5},
      five: {name: FIVE, value: 6},
      six: {name: SIX, value: 7},
      seven: {name: SEVEN, value: 8},
      eight: {name: EIGHT, value: 9},
      nine: {name: NINE, value: 10},
      ten: {name: TEN, value: 11},
      more: {name: MORE, value: 12},
    },
    jobType: {
      selection: {name: SELECT, value: 0},
      F_TIME: {name: FULL_TIME, value: 1},
      P_TIME: {name: PART_TIME, value: 2},
      F_LANCE: {name: FREELANCE, value: 3},
    },
    category: {
      selection: {name: SELECT, value: 0},
      civil: {name: CIVIL, value: 1},
      oil: {name: OIL, value: 2},
      media: {name: MEDIA, value: 3},
      bank: {name: BANKING, value: 4},
      medical: {name: MEDICAL, value: 5},
      it: {name: IT, value: 6},
      ee: {name: EE, value: 7},
      safety: {name: SAFETY, value: 8},
      construction: {name: CONSTRUCTION, value: 9},
    },
  };

  const initialState = {
    category: defaultData.category.selection.value,
    experience: defaultData.exp.selection.value,
    country: defaultData.country.selection.value,
    gender: defaultData.gender.selection.value,
    jobType: defaultData.jobType.selection.value,
    qualification: '',
  };

  const reducer = (state, action) => {
    switch (action.type) {
      case RESET:
        return {...initialState};
      case CATE:
        return {...state, category: action.payload};
      case GENDER:
        return {...state, gender: action.payload};
      case EXP:
        return {...state, experience: action.payload};
      case COUNTRY:
        return {...state, country: action.payload};
      case JOBTYPE:
        return {...state, jobType: action.payload};
      default:
        return state;
    }
  };
  const [state, localDispatch] = useReducer(reducer, initialState);
  const [showLoading, setShowLoading] = useState(false);
  const search = async () => {
    // closePopup()
    // console.log(state);
    setShowLoading(true);
    const result = await FilterForjobs(state, err => {
      console.log(err);
      setShowLoading(false);
    });
    if (result) {
      console.log(result);
      onSearch(result);
      setShowLoading(false);
      closePopup();
    }
  };

  console.log(state);
  return (
    <Block style={styles.mainCon}>
      {/* <Block style={{borderWidth: 1}}> */}
      {/* ---------------------------------------------------------------------------- */}
      {/* category */}
      <FilterOptions
        title={category_title}
        values={dataToSend.category}
        selectedValue={state.category}
        changedItem={e => localDispatch({type: CATE, payload: e})}
      />
      <FilterOptions
        title={Experience}
        values={dataToSend.exp}
        selectedValue={state.experience}
        changedItem={e => localDispatch({type: EXP, payload: e})}
      />
      <FilterOptions
        title={jobType}
        values={dataToSend.jobType}
        selectedValue={state.jobType}
        changedItem={e => localDispatch({type: JOBTYPE, payload: e})}
      />
      <FilterOptions
        title={Country}
        values={dataToSend.country}
        selectedValue={state.country}
        changedItem={e => localDispatch({type: COUNTRY, payload: e})}
      />
      <FilterOptions
        title={Gender}
        values={dataToSend.gender}
        selectedValue={state.gender}
        changedItem={e => localDispatch({type: GENDER, payload: e})}
      />
      {/* ---------------------------------------------------------------------------- */}
      {/* </Block> */}
      {!showLoading ? (
        <Block row middle center style={styles.btnCon}>
          <Button
            onPress={search}
            center
            middle
            style={{...styles.btn, width: '62%'}}>
            <Text color={colors.primary}>{SEARCH}</Text>
          </Button>
          <Button
            onPress={() => localDispatch({type: RESET})}
            center
            middle
            style={{
              ...styles.btn,
              borderWidth: 0.3,
              borderStyle: 'dashed',
              width: sizes.getWidth('25%'),
              backgroundColor: 'transparent',
            }}>
            {/* <Image
              source={icons.roundClose}
              style={{
                resizeMode: 'contain',
                width: sizes.getWidth(20),
                height: sizes.getHeight(13),
                tintColor: colors.red,
              }}
            /> */}
            <Text color={colors.red}>{RESET}</Text>
          </Button>
        </Block>
      ) : (
        <Block row middle center style={styles.btnCon}>
          <ActivityIndicator color={colors.customRed} size="large" />
        </Block>
      )}

      <Button
        onPress={closePopup}
        center
        middle
        style={{
          ...styles.btn,
          // borderWidth: 1,
          backgroundColor: colors.primary,
          borderStyle: 'dashed',
          width: sizes.screenSize * 0.04,
          height: sizes.screenSize * 0.04,
          // backgroundColor: 'transparent',
          position: 'absolute',
          right: 0,
          right: !AR ? -sizes.getWidth(4) : null,
          left: AR ? -sizes.getWidth(4) : null,
          top: -sizes.getHeight(4),
          zIndex: 50,
        }}>
        <Image
          source={icons.roundClose}
          style={{
            resizeMode: 'contain',
            // flex: 1,
            // borderWidth: 3,
            width: sizes.getWidth(15),
            height: sizes.getHeight(13),
            tintColor: colors.spaceGreen,
          }}
        />
      </Button>
    </Block>
  );
};

export default JobFilterOptions;

const styles = StyleSheet.create({
  mainCon: {
    flex: 0,
    height: sizes.getHeight(80),
    paddingTop: sizes.getHeight(3),
    // marginTop: sizes.getHeight(7),
    // backgroundColor: 'red',
  },
  textField: {
    flex: 0,
    height: sizes.getHeight(10),
    borderWidth: 1,
    justifyContent: 'center',
    marginVertical: sizes.getWidth(0.5),
  },
  btnCon: {
    // borderWidth: 1,
    flex: 0,
    height: sizes.getHeight(8),
  },
  btn: {
    // width: '40%',
    height: '100%',
    marginHorizontal: sizes.getWidth(2),
    backgroundColor: colors.customRed,
    borderRadius: sizes.getWidth(1),
  },
});
