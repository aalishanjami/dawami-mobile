import React, {useState, useEffect} from 'react';
import {Block, Text, Button, FilterPopup} from 'components';
import {
  Modal,
  Image,
  ScrollView,
  SafeAreaView,
  Platform,
} from 'react-native';
import {SmallIntroTop, Title, Description, ReviewScreen} from './Section';
import {sizes, colors} from 'styles/theme';
import * as images from 'assets/images';
import * as icons from 'assets/icons';
import {useSelector} from 'react-redux';
import {CompanyFilter} from 'screens/Compaines/Section/CompanyFilter';
import {TrainingImageBasePath} from 'redux/apiConstants';
import {NAV} from 'utils';
const CatalogueDetails = props => {
  const {showDetails, navigation, route} = props;
  const [visible, setVisible] = useState(true);
  // const {showDetails} = props;
  const {currentLN} = useSelector(state => state.userInfo);
  const [details, setDetails] = useState(null);
  useEffect(() => {
    setDetails(route.params?.result);
  }, ['']);

  console.log('========================');
  console.log(details);

  if (details) {
    return (
      <>
        <Block
          // color={colors.spaceGreen}
          flex={false}
          style={{paddingTop: sizes.getHeight(4)}}>
          <Modal
            onRequestClose={() => {
              {
                setVisible(false), navigation.navigate(NAV.TRAININGS);
              }
            }}
            // visible={!showDetails && true}
            visible={visible}
            animationType="slide">
            <Block
              // color={colors.spaceGreen}
              center
              middle
              margin={[sizes.getHeight(Platform.OS === 'ios' ? 5 : 0), 0, 0, 0]}
              // style={{borderWidth: 3}}
              // color={colors.customRed}
              flex={1}>
              <Image
                // source={images.landscape}
                source={{uri: TrainingImageBasePath + details.image}}
                style={{width: '100%', height: '100%'}}
              />
              {/* Close */}
              <Button
                onPress={() => {
                  setVisible(!visible);
                  navigation.navigate(NAV.TRAININGS);
                }}
                style={{
                  position: 'absolute',
                  top: sizes.getHeight(1),
                  left: sizes.getWidth(2),
                }}>
                <Image
                  source={icons.close_white}
                  style={{
                    resizeMode: 'contain',
                    width: sizes.getWidth(4),
                  }}
                />
              </Button>
              {/* HEART */}
              {/* <Button
              style={{
                position: 'absolute',
                top: sizes.getHeight(1),
                right: sizes.getWidth(5),
              }}>
              <Image
                source={icons.empty_heart}
                style={{
                  resizeMode: 'contain',
                  tintColor: 'white',
                  width: sizes.getWidth(4),
                }}
              />
            </Button> */}
            </Block>
            <Block
              margin={[10, 0]}
              padding={[0, sizes.getWidth(5)]}
              flex={2}
              //   style={{borderWidth: 1}}
            >
              <ScrollView
                showsVerticalScrollIndicator={false}
                // style={{flex:1, borderWidth:1}}
              >
                {/* <SmallIntroTop {...details} /> */}
                
                
                
                <Title {...details} />
                <Description {...details} />


                {/* <ReviewScreen {...details} /> */}
              </ScrollView>
              {/* <Block
            space={'between'}
            row
            middle
            padding={[0, sizes.getWidth(5)]}
            style={{position: 'absolute', bottom: sizes.getHeight(1)}}>
            <Button
              center
              middle
              style={{
                borderColor: '#DBDBDB',
                width: sizes.getWidth(43),
                borderWidth: 1,
              }}>
              <Text h2>Preview</Text>
            </Button>
            <Button
              center
              middle
              style={{
                marginHorizontal: 10,
                width: sizes.getWidth(43),
                backgroundColor: '#00e200',
              }}>
              <Text h2 color={colors.primary}>
                Add To Cart
              </Text>
            </Button>
          </Block> */}
            </Block>
          </Modal>
        </Block>
      </>
    );
  } else {
    return (
      <Block>
        <Text>Noting to show </Text>
      </Block>
    );
  }
};

export default CatalogueDetails;
