import SmallIntroTop from './SmallIntroTop'
import Title from './Title'
import Description from './Description'
import ReviewScreen from './ReviewScreen'

export {Title,SmallIntroTop,Description,ReviewScreen}

