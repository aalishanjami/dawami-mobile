import React from 'react';
import {Block, Text} from 'components';
import {sizes, colors} from 'styles/theme';
import {ScrollView, StyleSheet} from 'react-native';
import {useSelector} from 'react-redux';
import {LANG_AR, LANG_EN} from 'redux/constants';

const Description = details => {
  const {currentLN} = useSelector(state => state.userInfo);
  const EN = currentLN === LANG_EN;

  const des = 'Description';
  const des_ar = 'وصف';
  const headingOne = 'What You Will Learn ? ';
  const headingOne_AR = 'ماذا ستتعلم؟';
  const req = 'Requirements';
  const req_ar = 'المتطلبات';
  const inclu = 'What this course include ? ';
  const inclu_ar = 'ما تتضمنه هذه الدورة';

  const desc = EN ? des : des_ar;
  const learn = EN ? headingOne : headingOne_AR;
  const description = EN ? req : req_ar;
  const included = EN ? inclu : inclu_ar;

  return (
    <Block center margin={[sizes.getHeight(3), 0, sizes.getHeight(2), 0]}>
      <Block
        color="#f0f0f099"
        style={{borderRadius: sizes.getWidth(3)}}
        padding={[sizes.getWidth(2)]}
        margin={[sizes.getHeight(0), 0, 0, 0]}>
        <Text>{desc}</Text>
        <Text h4 style={{textAlign: 'left'}}>
          {EN ? details.description : details.description_ar}
        </Text>
      </Block>

      <Block
        color="#f0f0f099"
        padding={[sizes.getWidth(2)]}
        style={{borderRadius: sizes.getWidth(3), width: '100%'}}
        margin={[sizes.getHeight(2), 0, 0, 0]}>
        <Text style={styles.heading}>
          {EN ? details.title1 : details.title1_ar}
        </Text>
        <Text h4 style={{textAlign: 'left'}}>
          {EN ? details.desc1 : details.desc1_ar}
        </Text>
      </Block>

      <Block
        color="#f0f0f099"
        padding={[sizes.getWidth(2)]}
        style={{borderRadius: sizes.getWidth(3), width: '100%'}}
        margin={[sizes.getHeight(2), 0, 0, 0]}>
        <Text style={styles.heading}>
          {EN ? details.title2 : details.title2_ar}
        </Text>

        <Text h4 style={{textAlign: 'left'}}>
          {EN ? details.desc2 : details.desc2_ar}
        </Text>
      </Block>

      <Block
        color="#f0f0f099"
        padding={[sizes.getWidth(2)]}
        style={{borderRadius: sizes.getWidth(3), borderWidth: 0, width: '100%'}}
        margin={[sizes.getHeight(1), 0, 0, 0]}>
        <Text style={styles.heading}>
          {EN ? details.title3 : details.title3_ar}
        </Text>

        <Text h4 style={{textAlign: 'left'}}>
          {EN ? details.desc3 : details.desc3_ar}
        </Text>
      </Block>
    </Block>
  );
};

export default Description;

const styles = StyleSheet.create({
  heading: {},
});
