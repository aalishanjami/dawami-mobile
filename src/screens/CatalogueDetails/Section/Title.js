import React from 'react';
import {Text, Block} from 'components';
import {colors, sizes} from 'styles/theme';
import {useSelector} from 'react-redux';
import {LANG_EN, LANG_AR} from 'redux/constants';
import {StyleSheet, Dimensions, Platform} from 'react-native';
import {phone} from 'assets/icons';
import {EMAIL_AR} from 'utils/LanguageHandler';

const ios = Platform.OS === 'ios';
const iphone6s = ios && Dimensions.get('screen').width === 375;
const Title = details => {
  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;
  const title = AR ? details.title_ar : details.title;
  const provider = AR
    ? details.training_provider.name_ar
    : details.training_provider.name;
  const cate = AR
    ? details.training_category.name_ar
    : details.training_category.name;
  const email = details.training_provider.email;
  const contact = details.training_provider.contact;
  const language = details.training_provider.language;
  const level = details.level;
  const price = details.price;
  const startDate = details.start_date;
  const endDate = details.end_date;

  const provder = 'Provider';
  const provider_ar = 'مزود';
  const Category = 'Category';
  const Category_ar = 'الفئة';
  const Phone = 'Phone';
  const Phone_ar = 'هاتف';
  const Language = 'Language';
  const Language_ar = 'لغة';
  const Email = 'Email';
  const Email_ar = 'البريد الإلكتروني';
  const Level = 'Level';
  const Level_ar = 'مستوى';
  const Price = 'Price';
  const Price_ar = 'السعر';
  const StartDate = 'Start Date';
  const StartDate_ar = 'تاريخ البدء';
  const enddate = 'End Date';
  const enddate_ar = 'تاريخ الانتهاء';

  const providr = AR ? provider_ar : provder;
  const categ = AR ? Category_ar : Category;
  const ph = AR ? Phone_ar : Phone;
  const ln = AR ? Language_ar : Language;
  const em = AR ? Email_ar : Email;
  const lvl = AR ? Level_ar : Level;
  const pr = AR ? Price_ar : Price;
  const sd = AR ? StartDate_ar : StartDate;
  const ed = AR ? enddate_ar : enddate;

  // const cate = AR
  //   ? details.training_category.name_ar
  //   : details.training_category.name;

  console.log('========TITlE=========');
  console.log(details);

  return (
    <Block flex={1}>
      <Text h2>{title}</Text>

      <Block row style={styles.mainCont}>
        <Block>
          <Text style={{...styles.heading, textAlign: AR ? 'right': "left"}}>
            {providr}
          </Text>
          <Text style={styles.info}>{provider}</Text>
          <Text style={{...styles.heading, textAlign: AR ? 'right': "left"}}>
            {categ}
          </Text>
          <Text style={styles.info}>{cate}</Text>
          <Text style={{...styles.heading, textAlign: AR ? 'right': "left"}}>
            {ph}
          </Text>
          <Text style={styles.info}>{contact}</Text>
          <Text style={{...styles.heading, textAlign: AR ? 'right': "left"}}>
            {ln}
          </Text>
          <Text style={styles.info}>{language}</Text>
          <Text style={{...styles.heading, textAlign: AR ? 'right': "left"}}>
            {em}
          </Text>
          <Text
            style={{
              ...styles.info,
              fontSize: sizes.customFont(8),
              paddingVertical: sizes.getWidth(1.2),
            }}>
            {email}
          </Text>
        </Block>
        <Block padding={[0, 0, 0, sizes.getWidth(3)]}>
          <Text style={{...styles.heading, textAlign: AR ? 'right': "left"}}>
            {lvl}
          </Text>
          <Text style={styles.info}>{level}</Text>
          <Text style={{...styles.heading, textAlign: AR ? 'right': "left"}}>
            {pr}
          </Text>
          <Text style={styles.info}> {price}</Text>
          <Text style={{...styles.heading, textAlign: AR ? 'right': "left"}}>
            {sd}
          </Text>
          <Text style={styles.info}>{startDate}</Text>
          <Text style={{...styles.heading, textAlign: AR ? 'right': "left"}}>
            {ed}
          </Text>
          <Text style={styles.info}>{endDate}</Text>
        </Block>
      </Block>

      {/* <Block row>
        {data.map(el => {
          return (
            <Block
            margin={[sizes.getHeight(1),sizes.getWidth(2),0,0]}
            padding={[sizes.getHeight(2),sizes.getWidth(2)]}
              center
              middle
              flex={false}
              style={{
                // width: sizes.getWidth(24),
                // borderWidth: 1,
                backgroundColor:'#DBDBDB',
                height: sizes.getHeight(3),
              }}>
              <Text h4>{el.name}</Text>
            </Block>
          );
        })}
      </Block> */}
    </Block>
  );
};

export default Title;

const styles = StyleSheet.create({
  mainCont: {
    // borderWidth: 1,
    backgroundColor: '#f0f0f099',
    padding: sizes.getWidth(3),
    marginTop: sizes.getHeight(2),
    borderRadius: sizes.getWidth(2),
  },
  heading: {
    fontSize: sizes.customFont(iphone6s ? 7 : 11),
    // color:colors.
    fontWeight: 'bold',
  },
  info: {
    fontSize: sizes.customFont(10),
    marginLeft: sizes.getWidth(2),
    color: colors.primary,
    borderRadius: sizes.getWidth(1),
    padding: sizes.getWidth(0.5),
    backgroundColor: colors.spaceGreen,
    paddingLeft: sizes.getWidth(2),
  },
});
