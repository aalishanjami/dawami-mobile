import React, {useEffect, useState} from 'react';
import {Block, Text, Button, ActivitySign} from 'components';
import {Image, Platform, Dimensions} from 'react-native';
import {sizes, colors} from 'styles/theme';
import * as icons from 'assets/icons';
import StarRatings from './StarRating';
import {useSelector} from 'react-redux';
import {getTrainingDetails} from 'redux/actions';
import {TrainingImageBasePath} from 'redux/apiConstants';
import {LANG_AR} from 'redux/constants';
import {NAV} from 'utils';
const Product = props => {
  // const {name, price, image, rating, isLiked} = data;
  const {data, navigation, isLoading} = props;
  // console.log('=========DATA RECEIVED');
  // console.log(data);
  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;
  console.log('Current Language');
  console.log(currentLN);

  const goToDetails = async id => {
    // console.log(id)
    isLoading(true);
    const result = await getTrainingDetails(id, err => {
      console.log('Error');
      isLoading(false);
    });
    if (result) {

      console.log(result);

      navigation.navigate(NAV.CATELOG_DETAILS,{result} );
      isLoading(false);
    }
  };
  useEffect(() => {
    if (isLoading) {
      isLoading(false);
    }
  }, ['']);

  
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width === 375;

  return (
    <Button
      onPress={() => goToDetails(data.id)}
      flex={false}
      style={{
        borderRadius: sizes.withScreen(0.003),
        marginHorizontal: sizes.getWidth(2),
        marginTop: sizes.getHeight(3),
        // borderWidth: 1,
        // backgroundColor: colors.gray,
        // elevation: 0.2,
        borderStyle: 'dashed',
        // paddingBottom: sizes.getHeight(5),
        padding: sizes.getWidth(1),
        width: sizes.getWidth(43),
        height: sizes.getHeight(iphone6s ? 45 : 38),
      }}>
      <Block
        flex={false}
        center
        style={{borderWidth: 0, height: sizes.getHeight(21)}}>
        <Image
          source={{uri: TrainingImageBasePath + data.image}}
          style={{width: '100%', height: '100%'}}
        />
      </Block>
      <Block
        // color={'red'}
        flex={false}
        style={{
          marginTop: sizes.getHeight(1),
          paddingVertical: sizes.getHeight(1),
          // borderWidth: 1,
          overflow: 'hidden',
          height: sizes.getHeight(iphone6s ? 16 : 11),
        }}>
        <Text numberOfLines={2} ellipsizeMode="tail" h4 bold>
          {AR ? data.title_ar : data.title}
        </Text>
        <Text
          numberOfLines={iphone6s ? 3 : 5}
          ellipsizeMode="tail"
          style={{fontSize: sizes.customFont(iphone6s ? (AR ? 6 : 8) : AR?8:9)}}>
          {AR ? data.description_ar : data.description}
        </Text>
      </Block>
      <Block row flex={false} style={{borderWidth: 0}}>
        <Text bold h4 color={colors.gray2}>
          QAR :&nbsp;
        </Text>
        <Text style={{fontWeight: '900'}} h4>
          {data.price}
        </Text>
      </Block>
    </Button>
  );
};

export default Product;
