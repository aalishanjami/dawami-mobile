import React, {useEffect, useState} from 'react';
import {Block, Text, CustomHeader, ActivitySign, FilterPopup} from 'components';
import {Searchbar} from 'components';
import Product from './product';
import {
  FlatList,
  RefreshControl,
  Platform,
  SafeAreaView,
} from 'react-native';
import {catalogueData as data, WORDS, sortByProperty} from 'utils';
import {sizes, colors} from 'styles/theme';
import {TrainingFilter} from './TrainingFilter';
import {getAllTrainings} from 'redux/actions';
import {useSelector, useDispatch} from 'react-redux';
import {LANG_AR, TRAININGS} from 'redux/constants';
import {TRAINING_CATELOG} from 'utils/LanguageHandler';
const TrainingCatelogue = props => {
  const UPDATED_AT = 'updated_at';
  const {navigation, route} = props;
  const dispatch = useDispatch();
  const trainings = useSelector(state => state?.getLists?.Trainings);
  const [isLoading, setIsLoading] = useState(false);
  const [showPopup, setShowPopup] = useState(false);
  const {currentLN} = useSelector(state => state.userInfo);

  const AR = currentLN === LANG_AR;

  const foundNothing = 'Noting Found in Category.';
  const foundNothing_ar = 'مشيرا وجدت في الفئة.';
  const filter_des_ar = 'ابحث عن اسم التدريب';
  const filter_des = 'Search for training name';

  const nothingFound = AR ? foundNothing_ar : foundNothing;
  const tc = AR ? WORDS.TRAINING_CATE_AR : TRAINING_CATELOG;
  const filterDes = AR ? filter_des_ar : filter_des;

  // const fetchTrainings = async () => {
  //   const result = await getAllTrainings(err => {
  //     console.log('error in fetch ');
  //   });
  //   console.log('========');
  //   console.log(result);
  //   result && setResult(result);
  // };
  // =======================================================================
  // const [trainingList, setTrainingList] = useState(result);
  const [result, setResult] = useState(trainings);

  const searchHanlder = word => {
    var filteredList = [];
    trainings.forEach(element => {
      // console.log(element);
      var lowerTitle = element.title.toLowerCase();
      var lowerWord = word.toLowerCase();
      console.log(lowerWord);
      if (lowerTitle.startsWith(lowerWord)) {
        filteredList.push(element);
      }
      setResult(filteredList);
    });
  };
  const fillterPopup = () => {
    // setShowFilter(true);
  };

  const search = filteredCatelog => {
    setResult(filteredCatelog);
  };

  console.log('================================');
  console.log('i am in training catalog');
  console.log(result);
  console.log('================================');

  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = async () => {
    setIsLoading(true);
    const result = await getAllTrainings(err => {
      console.log('error in fetch ');
      setIsLoading(false);
    });
    if (result) {
      dispatch({
        type: TRAININGS,
        payload: result,
      });
      setIsLoading(false);
    }
    setIsLoading(false);
  };

  return (
    <>
      <SafeAreaView
        style={{flex: 0}}
        style={{backgroundColor: colors.spaceGreen}}
      />
      <SafeAreaView style={{flex: 1}}>
        <Block center>
          <CustomHeader navigation={navigation} />
          <Searchbar
            placeholder={filterDes}
            onChangeText={searchHanlder}
            onIconPress={() => setShowPopup(true)}
            title={tc}
          />
          <Block
            width={sizes.getWidth(95)}
            style={{backgroundColor: 'transparent'}}>
            {result && result.length ? (
              <FlatList
                refreshControl={
                  <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                  />
                }
                showsVerticalScrollIndicator={false}
                numColumns={2}
                data={result.sort(sortByProperty(UPDATED_AT))}
                renderItem={({item, index}) => (
                  <Product
                    key={index}
                    data={item}
                    navigation={navigation}
                    isLoading={value => setIsLoading(value)}
                  />
                )}
              />
            ) : (
              <Block center middle>
                <Text h2 color={colors.gray}>
                  {nothingFound}
                </Text>
              </Block>
            )}
          </Block>
          {isLoading && <ActivitySign />}
          {showPopup && (
            <FilterPopup>
              <TrainingFilter
                onSearch={e => search(e)}
                closePopup={() => setShowPopup(false)}
              />
            </FilterPopup>
          )}
        </Block>
      </SafeAreaView>
    </>
  );
};

export default TrainingCatelogue;
