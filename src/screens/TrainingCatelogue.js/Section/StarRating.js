import React, { useState } from 'react'
import { Rating, AirbnbRating } from 'react-native-ratings';
import { sizes, colors } from 'styles/theme';
import { Block, Text } from 'components';

const StarRating = () => {
    const [rating, setRating] = useState(0)
    const ratingCompleted = (rating) => {
        console.log("Rating is: " + rating)
        setRating(rating)
    }
    return (
        <Block center row flex={false}>
            <Text color={'#f1c40f'} style={{ fontSize: sizes.customFont(10), marginRight: sizes.getWidth(1) }}>{rating}.0</Text>
            <AirbnbRating
                count={5}
                reviews={[1, 2, 3, 4, 5]}
                showRating={false}
                defaultRating={rating}
                starStyle={{ margin: 1 }}
                size={10}
                reviewSize={20}
                onFinishRating={ratingCompleted}
                starContainerStyle={{ margin: 0 }}
            />
            <Text style={{ fontSize: sizes.customFont(8) }} color={colors.gray}>(1,234)</Text>

        </Block>
    )
}

export default StarRating
