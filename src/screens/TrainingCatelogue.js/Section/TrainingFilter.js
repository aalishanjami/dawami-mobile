import React, {useReducer, useState} from 'react';
import {Block, Text, Button, FilterOptions} from 'components';
import {sizes, colors} from 'styles/theme';
import {
  Image,
  TextInput,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
  Dimensions,
  Platform,
} from 'react-native';
import * as icons from 'assets/icons';
import {Picker} from '@react-native-community/picker';
import {FilterForTraining} from 'redux/actions';
import {useSelector} from 'react-redux';
import {LANG_AR, LANG_EN} from 'redux/constants';
import {WORDS} from 'utils';

const TrainingFilter = props => {
    const ios = Platform.OS === 'ios';
    const iphone6s = Dimensions.get('screen').width === 375;

  const {closePopup, onSearch} = props;
  const {currentLN} = useSelector(state => state.userInfo);
  const ARB = currentLN === LANG_AR;
  // -------------------------------------------
  const SELECT = ARB ? WORDS.PleaseSelect_AR : WORDS.PleaseSelect;
  const FINANCE = ARB ? WORDS.Finana_AR : WORDS.Finance;
  const IT = ARB ? WORDS.InfomationTec_AR : WORDS.InfomationTec;
  const DESIGN = ARB ? WORDS.Design_AR : WORDS.Design;
  const HR = ARB ? WORDS.HR_AR : WORDS.HR;
  const CR = ARB ? WORDS.ClassRoom_AR : WORDS.ClassRoom;
  const ONLINE = ARB ? WORDS.Online_AR : WORDS.Online;
  const AR = ARB ? WORDS.Arabic_AR : WORDS.Arabic;
  const EN = ARB ? WORDS.Engish_AR : WORDS.English;
  const BEGIN = ARB ? WORDS.Beginner_AR : WORDS.Beginner;
  const INTER = ARB ? WORDS.Intermediate_AR : WORDS.Intermediate;
  const EXPERT = ARB ? WORDS.Expert_AR : WORDS.Expert;
  const RESET = ARB ? WORDS.Reset_AR : WORDS.Reset;
  const SEARCH = ARB ? WORDS.SEARCH_AR : WORDS.SEARCH;
  const category_title = ARB ? WORDS.CATE_AR : WORDS.CATE;
  const Type = ARB ? WORDS.Type_AR : WORDS.Type;
  const Lang = ARB ? 'لغة' : WORDS.LANG;
  const level = ARB ? WORDS.Experience_AR : WORDS.Experience;
  // const RESET = ARB ? WORDS.Reset_AR : WORDS.Reset;
  // -------------------------------------------

  // ========STATE VARIANTS STARTED
  const CATE = ARB ? WORDS.CATE_AR : WORDS.CATE;
  const TYPE = ARB ? WORDS.Type_AR : WORDS.Type;
  const LANG = ARB ? WORDS.LANG_AR : WORDS.LANG;
  const LEVEL = ARB ? WORDS.LEVEL_AR : WORDS.LEVEL;
  // ========STATE VARIANTS ENDED

  const dataToSend = {
    category: [
      {name: SELECT, value: 0},
      {name: FINANCE, value: 1},
      {name: IT, value: 2},
      {name: DESIGN, value: 3},
      {name: HR, value: 4},
    ],
    type: [
      {name: SELECT, value: 0},
      {name: CR, value: 1},
      {name: ONLINE, value: 2},
    ],
    lang: [
      {name: SELECT, value: 0},
      {name: EN, value: 1},
      {name: AR, value: 2},
    ],
    level: [
      {name: SELECT, value: 0},
      {name: BEGIN, value: 1},
      {name: INTER, value: 2},
      {name: EXPERT, value: 3},
    ],
  };

  const defaultData = {
    category: {
      selection: {name: SELECT, value: 0},
      fin: {name: FINANCE, value: 1},
      it: {name: IT, value: 2},
      design: {name: DESIGN, value: 3},
      hr: {name: HR, value: 4},
    },
    type: {
      selection: {name: SELECT, value: 0},
      classroom: {name: CR, value: 1},
      online: {name: ONLINE, value: 2},
    },
    lang: {
      selection: {name: SELECT, value: 0},
      en: {name: EN, value: 1},
      ar: {name: AR, value: 2},
    },
    level: {
      selection: {name: SELECT, value: 0},
      beginner: {name: BEGIN, value: 1},
      inter: {name: INTER, value: 2},
      expert: {name: EXPERT, value: 3},
    },
  };

  const initialState = {
    category: defaultData.category.selection.value,
    type: defaultData.type.selection.value,
    lang: defaultData.lang.selection.value,
    level: defaultData.level.selection.value,
  };

  const reducer = (state, action) => {
    switch (action.type) {
      case RESET:
        return {...initialState};
      case CATE:
        return {...state, category: action.payload};
      case TYPE:
        return {...state, type: action.payload};
      case LANG:
        return {...state, lang: action.payload};
      case LEVEL:
        return {...state, level: action.payload};
      default:
        return state;
    }
  };
  const [state, localDispatch] = useReducer(reducer, initialState);

  const [showLoading, setShowLoading] = useState(false);
  const search = async () => {
    // closePopup()
    // console.log(state);
    setShowLoading(true);
    const result = await FilterForTraining(state, err => {
      console.log(err);
      setShowLoading(false);
    });
    if (result) {
      console.log(result);
      onSearch(result);
      setShowLoading(false);
      closePopup();
    }
  };


  return (
    <Block style={styles.mainCon}>
      <Block>
        {/* ---------------------------------------------------------------------------- */}
        {/* category */}
        <FilterOptions
          title={category_title}
          values={dataToSend.category}
          selectedValue={state.category}
          changedItem={e => localDispatch({type: CATE, payload: e})}
        />
        <FilterOptions
          title={TYPE}
          values={dataToSend.type}
          selectedValue={state.type}
          changedItem={e => localDispatch({type: TYPE, payload: e})}
        />
        <FilterOptions
          title={Lang}
          values={dataToSend.lang}
          selectedValue={state.lang}
          changedItem={e => localDispatch({type: LANG, payload: e})}
        />
        <FilterOptions
          title={level}
          values={dataToSend.level}
          selectedValue={state.level}
          changedItem={e => localDispatch({type: LEVEL, payload: e})}
        />

        {/* ---------------------------------------------------------------------------- */}
      </Block>
      {!showLoading ? (
        <Block row middle center style={styles.btnCon}>
          <Button
            onPress={search}
            center
            middle
            style={{...styles.btn, width: '60%'}}>
            <Text
              style={{
                fontSize: iphone6s ? sizes.customFont(10) : sizes.h4,
              }}
              color={colors.primary}>
              {SEARCH}
            </Text>
          </Button>

          <Button
            onPress={() => localDispatch({type: RESET})}
            center
            middle
            style={{
              ...styles.btn,
              borderWidth: 0.3,
              borderStyle: 'dashed',
              width: sizes.getWidth('25%'),
              backgroundColor: 'transparent',
            }}>
            {/* <Image
              source={icons.roundClose}
              style={{
                resizeMode: 'contain',
                width: sizes.getWidth(20),
                height: sizes.getHeight(13),
                tintColor: colors.red,
              }}
            /> */}
            <Text
              style={{
                fontSize: iphone6s ? sizes.customFont(10) : sizes.h4,
              }}
              color={colors.red}>
              {RESET}
            </Text>
          </Button>
        </Block>
      ) : (
        <Block row middle center style={styles.btnCon}>
          <ActivityIndicator color={colors.customRed} size="large" />
        </Block>
      )}

      <Button
        onPress={closePopup}
        center
        middle
        style={{
          ...styles.btn,
          // borderWidth: 1,
          backgroundColor: colors.primary,
          borderStyle: 'dashed',
          width: sizes.screenSize * 0.04,
          height: sizes.screenSize * 0.04,
          // backgroundColor: 'transparent',
          position: 'absolute',
          right: 0,
          right: !ARB ? -sizes.getWidth(4) : null,
          left: ARB ? -sizes.getWidth(4) : null,
          top: -sizes.getHeight(5),
          zIndex: 50,
        }}>
        <Image
          source={icons.roundClose}
          style={{
            resizeMode: 'contain',
            // flex: 1,
            width: sizes.getWidth(15),
            height: sizes.getHeight(13),
            tintColor: colors.spaceGreen,
          }}
        />
      </Button>
    </Block>
  );
};

export {TrainingFilter};

const styles = StyleSheet.create({
  mainCon: {
    flex: 0,
    height: sizes.getHeight(65),
  },
  textField: {
    flex: 0,
    height: sizes.getHeight(10),
    borderWidth: 1,
    justifyContent: 'center',
    marginVertical: sizes.getWidth(0.5),
  },
  btnCon: {
    // borderWidth: 1,
    flex: 0,
    height: sizes.getHeight(8),
  },
  btn: {
    // width: '40%',
    height: '100%',
    marginHorizontal: sizes.getWidth(2),
    backgroundColor: colors.customRed,
    borderRadius: sizes.getWidth(1),
  },
});
