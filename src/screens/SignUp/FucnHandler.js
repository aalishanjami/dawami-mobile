import {constants} from './constants'
import { checkUserNameAvailability } from 'redux/actions';
import { CustomAlert } from 'components';
import { AlertTiming } from 'redux/constants';
 

const {USERNAME} = constants

 export const onFocusOut = async props => {

    const {
      name,
      state,
      messageRef,
    //   funcs
      setIsChecking,
      setCurrentStatusIcon,
    } = props;


    if (name === USERNAME) {
      if (state.fields.username.value !== "") {
        setIsChecking(true);
        const result = await checkUserNameAvailability(
          state.fields.username.value,
          err =>
            messageRef.current?.show(
              <CustomAlert textColor={colors.primary} text={connectError} />,
              AlertTiming,
            ),
          setIsChecking(false),
          setCurrentStatusIcon(icons.warning),
        );
        result &&
          (result.success
            ? (console.log('i am in ok'),
              setIsChecking(false),
              setCurrentStatusIcon(icons.tic),
              setNameisAvailable(true),
              setInputStatusBar(result.message))
            : (console.log('i am in not'),
              setIsChecking(false),
              setCurrentStatusIcon(icons.warning),
              setNameisAvailable(true),
              setInputStatusBar(result.message),
              toggleLoginButton()));
      } else {
        // if user type nothing
        // setIcon(null);
      }
    } else if (name === 'email') {
      if (state.fields.email.value !== '' && state.fields.email.isValid) {
        setIsCheckingEmail(true);
        const result = await checkEmailAvailability(
          state.fields.email.value,
          err =>
            messageRef.current?.show(
              <CustomAlert
                textColor={colors.primary}
                text={connectError}
              />,
              alertTiming,
            ),
          setIsCheckingEmail(false),
          toggleLoginButton(),
        );
        result &&
          (result.success
            ? (console.log('i am in ok email'),
              setIsCheckingEmail(false),
              setCurrentStatusIconEmail(icons.tic),
              setEmailIsAvailable(true),
              setInputStatusBarEmail(result.message))
            : (console.log('i am in not email '),
              setIsCheckingEmail(false),
              setCurrentStatusIconEmail(icons.warning),
              setEmailIsAvailable(true),
              setInputStatusBarEmail(result.message),
              toggleLoginButton()));
      } else {
        messageRef.current?.show(
          <CustomAlert text="Please wait.." />,
          alertTiming,
        );
      }
    }
 }