import React, {useState, useRef, useEffect, useCallback} from 'react';
import {
  Block,
  Text,
  Button,
  SquareButton,
  TextField,
  ActivitySign,
  CustomModal,
  PopupVerification,
  CustomAlert,
} from 'components';
import {
  Image,
  TextInput,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
  Keyboard,
  Alert,
  View,
  SafeAreaView,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
// import {CheckBox} from 'react-native';
import * as icons from 'assets/icons';
import {colors, sizes} from 'styles/theme';
import {
  SignupFormState,
  validateField,
  validateFields,
  WORDS,
  // fbHandler,
  // googleSigning,
} from 'utils';
import Toast from 'react-native-easy-toast';
import {GoogleSignin} from '@react-native-community/google-signin';
import {registerUser, checkUserNameAvailability} from 'redux/actions';
import {useDispatch, useSelector} from 'react-redux';
import {ADD_USER_INFO, REMOVE_USER_INFO, LANG_AR} from 'redux/constants';
import {
  checkEmailAvailability,
  RemoveRegisteredAccount,
  removeUserInfo,
  VerifyUser,
} from 'redux/actions/auth';
import {internetError} from 'utils/LanguageHandler';
import {onFocusOut,constants} from './FucnHandler'


export const Signup = ({navigation}) => {
  const alertTiming = 3000;
  const dispatch = useDispatch();
  const messageRef = useRef();
  const [state, setState] = useState({
    loginBtnDisable: false,
    acceptTerm: false,
    fields: {...SignupFormState},
  });

  // ==========SIGN UP=========
  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;
  const typeNAME = 'Name';
  const typeNAME_AR = ' اسمًا';
  const chooseUserName = 'Username';
  const chooseUserName_AR = 'اسم المستخدم';
  const typeEmail = 'Email Address';
  const typeEmail_AR = 'عنوان البريد الالكترونى';
  const confirmPass = 'Confirm Password';
  const confirmPass_AR = 'تأكيد كلمة المرور';
  const SignUp = 'Sign Up';
  const SignUp_AR = 'سجل';
  const acceptTerm = 'Accept Term & Conditions';
  const acceptTerm_AR = 'قبول الشروط والأحكام';
  const haveAccount = 'Already Have An Account ? Log In';
  const haveAccount_AR = 'هل لديك حساب ؟ تسجيل الدخول';
  const passwordError = "Password doesn't match.";
  const passwordError_ar = 'كلمة المرور غير متطابقة. حاول مجددا';
  const passwd = 'Password';
  const passwd_ar = 'كلمه السر';

  const na = AR ? typeNAME_AR : typeNAME;
  const cu = AR ? chooseUserName_AR : chooseUserName;
  const te = AR ? typeEmail_AR : typeEmail;
  const cp = AR ? confirmPass_AR : confirmPass;
  const su = AR ? SignUp_AR : SignUp;
  const at = AR ? acceptTerm_AR : acceptTerm_AR;
  const ha = AR ? haveAccount_AR : haveAccount;
  const connectError = AR ? WORDS.internetError_ar : internetError;
  const acceptTerms = AR ? acceptTerm_AR : acceptTerm;
  const psswd = AR ? passwd_ar : passwd;
  const passwordErr = AR ? passwordError_ar : passwordError;
  // ==========================

  const handleInput = ({name, text}) => {
    const newField = validateField(state.fields[name], text);
    setState({fields: {...state.fields, [name]: newField}});
  };

  // =========================================================================================================
  // =================== on Error Keyboard Position
  const setAlertToTop = sizes.getDimensions.height;
  const setAlertToBottom = sizes.getDimensions.height / 10;
  const [alertBar, setAlertBar] = useState(setAlertToBottom);
  // ================USERNAME INPUT HANDLER ======================
  const [nameIsAvailable, setNameisAvailable] = useState(false);
  const [isChecking, setIsChecking] = useState(false);
  const [currentStatusIcon, setCurrentStatusIcon] = useState(null);
  const [inputStatusBar, setInputStatusBar] = useState(null);
  // ========== EMAIL INPUT HANDLER =======================
  const [emailIsAvailable, setEmailIsAvailable] = useState(false);
  const [isCheckingEmail, setIsCheckingEmail] = useState(false);
  const [currentStatusIconEmail, setCurrentStatusIconEmail] = useState(null);
  const [inputStatusBarEmail, setInputStatusBarEmail] = useState(null);
  // ==========================================================================================================
  const [isLoading, setIsLoading] = useState(false);
  function toggleLoginButton() {
    setState(state => ({...state, loginBtnDisable: !state.loginBtnDisable}));
  }
  // const {userInfo} = useSelector(state => state.userInfo);
  const userInfo = useSelector(state => state.auth.userBasicProfile);
  // console.log(userInfo);
  const handleSubmit = async () => {
    // =================
    const result = validateFields(state.fields);
    const userDetails = {
      name: state.fields.name.value,
      email: state.fields.email.value,
      password: state.fields.password.value,
      confirmPassword: state.fields.confirmPassword.value,
      userName: state.fields.username.value,
    };
    if (result.validity) {
      setIsLoading(true);
      toggleLoginButton();
      if (state.fields.confirmPassword.value === state.fields.password.value) {
        if (state.acceptTerm) {
          // in case username or email availablity is under validation from api and user hits submit
          if (
            !isChecking &&
            currentStatusIconEmail === icons.tic &&
            currentStatusIcon === icons.tic
          ) {
            const reg_result = await registerUser(userDetails, err => {
              err && err.error
                ? (messageRef.current?.show(
                    <Text h4 color={colors.primary}>
                      {err.error}
                    </Text>,
                  ),
                  // toggleLoginButton(),
                  setIsLoading(false))
                : (messageRef.current?.show(
                    <CustomAlert
                      textColor={colors.primary}
                      text={connectError}
                    />,
                    alertTiming,
                  ),
                  setIsLoading(false),
                  toggleLoginButton());
            });
            reg_result &&
              (dispatch({type: ADD_USER_INFO, payload: reg_result.user}),
              setIsLoading(true),
              toggleLoginButton(prev => !prev),
              setModalVisibility(true));
            // in case username or email availablity is under validation from api and user hits submit else block
          } else {
            messageRef.current?.show(
              <CustomAlert textColor={colors.primary} text="Please Wait.." />,
              1500,
            );
            toggleLoginButton();
          }
          // =========
        } else {
          messageRef.current?.show(
            <CustomAlert textColor={colors.primary} text={acceptTerms} />,
            alertTiming,
          ),
            toggleLoginButton();
          setIsLoading(false);
        }
      } else {
        messageRef.current?.show(
          <CustomAlert textColor={colors.primary} text={passwordErr} />,
          alertTiming,
        ),
          setIsLoading(false);
        toggleLoginButton();
        // }
      }
    } else {
      // if password doesn't match
      // if(state.fields.confirmPassword.value.length !== state.fields.password.value.length)
      if (userDetails.confirmPassword.length !== userDetails.password.length) {
        messageRef.current?.show(
          <CustomAlert textColor={colors.primary} text={passwordErr} />,
          alertTiming,
        ),
          setIsLoading(false);
      }
      toggleLoginButton();
      setIsLoading(false);
    }
    //finally
    setState(state => ({...state, fields: result.newFields}));
    setIsLoading(false);
  };

  useEffect(() => {
    _configureGoogleSignIn();
  });

  function _configureGoogleSignIn() {
    GoogleSignin.configure({
      offlineAccess: false,
    });
  }
  // ==============================================================
  const onFocusOut = async name => {
    console.log('i am launched');
    if (name === 'username') {
      if (state.fields.username.value !== '') {
        setIsChecking(true);
        const result = await checkUserNameAvailability(
          state.fields.username.value,
          err =>
            messageRef.current?.show(
              <CustomAlert textColor={colors.primary} text={connectError} />,
              alertTiming,
            ),
          setIsChecking(false),
          setCurrentStatusIcon(icons.warning),
        );
        // console.log('result')
        // console.log(result)
        result &&
          (result.success
            ? (console.log('i am in ok'),
              setIsChecking(false),
              setCurrentStatusIcon(icons.tic),
              setNameisAvailable(true),
              setInputStatusBar(result.message))
            : (console.log('i am in not'),
              setIsChecking(false),
              setCurrentStatusIcon(icons.warning),
              setNameisAvailable(true),
              setInputStatusBar(result.message),
              toggleLoginButton()));
      } else {
        // if user type nothing
        // setIcon(null);
      }
    } else if (name === 'email') {
      if (state.fields.email.value !== '' && state.fields.email.isValid) {
        setIsCheckingEmail(true);
        const result = await checkEmailAvailability(
          state.fields.email.value,
          err =>
            messageRef.current?.show(
              <CustomAlert
                textColor={colors.primary}
                text={connectError}
              />,
              alertTiming,
            ),
          setIsCheckingEmail(false),
          toggleLoginButton(),
        );
        result &&
          (result.success
            ? (console.log('i am in ok email'),
              setIsCheckingEmail(false),
              setCurrentStatusIconEmail(icons.tic),
              setEmailIsAvailable(true),
              setInputStatusBarEmail(result.message))
            : (console.log('i am in not email '),
              setIsCheckingEmail(false),
              setCurrentStatusIconEmail(icons.warning),
              setEmailIsAvailable(true),
              setInputStatusBarEmail(result.message),
              toggleLoginButton()));
      } else {
        messageRef.current?.show(
          <CustomAlert
            textColor={colors.primary}
            text="Please wait.."
          />,
          alertTiming,
        );
      }
    }
  };

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', () => {
      setAlertBar(setAlertToTop);
    });
    return () => {
      if (Keyboard.dismiss) {
        Keyboard.addListener('keyboardDidShow', () => {
          setAlertBar(setAlertToTop);
        });
      } else {
        Keyboard.addListener('keyboardDidHide'),
          () => {
            setAlertBar(setAlertToBottom);
          };
      }
    };
  });
  // pop up
  const [modalVisibility, setModalVisibility] = useState(false);
  // const [modalVisibility, setModalVisibility] = useState(true);
  const [codeStatus, setCodeStatus] = useState(false);
  const [modalIndicator, setModalIndicator] = useState(false);
  const [modalError, setModalError] = useState('');
  const nameRef = useRef();
  const usernameRef = useRef();
  const emailRef = useRef();
  const passRef = useRef();
  const conPassRef = useRef();
  const isValidCode = async status => {
    if (status) {
      // alert(status);
      console.log(userInfo);
      const result = await VerifyUser(userInfo, err => {
        console.log(err);
        alert(err);
      });
      result.success &&
        (setModalVisibility(false),
        nameRef.current?.clear(),
        usernameRef.current?.clear(),
        emailRef.current?.clear(),
        passRef.current?.clear(),
        conPassRef.current?.clear(),
        setState(state => ({
          state,
          fields: SignupFormState,
          acceptTerm: false,
          loginBtnDisable: false,
        })),
        setCurrentStatusIcon(null),
        setInputStatusBar(null),
        setInputStatusBarEmail(null),
        setCurrentStatusIconEmail(null),
        navigation.navigate('Login', {
          registerStatus: result.success,
          message: result.message,
        }));
    } else {
      setCodeStatus(true);
    }
  };
  const onBackPress = () => {
    // setModalVisibility(false)
    console.log(userInfo.email);
    Alert.alert('Final Warning', 'All Submitted record will be removed.', [
      // stay here
      {text: 'No, I wanted to continue', onPress: () => console.log(' Cancel')},
      // call api to remove
      {
        text: 'Yes, Remove',
        onPress: async () => {
          return (
            setModalIndicator(true),
            await RemoveRegisteredAccount(
              userInfo.email,
              err => (
                setModalError('Please Check your Internet'),
                console.log('error removing'),
                console.log(err),
                setModalIndicator(false)
              ),
            )
              .then(
                dispatch({
                  type: REMOVE_USER_INFO,
                  payload: {userInfo: null},
                }),
                setModalIndicator(false),
                setModalVisibility(false),
                nameRef.current?.clear(),
                usernameRef.current?.clear(),
                emailRef.current?.clear(),
                passRef.current?.clear(),
                conPassRef.current?.clear(),
                setState(state => ({
                  state,
                  fields: SignupFormState,
                  acceptTerm: false,
                  loginBtnDisable: false,
                })),
                setCurrentStatusIcon(null),
                setInputStatusBar(null),
                setInputStatusBarEmail(null),
                setCurrentStatusIconEmail(null),
                console.log(state),
              )
              .catch(e => {
                console.log('CATCH STATE IN IF CANCEL');
                alert(e);
                setModalIndicator(false);
              })
          );
        },
      },
    ]);
  };

  return (
    <>
      <SafeAreaView style={{flex: 0}} />
      <SafeAreaView style={{flex: 1}}>
        <Block>
          {/* Image */}
          <Block flex={false} height={sizes.getHeight(25)} center middle>
            <Image
              source={icons.red_logo}
              style={{resizeMode: 'contain', flex: 0.6}}
            />
          </Block>

          <ScrollView>
            {/* Inputs */}
            <Block padding={[0, sizes.getWidth(7), 0, sizes.getWidth(7)]}>
              <TextField
                inputRef={nameRef}
                onChangeText={handleInput}
                name={'name'}
                inputStyling={styles.inputStyle}
                placeholder={na}
              />

              {!state.fields.password.isValid && (
                <Text h4 color={colors.red}>
                  {state.fields.name.errorMessage}
                </Text>
              )}
              {/* =========================================USERNAME============================================== */}
              <Block center flex={false} row>
                <TextField
                  inputRef={usernameRef}
                  onFocus={() => {
                    setCurrentStatusIcon(null), setInputStatusBar(null);
                  }}
                  onBlur={() => onFocusOut('username')}
                  onChangeText={handleInput}
                  name={'username'}
                  inputStyling={styles.inputStyle}
                  placeholder={cu}
                />
                {nameIsAvailable && (
                  <Image
                    source={currentStatusIcon}
                    style={{
                      marginHorizontal: sizes.border,
                      position: 'absolute',
                      right: 0,
                      resizeMode: 'contain',
                      width: sizes.getWidth(4),
                      tintColor:
                        (currentStatusIcon === icons.warning && colors.red) ||
                        (currentStatusIcon === icons.tic && 'green') ||
                        'black',
                    }}
                  />
                )}
                {isChecking && (
                  <ActivityIndicator
                    size="small"
                    color={colors.customRed}
                    style={{position: 'absolute', right: sizes.getWidth(2)}}
                  />
                )}
              </Block>
              {inputStatusBar !== null && (
                <Text
                  h4
                  color={
                    currentStatusIcon === icons.warning ? colors.red : 'green'
                  }>
                  {inputStatusBar}
                </Text>
              )}
              {!state.fields.password.isValid && (
                <Text h4 color={colors.red}>
                  {state.fields.username.errorMessage}
                </Text>
              )}
              {/* =========================================Email============================================== */}
              <Block center flex={false} row>
                <TextField
                  inputRef={emailRef}
                  onFocus={() => {
                    setCurrentStatusIconEmail(null),
                      setInputStatusBarEmail(null);
                  }}
                  onBlur={() => onFocusOut('email')}
                  onChangeText={handleInput}
                  name={'email'}
                  inputStyling={styles.inputStyle}
                  autoCompleteType={'email'}
                  placeholder={te}
                  keyboardType="email-address"
                />
                {emailIsAvailable && (
                  <Image
                    source={currentStatusIconEmail}
                    style={{
                      marginHorizontal: sizes.border,
                      position: 'absolute',
                      right: 0,
                      resizeMode: 'contain',
                      width: sizes.getWidth(4),
                      tintColor:
                        (currentStatusIconEmail === icons.warning &&
                          colors.customRed) ||
                        (currentStatusIconEmail === icons.tic && 'green') ||
                        'black',
                    }}
                  />
                )}
                {isCheckingEmail && (
                  <ActivityIndicator
                    size="small"
                    color={colors.customRed}
                    style={{position: 'absolute', right: sizes.getWidth(2)}}
                  />
                )}
              </Block>
              {inputStatusBarEmail !== null && (
                <Text
                  h4
                  style={{textTransform: 'capitalize'}}
                  color={
                    currentStatusIconEmail === icons.warning
                      ? colors.red
                      : 'green'
                  }>
                  {inputStatusBarEmail}
                </Text>
              )}
              {!state.fields.email.isValid && (
                <Text h4 color={colors.red}>
                  {state.fields.email.errorMessage}
                </Text>
              )}

              {/* =========================================Password============================================== */}
              <TextField
                inputRef={passRef}
                secureTextEntry={true}
                onChangeText={handleInput}
                name={'password'}
                inputStyling={styles.inputStyle}
                autoCompleteType={'password'}
                placeholder={psswd}
              />
              {!state.fields.password.isValid && (
                <Text h4 color={colors.red}>
                  {state.fields.password.errorMessage}
                </Text>
              )}
              <TextField
                inputRef={conPassRef}
                secureTextEntry={true}
                onChangeText={handleInput}
                name={'confirmPassword'}
                inputStyling={styles.inputStyle}
                placeholder={cp}
              />
              {!state.fields.password.isValid && (
                <Text h4 color={colors.red}>
                  {state.fields.password.errorMessage}
                </Text>
              )}
            </Block>

            {/* remember Me */}
            <Block
              padding={[0, sizes.getWidth(6), 0, sizes.getWidth(6)]}
              center
              middle
              flex={false}
              margin={[sizes.getHeight(3),0,0,0]}
              >
              <SquareButton
                disabled={state.loginBtnDisable}
                onPress={handleSubmit}
                bgColor={
                  state.loginBtnDisable ? colors.gray : colors.spaceGreen
                }
                textColor={colors.primary}>
                {su}
              </SquareButton>
            </Block>

            <Block
              margin={[sizes.getHeight(3), 0, 0, 0]}
              padding={[0, sizes.getWidth(5), 0, sizes.getWidth(5)]}
              middle
              style={{
                // backgroundColor: 'red',
                // borderWidth: 1,
                height: sizes.getHeight(7),
              }}>
              <Block
                row
                center
                style={{
                  // borderWidth: 1,
                  width: sizes.getWidth(65),
                  // justifyContent: 'space-between',
                }}>
                <Block>
                  <CheckBox
                    lineWidth={1}
                    boxType={'square'}
                    tintColor={colors.gray2}
                    onCheckColor={colors.primary}
                    onFillColor={colors.customRed}
                    onTintColor={'transparent'}
                    animationDuration={0.4}
                    // disabled={false}
                    onAnimationType={'bounce'}
                    offAnimationType={'stroke'}
                    value={state.acceptTerm}
                    onChange={() => {
                      setState(state => ({
                        ...state,
                        acceptTerm: !state.acceptTerm,
                      }));
                    }}
                  />
                </Block>
                <Block flex={5}>
                  <Text style color={colors.gray2}>
                    {acceptTerms}
                  </Text>
                </Block>
              </Block>
            </Block>

            {/* Social Login */}
            <Block
              margin={[sizes.getHeight(3), 0, 0, 0]}
              padding={[0, sizes.getWidth(7), 0, sizes.getWidth(7)]}
              flex={4}>
              <Block center>
                <SquareButton
                  onPress={() => navigation.navigate('Login')}
                  noBorder
                  width={'90%'}>
                  <Text h2 color={colors.gray}>
                    {ha}
                  </Text>
                </SquareButton>
              </Block>
              {/* ============================ */}
            </Block>
          </ScrollView>
          <Toast
            ref={messageRef}
            style={{
              backgroundColor: colors.customRed,
              width: sizes.getWidth(100),
              borderRadius: 1,
            }}
            // positionValue={sizes.getDimensions.height /10}
            positionValue={alertBar}
            fadeInDuration={100}
            fadeOutDuration={300}
            opacity={1}
          />

          <CustomModal closeModal={onBackPress} isVisible={modalVisibility}>
            <PopupVerification
              errorCode={modalError}
              indicatorStatus={modalIndicator}
              codeStatus={codeStatus}
              isValid={isValidCode}
              compareWithCode={`${userInfo?.confirmation_code}`}
            />
          </CustomModal>

          {isLoading && <ActivitySign />}

          {/* <View style={{justifyContent: 'center', flex: 1, alignItems: 'center'}}>
      <Text> check box</Text>
      <View
        style={{
          borderWidth: 1,
          height: sizes.getHeight(5),
          justifyContent: 'center',
          width: sizes.getWidth(10),
          alignItems: 'center',
          alignContent:'center'
        }}>
        <CheckBox
          // lineWidth={2}
          // hideBox={false}
          // boxType={'square'}
          // tintColor={colors.gray2}
          // onCheckColor={colors.customRed}
          // onFillColor={colors.customRed}
          // onTintColor={colors.brown}
          // animationDuration={0.5}
          // disabled={false}
          // onAnimationType={'bounce'}
          // offAnimationType={'stroke'}
          // onChangeValue={e => alert('value')}
          //     ...state,
          // value={state.acceptTerm}
          // onChange={() => {
          //   setState(state => ({
          //     acceptTerm: !state.acceptTerm,
          //   }));
          // }}
        />
      </View>
    </View> */}
        </Block>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  inputStyle: {
    borderBottomColor: colors.gray,
    borderBottomWidth: 0.7,
    width: '100%',
    paddingRight: sizes.getWidth(10),
    // borderWidth:2,
    textAlign: 'left',
    height: sizes.getHeight(6),
  },
});
