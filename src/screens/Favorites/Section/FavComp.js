import React, {useEffect} from 'react';
import {Block, Text, Button, CustomHeader} from 'components';
import {sizes, colors} from 'styles/theme';
import {Image, FlatList, StyleSheet} from 'react-native';
import * as icons from 'assets/icons';
import {useSelector, useDispatch} from 'react-redux';
import {companyLogo} from 'redux/apiConstants';
import {LANG_EN, LANG_AR, SAVE_JOB_STATUS, CURRENT_COMP} from 'redux/constants';

const FavComp = props => {
  const {navigation} = props;
  const dispatch = useDispatch();
  const appliedData = useSelector(state => state?.getLists?.favComps);
  const {currentLN} = useSelector(state => state.userInfo);

  useEffect(() => {
    console.log('MOunt');
    return () => console.log('UNMOUNT');
  }, ['']);

  const onselect = selected => {
    dispatch({
      type: CURRENT_COMP,
      payload: selected,
    });
    console.log(navigation.navigate('AboutCompany'));
  };
  return (
    <Block>
      <CustomHeader navigation={navigation} bg={colors.primary} />
      {appliedData.length ? (
        <FlatList
          data={appliedData.reverse()}
          keyExtractor={index => {
            return index.toString();
          }}
          renderItem={(item, index) => {
            console.log('item');
            console.log(item);
            return (
              <Block
                center
                padding={[0, sizes.getWidth(6)]}
                // margin={[sizes.getHeight(0), 0, 0, 0]}
                // style={{borderWidth: 1}}
              >
                <Button
                  onPress={() => onselect(item.item)}
                  row
                  flex={false}
                  style={{
                    // borderWidth: 1,
                    elevation: 2,
                    backgroundColor: colors.primary,
                    width: '100%',
                    padding: sizes.getWidth(2),
                    borderRadius: sizes.getWidth(2),
                    height: sizes.getHeight(18),
                  }}>
                  <Block center middle flex={false} style={{width: '25%'}}>
                    <Image
                      // source={icons.qnbLogo}
                      source={{
                        uri: companyLogo + item.item?.logo,
                      }}
                      style={{
                        resizeMode: 'contain',
                        width: sizes.getWidth(20),
                        height: sizes.getHeight(10),
                      }}
                    />
                  </Block>
                  <Block middle padding={[0, 0, 0, sizes.getWidth(2)]}>
                    <Text style={{textAlign: 'left'}}>
                      {currentLN === LANG_AR
                        ? item.item.name_ar
                        : item.item.name_en}
                    </Text>
                    <Text
                      numberOfLines={1}
                      ellipsizeMode="tail"
                      color={colors.gray}
                      style={{fontSize: sizes.customFont(10)}}>
                      About :&nbsp;
                      {currentLN === LANG_AR
                        ? item.item.about_us_ar
                        : item.item.about_us_en}
                    </Text>
                  </Block>
                </Button>
              </Block>
            );
          }}
        />
      ) : (
        <Block center middle>
          <Text h1 color={colors.gray}>
            No Record Found.
          </Text>
        </Block>
      )}
    </Block>
  );
};

export {FavComp};

const styles = StyleSheet.create({});
