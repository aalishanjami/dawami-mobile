import React from 'react';
import {Block, Text, Button, CustomHeader} from 'components';
import {sizes, colors} from 'styles/theme';
import {Image, FlatList, StyleSheet} from 'react-native';
import * as icons from 'assets/icons';
import {useSelector, useDispatch} from 'react-redux';
import {companyLogo} from 'redux/apiConstants';
import {SAVE_JOB_STATUS, LANG_AR} from 'redux/constants';

const AppliedJob = props => {
  const {navigation} = props;
  const dispatch = useDispatch();
  const appliedData = useSelector(state => state?.getLists?.appliedJobs);
  const {currentLN} = useSelector(state => state.userInfo);

  console.log('===================Applied JOBS ==============');
  console.log(appliedData);

  const onselect = selected => {
    // console.log(selected);
    dispatch({type: SAVE_JOB_STATUS, payload: selected});
    console.log(navigation.navigate('jobDetails'));
  };
  return (
    <Block>
      <CustomHeader navigation={navigation} bg={colors.primary} />
      {appliedData.length ? (
        <FlatList
          data={appliedData.reverse()}
          keyExtractor={index => {
            return index.toString();
          }}
          renderItem={(item, index) => {
            console.log('item');
            console.log(item);
            return (
              <Block
                center
                padding={[0, sizes.getWidth(6)]}
                // margin={[sizes.getHeight(0), 0, 0, 0]}
                // style={{borderWidth: 1}}
              >
                <Button
                  onPress={() => onselect(item.item)}
                  row
                  flex={false}
                  style={{
                    // borderWidth: 1,
                    elevation: 2,
                    backgroundColor: colors.primary,
                    width: '100%',
                    padding: sizes.getWidth(2),
                    borderRadius: sizes.getWidth(2),
                    height: sizes.getHeight(18),
                  }}>
                  <Block center middle flex={false} style={{width: '25%'}}>
                    <Image
                      source={{
                        uri: companyLogo + item.item?.company.logo,
                      }}
                      style={{
                        resizeMode: 'contain',
                        width: sizes.getWidth(20),
                        height: sizes.getHeight(10),
                      }}
                    />
                  </Block>
                  <Block
                    padding={[0, 0, 0, sizes.getWidth(2)]}
                    middle
                    style={{width: '75%'}}
                    flex={false}>
                    <Text h3 style={{lineHeight: 30, textAlign: 'left'}}>
                      {currentLN === LANG_AR
                        ? item.item.title_ar
                        : item.item.title_en}
                    </Text>
                    <Text h4 color={colors.gray}>
                      Company:&nbsp;
                      <Text h4 style={{fontWeight: '100'}}>
                        {currentLN === LANG_AR
                          ? item.item.company.name_ar
                          : item.item.company.name_en}
                      </Text>
                    </Text>
                  </Block>
                </Button>
              </Block>
            );
          }}
        />
      ) : (
        <Block center middle>
          <Text h1 color={colors.gray}>
            No Record Found.
          </Text>
        </Block>
      )}
    </Block>
  );
};

export {AppliedJob};

const styles = StyleSheet.create({});
