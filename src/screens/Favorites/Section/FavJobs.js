import React from 'react';
import {Block, Text, Button} from 'components';
import {sizes, colors} from 'styles/theme';
import {Image, FlatList, StyleSheet} from 'react-native';
import * as icons from 'assets/icons';
import {useSelector, useDispatch} from 'react-redux';
import {companyLogo} from 'redux/apiConstants';
import {SAVE_JOB_STATUS, LANG_AR} from 'redux/constants';

const FavJobs = props => {
  const {navigation} = props;
  const appliedData = useSelector(state => state?.getLists?.favJobs);
  const {currentLN} = useSelector(state => state.userInfo);

  const dispatch = useDispatch();
  console.log('===================FAV JOBS ==============');
  console.log(appliedData);

  const onselect = selected => {
    // console.log(selected);
    dispatch({type: SAVE_JOB_STATUS, payload: selected});
    console.log(navigation.navigate('jobDetails'));
  };
  return (
    <Block>
      {appliedData.length ? (
        <FlatList
          // inverted={true}
          data={appliedData.reverse()}
          keyExtractor={index => {
            return index.toString();
          }}
          renderItem={(item, index) => {
            console.log('item fav jobs ');
            console.log(item);
            return (
              <Block
                center
                padding={[0, sizes.getWidth(6)]}
                // margin={[sizes.getHeight(0), 0, 0, 0]}
                // style={{borderWidth: 1}}
              >
                <Button
                  onPress={() => onselect(item.item)}
                  row
                  flex={false}
                  style={{
                    // borderWidth: 1,
                    elevation: 2,
                    backgroundColor: colors.primary,
                    width: '100%',
                    padding: sizes.getWidth(2),
                    borderRadius: sizes.getWidth(2),
                    height: sizes.getHeight(18),
                  }}>
                  <Block center middle flex={false} style={{width: '25%'}}>
                    <Image
                      // source={icons.qnbLogo}
                      source={{
                        uri: companyLogo + item.item?.company?.logo,
                      }}
                      style={{
                        resizeMode: 'contain',
                        width: sizes.getWidth(20),
                        height: sizes.getHeight(10),
                      }}
                    />
                  </Block>
                  <Block
                    padding={[0, 0, 0, sizes.getWidth(2)]}
                    middle
                    style={{width: '75%'}}
                    flex={false}>
                    <Text h3 style={{lineHeight: 30, textAlign: 'left'}}>
                      {currentLN === LANG_AR
                        ? item.item.title_ar
                        : item.item.title_en}
                    </Text>
                    <Text h4 color={colors.gray}>
                      Company:&nbsp;
                      <Text h4 style={{fontWeight: '100'}}>
                        {currentLN === LANG_AR
                          ? item.item.company.name_ar
                          : item.item.company.name_en}
                      </Text>
                    </Text>
                  </Block>
                </Button>
              </Block>
            );
          }}
        />
      ) : (
        <Block center middle>
          <Text h1 color={colors.gray}>
            No Record Found.
          </Text>
        </Block>
      )}
    </Block>
  );
};

export {FavJobs};

const styles = StyleSheet.create({});
