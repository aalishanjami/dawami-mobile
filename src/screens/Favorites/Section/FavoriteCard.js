import React from 'react';
import {Block, Text, Button} from 'components';
import {sizes, colors} from 'styles/theme';
import {Image, FlatList, StyleSheet} from 'react-native';
import * as icons from 'assets/icons';

const FavoriteCard = props => {
  const {item} = props;
  return (
    <Block
      center
      padding={[0, sizes.getWidth(6)]}
      // margin={[sizes.getHeight(0), 0, 0, 0]}
    >
      <Button
        row
        flex={false}
        style={{
          elevation: 2,
          backgroundColor: colors.primary,
          width: '100%',
          borderRadius: sizes.getWidth(2),
          height: sizes.getHeight(18),
        }}>
        <Block center middle flex={false} style={{width: '25%'}}>
          <Image
            source={icons.qnbLogo}
            style={{resizeMode: 'contain', width: sizes.getWidth(20)}}
          />
        </Block>
        <Block middle style={{width: '75%'}} flex={false}>
          <Text h2 bold style={{lineHeight: 30}}>
            Qatar National Bank
          </Text>
          <Text bold h3>
            Job Title:
            <Text h4 color={colors.gray} style={{fontWeight: '100'}}>
              Web Developer
            </Text>{' '}
          </Text>
        </Block>
      </Button>
    </Block>
  );
};

export {FavoriteCard};

const styles = StyleSheet.create({});
