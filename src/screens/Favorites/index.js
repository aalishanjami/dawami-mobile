import React, {useEffect, useState} from 'react';
import {Block, Text, Button, ActivitySign, CustomHeader} from 'components';
import {sizes, colors} from 'styles/theme';
import * as icons from 'assets/icons';
// import {FavoriteCard} from './Section/FavoriteCard';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {NavigationContainer} from '@react-navigation/native';
// import {AppliedJob} from './Section/AppliedJob';
import {FavJobs} from './Section/FavJobs';
// import {FavComp} from './Section/FavComp';
import {useSelector, batch, useDispatch} from 'react-redux';
import {FavoriteJob, FavoriteComp, AppliedJobs} from 'redux/actions';
import {ADD_FAV_JOB, ADD_FAV_COMP, APPLIED_JOB} from 'redux/constants';

const Favorites = props => {
  const {navigation, route} = props;
  const dispatch = useDispatch();
  const id = useSelector(state => state.auth?.userBasicProfile?.id);
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    fetchData();
  }, ['']);

  // const fetchData = async () => {
  //   setIsLoading(true);
  //   const result_favJobs = await FavoriteJob(id, err => {
  //     console.log(err);
  //     setIsLoading(false);
  //   });
  //   // const result_favComp = await FavoriteComp(id, err => {
  //   //   console.log(err);
  //   //   setIsLoading(false);
  //   // });
  //   // const result_appliedJobs = await AppliedJobs(id, err => {
  //   //   console.log(err);
  //   //   setIsLoading(false);
  //   // });

  //   dispatch({type: ADD_FAV_JOB, payload: result_favJobs});

  //   // batch(() => {
  //   //   dispatch({type: ADD_FAV_JOB, payload: result_favJobs});
  //   //   dispatch({type: ADD_FAV_COMP, payload: result_favComp});
  //   //   dispatch({type: APPLIED_JOB, payload: result_appliedJobs});
  //   // });
  //   setIsLoading(false);

  //   // console.log(result_favJobs);
  //   // console.log(result_favComp);
  //   // console.log(result_appliedJobs);
  // };

  const TabStack = createMaterialTopTabNavigator();
  return (
    <Block>
      <CustomHeader tint={colors.customRed} navigation={navigation} />
      {/* <Block
        color={colors.primary}
        flex={false}
        middle
        center
        height={sizes.getHeight(3)}>
       
      </Block>
      <TabStack.Navigator
        tabBarOptions={{
          labelStyle: {fontSize: sizes.customFont(11)},
          inactiveTintColor: colors.gray,
          activeTintColor: colors.customRed,
          indicatorContainerStyle: {
            borderTopColor: colors.brown,
          },
          indicatorStyle: {
            backgroundColor: colors.customRed,
          },
        }}>
        <TabStack.Screen name="Applied" component={AppliedJob} />
        <TabStack.Screen name="Favorite Company" component={FavComp} />
        <TabStack.Screen name="Favorite Jobs" component={FavJobs} />
      </TabStack.Navigator> */}
      <FavJobs />
      {isLoading && <ActivitySign />}
    </Block>
  );
};

export default Favorites;
