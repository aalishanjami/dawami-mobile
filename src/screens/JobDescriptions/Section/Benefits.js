import React from 'react';
import {Block, Text} from 'components';
import {Image, ScrollView} from 'react-native';
import * as icons from 'assets/icons';
import * as images from 'assets/images';
import {sizes, colors} from 'styles/theme';
import {useSelector} from 'react-redux';
import {LANG_AR} from 'redux/constants';
import {WORDS} from 'utils';

const OtherBenifits = props => {
  const {currentLN} = useSelector(state => state.userInfo);
  const jobDetails = useSelector(state => state.getLists.currentJobStatus);
  const AR = currentLN === LANG_AR;
  const noDataFound = AR ? WORDS.NoDataFound_AR : WORDS.NoDataFound;
  return (
    <ScrollView>
      <Block
        padding={[sizes.getWidth(3), sizes.getWidth(5), 0, sizes.getWidth(5)]}>
        <Block margin={[sizes.getHeight(2), 0, 0, 0]}>
          <Text h4 bold>
            {AR ? WORDS.BEN_AR : 'Other Benifits'}
          </Text>
          <Text
            padding={[10, 0]}
            style={{fontSize: sizes.customFont(10), lineHeight: 18}}>
            {AR
              ? jobDetails?.other_benifits_ar || noDataFound
              : jobDetails?.other_benifits_en || noDataFound}
          </Text>
        </Block>
      </Block>
    </ScrollView>
  );
};

export default OtherBenifits;
