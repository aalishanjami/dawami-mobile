import React from 'react';
import {Block, Text} from 'components';
import {Image, ScrollView} from 'react-native';
import * as icons from 'assets/icons';
import * as images from 'assets/images';
import {sizes, colors} from 'styles/theme';
import AboutQatarGass from './About';
import OtherBenifits from './Benefits';
import EduWithExp from './EduWithExp';
import Responsibilities from './Responsibilities';
import {useSelector} from 'react-redux';
import {LANG_AR} from 'redux/constants';
const ShowAll = props => {
  const {currentLN} = useSelector(state => state.userInfo);
  const jobDetails = useSelector(state => state.getLists.currentJobStatus);
  const AR = currentLN === LANG_AR;

  return (
    <Block>
      <ScrollView
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        <Block
          margin={[sizes.getHeight(2), 0, 0, 0]}
          padding={[
            sizes.getWidth(3),
            sizes.getWidth(5),
            0,
            sizes.getWidth(5),
          ]}>
          <Text h4 bold>
            {AR ? 'المسمى الوظيفي' : ' Job Description'}
          </Text>
          <Text
            padding={[10, 0]}
            style={{
              fontSize: sizes.customFont(10),
              lineHeight: 18,
            }}>
            {AR ? jobDetails?.description_ar : jobDetails?.description_en}
          </Text>
        </Block>
        <Responsibilities />
        <EduWithExp />
        <OtherBenifits />
        <AboutQatarGass />
      </ScrollView>
    </Block>
  );
};

export default ShowAll;
