import React from 'react';
import {Block, Text} from 'components';
import {Image, ScrollView} from 'react-native';
import * as icons from 'assets/icons';
import * as images from 'assets/images';
import {sizes, colors} from 'styles/theme';
import {useSelector} from 'react-redux';
import {LANG_AR} from 'redux/constants';
import {WORDS} from 'utils';

const Responsibilities = props => {
  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;
  const jobDetails = useSelector(state => state.getLists.currentJobStatus);
  const noDataFound = AR ? WORDS.NoDataFound_AR : WORDS.NoDataFound;

  return (
    <ScrollView>
      <Block>
        <Block
          margin={[
            sizes.getHeight(2),
            sizes.getHeight(3),
            0,
            sizes.getHeight(3),
          ]}>
          <Text h4 bold>
            {AR ? WORDS.RES_AR : WORDS.RES}
          </Text>
          <Text
            padding={[10, 0]}
            style={{fontSize: sizes.customFont(10), lineHeight: 18}}>
            {currentLN === LANG_AR
              ? jobDetails?.responsibilities_ar || noDataFound
              : jobDetails?.responsibilities_en || noDataFound}
          </Text>
        </Block>
      </Block>
    </ScrollView>
  );
};

export default Responsibilities;
