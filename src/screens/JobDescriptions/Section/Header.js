import React, {useEffect, useState, cloneElement} from 'react';
import {Block, Text, CustomHeader, Button} from 'components';
import {colors, sizes} from 'styles/theme';
import {Image, StyleSheet, ActivityIndicator} from 'react-native';
import * as icons from 'assets/icons';
import {useSelector, useDispatch, batch} from 'react-redux';
import {
  FavoriteRequest,
  ApplyToJob,
  AuthUserViewJob,
  FollowingJob,
} from 'redux/actions';
import {SAVE_JOB_STATUS, FOLLOW, LANG_AR, CURRENT_COMP} from 'redux/constants';
// import {color} from 'react-native-reanimated';
// import {JobLocation} from 'screens/UserProfile/Section/Merger/PreferredJobs/Section';
import {companyLogo} from 'redux/apiConstants';
import {WORDS} from 'utils';
import DateDiff from 'date-diff';
import {diff} from 'react-native-reanimated';

const PortfolioHeader = props => {
  const {navigation, route} = props;
  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;
  const id = useSelector(state => state.auth?.userBasicProfile?.id);
  const {status} = useSelector(state => state.auth);
  const jobDetails = useSelector(state => state.getLists?.currentJobStatus);
  const currentComp = useSelector(state => state.getLists?.currentCompStatus);
  const dispatch = useDispatch();
  const currentCompany = useSelector(
    state => state.getLists?.currentCompStatus,
  );
  const currentCompanyJobs = currentCompany?.jobs;

  const posted = AR ? 'تم النشر' : 'Posted';
  const apply = AR ? 'تطبيق' : 'Apply for this job';
  const applied = AR ? 'مطبقة' : 'Applied';
  const loginFirst = AR ? 'سجل الدخول أولا' : 'Login Required';

  // ===========================================================
  const [favLoading, setFavLoading] = useState(false);
  const favorite = async item => {
    setFavLoading(true);
    const result = await FavoriteRequest(item.id, id, err => {
      console.log('ERROR IN FAV');
      console.log(err);
      setFavLoading(false);
    });
    result && dispatch(result);
    setFavLoading(false);
    // if user in coming form company => vacancies => job des.. here
    // console.log(item);
    // console.log(currentCompanyJobs[0]);
    // console.log(currentCompanyJobs[0] === item);

    // console.log(jobDetails);
    console.log('beacuse user not visited company this is wahy');
    const a = currentCompanyJobs && currentCompanyJobs.indexOf(item);
    if (a) {
      // means user coming form company vancies
      if (item.company.id === currentCompany.id) {
        console.log('user is coming from company vaccanies');
        // // const b = currentCompanyJobs.indexOf(item);
        // // console.log(b);
        // // console.log(item);
        // let newUpdatedObj = false;
        // currentCompanyJobs.forEach(element => {
        //   if (element.id === item.id) {
        //     // console.log(result.payload);
        //     // newUpdatedObj = element;
        //     newUpdatedObj = {
        //       ...item,
        //       saved_status: result.payload.current_status,
        //     };
        //   }
        // });
        // // now i can compare
        // // console.log(newUpdatedObj);
        // const indexNumber = currentCompanyJobs.indexOf();
        // console.log(indexNumber);
        // // currentCompanyJobs.splice()
      }
    } else {
      console.log('user never visit company before');
    }

    // ----------------------------------------------------------
    // setFavLoading(true);
    // const result = await FollowingJob(item.company_id, id, err => {
    //   console.log('ERROR IN FAV');
    //   console.log(err);
    //   setFavLoading(false);
    // });
    // // console.log('===========RESULT Follow');
    // // console.log(result);
    // const isFollowing = result === 1 ? true : false;
    // result && dispatch({type: FOLLOW, payload: isFollowing});
    // setFavLoading(false);
  };

  // APPLY JOB ========================================
  const [jobLoading, setJobLoading] = useState(false);
  const applyJob = async jobId => {
    setJobLoading(true);
    const result = await ApplyToJob(jobId, id, err => {
      console.log('err');
      console.log(err);
      setJobLoading(false);
    });
    // console.log('--------------APPLICATION RESULT---------');
    console.log(result);
    result && (dispatch(result), setJobLoading(false));
  };

  // const application_user_count = AR
  //   ? WORDS.applicationRec_ar
  //   : WORDS.applicationRec;

  // console.log('==================================================');
  // console.log(jobDetails);
  // console.log(DateDiff);
  const daysAgo = AR ? 'أيام مضت' : 'Days Ago';
  const monthAgo = AR ? 'منذ شهر' : 'Month Ago';
  const application = AR ? WORDS.Application_ar : WORDS.Application;
  const fullTime = AR ? WORDS.FullTime_AR : WORDS.FullTime;
  const Pakistan = AR ? WORDS.Pakistan_AR : WORDS.Pakistan;
  const qatar = AR ? WORDS.Qatar_AR : WORDS.Qarat;
  const country =
    jobDetails?.country_id === '1'
      ? qatar + '\r-\r' + fullTime
      : Pakistan + '\r-\r' + fullTime;

  let dbExactDate = new Date(jobDetails?.created_at.replace(/\s+/g, 'T'));
  let dbYear = dbExactDate.getFullYear();
  let dbMonth = dbExactDate.getMonth();
  let dbDay = dbExactDate.getDate();

  let year = new Date().getFullYear();
  let month = new Date().getMonth();
  let day = new Date().getDate();

  var date1 = new Date(dbYear, dbMonth, dbDay); // 2015-12-1
  // console.log(new Date(jobDetails.created_at).toDateString());
  // console.log(date1);
  var date2 = new Date(year, month, day); // 2014-01-1
  var diff = new DateDiff(date2, date1);
  var difference =
    diff.months() > 1
      ? diff.months() + ' ' + monthAgo
      : diff.days() + ' ' + daysAgo;
  console.log('==================');
  console.log(difference);
  // console.log(date1);
  // console.log(date2);
  // console.log(diff.weeks());
  // console.log('==================');

  return (
    <Block
      flex={false}
      style={{
        backgroundColor: colors.primary,
        // borderWidth: 1,
        height: sizes.getHeight(20),
      }}>
      {/* <CustomHeader /> */}
      <Block
        padding={[10, sizes.getWidth(5), 0, sizes.getWidth(5)]}
        row
        flex={3}
        // style={{borderWidth: 1, backgroundColor: 'red'}}
      >
        <Block flex={3}>
          <Text
            h2
            bold
            style={{
              textAlign: 'left',
            }}>
            {currentLN === LANG_AR
              ? jobDetails?.company?.name_ar
              : jobDetails?.company?.name_en}
          </Text>
          <Text
            h4
            style={{
              fontWeight: 'bold',
              textAlign: 'left',
            }}>
            {currentLN === LANG_AR
              ? jobDetails?.title_ar
              : jobDetails?.title_en}
          </Text>
          <Text h4 style={{textAlign: 'left'}}>
            {country}
          </Text>
          <Text
            style={{fontSize: sizes.customFont(10), textAlign: 'left'}}
            color={colors.gray2}>
            {posted}&nbsp;{difference} -
            {id &&
            ( AR
              ? application + jobDetails.applied_user_count
              : jobDetails?.applied_user_count + ' ' + application)}
          </Text>

          {/* <Text h4>{application_user_count}</Text> */}
        </Block>
        <Block
          center
          middle
          flex={false}
          style={{
            // borderWidth: 1,
            height: sizes.getHeight(13),
            width: sizes.getWidth(30),
          }}>
          {/* {jobDetails.logo && ( */}
          <Image
            source={{uri: companyLogo + jobDetails?.company?.logo}}
            style={styles.compLogo}
          />
          {/* )} */}
        </Block>
      </Block>
      {/* ============================================================================================ */}
      <Block
      style={{borderWidth:0, marginTop:sizes.getHeight(5)}}
      padding={[0, sizes.getWidth(5)]} row center flex={3}>
        <Block
          flex={false}
          // style={{borderWidth: 1}}
          center
          middle
          margin={[0, sizes.getWidth(3), 0, 0]}>
          {status && (
            <Button
              onPress={() => favorite(jobDetails)}
              center
              middle
              style={{
                height: sizes.screenSize * 0.03,
                width: sizes.screenSize * 0.03,
                borderWidth: 0.5,
                borderColor: colors.spaceGreen,
                borderRadius: sizes.withScreen(0.5),
                backgroundColor:
                  jobDetails.saved_status === 1
                    ? colors.spaceGreen
                    : 'transparent',
              }}>
              {!favLoading ? (
                <Image
                  source={icons.empty_heartOne}
                  style={{
                    resizeMode: 'contain',
                    flex: 1,
                    tintColor: colors.spaceGreen,
                    tintColor:
                      jobDetails.saved_status === 1
                        ? colors.primary
                        : colors.spaceGreen,
                  }}
                />
              ) : (
                <ActivityIndicator
                  color={
                    jobDetails.saved_status === 1
                      ? colors.primary
                      : colors.spaceGreen
                  }
                />
              )}
            </Button>
          )}
        </Block>
        {/* ============================================================================================ */}
        <Block
          center
          middle
          flex={false}
          style={{
            // borderWidth: 1,
            height: sizes.getHeight(6),
            width: sizes.getWidth(43),
          }}>
          {!jobLoading ? (
            <Button
              disabled={
                !status ? true : jobDetails.applied_status === 0 ? false : true
              }
              onPress={() => applyJob(jobDetails.id)}
              center
              middle
              style={{
                paddingHorizontal: sizes.getWidth(3),
                borderRadius: sizes.withScreen(0.09),
                elevation: 5,
                backgroundColor: !status
                  ? colors.gray
                  : jobDetails.applied_status === 0
                  ? colors.brown
                  : colors.gray,
                width: sizes.getWidth(40),
              }}>
              <Text h4 color={colors.primary}>
                {!status
                  ? loginFirst
                  : jobDetails.applied_status === 0
                  ? apply
                  : applied}
              </Text>
            </Button>
          ) : (
            <Block
              middle
              center
              flex={false}
              style={{
                borderWidth: 0.4,
                borderColor: colors.gray2,
                borderRadius: sizes.getWidth(5),
              }}
              height="95%"
              width="95%">
              <ActivityIndicator color={colors.spaceGreen} />
            </Block>
          )}
        </Block>
      </Block>
    </Block>
  );
};

const styles = StyleSheet.create({
  compLogo: {
    resizeMode: 'contain',
    // borderWidth: 1,
    width: sizes.getWidth(25),
    height: sizes.getHeight(10),
  },
});
export default PortfolioHeader;
