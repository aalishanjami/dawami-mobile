import React from 'react';
import {Block, Text} from 'components';
import {Image, ScrollView} from 'react-native';
import * as icons from 'assets/icons';
import * as images from 'assets/images';
import {sizes, colors} from 'styles/theme';
import {useSelector} from 'react-redux';
import {LANG_AR} from 'redux/constants';
import {WORDS} from 'utils';

const EduWithExp = props => {
  const {currentLN} = useSelector(state => state.userInfo);
  const jobDetails = useSelector(state => state.getLists?.currentJobStatus);
  const AR = currentLN === LANG_AR;
  const noDataFound = AR ? WORDS.NoDataFound_AR : WORDS.NoDataFound;

  return (
    <ScrollView>
      <Block
        padding={[sizes.getWidth(2), sizes.getWidth(5), 0, sizes.getWidth(5)]}>
        <Block margin={[sizes.getHeight(1), 0, 0, 0]}>
          <Text h4 bold>
            {AR ? WORDS.EDU_AR : WORDS.EDU}
          </Text>
          <Text
            padding={[10, 0]}
            style={{fontSize: sizes.customFont(10), lineHeight: 18}}>
            {AR
              ? jobDetails?.skills_ar || noDataFound
              : jobDetails?.skills_en || noDataFound}
          </Text>
        </Block>
      </Block>
    </ScrollView>
  );
};

export default EduWithExp;
