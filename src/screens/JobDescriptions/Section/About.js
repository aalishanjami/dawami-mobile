import React from 'react';
import {Block, Text, Button} from 'components';
import {Image, ScrollView, StyleSheet} from 'react-native';
import * as icons from 'assets/icons';
import * as images from 'assets/images';
import {sizes, colors} from 'styles/theme';
import {useSelector, useDispatch} from 'react-redux';
import {LANG_AR, CURRENT_COMP} from 'redux/constants';
import {WORDS, NAV} from 'utils';

const AboutQatarGass = props => {
  const {navigation, showBtn} = props;
  const {currentLN} = useSelector(state => state.userInfo);
  const jobDetails = useSelector(state => state.getLists.currentJobStatus);
  const AR = currentLN === LANG_AR;
  const jobsAvailable = AR ? 'المهن متوفرة' : 'Jobs Available';
  const dispatch = useDispatch();

  console.log('=========ABOUT======JOB=======');
  console.log(navigation);
  const goToRelaventCompany = detials => {
    dispatch({type: CURRENT_COMP, payload: detials.company});
    navigation.navigate(NAV.COMPANY_DETAILS);
  };

  return (
    <ScrollView>
      <Block
        padding={[sizes.getWidth(2), sizes.getWidth(5), 0, sizes.getWidth(5)]}>
        <Block margin={[sizes.getHeight(3), 0, 0, 0]}>
          <Text h4 bold>
            {AR ? WORDS.ABOUT_AR : WORDS.ABOUT}
          </Text>
          <Text
            padding={[10, 0]}
            style={{fontSize: sizes.customFont(10), lineHeight: 18}}>
            {AR
              ? jobDetails?.company?.about_us_ar || ' No Data Available'
              : jobDetails?.company?.about_us_en || 'لا يوجد معلومات'}
          </Text>
        </Block>
        <Block>
          {showBtn && (
            <Button
              onPress={() => goToRelaventCompany(jobDetails)}
              style={styles.aboutBtn}>
              <Text h4 color={colors.primary}>
                {jobDetails?.open_jobs_count} {jobsAvailable}
              </Text>
            </Button>
          )}
        </Block>
      </Block>
    </ScrollView>
  );
};

export default AboutQatarGass;

const styles = StyleSheet.create({
  aboutBtn: {
    // borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: sizes.getWidth(40),
    elevation: 4,

    borderRadius: sizes.screenSize * 0.9,
    backgroundColor: colors.brown,
  },
});
