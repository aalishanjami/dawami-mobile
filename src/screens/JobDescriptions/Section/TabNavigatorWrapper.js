import React, {useState, useEffect} from 'react';
import {Block, Text, Button} from 'components';
import {TabView, SceneMap, TabBar, ScrollPager} from 'react-native-tab-view';
import {Dimensions, StyleSheet} from 'react-native';
// import {About, Vacancies, Media, Contact} from 'screens/CompanyProfile';
import {
  ShowAll,
  Responsibilities,
  AboutQatarGass,
  EduWithExp,
  OtherABifits,
} from 'screens/JobDescriptions';
import {colors, sizes} from 'styles/theme';
import {useSelector} from 'react-redux';
import {LANG_AR} from 'redux/constants';
import {ScrollView} from 'react-native-gesture-handler';
import OtherBenifits from './Benefits';
import {ABOUT} from 'utils/LanguageHandler';
import {About} from 'redux/apiConstants';
import {WORDS} from 'utils';

const TabNavigatorWrapper = navigation => {
  const {currentLN} = useSelector(state => state?.userInfo);
  const nameOfComp = useSelector(state => state?.getLists?.currentCompStatus);
  const nameOfJob = useSelector(state => state?.getLists?.currentJobStatus);
  const AR = currentLN === LANG_AR;
  const [index, setIndex] = React.useState(0);
  const [routes] = useState([
    {key: 'showall', title: 'Show All'},
    {key: 'res', title: 'Responsibilities'},
    {key: 'edu', title: 'Education & Experience'},
    {key: 'benifits', title: 'Other Benifits'},
    {key: 'about', title: 'About Qatar Gass'},
  ]);
  const initialLayout = {width: Dimensions.get('window').width};

  // const renderScene = SceneMap({
  //   showall: () => <ShowAll />,
  //   res: () => <Responsibilities />,
  //   edu: () => <EduWithExp />,
  //   benifits: () => <OtherBenifits />,
  //   about: () => <AboutQatarGass />,
  // });

  // const renderTabBar = props => (
  //   <TabBar
  //     {...props}
  //     scrollEnabled={true}
  //     indicatorStyle={{backgroundColor: colors.spaceGreen}}
  //     style={{
  //       backgroundColor: 'transparent',
  //       elevation: 0,
  //       borderBottomColor: colors.spaceGreen,
  //       borderBottomWidth: 0.9,
  //     }}
  //     indicatorStyle={{
  //       borderWidth: 2.2,
  //       borderColor: colors.spaceGreen,
  //       marginLeft: sizes.getWidth(2),
  //       borderTopLeftRadius: sizes.getWidth(10),
  //       borderTopRightRadius: sizes.getWidth(10),
  //     }}
  //     activeColor={colors.spaceGreen}
  //     inactiveColor={colors.gray2}
  //     labelStyle={{
  //       textTransform: 'capitalize',
  //       fontSize: sizes.withScreen(0.01),
  //       fontWeight: 'bold',
  //     }}
  //     tabStyle={{
  //       height: sizes.getHeight(5.5),
  //       width: 'auto',
  //       padding: 0,
  //       paddingHorizontal: sizes.getWidth(1),
  //       marginLeft: sizes.getWidth(2),
  //     }}
  //   />
  // );

  // ===========
  //  showall: () => <ShowAll />,
  // res: () => <Responsibilities />,
  // edu: () => <EduWithExp />,
  // benifits: () => <OtherBenifits />,
  // about: () => <AboutQatarGass />,
  let SW = AR ? WORDS.SHOW_ALL_AR : WORDS.SHOW_ALL;
  let RES = AR ? WORDS.RES_AR : WORDS.RES;
  let EDU = AR ? WORDS.EDU_AR : WORDS.EDU;
  let BEN = AR ? WORDS.BEN_AR : WORDS.BEN;
  let AB = AR
    ? `${WORDS.AboutCompany_AR} ${nameOfComp?.name_ar ||
        nameOfJob.company.name_ar}`
    : `${WORDS.AboutCompany} ${nameOfComp?.name_en ||
        nameOfJob.company.name_en}`;
  let [currentTab, setCurrentTab] = useState(SW);

  useEffect(() => {
    setCurrentTab(SW);
    return () => currentTab;
  }, [currentLN]);

  const changeTab = current => {
    switch (current) {
      case SW:
        return setCurrentTab(SW);
      // return;
      case RES:
        return setCurrentTab(RES);
      // return;
      case EDU:
        return setCurrentTab(EDU);
      // return;
      case BEN:
        return setCurrentTab(BEN);
      case AB:
        return setCurrentTab(AB);
      // return;
      default:
        return setCurrentTab(AB);
    }
  };
  // console.log('=====');
  // console.log(currentTab);

  return (
    // <TabView
    //   navigationState={{index, routes}}
    //   renderScene={renderScene}
    //   onIndexChange={setIndex}
    //   // initialLayout={()=><Contact />}
    //   renderTabBar={renderTabBar}
    // />
    <Block>
      <Block style={styles.headingCon}>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          // style={{borderWidth: 0}}
          contentContainerStyle={{
            // height: '100%',
            height: sizes.getHeight(5.9),
            // borderWidth: 3,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Button
            onPress={() => changeTab(SW)}
            style={{
              ...styles.tabBtn,
              borderBottomWidth: currentTab === SW ? 5 : 0,
            }}>
            <Text style={styles.tabBtnText}>{SW}</Text>
          </Button>
          <Button
            onPress={() => changeTab(RES)}
            style={{
              ...styles.tabBtn,
              width: sizes.getWidth(25),
              borderBottomWidth: currentTab === RES ? 5 : 0,
            }}>
            <Text
              //  style={styles.tabBtnText}
              style={{...styles.tabBtnText, fontSize: sizes.customFont(10)}}>
              {RES}
            </Text>
          </Button>
          <Button
            onPress={() => changeTab(EDU)}
            style={{
              ...styles.tabBtn,
              width: sizes.getWidth(32),
              borderBottomWidth: currentTab === EDU ? 5 : 0,
            }}>
            <Text
              style={{...styles.tabBtnText, fontSize: sizes.customFont(10)}}>
              {EDU}
            </Text>
          </Button>
          <Button
            onPress={() => changeTab(BEN)}
            style={{
              ...styles.tabBtn,
              borderBottomWidth: currentTab === BEN ? 5 : 0,
            }}>
            <Text style={styles.tabBtnText}>{BEN}</Text>
          </Button>
          <Button
            onPress={() => changeTab(AB)}
            style={{
              ...styles.tabBtn,
              borderBottomWidth: currentTab === AB ? 5 : 0,
            }}>
            <Text style={styles.tabBtnText}>{AB}</Text>
          </Button>
        </ScrollView>
      </Block>
      <Block>
        {currentTab === SW && <ShowAll />}
        {currentTab === RES && <Responsibilities />}
        {currentTab === EDU && <EduWithExp />}
        {currentTab === BEN && <OtherBenifits />}
        {currentTab === AB && (
          <AboutQatarGass navigation={navigation} showBtn />
        )}
      </Block>
    </Block>
  );
};

const styles = StyleSheet.create({
  headingCon: {
    flex: 0,
    height: sizes.getHeight(6),
    width: sizes.getWidth('100%'),
    borderBottomWidth: 0.6,
    alignItems: 'center',
    marginTop: sizes.getHeight(0.3),
    flexDirection: 'row',
    justifyContent: 'space-between',
    // paddingHorizontal: sizes.getWidth(3),
  },
  tabBtn: {
    // borderWidth: 1,
    width: sizes.getWidth(23),
    height: '100%',
    justifyContent: 'center',
    // backgroundColor: 'red',
    // flex: 1,
    alignItems: 'center',
    paddingHorizontal: sizes.getWidth(3),
    borderBottomColor: colors.spaceGreen,
    paddingHorizontal: 0,
  },
  tabBtnText: {
    color: colors.spaceGreen,
    fontSize: sizes.customFont(10),
  },
});

export default TabNavigatorWrapper;
