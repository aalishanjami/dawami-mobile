import React, {useEffect, useState} from 'react';
import {Block, Text, CustomHeader} from 'components';
import {Header, TabWrapper} from 'screens/JobDescriptions';
import {sizes, colors} from 'styles/theme';
import {job} from 'assets/icons';
import {useSelector} from 'react-redux';
import {SafeAreaView} from 'react-native';
const JobDesciption = props => {
  const {navigation, route} = props;
  const {currentLN} = useSelector(state => state.userInfo);
  const [jobDetails, setJobDetails] = useState(null);
  const [applyJobBtn, setApplyJobBtn] = useState(false);
  // console.log(route.params?.companyDetail);
  
  return (
    <>
      <SafeAreaView
        style={{flex: 0}}
        style={{backgroundColor: colors.spaceGreen}}
      />
      <Block>
        <CustomHeader navigation={navigation} />
        <Header navigation={navigation} route={route} />
        <Block flex={5}>
          <TabWrapper {...navigation} />
        </Block>
      </Block>
    </>
  );
};

export default JobDesciption;
