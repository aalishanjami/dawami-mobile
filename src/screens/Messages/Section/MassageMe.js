import React from 'react';
import {StyleSheet, Image, Platform, Dimensions} from 'react-native';
import {Block, Text, Button} from 'components';
import {colors, sizes} from 'styles/theme';
import {ImageCon} from './chatHeader';
import * as icons from 'assets/icons';
import {FileMe} from './fileMe';

const Messages = props => {
  const {value, color} = props;
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width <= 375;

  // console.log('============== CHAT NE TEXT ==============');
  // console.log(value.message);
  return (
    <Text
      h3
      style={{
        textAlign: 'right',
        fontSize: sizes.customFont(iphone6s ? 13 : 18),
      }}
      color={color || colors.black}>
      {value.message}
    </Text>
  );
};

export {Messages};
