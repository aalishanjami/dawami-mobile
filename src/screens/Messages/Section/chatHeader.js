import React from 'react';
import {StyleSheet, Image} from 'react-native';
import {Block, Text, Button} from 'components';
import {sizes, colors} from 'styles/theme';
import * as icons from 'assets/icons';
import {companyLogo} from 'redux/apiConstants';

const ChatHeader = props => {
  const {details, navigation, bc} = props;
  // const imagePath = `https://dawami.wedigits.dev/images/co/`;

  return (
    <Block
      row
      // color={'#343434'}
      color={'#2C292B'}
      // padding={[0, sizes.getWidth(4)]}
      flex={false}
      style={{elevation: 10, height: sizes.getHeight(10)}}>
      <Block flex={false} center middle>
        {/* <Text>Back</Text> */}
        <Button
          onPress={() => navigation.goBack()}
          activeOpacity={0.3}
          center
          middle
          style={{width: '100%', height: '100%'}}>
          <Image
            source={icons.goBack1}
            style={{
              resizeMode: 'contain',
              height: sizes.getHeight(7),
              width: sizes.getWidth(10),
              tintColor: colors.customRed,
            }}
          />
        </Button>
      </Block>
      <Block
        center
        middle
        flex={false}
        style={{...styles.logo}}
        padding={[sizes.getHeight(0.5), 0]}>
        {/* <Block
          margin={[0, sizes.getWidth(2), 0, 0]}
          flex={false}
          style={{...styles.imgCon, borderColor: bc}}
        /> */}
        {details.avatar ? (
          <Image
            // source={icons.goBack1}

            source={{uri: `${companyLogo}${details.avatar}`}}
            style={{
              resizeMode: 'contain',
              // flex: 1,
              // borderWidth: 1,
              height: sizes.screenSize * 0.05,
              width: sizes.screenSize * 0.05,
              marginHorizontal: sizes.getWidth(2),
              alignSelf: 'center',
            }}
          />
        ) : (
          <Image
            // source={icons.goBack1}

            source={icons.avatarDummy}
            style={{
              resizeMode: 'contain',
              height: sizes.getHeight(7),
              width: sizes.getWidth(10),
              tintColor: colors.primary,

              marginHorizontal: sizes.getWidth(2),
            }}
          />
        )}
      </Block>
      <Block middle>
        <Text color={colors.primary}> {details.name} </Text>
      </Block>
    </Block>
  );
};

export const ImageConForMe = props => {
  console.log('IMAGE CON ====');
  const userImgPath = `https://dawami.wedigits.dev/images/user_avatars/`;
  console.log(props);
  return (
    <Block
      center
      middle
      style={styles.logo}
      padding={[sizes.getHeight(0.5), 0]}>
      {/* <Block flex={false} style={styles.imgCon} /> */}
      {!props.value ? (
        <Image source={icons.avatarDummy} style={styles.pic} />
      ) : (
        <Image
          source={{uri: userImgPath + props.value}}
          style={{...styles.pic, resizeMode: 'contain'}}
        />
      )}
    </Block>
  );
};

export const ImageCon = props => {
  // console.log('IMAGE CON ====');
  // console.log(props.value.avatar);
  return (
    <Block
      center
      middle
      style={styles.logo}
      padding={[sizes.getHeight(0.5), 0]}>
      {/* <Block flex={false} style={styles.imgCon} /> */}
      {!props.value.avatar ? (
        <Image source={icons.avatarDummy} style={styles.pic} />
      ) : (
        <Image
          source={{uri: companyLogo + props.value.avatar}}
          style={{...styles.pic, resizeMode: 'contain'}}
        />
      )}
    </Block>
  );
};

export {ChatHeader};

const styles = StyleSheet.create({
  logo: {
    // borderWidth:1,
    // backgroundColor:'pink'
    // borderColor:'red'
  },
  imgCon: {
    borderWidth: 1,
    // borderColor: colors.primary,
    width: sizes.screenSize * 0.04,
    height: sizes.screenSize * 0.04,
    borderRadius: sizes.screenSize * 100,
  },
  pic: {
    width: sizes.screenSize * 0.043,
    height: sizes.screenSize * 0.043,
  },
});
