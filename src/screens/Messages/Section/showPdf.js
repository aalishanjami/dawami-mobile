import React, {useRef, useEffect} from 'react';
import {StyleSheet, Platform, ScrollView, SafeAreaView} from 'react-native';
import PDFView from 'react-native-view-pdf';
import {Block, Text} from 'components';
import {colors, sizes} from 'styles/theme';
import RNFetchBlob from 'rn-fetch-blob';
import Pdf from 'react-native-pdf';

const ShowPdf = props => {
  const {
    data: {path},
  } = props;
  // console.log('*******SHOW-PDF*******');
  // console.log(path);

  console.log('SHOW PDF');
  console.log(path);

  // const source = {uri:path, cache:true};
  const source = {
    cache: true,
    uri: path,
  
  };

  console.log(source);
  return (
    <>
      <SafeAreaView style={{flex: 1}}>
        <Block
          // center
          middle
          style={{borderWidth: 0, borderColor: 'red'}}
          height={sizes.getHeight(90)}>
          <Pdf
            style={{flex: 1, width: '100%', height: '100%'}}
            source={source}
            onLoadComplete={(numberOfPages, filePath) => {
              console.log(`number of pages: ${numberOfPages}`);
            }}
            onPageChanged={(page, numberOfPages) => {
              console.log(`current page: ${page}`);
            }}
            onError={error => {
              console.log('ERROR STATE');
              console.log(error);
            }}
            onPressLink={uri => {
              console.log(`Link presse: ${uri}`);
            }}
          />
        </Block>
      </SafeAreaView>
      <SafeAreaView style={{flex: 0}} />
    </>
  );
};

export {ShowPdf};

const styles = StyleSheet.create({});
