import React, {useEffect, useState, useRef} from 'react';
import {
  Block,
  Text,
  ActivitySign,
  Button,
  CustomAlert,
  CustomHeader,
} from 'components';
import {colors, sizes} from 'styles/theme';
import {getStoredState} from 'redux-persist';
import {getSpecificUserChat, SaveChat, SendFile} from 'redux/actions';
import {
  StyleSheet,
  FlatList,
  TextInput,
  Image,
  ScrollView,
  PermissionsAndroid,
  ActivityIndicator,
  Platform,
  SafeAreaView,
  Keyboard,
  Dimensions,
} from 'react-native';
import {ChatMe} from './chatMe';
import {ChatOther} from './chatOther';
import {ChatHeader} from './chatHeader';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {YellowBox} from 'react-native';
import * as icons from 'assets/icons';
import messaging from '@react-native-firebase/messaging';
import DocumentPicker from 'react-native-document-picker';
import {ShowPdf} from './showPdf';
import RNFetchBlob from 'rn-fetch-blob';
import Toast from 'react-native-easy-toast';
import {ChatWith, StoreFile} from 'redux/apiConstants';
import Axios from 'axios';
// import RNFS from 'react-native-fs';
import {useSelector} from 'react-redux';
import {NewChatHeader} from './NewChatHeader';

YellowBox.ignoreWarnings(['VirtualizedLists should never be nested']);
const ios = Platform.OS === 'ios';
const iphone6s = Dimensions.get('screen').width <= 375;
const Chat = props => {
  const {navigation, route} = props;
  const me = route.params?.me || null;
  const other = route.params?.other || null;
  // const chats = route.params.chat;
  const details = route.params.details;

  // console.log('==========DETAIL FROM MESSAGE ');
  // console.log(details);

  const messageRef = useRef();
  const [chats, setChats] = useState();
  const [message, setMessage] = useState();
  const [btnDisable, setBtnDisable] = useState(true);
  const [isWaiting, setIsWaiting] = useState(false);
  const [showPdf, setShowPdf] = useState(false);
  const [fileDetail, setFileDetail] = useState(null);
  // const [response,setResponse] = useState()
  // const [me,setMe] = useState()
  // const [other,setOther] = useState()

  const fetch = async () => {
    setIsWaiting(true);
    const result = await getSpecificUserChat(me, other, err => {
      console.log('....ERROR.....');
      // console.log(err?.message);
      // console.log(err);
      messageRef.current?.show(
        <CustomAlert textAlign={'center'} text={err?.message || err} />,
        3500,
      );
      setIsWaiting(false);
    });
    if (result) {
      setChats(result);
      setIsWaiting(false);
    }
  };

  const flatRef = useRef();
  const inputRef = useRef();

  useEffect(() => {
    const unsubscribe = messaging().onMessage(async message => {
      fetch();
    });
    // return () => unsubscribe();
    return () => null
  }, ['']);

  const inputHandler = e => {
    e.length ? (setBtnDisable(false), setMessage(e)) : setBtnDisable(true);
  };

  const submitHanlder = async () => {
    setBtnDisable(true);
    inputRef.current?.clear();
    const result = await SaveChat(me, other, message, err => {
      console.log('error');
      console.log(err);
    });
    // console.log(result);
    setChats(prev => [...prev, result]);
    fetch();
  };

  useEffect(() => {
    fetch();
    return () => null;
  }, ['']);
  // ===================================================PDF VIEW

  const documentHandler = async () => {
    if (Platform.OS === 'android') {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // console.log('ACCESSED');
        try {
          const document = await DocumentPicker.pick({
            type: [DocumentPicker.types.pdf],
          });
          const baseDir = RNFetchBlob.fs.dirs;
          const inDownloads = `file://${baseDir.DownloadDir}/${document.name}`;
          const inDocument = `file://${baseDir.DocumentDir}/${document.name}`;
          if (inDocument) {
            console.log('in doc');
            setShowPdf({path: inDocument});
            // setFileDetail(inDocument)
            setFileDetail({
              name: document.name,
              uri: document.uri,
              type: document.type,
            });
          } else {
            console.log('in download');
            setShowPdf({path: fullPath});
            setFileDetail(inDownloads);
          }
          setShowPdf({path: inDownloads});

          // console.log(await RNFetchBlob.fs.stat(fullPath))
        } catch (e) {
          // console.log('e')
          // console.log(e)
          if (DocumentPicker.isCancel(e)) {
            messageRef.current?.show(
              <CustomAlert textAlign={'center'} text={`Cancelled.`} />,
              2000,
            );
          } else {
            console.log('Some Error');
          }
        }

        // ===============================================
      } else if (granted === PermissionsAndroid.RESULTS.DENIED) {
        messageRef.current?.show(
          <CustomAlert
            textAlign={'center'}
            text={`Stroage Read Permission Denied. \n App Needs Storage Read Permission. Please Allow`}
          />,
          4000,
        );
      } else if (granted === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
        messageRef.current?.show(
          <CustomAlert
            textAlign={'center'}
            text={`Please go To Setting > App > Permission \n Allow Permission of storage `}
          />,
          4000,
        );
      }
    }
    // IOS
    else {
      alert('ios');
      try {
        const document = await DocumentPicker.pick({
          type: [DocumentPicker.types.pdf],
        });
        const baseDir = RNFetchBlob.fs.dirs;
        // const inDownloads = `file://${baseDir.DownloadDir}/${document.name}`;
        const inDocument = `file://${baseDir.DocumentDir}/${document.name}`;

        console.log(inDocument);
        if (inDocument) {
          // alert("here")
          setFileDetail(inDocument);
          uploadHandler(me, other);

          // setShowPdf({path: inDocument});
          setFileDetail({
            name: document.name,
            uri: document.uri,
            type: document.type,
          });
        } else {
          console.log('in download');
          // setShowPdf({path: fullPath});
          // setFileDetail(inDownloads);
        }
        // setShowPdf({path: inDownloads});

        // console.log(await RNFetchBlob.fs.stat(fullPath))
      } catch (e) {
        if (DocumentPicker.isCancel(e)) {
          messageRef.current?.show(
            <CustomAlert textAlign={'center'} text={`Cancelled.`} />,
            2000,
          );
        } else {
          console.log('Some Error');
        }
      }
    }
  };
  // ===================================================UPLOAD HANDLER
  const uploadHandler = async (sender, reciever) => {
    setShowPdf(false);
    setIsWaiting(true);
    const result = await SendFile(sender, reciever, fileDetail, err => {
      // console.log('EEEE');
      // console.log(err);
      messageRef.current?.show(
        <CustomAlert
          textAlign={'center'}
          text={`Ubable to upload file.. please try again later`}
        />,
        3500,
      );
      setIsWaiting(false);
    });
    setIsWaiting(false);
    fetch();
  };
  // ===================================================UPLOAD HANDLER
  // console.log('====================');
  // console.log(details);
  // console.log('====================');
  const KeyboardAwareScrollViewRef = useRef();
  return (
    <>
      <SafeAreaView style={{flex: 0, backgroundColor: colors.spaceGreen}} />
      <SafeAreaView style={{flex: 1, borderWidth: 0}}>
        {/* ---------------------SHOW PDF START------------------------ */}
 {showPdf && (
          <Block style={styles.preview}>
            <Block
              space={'between'}
              row
              flex={false}
              padding={[
                0,
                sizes.getWidth(3),
                sizes.getHeight(1),
                sizes.getWidth(3),
              ]}
              height={sizes.getHeight(10)}
              color={colors.primary}>
              <Button
                onPress={() => uploadHandler(me, other)}
                center
                middle
                style={{
                  width: sizes.getWidth(30),
                  height: '100%',
                  borderRadius: sizes.getWidth(0.7),
                  backgroundColor: colors.customRed,
                  paddingHorizontal: sizes.getWidth(2),
                }}>
                <Text h4 color={colors.primary}>
                  Yes ! This One
                </Text>
              </Button>

              <Button
                onPress={() => setShowPdf(null)}
                center
                middle
                style={{
                  height: '100%',
                  width: sizes.getWidth(30),
                  borderRadius: sizes.getWidth(0.7),
                  backgroundColor: colors.customRed,
                  paddingHorizontal: sizes.getWidth(2),
                }}>
                <Text h4 color={colors.primary}>
                  Cancel
                </Text>
              </Button>
            </Block>
            <ShowPdf data={showPdf} />
          </Block>
        )}
       
        {/* ---------------------SHOW PDF END------------------------ */}
        <KeyboardAwareScrollView
          ref={KeyboardAwareScrollViewRef}
          alwaysBounceHorizontal={true}
          contentContainerStyle={{flex: 1, margin:0, padding:0}}
          automaticallyAdjustContentInsets={true}
          scrollEnabled={false}
          keyboardDismissMode="on-drag"
          contentInsetAdjustmentBehavior="scrollableAxes"
          keyboardShouldPersistTaps="always">
          <Block color={colors.primary}>
            {/* header */}
            <NewChatHeader navigation={navigation} name={details.name} />
            {/* ======================================================================== */}

            <Block>
              <Block style={styles.chatCon}>
                {chats?.length ? (
                  <ScrollView
                    automaticallyAdjustContentInsets={true}
                    showsVerticalScrollIndicator={false}
                    onContentSizeChange={(w, h) => {
                      flatRef.current?.scrollTo({y: h, animated: true});
                    }}
                    ref={flatRef}
                    nestedScrollEnabled={true}>
                    {chats.map((item, index) => {
                      if (item.sender_id === me) {
                        return (
                          <ChatMe
                            details={details}
                            key={item.id}
                            value={item}
                          />
                        );
                      } else {
                        return (
                          <ChatOther
                            details={details}
                            key={item.id}
                            value={item}
                          />
                        );
                      }
                    })}
                  </ScrollView>
                ) : (
                  <Block center middle>
                    <Text
                      color={colors.gray}
                      style={{fontSize: sizes.customFont(iphone6s ? 8 : 14)}}>
                      Please Wait....
                    </Text>
                  </Block>
                )}
              </Block>
              {/* ============ ACTION BUTTON ============ */}
              <Block style={styles.actionBtnCon}>
                {/* --------------------------------------------- */}
                <Block>
                  <Button
                    onPress={documentHandler}
                    activeOpacity={0.3}
                    center
                    middle
                    style={{
                      // borderWidth: 1,
                      width: '100%',
                      height: '100%',
                    }}>
                    <Image
                      source={icons.attachment}
                      style={{
                        resizeMode: 'contain',
                        width: sizes.getWidth(4),
                        height: sizes.getHeight(4),
                      }}
                    />
                  </Button>
                </Block>
                {/* --------------------------------------------- */}
                <Block
                  flex={5}
                  middle
                  color={colors.primary}
                  padding={[0, sizes.getWidth(3)]}>
                  <TextInput
                    ref={inputRef}
                    blurOnSubmit={false}
                    returnKeyType="send"
                    onSubmitEditing={submitHanlder}
                    placeholder="Please Type..."
                    placeholderTextColor={colors.gray}
                    style={{
                      // borderWidth: 1,
                      width: '100%',
                      height: '100%',
                      color: colors.gray5,
                    }}
                    onChangeText={e => inputHandler(e)}
                  />
                </Block>

                <Block>
                  <Block middle center style={{width: '100%'}}>
                    <Button
                      disabled={btnDisable}
                      onPress={submitHanlder}
                      activeOpacity={0.3}
                      center
                      middle
                      style={{
                        // borderWidth: 1,
                        height: sizes.screenSize * 0.03,
                        width: sizes.screenSize * 0.03,
                        borderRadius: sizes.screenSize * 3,
                        backgroundColor: colors.spaceGreen,
                        // width: '100%',
                      }}>
                      <Image
                        source={icons.send}
                        style={{
                          resizeMode: 'contain',
                          width: sizes.getWidth(3.5),
                          // flex: 0.35,
                          height: sizes.getHeight(3.5),
                          tintColor: btnDisable
                            ? colors.gray
                            : colors.primary,
                          tintColor: colors.primary,
                          transform: [{rotate: '-40deg'}],
                        }}
                      />
                    </Button>
                  </Block>
                </Block>
                {/* --------------------------------------------- */}
              </Block>
              {/* ============ ACTION BUTTON ENDED ============ */}
            </Block>
            {/* ======================================================================== */}

            {/* =================Toast=================== */}
            <Toast
              ref={messageRef}
              style={{
                backgroundColor: colors.primary,
                width: sizes.getWidth(100),
                borderRadius: 2,
              }}
              positionValue={sizes.getDimensions.height}
              fadeInDuration={200}
              fadeOutDuration={100}
            />
            {/* =================Toast=================== */}

            {isWaiting && (
              <Block
                center
                middle
                color={colors.primary}
                style={{
                  position: 'absolute',
                  right: sizes.getWidth(43.5),
                  // top: sizes.getHeight(10),
                  width: '10%',
                  height: '5%',
                  // borderWidth: 1,
                  borderRadius: sizes.getWidth(1),
                  borderColor: 'white',
                }}>
                {/* =================Toast=================== */}

                <ActivityIndicator size={'small'} color={colors.customRed} />
              </Block>
            )}
            {/* Waiting */}
          </Block>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  chatCon: {
    flex: 9,
    // flex: 0,
    // height: sizes.getHeight(ios ? (iphone6s ? 69 : 68) : 73.5),
    // backgroundColor:'red'
  },
  chats: {
    // borderWidth: 10,
    // height: sizes.getHeight(70),
  },
  actionBtnCon: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 0.5,
    // borderWidth: 1,
    flex: 0,
    height: sizes.getHeight(iphone6s ? 8 : 7),
    borderColor: colors.gray2,
    // flexDirection: 'row',
    // justifyContent: 'center',
    // alignItems: 'center',
    // borderTopWidth: 0.5,
    // borderWidth: 0,
    // height: sizes.getHeight(iphone6s ? 11 : 6),
    // borderColor: colors.gray2,
  },
  bcLogo: {
    position: 'absolute',
    left: sizes.withWidth(1) / 4,
  },
  bcImg: {
    opacity: 1,
    tintColor: 'gray',
    resizeMode: 'contain',
    height: sizes.getHeight(15),
    width: sizes.getWidth(50),
  },
  preview: {
    position: 'absolute',
    width: '100%',
    // top:sizes.getHeight(10),
    // height: '100%',
    // elavate:10,
    zIndex: 10,
    // borderWidth: 10,
    flex: 1,
  },
});

export default Chat;
