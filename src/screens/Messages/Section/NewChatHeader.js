import React from 'react'
import { StyleSheet, Platform, Dimensions } from 'react-native'
import { CustomHeader,Block,Text,Button } from 'components'
import { colors,sizes } from 'styles/theme'
import { NAV } from 'utils'

const NewChatHeader = props => {
    const {navigation,name} = props
      const ios = Platform.OS === 'ios';
      const iphone6s = Dimensions.get('screen').width <= 375;
    return (
      <>
        <CustomHeader
          navigation={navigation}
          tint={colors.primary}
          backBtn
          bg={colors.customRed}
        />
        <Block
          color={colors.customRed}
          middle
          center
          flex={false}
          height={sizes.getHeight(7)}>
          <Text
            style={{
              marginBottom: sizes.getHeight(1),

              fontSize: sizes.customFont(iphone6s ? 14 : 18),
            }}
            h1
            color={colors.primary}>
            {name}
          </Text>
        </Block>
      </>
    );
}

export  {NewChatHeader}

const styles = StyleSheet.create({})
