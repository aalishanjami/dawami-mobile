import React, {useRef} from 'react';
import {
  StyleSheet,
  Image,
  PermissionsAndroid,
  Platform,
  Alert,
  Dimensions,
} from 'react-native';
import {Block, Text, Button, CustomAlert} from 'components';
import {colors, sizes} from 'styles/theme';
import {ImageCon} from './chatHeader';
import * as icons from 'assets/icons';
import RNFetchBlob from 'rn-fetch-blob';
import Toast from 'react-native-easy-toast';

const FileMe = props => {
  const {value} = props;

  // const downloadFile = (url, fileName) => {
  //   const {config, fs} = RNFetchBlob;

  //   const downloads = fs.dirs.DownloadDir;
  //   return config({
  //     fileCache: true,
  //     addAndroidDownloads: {
  //       title: 'download success',
  //       useDownloadManager: true,
  //       notification: true,
  //       path: downloads + '/' + fileName + '.pdf',
  //     },
  //   }).fetch('GET', url);
  // };

  // const onDownload = value => {
  //   const link = `https://dawami.wedigits.dev/messages/files/`;
  //   const fileName = value.fileName;
  //   // downloadFile(link, fileName);
  //   try {
  //     const granted = PermissionsAndroid.request(
  //       PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
  //     );
  //     if (granted === PermissionsAndroid.PERMISSIONS.GRANTED) {
  //       // messageRef.current?.show(<CustomAlert text="Great" />)
  //       // downloadFile(link,fileName)
  //       console.log('ACCESSED');
  //     } else if (granted === PermissionsAndroid.PERMISSIONS.DENIED) {
  //       // messageRef.current?.show(<CustomAlert text="Permission Denied" />)
  //       console.log('NOT ACCESSED');
  //     }
  //   } catch (e) {
  //     console.log('ERROR');
  //     console.log(e);
  //     alert('Permissions Required, But getting some error');
  //   }
  // };

  // const onDownload = async value => {
  //   // console.log(value);
  //   const link = `https://dawami.wedigits.dev/messages/files/`;
  //   const fileName = value.fileName;
  //   try{
  //     const granted = PermissionsAndroid.request(
  //       PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
  //     )
  //     if(granted === PermissionsAndroid.PERMISSIONS.GRANTED){
  //       // messageRef.current?.show(<CustomAlert text="Great" />)
  //       // downloadFile(link,fileName)
  //       console.log('ACCESSED')
  //     }else if (granted === PermissionsAndroid.PERMISSIONS.DENIED){
  //       // messageRef.current?.show(<CustomAlert text="Permission Denied" />)
  //       console.log('NOT ACCESSED')
  //     }
  //   }
  //   catch(e){
  //     console.log('ERROR')
  //     console.log(e)
  //     alert("Permissions Required, But getting some error")
  //   }
  // };

  const downloadFile = (url, fileName, realName) => {
    const fullPath = `${url}/${fileName}`;
    const isIOS = Platform.OS === 'ios';
    const {DocumentDir, DownloadDir} = RNFetchBlob.fs.dirs;
    const actualPath = Platform.select({
      ios: DocumentDir,
      android: DownloadDir,
    });
    const configOptions = Platform.select({
      ios: {
        fileCache: true,
        appendExt: 'pdf',
      },
      android: {
        fileCache: false,
        addAndroidDownloads: {
          useDownloadManager: true,
          title: realName,
          description: 'Downloading...',
          mime: 'application/pdf',
          mediaScannable: true,
          notification: true,
          path: `${actualPath}/${fileName}`,
        },
      },
    });
let dummyPath = 'https://dawami.wedigits.dev/resumes/drafts/Multicountries-Support.pdf1598305664.pdf'
https: if (isIOS) {
  // console.log("IOS DEVICE")
  RNFetchBlob.config(configOptions)
    // .fetch('GET', fullPath)
    .fetch('GET', dummyPath)
    .then(async res => {
      RNFetchBlob.ios.previewDocument(res.path());
      const options = {
        title: realName,
        urls: [`file://${fullPath}`],
      };
    })
    .catch(error => {
      console.log('error', error);
    });
  return;
}
    // else {
    console.log('android');
    RNFetchBlob.config(configOptions)
      .fetch('GET', fullPath)
      .progress((received, total) => {
        console.log('progress : ' + received + ' / ' + total);
      })
      .then(resp => {
        RNFetchBlob.android.actionViewIntent(resp.path());
        Alert.alert('Success');
      });
  };
  //   const downloads = fs.dirs.DownloadDir;
  //   return config({
  //     fileCache: true,
  //     addAndroidDownloads: {
  //       title:realName,
  //       useDownloadManager: true,
  //       notification: true,
  //       path: `${downloads}/${fileName}`,
  //     },
  //   }).fetch('GET', fullPath);
  // };

  const onDownload = async value => {
    console.log(value);
    const link = `https://dawami.wedigits.dev/messages/files`;
    const fileName = value.filename;
    const realName = value.realname;
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        );
        console.log(granted);
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          // messageRef.current?.show(<CustomAlert text="Great" />)
          downloadFile(link, fileName, realName);
        } else if (granted === PermissionsAndroid.RESULTS.DENIED) {
          // messageRef.current?.show(<CustomAlert text="Permission Denied" />)
        } else {
          alert('Error to get permission. Please contact administrator.');
        }
      } catch (e) {
        alert('Permissions Required, But getting some error');
      }
    } else {
      downloadFile(link, fileName, realName);
    }
  };

  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width <= 375;

  return (
    <Block flex={false}>
      <Button
        onPress={() => onDownload(value)}
        opacity={0.3}
        style={styles.downloadFileCon}>
        <Block>
          <Image source={icons.downloadSign} style={styles.downloadImg} />
        </Block>
        <Block center padding={[0, 0, 0, sizes.getWidth(4)]} flex={4}>
          <Text
            numberOfLines={1}
            ellipsizeMode="tail"
            style={{
              ...styles.downloadText,
              fontSize: sizes.customFont(iphone6s ? 13 : 15),
            }}>
            {value.realname}
          </Text>
        </Block>
      </Button>
    </Block>
  );
};

export {FileMe};

const styles = StyleSheet.create({
  downloadFileCon: {
    borderWidth: 0.5,
    borderStyle: 'dashed',
    borderColor: colors.customRed,
    borderRadius: sizes.getWidth(0.7),
    // backgroundColor: '#f1f0f099',
    // backgroundColor: colors.gray3,
    minWidth: sizes.getWidth(35),
    // paddingHorizontal: sizes.getWidth(2),
    flexDirection: 'row',
    alignItems: 'center',
  },
  downloadText: {
    textAlign: 'left',
    color: colors.black,
    textTransform: 'capitalize',
  },
  downloadImg: {
    resizeMode: 'contain',
    width: sizes.getWidth(10),
    tintColor: colors.customRed,
  },
});
