import React from 'react';
import {StyleSheet, Image, Dimensions, Platform} from 'react-native';
import {Block, Text, Button} from 'components';
import {sizes, colors} from 'styles/theme';
import {ImageCon} from './chatHeader';
import * as icons from 'assets/icons';
import {FileOther} from './FileOther';
import {Messages} from './MassageMe';
import {ShowDate} from './date';

const ChatOther = props => {
  const {value, index, details} = props;
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width <= 375;
  console.log('============== CHAT Other ==============');
  console.log(value);
  // if(value.message){
  return (
    ((value.message === null && value.filename !== null) ||
      (value.message !== null && value.filename === null)) && (
      <Block row style={styles.me}>
        <ImageCon value={details} />
        <Block flex={5} middle crossLeft padding={[sizes.getHeight(0)]}>
          <Block
            flex={false}
            middle
            style={{
              ...styles.chat,
              backgroundColor: value.filename ? 'transparent' : '#8b9b97',
              paddingLeft: value.filename
                ? sizes.getWidth(0)
                : sizes.getWidth(1),
              paddingVertical: value.filename ? 0 : sizes.getWidth(2),
              marginBottom: value.filename ? 0 : sizes.getWidth(1),
            }}>
            {/* <Block style={styles.indicator} /> */}
            {value.filename && <FileOther value={value} />}
            {value.message && (
              <Messages color={colors.primary} value={value} />
            )}
          </Block>
          <ShowDate left value={value} />
        </Block>
      </Block>
    )
  );
};

export {ChatOther};

const styles = StyleSheet.create({
  me: {
    marginVertical: sizes.getHeight(1),
    // borderWidth:1,
    justifyContent: 'center',
    // flex:0,
    // height: sizes.getHeight(9),
  },
  logo: {
    // borderWidth:1,
    // backgroundColor:'pink'
  },
  imgCon: {
    borderWidth: 0.4,
    width: sizes.screenSize * 0.056,
    height: sizes.screenSize * 0.056,
    borderRadius: sizes.screenSize * 100,
  },
  chat: {
    // borderWidth:1,
    backgroundColor: '#393939',
    // backgroundColor: colors.lightPink,
    justifyContent: 'flex-start',
    color: colors.customRed,
    paddingRight: sizes.getWidth(3),
    // paddingVertical: sizes.getWidth(1),
    // paddingLeft: sizes.getWidth(1),
    borderRadius: sizes.getWidth(1.3),
    maxWidth: sizes.getWidth(78),
    position: 'relative',
  },
  // indicator:{
  //   position:'absolute',
  //   backgroundColor:'red',

  //   height:sizes.getHeight(3),
  //   width:sizes.getWidth(10)
  // }
});
