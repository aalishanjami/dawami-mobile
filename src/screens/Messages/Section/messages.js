import React, {useEffect, useState} from 'react';
import {Text, Block, Button, CustomHeader} from 'components';
import * as icons from 'assets/icons';
import {Image, Platform} from 'react-native';
import {sizes, colors} from 'styles/theme';
import {useSelector, useDispatch} from 'react-redux';
import {getSpecificUserChat} from 'redux/actions';

import messaging from '@react-native-firebase/messaging';
import {companyLogo} from 'redux/apiConstants';
import {NAV} from 'utils';
import {LANG_AR, ALL_CHATS, UPDATE_CHAT} from 'redux/constants';
import {iphone6s} from 'styles/theme';

// SUB COMPONENT

const Messages = props => {
  const {item, navigation, route} = props;
  const {id} = useSelector(state => state.auth.userBasicProfile);
  const {currentLN} = useSelector(state => state?.userInfo);
  const AR = currentLN === LANG_AR;
  const all_chats = useSelector(state => state.getLists?.all_chats);
  const dispatch = useDispatch();

  // console.log(item)
  const startChat = async () => {
    // console.log(all_chats);
    // console.log(item);
    let updatedObj = {};
    let remainingChats = [];
    const me = id.toString();
    const other = item.pivot.sender_id;
    // setCommingMsg(null);
    all_chats.forEach(singleRecord => {
      if (singleRecord.id === item.id) {
        updatedObj = {
          ...singleRecord,
          unread_count: 0,
        };
        remainingChats = all_chats.filter(x => x.id !== item.id);
      }
    });
    
    let newChatList = [...remainingChats, updatedObj];
    props.msgList(newChatList);
    dispatch({
      type: ALL_CHATS,
      payload: newChatList,
    });
    navigation.navigate(NAV.CHAT, {
      me: me,
      other: other,
      details: item, //list of messages
    });
  };

  const [comingMsg, setCommingMsg] = useState(null);
  useEffect(() => {
    // console.log(route.params?.other);
    setCommingMsg(route.params?.other);
  }, [route.params?.other]);






  return (
    <Button
      onPress={startChat}
      center
      row
      style={{
        borderBottomWidth: 0.7,
        borderBottomColor: colors.gray,
        height: '100%',
      }}>
      <Block
        middle
        center
        flex={false}
        style={{borderWidth: 0, height: '100%', width: sizes.getWidth(20)}}>
        {item.company && item.company.logo ? (
          <Image
            // source={icons.qnbLogo}
            source={{uri: companyLogo + item?.company?.logo}}
            style={{
              resizeMode: 'contain',
              width: sizes.getWidth(15),
              height: sizes.getHeight(9),
              marginRight: sizes.getWidth(4),
            }}
          />
        ) : (
          <Image
            // source={icons.qnbLogo}
            source={icons.avatarDummy}
            style={{
              resizeMode: 'contain',
              width: sizes.getWidth(13),
              height: sizes.getHeight(10),
              marginRight: sizes.getWidth(4),
            }}
          />
        )}
      </Block>
      <Block
        flex={4}
        // style={{borderWidth: 1}}
      >
        <Text style={{textAlign: 'left', fontSize:sizes.customFont(iphone6s?9:18)}} color={colors.gray5}>
          {AR ? item.name_ar : item.name || "--"}
        </Text>
        {/* <Text style={{fontSize: sizes.customFont(9)}} color={colors.customRed}>
          {item.email}
        </Text> */}
      </Block>

      <Block middle crossRight style={{height: '100%'}}>
        {/* {item.id === comingMsg && ( */}
        {item?.unread_count !== 0 ? (
          <Block
            center
            middle
            flex={false}
            style={{
              borderWidth: 1,
              height: sizes.screenSize * 0.025,
              width: sizes.screenSize * 0.025,
              borderRadius: sizes.screenSize * 3,
              // backgroundColor: colors.darkBrown,
              backgroundColor: colors.customRed,
              // textAlign: 'center',
            }}>
            <Text
              style={{fontSize: sizes.customFont(10)}}
              color={colors.primary}>
              {/* {item.unreadMessages} */}
              {item?.unread_count}
              {/* New */}
            </Text>
          </Block>
        ) : (
          <Block
            center
            middle
            flex={false}
            style={{
              // borderWidth: 1,
              height: sizes.screenSize * 0.025,
              width: sizes.screenSize * 0.06,
              // borderRadius: sizes.screenSize * 3,
              // backgroundColor: colors.darkBrown,
              // backgroundColor: colors.customRed,
              // textAlign: 'center',
            }}>
            <Text style={{fontSize: sizes.customFont(iphone6s?8:10)}} color={colors.gray}>
              {/* {item.unreadMessages} */}
              {item?.last_message_time}
              {/* New */}
            </Text>
          </Block>
        )}

        {/* )} */}
      </Block>
    </Button>
  );
};

export default Messages;
