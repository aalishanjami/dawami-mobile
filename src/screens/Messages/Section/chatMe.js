import React from 'react';
import {StyleSheet, Image} from 'react-native';
import {Block, Text, Button} from 'components';
import {colors, sizes} from 'styles/theme';
import {ImageCon, ImageConForMe} from './chatHeader';
import * as icons from 'assets/icons';
import {FileMe} from './fileMe';
import {Messages} from './MassageMe';
import {ShowDate} from './date';
import {useSelector} from 'react-redux';

const ChatMe = props => {
  const {value, index, details} = props;
  const myDetails = useSelector(state => state.auth?.userBasicProfile);
  console.log('============== CHAT ME ==============');
  console.log(myDetails);
  return (
    ((value.message === null && value.filename !== null) ||
      (value.message !== null && value.filename === null)) && (
      <Block row style={styles.me}>
        <Block
          flex={5}
          middle
          crossRight
          padding={[sizes.getHeight(0), sizes.getWidth(3), 0, 0]}>
          <Block
            color={!value.filename && colors.primary}
            style={{
              ...styles.chat,
              borderWidth: value.message ? 0.5 : 0,
              borderColor: colors.gray,
              paddingVertical: value.filename ? 0 : sizes.getWidth(2),
              marginBottom: value.filename ? 0 : sizes.getWidth(1),
            }}>
            {value.filename && <FileMe value={value} />}
            {value.message && <Messages value={value} />}
          </Block>
          <ShowDate right value={value} />
        </Block>
        <ImageConForMe value={myDetails.avatar} />
      </Block>
    )
  );
};

export {ChatMe};

const styles = StyleSheet.create({
  me: {
    marginVertical: sizes.getHeight(1),
    // borderWidth:1,
    justifyContent: 'center',
    // flex:0,
    // height: sizes.getHeight(9),
  },
  logo: {
    // borderWidth:1,
    // backgroundColor:'pink'
  },
  imgCon: {
    borderWidth: 0.3,
    borderColor: colors.gray,
    width: sizes.screenSize * 0.056,
    height: sizes.screenSize * 0.056,
    borderRadius: sizes.screenSize * 100,
  },
  chat: {
    flex: 0,
    alignItems: 'center',
    justifyContent: 'flex-end',
    // borderWidth: 1,
    // borderColor:'red',
    color: colors.customRed,
    borderRadius: sizes.getWidth(2),
    color: colors.customRed,
    paddingRight: sizes.getWidth(2),
    paddingVertical: sizes.getWidth(1),
    paddingLeft: sizes.getWidth(1),
    borderRadius: sizes.getWidth(1.3),
    maxWidth: sizes.getWidth(78),
  },
  msg: {
    // backgroundColor:'red',
    alignSelf: 'flex-end',
    textAlign: 'left',
  },
});
