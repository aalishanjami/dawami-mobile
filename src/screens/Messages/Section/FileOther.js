import React from 'react';
import {StyleSheet, Image, PermissionsAndroid, Platform} from 'react-native';
import {Block, Text, Button} from 'components';
import {colors, sizes} from 'styles/theme';
import {ImageCon} from './chatHeader';
import * as icons from 'assets/icons';
import RNFetchBlob from 'rn-fetch-blob';

const FileOther = props => {
  const {value} = props;


   const downloadFile = (url, fileName, realName) => {
     const fullPath = `${url}/${fileName}`;
     const isIOS = Platform.OS === 'ios';
     const {DocumentDir, DownloadDir} = RNFetchBlob.fs.dirs;
     const actualPath = Platform.select({
       ios: DocumentDir,
       android: DownloadDir,
     });
     const configOptions = Platform.select({
       ios: {
         fileCache: true,
         appendExt: 'pdf',
       },
       android: {
         fileCache: false,
         addAndroidDownloads: {
           useDownloadManager: true,
           title: realName,
           description: 'Downloading...',
           mime: 'application/pdf',
           mediaScannable: true,
           notification: true,
           path: `${actualPath}/${fileName}`,
         },
       },
     });

     if (isIOS) {
       // console.log("IOS DEVICE")
       RNFetchBlob.config(configOptions)
         .fetch('GET', fullPath)
         .then(async res => {
           RNFetchBlob.ios.previewDocument(res.path());
           const options = {
             title: realName,
             urls: [`file://${fullPath}`],
           };
         })
         .catch(error => {
           console.log('error', error);
         });
       return;
     }
     // else {
     console.log('android');
     RNFetchBlob.config(configOptions)
       .fetch('GET', fullPath)
       .progress((received, total) => {
         console.log('progress : ' + received + ' / ' + total);
       })
       .then(resp => {
         RNFetchBlob.android.actionViewIntent(resp.path());
         Alert.alert('Success');
       });
   };

  // const downloadFile = (url, fileName) => {
  //   const {config, fs} = RNFetchBlob;

  //   const downloads = fs.dirs.DownloadDir;
  //   return config({
  //     fileCache: true,
  //     addAndroidDownloads: {
  //       title: 'download success',
  //       useDownloadManager: true,
  //       notification: true,
  //       path: downloads + '/' + fileName + '.pdf',
  //     },
  //   }).fetch('GET', url);
  // };

  // const onDownload = value => {
  //   console.log(value);
  //   const link = `https://dawami.wedigits.dev/messages/files/`;
  //   const fileName = value.fileName;
  //   downloadFile(link, fileName);
  // };

  const onDownload = async value => {
    // console.log(value);
    const link = `https://dawami.wedigits.dev/messages/files`;
    const fileName = value.fileName;
    const realName = value.realname;
    if(Platform.OS === "android"){
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      console.log(granted);
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // messageRef.current?.show(<CustomAlert text="Great" />)
        downloadFile(link, fileName, realName);
      } else if (granted === PermissionsAndroid.RESULTS.DENIED) {
        // messageRef.current?.show(<CustomAlert text="Permission Denied" />)
        console.log('NOT ');
      } else {
        alert('Error to get permission. Please contact administrator.');
      }
    } catch (e) {
      alert('Permissions Required, But getting some error');
    }
  }else{
    downloadFile(link, fileName, realName);
  }
  };

  return (
    <Block flex={false} width={sizes.getWidth(30)}>
      <Button
        onPress={() => onDownload(value)}
        opacity={0.3}
        style={styles.downloadFileCon}>
        <Block flex={3}>
          <Text
            numberOfLines={1}
            ellipsizeMode="tail"
            h3
            style={styles.downloadText}>
            {value.realname}
          </Text>
        </Block>
        <Block>
          <Image source={icons.downloadSign} style={styles.downloadImg} />
        </Block>
      </Button>
    </Block>
  );
};

export {FileOther};

const styles = StyleSheet.create({
  downloadFileCon: {
    borderWidth: 0.6,
    borderStyle: 'dashed',
    borderColor: colors.customRed,
    borderRadius: sizes.getWidth(0.7),
    // backgroundColor: '#393939',
    minWidth: sizes.getWidth(35),
    paddingHorizontal: sizes.getWidth(2),
    flexDirection: 'row',
    alignItems: 'center',
  },
  downloadText: {
    textAlign: 'left',
    color: colors.black,
    textTransform: 'capitalize',
  },
  downloadImg: {
    resizeMode: 'contain',
    width: sizes.getWidth(10),
    tintColor: colors.customRed,
  },
});
