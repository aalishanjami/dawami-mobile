import React from 'react';
import {StyleSheet} from 'react-native';
import {Block, Text} from 'components';
import {sizes, colors} from 'styles/theme';

const ShowDate = ({value, right, left}) => {
  let {created_at} = value;
  let time = created_at.split(' ')[1];
  // console.log(
  // {new Date().toLocaleTimeString([], {hour12: '2-digit', minute: '2-digit'})}
  // );
  return (
    <Text
      h4
      style={{
        ...styles.dateStyle,
        marginRight:
          value.filename && right ? sizes.getWidth(1) : sizes.getWidth(1),
        marginLeft:
          value.filename && left ? sizes.getWidth(1) : sizes.getWidth(1),
      }}>
      {time}
    </Text>
  );
};

export {ShowDate};

const styles = StyleSheet.create({
  dateStyle: {
    fontSize: sizes.customFont(9),
    color: colors.gray2,
    // borderWidth: 1,
  },
});
