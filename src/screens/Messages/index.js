import React, {useEffect, useRef, useState} from 'react';
import {
  Block,
  Text,
  Button,
  CustomInput,
  ActivitySign,
  CustomAlert,
  CustomHeader, TextField
} from 'components';
import {colors, sizes, iphone6s} from 'styles/theme';
import * as icons from 'assets/icons';
import {FlatList, Image, SafeAreaView, StyleSheet} from 'react-native';
import {inbox, WORDS, NAV} from 'utils';
import Messages from './Section/messages';
import {useSelector, useDispatch} from 'react-redux';
import {GetAllUserChat, fetchMessage} from 'redux/actions';
import Toast from 'react-native-easy-toast';
import messaging from '@react-native-firebase/messaging';
import {LANG_AR} from 'redux/constants';

const {getWidth,getHeight,screenSize,customFont} = sizes
const {spaceGreen,primary,customRed} = colors
const Message = ({navigation, route}) => {
  const dispatch = useDispatch();
  // const {id} = useSelector(state => state.auth.userBasicProfile);
  const id = useSelector(state => state.auth?.userBasicProfile?.id);
  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;
  const {status} = useSelector(state => state.auth);
  const all_chats = useSelector(state => state.getLists?.all_chats);
  const messageRef = useRef();
  const [isWaiting, setIsWaiting] = useState(false);
  
   const getMessages = () => {
     const action =  {dispatch, messageRef, id}
      fetchMessage(action,setIsWaiting)
        .then(data => {
          console.log(data)
          setMsgList(data);
          setIsWaiting(false);
        })
        .catch(e => {
          alert(e);
          setIsWaiting(false);
        });
   }
  
  useEffect(() => {
    getMessages()
    if(isWaiting){
      setTimeout(() => {
        setIsWaiting(false)
      }, 3000);
    }
    return () => null

  }, ['']);

  useEffect(() => {
    const unsub = messaging().onMessage(async remoteMessage => {
      getMessages()
    })
    return () => unsub()
  }, [''])




  const [msgList, setMsgList] = useState(all_chats);
  // const [msgList, setMsgList] = useState(all_chats);
  const searchHandler = word => {
    var filteredList = [];
    all_chats.forEach(element => {
      var lowerTitle = element.name.toLowerCase();
      var lowerWord = word.toLowerCase();
      if (lowerTitle.startsWith(lowerWord)) {
        filteredList.push(element);
      }
      // console.log(filteredList);
      setMsgList(filteredList);
    });
  };
  const inputHandler = ({name, text}) => {
    searchHandler(text);
  };

  const search = AR ? WORDS.SEARCH_AR : WORDS.SEARCH;
  const title = AR ? 'المحادثات' : 'Conversation';

  return (
    <>
      <SafeAreaView
        style={{
          flex: 0,
        }}
        style={{
          backgroundColor: colors.spaceGreen,
        }}
      />
      <Block>
        <CustomHeader navigation={navigation} />
        {/* header */}
        <Block
          flex={false}
          height={sizes.getHeight(15)}
          style={{
            borderWidth: 0,
          }}>
          <Block
            flex={false}
            center
            middle
            style={{
              backgroundColor: colors.spaceGreen,
              height: sizes.getHeight(iphone6s ? 10 : 9),
              marginBottom: sizes.getHeight(iphone6s ? 2 : 4),
            }}>
            <Text
              color={colors.primary}
              style={{fontSize: sizes.customFont(iphone6s ? 11 : 18)}}>
              {title}
            </Text>
          </Block>
          {/* SEARCH======================= */}
          {status && (
            // <Block
            //   margin={[sizes.getHeight(iphone6s ? 7 : 0), 0, 0, 0]}
            //   padding={[0, sizes.getWidth(5)]}
            //   center
            //   middle
            //   flex={2}>
            //   <Button
            //     middle
            //     style={{
            //       width: '100%',
            //       borderColor: colors.gray,
            //       borderWidth: 1,
            //       borderRadius: sizes.getWidth(5),
            //     }}>
            //     {/* <CustomInput
            //       onChangeText={e => inputHandler(e)}
            //       placeholder={search}
            //       source={icons.search}
            //       imgStyling={{
            //         tintColor: colors.spaceGreen,
            //         height: sizes.getHeight(5),
            //       }}
            //     /> */}
            //   </Button>
            // </Block>
            <Block style={styles.search}>
              <Image source={icons.search} style={styles.searchImage} />
              <TextField
                blurOnSubmit={true}
                returnKeyType={'done'}
                onChangeText={e => inputHandler(e)}
                placeholder={search}
                name={'location'}
                inputStyling={styles.inputStyle}
              />
            </Block>
          )}
          {/* SEARCH======================= */}
        </Block>
        {/* list */}
        <Block margin={[sizes.getHeight(5), 0, 0, 0]} flex={6}>
          {all_chats?.length ? (
            <FlatList
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
              keyExtractor={(item, index) => {
                return index.toString();
              }}
              data={msgList}
              renderItem={({item}) => {
                return (
                  <Block
                    middle
                    height={sizes.getHeight(12)}
                    margin={[sizes.getHeight(0.3), 0, 0, 0]}
                    padding={[0, sizes.getWidth(5)]}
                    style={{
                      borderWidth: 0,
                    }}>
                    <Messages
                      msgList={e => setMsgList(e)}
                      item={item}
                      navigation={navigation}
                      route={route}
                    />
                  </Block>
                );
              }}
            />
          ) : (
            <Block
              center
              middle
              style={{
                borderWidth: 0,
              }}>
              <Text color={colors.gray}>
                {status
                  ? !AR
                    ? `Sorry ! There is no message in your inbox`
                    : `آسف ! لا توجد رسالة في صندوق الوارد الخاص بك
`
                  : ` You Need To Login First`}
              </Text>
              {/* <Button
              activeOpacity={0.5}
              center
              middle
              style={{
                marginTop:sizes.getHeight(4),
                borderRadius:sizes.getWidth(0.7),
                backgroundColor: colors.brown,
                paddingHorizontal: sizes.getWidth(4),
              }}>
              <Text h4 color={colors.primary}>Start New Chat</Text>
            </Button> */}
            </Block>
          )}

          {isWaiting && <ActivitySign />}
          <Toast
            ref={messageRef}
            style={{
              backgroundColor: colors.customRed,
              width: sizes.getWidth(100),
              borderRadius: 2,
            }}
            positionValue={sizes.getDimensions.height}
            fadeInDuration={200}
            fadeOutDuration={100}
            opacity={1}
          />
        </Block>
      </Block>
    </>
  );
};

const styles = StyleSheet.create({
  search: {
    flexDirection: 'row',
    borderWidth: 0.6,
    borderColor: spaceGreen,
    flex: 0,
    height: screenSize * 0.035,
    marginVertical: getHeight(0.5),
    marginHorizontal: getWidth(5),
    alignItems: 'center',
    borderRadius: screenSize * 0.2,
    paddingHorizontal: getWidth(3),
  },
  searchImage: {
    resizeMode: 'contain',
    width: screenSize * 0.03,
    height: screenSize * 0.03,
    tintColor: spaceGreen,
  },
  inputStyle: {
    // borderWidth:1,
    width: '95%',
    paddingVertical: 0,
    paddingHorizontal: getWidth(1),
    height: '100%',
    color: spaceGreen,
    textAlign: 'left',
    fontSize: customFont(iphone6s ? 8 : 14),
  },
});

export default Message;
