import React, {useEffect, useState} from 'react';
import {Block, Text, CustomHeader, ActivitySign} from 'components';
import {sizes, colors} from 'styles/theme';
import {
  Image,
  ScrollView,
  Platform,
  SafeAreaView,
  Dimensions,
} from 'react-native';
import * as images from 'assets/images';
import * as icons from 'assets/icons';
import {useSelector} from 'react-redux';
import {AboutUs} from 'redux/actions';
import {LANG_AR} from 'redux/constants';

const About = ({navigation, route}) => {
  const ios = Platform.OS === 'ios';
  const iphone6s = Dimensions.get('screen').width <= 375;

  const {currentLN} = useSelector(state => state.userInfo);
  const AR = currentLN === LANG_AR;
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    fetchData();
  }, ['']);

  const fetchData = async () => {
    setIsLoading(true);
    const result = await AboutUs(err => {
      alert(err);
      setIsLoading(false);
    });
    result && setData(result);
    setIsLoading(false);
  };

  console.log(data);
  const backImg = 'https://dawami.wedigits.dev/images/homepage/background.png';

  return (
    <>
      <SafeAreaView
        style={{flex: 0}}
        style={{backgroundColor: colors.spaceGreen}}
      />
      <Block>
        <Block
          style={{
            flex: 0,
            position: 'absolute',
            // top: sizes.getHeight(Platform.OS === 'android' ? 4 : 0),
            width: '100%',
            left: 0,
            zIndex: 3,
          }}>
          <CustomHeader
            backBtn
            tint={colors.primary}
            bg={'transparent'}
            navigation={navigation}
          />
        </Block>
        <ScrollView style={{flex: 1}}>
          <Block
            flex={false}
            center
            middle
            style={{
              position: 'relative',
              width: '100%',
              // borderWidth:2,
              height: sizes.getHeight(33),
              top: -sizes.getHeight(2),
              overflow: 'hidden',
            }}>
            <Image
              source={{uri: backImg}}
              style={{
                resizeMode: 'cover',
                width: '100%',
                height: '100%',
              }}
            />
          </Block>

          <Block
            flex={false}
            center
            middle
            style={{
              backgroundColor: colors.spaceGreen,
              opacity: 0.7,
              position: 'absolute',
              width: '100%',
              height: sizes.getHeight(31),
              top: sizes.getHeight(0),
              overflow: 'hidden',
            }}
          />
          <Block
            padding={[sizes.getWidth(5)]}
            style={
              {
                // borderWidth:2,
                // marginTop: -sizes.getHeight(10),
              }
            }>
            {/* -------------------------------------------------------------- */}
            <Text
              h2
              bold
              style={{
                fontSize: sizes.customFont(iphone6s ? 10 : 18),
              }}>
              {/* Your Future Job Gateway */}
              {currentLN === LANG_AR
                ? data?.main.title_ar || 'Please Wait'
                : data?.main.title || 'Please Wait'}
            </Text>
            <Text h4 bold style={{lineHeight: 30}} color={colors.customRed}>
              {/* Over 152,152,254 great job offers you deserve! */}
              {AR ? 'على' : 'Over'} {data?.job_counter}
              {AR
                ? 'يقدم لك عمل عظيم تستحقه!'
                : `great job offers you deserve!`}
            </Text>
            <Text
              h3
              style={{
                fontSize: sizes.customFont(iphone6s ? 10 : 18),
                lineHeight: 22,
              }}>
              {/* There are many variations of passages of Lorem Ipsum available, but
            the majority have suffered alteration in some form, by injected
            humour, or randomised words which don't look even slightly
            believable. If you are going to use a passage of Lorem Ipsum, you
            need to be sure */}
              {currentLN === LANG_AR
                ? data?.main.description_ar || 'Record is Fetching For you'
                : data?.main.description || 'Record is Fetching For you'}
            </Text>

            <Block
              center
              row
              margin={[sizes.getHeight(2), 0, 0, 0]}
              // style={{borderWidth: 1}}
            >
              <Block
                middle
                center
                style={{
                  flex: 0,
                  // borderWidth: 1,
                  width: sizes.getWidth(10),
                  height: sizes.getHeight(6),
                  marginRight: sizes.getWidth(2),
                }}>
                <Image
                  source={icons.aboutOne}
                  style={{resizeMode: 'contain', width: sizes.getWidth(8)}}
                />
              </Block>
              <Block flex={false} width={sizes.getWidth(78)}>
                <Text
                  h4
                  bold
                  style={{
                    fontSize: sizes.customFont(iphone6s ? 10 : 18),
                    textAlign: 'left',
                  }}>
                  {/* Find Your Dream Job */}
                  {currentLN === LANG_AR
                    ? data?.bullets.title_ar1 || 'لا يوجد عنوان محدد'
                    : data?.bullets.title1 || 'No Given Title'}
                </Text>
                <Text
                  h4
                  style={{
                    textAlign: 'left',
                    fontSize: sizes.customFont(iphone6s ? 10 : 18),
                  }}>
                  {/* the majority have suffered alteration in some form, by injected
                humour, or randomised words which don't look even slightly
                believable */}
                  {currentLN === LANG_AR
                    ? data?.bullets.description_ar1 || 'لا يوجد عنوان محدد'
                    : data?.bullets.description1 || 'No Given Details'}
                </Text>
              </Block>
            </Block>

            {/* -------------------------------------------------------------- */}
            <Block
              center
              row
              margin={[sizes.getHeight(2), 0, 0, 0]}
              // style={{borderWidth: 1}}
            >
              <Block
                middle
                center
                style={{
                  flex: 0,
                  // borderWidth: 1,
                  width: sizes.getWidth(10),
                  height: sizes.getHeight(6),
                  marginRight: sizes.getWidth(2),
                }}>
                <Image
                  source={icons.aboutTwo}
                  style={{resizeMode: 'contain', width: sizes.getWidth(8)}}
                />
              </Block>
              <Block flex={false} width={sizes.getWidth(78)}>
                <Text
                  h4
                  bold
                  style={{
                    textAlign: 'left',

                    fontSize: sizes.customFont(iphone6s ? 10 : 18),
                  }}>
                  {/* Find Your Dream Job */}
                  {currentLN === LANG_AR
                    ? data?.bullets.title_ar2 || 'لا يوجد عنوان محدد'
                    : data?.bullets.title2 || 'No Given Title'}
                </Text>
                <Text
                  h4
                  style={{
                    textAlign: 'left',
                    fontSize: sizes.customFont(iphone6s ? 10 : 18),
                  }}>
                  {/* the majority have suffered alteration in some form, by injected
                humour, or randomised words which don't look even slightly
                believable */}
                  {currentLN === LANG_AR
                    ? data?.bullets.description_ar2 || 'لا يوجد عنوان محدد'
                    : data?.bullets.description2 || 'No Given Details'}
                </Text>
              </Block>
            </Block>
            {/* -------------------------------------------------------------- */}
            {/* -------------------------------------------------------------- */}
            <Block
              center
              row
              margin={[sizes.getHeight(2), 0, 0, 0]}
              // style={{borderWidth: 1}}
            >
              <Block
                middle
                center
                style={{
                  flex: 0,
                  // borderWidth: 1,
                  width: sizes.getWidth(10),
                  height: sizes.getHeight(6),
                  marginRight: sizes.getWidth(2),
                }}>
                <Image
                  source={icons.aboutThree}
                  style={{resizeMode: 'contain', width: sizes.getWidth(8)}}
                />
              </Block>
              <Block flex={false} width={sizes.getWidth(78)}>
                <Text
                  h4
                  bold
                  style={{
                    textAlign: 'left',
                    fontSize: sizes.customFont(iphone6s ? 10 : 18),
                  }}>
                  {/* Find Your Dream Job */}
                  {currentLN === LANG_AR
                    ? data?.bullets.title_ar3 || 'لا يوجد عنوان محدد'
                    : data?.bullets.title3 || 'No Given Title'}
                </Text>
                <Text
                  h4
                  style={{
                    textAlign: 'left',
                    fontSize: sizes.customFont(iphone6s ? 10 : 18),
                  }}>
                  {/* the majority have suffered alteration in some form, by injected
                humour, or randomised words which don't look even slightly
                believable */}
                  {currentLN === LANG_AR
                    ? data?.bullets.description_ar3 || 'لا يوجد عنوان محدد'
                    : data?.bullets.description3 || 'No Given Details'}
                </Text>
              </Block>
            </Block>

            {/* end */}
          </Block>
        </ScrollView>
        {isLoading && <ActivitySign />}
      </Block>
    </>
  );
};

export default About;
