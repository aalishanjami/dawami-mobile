import React, {useEffect} from 'react';
import Navigations from 'navigations';
import {Provider, useSelector} from 'react-redux';
import {store, persistedStore} from 'redux/storeConfig';
import {PersistGate} from 'redux-persist/integration/react';
import {enableScreens} from 'react-native-screens';
import {Block, Text} from 'components';
import messaging from '@react-native-firebase/messaging';
import {SafeAreaView, StatusBar, StyleSheet} from 'react-native';
import {colors, ios} from 'styles/theme';

export const App = () => {
  enableScreens();
  return (
    <Provider store={store}>
      <PersistGate persistor={persistedStore}>
        <Navigations />
      </PersistGate>
    </Provider>
  );
};

const styles = StyleSheet.create({
  topSafeArea: {
    flex: 0,
    backgroundColor: colors.customRed,
  },
});
